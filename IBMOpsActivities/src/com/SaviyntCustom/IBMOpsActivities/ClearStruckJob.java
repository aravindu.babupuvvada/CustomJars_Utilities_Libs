package com.SaviyntCustom.IBMOpsActivities;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;


public class ClearStruckJob {
    Logger logger = null;

    public ClearStruckJob() {
        String saviyntHome = System.getenv("SAVIYNT_HOME");
        //System.setProperty("LOG_PATH", saviyntHome);
        PropertyConfigurator.configure(saviyntHome + File.separator + "customJarsLog4j.properties");
        logger = Logger.getLogger("opsActivitiesLogger");
    }
    public String appendQuotes(String str)
    {
        StringBuilder sb = new StringBuilder();
            sb.append("'");
            sb.append(str);
            sb.append("'");
            return sb.toString();
    }
    public String[] removeSpecialChars(String triggerNames)
    {
        String[] dataArr = triggerNames.split(",");

        for (int k = 0; k < dataArr.length; k++) {
            String newtriggerName =  dataArr[k];
            dataArr = ArrayUtils.removeElement(dataArr, "*");
            dataArr = ArrayUtils.removeElement(dataArr, "%");
            dataArr = ArrayUtils.removeElement(dataArr, " ");
            dataArr = ArrayUtils.removeElement(dataArr, "");
        }
        return dataArr;
    }

    public void StruckJob(Connection con, Properties prop, String triggerNames) {
        logger.info("***** Inside ClearStruckJob->StruckJob *****");
        try {
            StringBuilder mtSTR = new StringBuilder();
            StringBuilder nonMTSTR = new StringBuilder();
            String[] dataArr = removeSpecialChars(triggerNames);
            for (int k = 0; k < dataArr.length; k++) {
                String triggerName   = dataArr[k];
                if (triggerName.startsWith("MT_"))
                    mtSTR.append(appendQuotes(dataArr[k])).append(",");
                if (!triggerName.startsWith("MT_"))
                    nonMTSTR.append(appendQuotes(dataArr[k])).append(",");
            }
            String mtResult = mtSTR.deleteCharAt(mtSTR.length() - 1).toString();
            String nonMTResult = nonMTSTR.deleteCharAt(nonMTSTR.length() - 1).toString();

            if (mtResult.contains("MT_")) {
                logger.info("#####################################################");
                logger.info("Clear Struck Job From 3 Tables - " + triggerNames);
                logger.info("#####################################################");
                //String jobList = appendQuotes(dataArr).toString();
                handleDeleteQry(prop, con, "STRUCK_FIRED_DELETE", mtResult);
                logger.info("STRUCK_FIRED_DELETE Query Executed - " + mtResult);
                handleQry(prop, con, "STRUCK_FIRED_SELECT", mtResult);
                logger.info("STRUCK_FIRED_SELECT Query Executed - " + mtResult);
                handleDeleteQry(prop, con, "STRUCK_SIMPLE_DELETE", mtResult);
                logger.info("STRUCK_SIMPLE_DELETE Query Executed - " + mtResult);
                handleQry(prop, con, "STRUCK_SIMPLE_SELECT", mtResult);
                logger.info("STRUCK_SIMPLE_SELECT Query Executed - " + mtResult);
                handleDeleteQry(prop, con, "STRUCK_QRTZT_DELETE", mtResult);
                logger.info("STRUCK_QRTZT_DELETE Query Executed - " + mtResult);
                handleQry(prop, con, "STRUCK_QRTZT_SELECT", mtResult);
                logger.info("STRUCK_QRTZT_SELECT Query Executed - " + mtResult);
            }

            if (!nonMTResult.contains("MT_"))
            {
                logger.info("#####################################################");
                logger.info("Clear Struck Job From STRUCK_FIRED Table - " + nonMTResult);
                logger.info("#####################################################");
                //String jobList = appendQuotes(dataArr).toString();
                handleDeleteQry(prop, con, "STRUCK_FIRED_DELETE", nonMTResult);
                logger.info("STRUCK_FIRED_DELETE Query Executed - " + nonMTResult);
                handleQry(prop, con, "STRUCK_FIRED_SELECT", nonMTResult);
                logger.info("STRUCK_FIRED_SELECT Query Executed - " + nonMTResult);
            }

        } catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
        finally {
        }
    }
    public void ResetStruckJobStatus(Connection con, Properties prop, String triggerNames) {
        logger.info("***** Inside ClearStruckJob->ResetStruckJobStatus *****");
        try {
            String[] dataArr = removeSpecialChars(triggerNames);
            StringBuilder str = new StringBuilder();
            for (int k = 0; k < dataArr.length; k++) {
                    String strVal= dataArr[k];
                str.append(appendQuotes(strVal)).append(",");
            }
            String result = str.deleteCharAt(str.length() - 1).toString();
                logger.info("#####################################################");
                logger.info("Reset Struck Job Status in qrtz_triggers Table");
                logger.info("#####################################################");
                //String jobList = appendQuotes(dataArr).toString();
                handleQry(prop, con, "RESET_STRUCK_JOB_STATUS", result.toString());
                logger.info("RESET_STRUCK_JOB_STATUS Query Executed - " + result.toString());
                handleDeleteQry(prop, con, "RESET_STRUCK_JOB_STATUS_UPDATE", result.toString());
                logger.info("RESET_STRUCK_JOB_STATUS_UPDATE Query Executed - " + result.toString());
                handleQry(prop, con, "RESET_STRUCK_JOB_STATUS_SELECT", result.toString());
                logger.info("RESET_STRUCK_JOB_STATUS_SELECT Query Executed - " + result.toString());

        } catch (Exception e) {
            logger.error("Exception - " + e.getMessage(), e);
        } finally {
        }
    }
    public Integer isStringInt(String s)
    {
        try
        {
            return Integer.parseInt(s);
        } catch (NumberFormatException ex)
        {
            throw ex;
        }
    }
    public void killLongRunningQueries(Connection con, Properties prop, String jobIds) {
        logger.info("***** Inside ClearStruckJob->killLongRunningQueries *****");
        try {

            String[] dataArr = removeSpecialChars(jobIds);

            for (int k = 0; k < dataArr.length; k++) {
                int jobId = isStringInt(dataArr[k]);
                if (jobId > 2) {
                    logger.info("#####################################################");
                    logger.info("Kill Long Running Queries");
                    logger.info("#####################################################");
                    //String jobList = appendQuotes(dataArr).toString();
                    handleQry(prop, con, "KILL_LONG_RUNNING_QUERIES", jobId);
                    logger.info("KILL_LONG_RUNNING_QUERIES Executed - " + jobId);

                } else {
                    logger.error("Invalid input : " + jobId + " , Please enter valid long running query id");
                }
            }
        } catch (Exception e) {
            logger.error("Exception - " + e.getMessage(), e);
        } finally {
        }
    }

    public void handleDeleteQry(Properties prop, Connection con, String qryName,String jobList) {
        logger.info("Inside ClearStruckJob-->handleDeleteQry");

        try {

            logger.info("Query  - " + qryName);
            String Qry = prop.getProperty(qryName)
                    .replace("${triggername}", jobList);
            PreparedStatement  pst = con.prepareStatement(Qry);
            logger.info("Executing statement: "+pst.toString());
            int result = pst.executeUpdate();
            logger.info("Query execution status - " + result);
            con.commit();
        } catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
        finally {
        }
    }

    public void handleQry(Properties prop, Connection con, String qryName, String jobList) {
        logger.info("Inside ClearStruckJob-->handleQry");

        try {
            int rowCount;
            rowCount=0;
            logger.info("Query  - " + qryName);
            String Qry = prop.getProperty(qryName)
                    .replace("${triggername}", jobList);
            PreparedStatement  pst = con.prepareStatement(Qry);
            logger.info("Executing statement: "+pst.toString());
            ResultSet result = pst.executeQuery();
            if(((ResultSet) result).last()){

                rowCount= result.getRow();
            }

            logger.info("Query execution status - " + rowCount);
            con.commit();
        } catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
        finally {
        }

    }


    public void handleQry(Properties prop, Connection con, String qryName, int jobID) {
        logger.info("Inside ClearStruckJob-->handleQry");

        try {
            int rowCount;
            rowCount=0;
            logger.info("Query  - " + qryName);
            String Qry = prop.getProperty(qryName)
                    .replace("${triggername}", String.format("%d",jobID));
            PreparedStatement  pst = con.prepareStatement(Qry);
            logger.info("Executing statement: "+pst.toString());
            int result = pst.executeUpdate();
            if (result==0)
                logger.info("Query Cleared - "+ jobID+ "and execution status - " + result);
            con.commit();
        } catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
        finally {
        }

    }
}
