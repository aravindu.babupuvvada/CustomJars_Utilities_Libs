package com.SaviyntCustom.IBMOpsActivities;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Arrays;
import java.util.Properties;
import java.util.stream.IntStream;

public class QueryHandlerApp {
	Logger logger = null;

	public QueryHandlerApp() {
		String saviyntHome = System.getenv("SAVIYNT_HOME");
		//System.setProperty("LOG_PATH", saviyntHome);
		PropertyConfigurator.configure(saviyntHome + File.separator + "customJarsLog4j.properties");
		logger = Logger.getLogger("opsActivitiesLogger");
	}

	public void handleInsertQry(Properties prop, Connection con,
								String qryName, String epKey, String secsysKey) {
		logger.info("Inside handleInsertQry");

		try {
			String desc = prop.getProperty(qryName + "_Description");

			String val;
			int batchSize = Integer.parseInt(prop.getProperty("CUSTOMQRY_BATCHSIZE"));
			int cnt = 0, cntQueries = 0, cntInserts = 0;

			logger.info("Batch Size - " + batchSize);
			logger.info("Query description - " + desc);
			String insertQry = prop.getProperty(qryName + "_INSERT")
					.replace("${endpointkey}", epKey)
					.replace("${secsyskey}", secsysKey);

			String selectQry = prop.getProperty(qryName + "_SELECT")
					.replace("${endpointkey}", epKey)
					.replace("${secsyskey}", secsysKey);

			PreparedStatement pst = con.prepareStatement(selectQry);
			logger.info("Executing select query - " + pst.toString());
			ResultSet rs = pst.executeQuery();

			PreparedStatement  insertStmt = con.prepareStatement(insertQry);
			ResultSetMetaData rsmd = rs.getMetaData();
			int column_count = rsmd.getColumnCount();
			logger.info("Number of columns to be fetched: " + column_count);

			while (rs.next()) {
				for (int i=1; i<=column_count; i++) {
					val = rs.getString(i);
					insertStmt.setString(i, val);
				}
				logger.info("Adding query to the batch. Qry - " + insertStmt.toString());
				insertStmt.addBatch();

				cnt++;
				cntQueries++;
				if( cnt == batchSize ) {
					logger.info("Executing the batch after batch size...");
					int[] insertCounts = insertStmt.executeBatch();
					con.commit();
					cntInserts += IntStream.of(insertCounts).sum();
					logger.info("Total inserts - " + Arrays.toString(insertCounts));

					cnt = 0;
				}
			}
			if( cnt != 0 ) {
				logger.info("Executing the batch at the end...");
				int[] insertCounts = insertStmt.executeBatch();
				cntInserts += IntStream.of(insertCounts).sum();
				logger.info("Total inserts - " + Arrays.toString(insertCounts));
			}
			con.commit();
			logger.info("Number of records fetched : " + cntQueries);
			logger.info("Number of records inserted : " + cntInserts);
		} catch(Exception e) {
			logger.error("Exception - " +  e.getMessage(), e);
		}
		finally {
		}
	}

	public void handleQry(Properties prop, Connection con,
						  String qryName, String epKey, String secsysKey) {
		logger.info("Inside handleQry");

		try {
			String desc = prop.getProperty(qryName + "_Description");

			logger.info("Query description - " + desc);
			String qry = prop.getProperty(qryName + "_QRY")
					.replace("${endpointkey}", epKey)
					.replace("${secsyskey}", secsysKey);

			PreparedStatement pst = con.prepareStatement(qry);
			logger.info("Executing statement: "+pst.toString());
			int result = pst.executeUpdate();
			logger.info("Query execution status - " + result);

			con.commit();
		} catch(Exception e) {
			logger.error("Exception - " +  e.getMessage(), e);
		}
		finally {
		}
	}

	public void handleUpdateQry(Properties prop, Connection con,
								String qryName, String epKey, String secsysKey) {
		logger.info("Inside handleUpdateQry");

		try {
			String desc = prop.getProperty(qryName + "_Description");

			String val;
			int batchSize = Integer.parseInt(prop.getProperty("CUSTOMQRY_BATCHSIZE"));
			int cnt = 0, cntQueries = 0, cntUpdates = 0;

			logger.info("Batch Size - " + batchSize);
			logger.info("Query description - " + desc);
			String updateQry = prop.getProperty(qryName + "_UPDATE")
					.replace("${endpointkey}", epKey)
					.replace("${secsyskey}", secsysKey);

			String selectQry = prop.getProperty(qryName + "_SELECT")
					.replace("${endpointkey}", epKey)
					.replace("${secsyskey}", secsysKey);

			PreparedStatement pst = con.prepareStatement(selectQry);
			logger.info("Executing select query - " + pst.toString());
			ResultSet rs = pst.executeQuery();

			PreparedStatement  updateStmt = con.prepareStatement(updateQry);
			ResultSetMetaData rsmd = rs.getMetaData();
			int column_count = rsmd.getColumnCount();
			logger.info("Number of columns to be fetched: " + column_count);

			while (rs.next()) {
				for (int i=1; i<=column_count; i++) {
					val = rs.getString(i);
					updateStmt.setString(i, val);
				}
				logger.info("Adding query to the batch. Qry - " + updateStmt.toString());
				updateStmt.addBatch();

				cnt++;
				cntQueries++;
				if( cnt == batchSize ) {
					logger.info("Executing the batch after batch size...");
					int[] updateCounts = updateStmt.executeBatch();
					con.commit();
					cntUpdates += IntStream.of(updateCounts).sum();
					logger.info("Total updates - " + Arrays.toString(updateCounts));

					cnt = 0;
				}
			}
			if( cnt != 0 ) {
				logger.info("Executing the batch at the end...");
				int[] updateCounts = updateStmt.executeBatch();
				cntUpdates += IntStream.of(updateCounts).sum();
				logger.info("Total updates - " + Arrays.toString(updateCounts));
			}
			con.commit();
			logger.info("Number of records fetched : " + cntQueries);
			logger.info("Number of records updated : " + cntUpdates);
		} catch(Exception e) {
			logger.error("Exception - " +  e.getMessage(), e);
		}
		finally {
		}
	}

	public void handleDeleteQry(Properties prop, Connection con,
								String qryName, String epKey, String secsysKey) {
		logger.info("Inside handleDeleteQry");

		try {
			String desc = prop.getProperty(qryName + "_Description");

			String val;
			int batchSize = Integer.parseInt(prop.getProperty("CUSTOMQRY_BATCHSIZE"));
			int cnt = 0, cntQueries = 0, cntDeletes = 0;

			logger.info("Batch Size - " + batchSize);
			logger.info("Query description - " + desc);
			String delQry = prop.getProperty(qryName + "_DELETE")
					.replace("${endpointkey}", epKey)
					.replace("${secsyskey}", secsysKey);

			String selectQry = prop.getProperty(qryName + "_SELECT")
					.replace("${endpointkey}", epKey)
					.replace("${secsyskey}", secsysKey);

			PreparedStatement pst = con.prepareStatement(selectQry);
			logger.info("Executing select query - " + pst.toString());
			ResultSet rs = pst.executeQuery();

			PreparedStatement  updateStmt = con.prepareStatement(delQry);
			ResultSetMetaData rsmd = rs.getMetaData();
			int column_count = rsmd.getColumnCount();
			logger.info("Number of columns to be fetched: " + column_count);

			while (rs.next()) {
				for (int i=1; i<=column_count; i++) {
					val = rs.getString(i);
					updateStmt.setString(i, val);
				}
				logger.info("Adding query to the batch. Qry - " + updateStmt.toString());
				updateStmt.addBatch();

				cnt++;
				cntQueries++;
				if( cnt == batchSize ) {
					logger.info("Executing the batch after batch size...");
					int[] deleteCounts = updateStmt.executeBatch();
					con.commit();
					cntDeletes += IntStream.of(deleteCounts).sum();
					logger.info("Total deletes - " + Arrays.toString(deleteCounts));

					cnt = 0;
				}
			}
			if( cnt != 0 ) {
				logger.info("Executing the batch at the end...");
				int[] deleteCounts = updateStmt.executeBatch();
				cntDeletes += IntStream.of(deleteCounts).sum();
				logger.info("Total deletes - " + Arrays.toString(deleteCounts));
			}
			con.commit();
			logger.info("Number of records fetched : " + cntQueries);
			logger.info("Number of records deleted : " + cntDeletes);
		} catch(Exception e) {
			logger.error("Exception - " +  e.getMessage(), e);
		}
		finally {
		}
	}
}
