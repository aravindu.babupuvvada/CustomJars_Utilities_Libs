package com.SaviyntCustom.IBMOpsActivities;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Execute {
	Logger logger = null;
	Properties prop = new Properties();
	public String outhToken;
	public static Connection con;

	public static void main(String[] args) {
		Map map=new HashMap();
		map.put("clearTriggers","%,*,MT_111111, ,TEST,*,MT_111113,MT_1111134,TEST23");
		map.put("resetJobStatus","*,MT_67890,%,*,%,,MT_67890");
		map.put("killQuery","12345566,12345567,*,%,, ,");
		//map.put("data","10.10.10.10_123456,11.11.11.11_123456");
		//map.put("data","9.209.229.168_123456");

		Execute act = new Execute();
		//act.serverDecommission(map);
		//act.applicationDecommission(null);
		act.ClearStruckJob(map);
	}
	public Execute() {
		String saviyntHome = System.getenv("SAVIYNT_HOME");
		//System.setProperty("LOG_PATH", saviyntHome);
		PropertyConfigurator.configure(saviyntHome + File.separator + "customJarsLog4j.properties");
		logger = Logger.getLogger("opsActivitiesLogger");
	}

	public void applicationDecommission(Map input) {
		logger.info("***** Application Decommission - Start *****");
		try {
			setup();
			ApplicationDecommission decom = new ApplicationDecommission();
			decom.decommission(con, prop);
		} catch(Exception e) {
			logger.error("Exception - " +  e.getMessage(), e);
		}
		finally {
			try {
				if(con!=null)
					con.close();
				logger.info("***** Application Decommission - End *****\n\n");
			} catch(Exception e) {
				logger.error("Exception - " +  e.getMessage(), e);

			}
		}
	}

	public void serverDecommission(Map input) {
		logger.info("***** Server Decommission - Start *****");
		String serverType = null, data = null;

		try {
			setup();
			if (input != null && (input.containsKey("servertype") && input.containsKey("data"))) {
				serverType = ((String) input.get("servertype"));
				data = ((String) input.get("data"));

				if( (serverType != null && (serverType.equalsIgnoreCase("GI_UNIX")
						|| serverType.equalsIgnoreCase("GI_WINDOWS")
						|| serverType.equalsIgnoreCase("NE")))
						&& (data != null && !data.isEmpty()) ) {
					logger.info("Server Type - " + serverType);
					logger.info("Data - " + data);

					String serverType1 = getCP22(prop, serverType);
					ServerDecommission decom = new ServerDecommission();
					decom.decommission(con, prop, serverType1, data);
				} else {
					logger.error("Invalid input. Please check and try again!");
				}
			}
		} catch(Exception e) {
			logger.error("Exception - " +  e.getMessage(), e);
		}
		finally {
			try {
				if(con!=null)
					con.close();
				logger.info("***** Server Decommission - End *****\n\n");
			} catch(Exception e) {
				logger.error("Exception - " +  e.getMessage(), e);

			}
		}
	}
	public void accountOwnershipChange(Map data) {
	}
	public void dndDelegate(Map data) {
	}
	public void ffidSetup(Map data) {
	}
	public void groupOwnershipChange(Map data) {
	}
	public void setupDelegate(Map data) {
	}

	public void ClearStruckJob(Map input) {
		logger.info("***** Clear Struck JOB/QUERY - Start *****");
		String clrTriggerName = "";
		try {
			setup();
			if (input != null && (input.containsKey("clearTriggers") )) {
				clrTriggerName = ((String) input.get("clearTriggers"));
				if( clrTriggerName != null ) {
					logger.info("Clear Trigger(s) - " + clrTriggerName);
					ClearStruckJob struck = new ClearStruckJob();
					struck.StruckJob(con, prop, clrTriggerName);
				} else {
					logger.error("Invalid input. Please check and try again!");
				}
			}
			String resetJobname = "";
			if (input != null && (input.containsKey("resetJobStatus") )) {
				resetJobname = ((String) input.get("resetJobStatus"));
				if (resetJobname != null) {
					logger.info("Reset Status JobName(s) - " + resetJobname);
					ClearStruckJob struck = new ClearStruckJob();
					struck.ResetStruckJobStatus(con, prop, resetJobname);
				} else {
					logger.error("Invalid input. Please check and try again!");
				}
			}
			String killqueries = "";
			if (input != null && (input.containsKey("killQuery"))) {
					killqueries = ((String) input.get("killQuery"));
					if (resetJobname != null) {
						ClearStruckJob struck = new ClearStruckJob();
						logger.info("Reset Status JobName(s) - " + killqueries );
						struck.killLongRunningQueries(con, prop, killqueries );
					} else {
						logger.error("Invalid input. Please check and try again!");
					}
				}
			}
		catch(Exception e) {
			logger.error("Exception - " +  e.getMessage(), e);
		}
		finally {
			try {
				if(con!=null)
					con.close();
				logger.info("***** Clear Struck JOB/QUERY - End *****\n\n");
			} catch(Exception e) {
				logger.error("Exception - " +  e.getMessage(), e);

			}
		}
	}
                                                                                                	public void setup() {
		try {
			InputStream inputStream = null;
			String saviyntHome = System.getenv("SAVIYNT_HOME");
			String propFileName = saviyntHome + File.separator + "SAMConfig.properties";
			inputStream = new FileInputStream(propFileName);
			prop.load(inputStream);

			AESCrypt aes = new AESCrypt();
			Class.forName("com.mysql.jdbc.Driver");
			String DB_URL = prop.getProperty("DBURL");
			String DB_USER = prop.getProperty("DBUSER");
			String DB_PASSWORD = aes.decryptIt(prop.getProperty("DBPASS"));
			logger.info("Connecting to DB - " + DB_URL);
			con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
			con.setAutoCommit(false);
		} catch(Exception e) {
			logger.error("Exception - " +  e.getMessage(), e);
		}
	}

	public String getCP22 (Properties prop, String serverType) {
		String serverTypeGIUnix = prop.getProperty("OPS_SERVERTYPEGIUNIX");
		String cp22GIUnix = prop.getProperty("OPS_CP22GIUNIX");
		String serverTypeGIWin = prop.getProperty("OPS_SERVERTYPEGIWIN");
		String cp22GIWin = prop.getProperty("OPS_CP22GIWIN");
		String serverTypeNE = prop.getProperty("OPS_SERVERTYPENE");
		String cp22NE = prop.getProperty("OPS_CP22NE");
		String serverTypeGB = prop.getProperty("OPS_SERVERTYPEGB");
		String cp22GB = prop.getProperty("OPS_CP22GB");

		if (serverType.equalsIgnoreCase(serverTypeGIUnix)) {
			return cp22GIUnix;
		} else if (serverType.equalsIgnoreCase(serverTypeGIWin)) {
			return cp22GIWin;
		} else if (serverType.equalsIgnoreCase(serverTypeNE)) {
			return cp22NE;
		} else if (serverType.equalsIgnoreCase(serverTypeGB)) {
			return cp22GB;
		}
		return "";
	}
}
