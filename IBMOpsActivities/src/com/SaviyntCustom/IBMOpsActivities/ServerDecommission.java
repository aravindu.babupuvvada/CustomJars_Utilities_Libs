package com.SaviyntCustom.IBMOpsActivities;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.File;
import java.io.FileFilter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Properties;

public class ServerDecommission {
    Logger logger = null;

    public ServerDecommission() {
        String saviyntHome = System.getenv("SAVIYNT_HOME");
        //System.setProperty("LOG_PATH", saviyntHome);
        PropertyConfigurator.configure(saviyntHome + File.separator + "customJarsLog4j.properties");
        logger = Logger.getLogger("opsActivitiesLogger");
    }

    public void removeAllKeyFiles (String path, String server) {
        logger.info("*******************************");
        logger.info("Inside removeAllKeyFiles");
        logger.info("Searching key files for server - " + server + " under location - " + path);
        File dir = new File(path);
        File[] allKeys =  dir.listFiles(new FileFilter() {
            public boolean accept(File pathname) {
                return pathname.getName().startsWith(server);
            }
        });

        for (int i = 0; i < allKeys.length; i++) {
            logger.info("Deleting the file - " + allKeys[i].getAbsolutePath());
            allKeys[i].delete();
        }
    }

    public void removeAllKeyFiles1 (String path, String server) {
        logger.info("*******************************");
        logger.info("Inside removeAllKeyFiles");
        logger.info("Searching key files for server - " + server + " under location - " + path);
        File dir = new File(path);
        File[] listOfFiles = dir.listFiles();
        ArrayList<File> allKeys = new ArrayList<>();

        if(listOfFiles != null) {
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile() && listOfFiles[i].getName().startsWith(server)) {
                    allKeys.add(listOfFiles[i]);
                }
            }

            for (int j = 0; j < allKeys.size(); j++) {
                logger.info("Deleting the file - " + allKeys.get(j).getAbsolutePath());
                allKeys.get(j).delete();
            }
        }
    }

    public boolean serverPresent(Connection con, Properties prop, String server, String serverType) {
        logger.info("Inside serverPresent");

        boolean status = false;
        try {
            String query1 = prop.getProperty("OPS_SERVERDECOMM_CHECKSERVER")
                    .replace("${servername}", server)
                    .replace("${servertype}", serverType);

            PreparedStatement pst1 = con.prepareStatement(query1);
            logger.info("Executing query - " + pst1.toString());
            ResultSet rs1 = pst1.executeQuery();
            if (rs1.next()) {
                status = true;
            }
        } catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        } finally {
            return status;
        }
    }

    public void decommission(Connection con, Properties prop, String serverType, String data) {
        logger.info("***** Inside ServerDecommission->decommission *****");
        String qryNameGIUnixConn = "OPS_SERVERDECOMM_GIUNIX";
        String qryNameGIWinConn = "OPS_SERVERDECOMM_GIWIN";
        String qryNameNEConn = "OPS_SERVERDECOMM_NE";
        String qryNameGBConn = "OPS_SERVERDECOMM_GB";
        String qryNameGen = "OPS_SERVERDECOMM_GENERIC";

        ArrayList<String> qryNameArr = new ArrayList<String>();
        
        if(serverType.equalsIgnoreCase(prop.getProperty("OPS_CP22GIUNIX"))) {
            qryNameArr.add(qryNameGIUnixConn);
            qryNameArr.add(qryNameGen);
        } else if (serverType.equalsIgnoreCase(prop.getProperty("OPS_CP22GIWIN"))) {
            qryNameArr.add(qryNameGIWinConn);
            qryNameArr.add(qryNameGen);
        } else if (serverType.equalsIgnoreCase(prop.getProperty("OPS_CP22NE"))) {
            qryNameArr.add(qryNameNEConn);
            qryNameArr.add(qryNameGen);
        } else {
            qryNameArr.add(qryNameGBConn);
            qryNameArr.add(qryNameGen);
        }
        QueryHandlerServer qryHandler = new QueryHandlerServer();

        try {
            String[] dataArr = data.split(",");
            for (int k = 0; k < dataArr.length; k++) {
                String server = dataArr[k].split("_")[0];
                String fd = dataArr[k].split("_")[1];

                if(server != null && fd != null) {
                    if(!serverPresent(con, prop, server, serverType)) {
                        logger.info("Server - " + server +
                                " is not present or already decommissioned. Please check!");
                    } else {
                        logger.info("#####################################################");
                        logger.info("Decommissioning server - " + server + ", FD - " + fd);
                        logger.info("#####################################################");

                        for (int j = 0; j < qryNameArr.size(); j++) {
                            String qryName = qryNameArr.get(j);
                            logger.info("-----------------------------------------------------");
                            if (qryName != null) {
                                String noQrsStr = qryName + "_No_Of_Queries";
                                int noQrs = Integer.parseInt(prop.getProperty(noQrsStr));
                                String execOrder = prop.getProperty(qryName + "_Execution_Order");
                                if (noQrs != 0 && (execOrder != null || !execOrder.isEmpty())) {
                                    logger.info("Number of queries in " + qryName + " - " + noQrs);
                                    String[] order = execOrder.split(",");

                                    if (noQrs == order.length) {
                                        for (int i = 0; i < order.length; i++) {
                                            logger.info("*******************************");
                                            logger.info("Executing query no - " + order[i]);
                                            String qryTypeStr = qryName + "_" + order[i] + "_Type";
                                            if (prop.getProperty(qryTypeStr).equalsIgnoreCase("INSERT")) {
                                                qryHandler.handleInsertQry(prop, con,
                                                        qryName + "_" + order[i], server, serverType, fd);
                                            } else if (prop.getProperty(qryTypeStr).equalsIgnoreCase("UPDATE")) {
                                                qryHandler.handleUpdateQry(prop, con,
                                                        qryName + "_" + order[i], server, serverType, fd);
                                            } else if (prop.getProperty(qryTypeStr).equalsIgnoreCase("DELETE")) {
                                                qryHandler.handleDeleteQry(prop, con,
                                                        qryName + "_" + order[i], server, serverType, fd);
                                            } else if (prop.getProperty(qryTypeStr).equalsIgnoreCase("AS-IS")) {
                                                qryHandler.handleQry(prop, con,
                                                        qryName + "_" + order[i], server, serverType, fd);
                                            }
                                        }
                                    } else {
                                        logger.error("Number of queries do not match with size of exec order.");
                                    }
                                } else {
                                    logger.error("Wrong configuration for " + qryName + ". Please check configurations!");
                                }
                            }
                        }

                        //Deleting the key files
                        if (serverType.equalsIgnoreCase(prop.getProperty("OPS_CP22GIUNIX"))) {
                            removeAllKeyFiles(prop.getProperty("OPS_SERVERDECOMM_GIUNIX_keylocation"), server);
                        } else if (serverType.equalsIgnoreCase(prop.getProperty("OPS_CP22GIWIN"))) {
                        } else if (serverType.equalsIgnoreCase(prop.getProperty("OPS_SERVERTYPENE"))) {
                            removeAllKeyFiles(prop.getProperty("OPS_SERVERDECOMM_NE_keylocation"), server);
                        } else {
                        }
                    }
                } else {
                    logger.error("Invalid entry for server - " + dataArr[k]);
                }
            }
        } catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
        finally {
        }
    }

}
