package com.SaviyntCustom.IBMOpsActivities;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.net.ssl.HttpsURLConnection;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class ApplicationDecommission {
    Logger logger = null;
    ArrayList<AppDetailsPojo> allApps = new ArrayList<AppDetailsPojo>();

    public ApplicationDecommission() {
        String saviyntHome = System.getenv("SAVIYNT_HOME");
        //System.setProperty("LOG_PATH", saviyntHome);
        PropertyConfigurator.configure(saviyntHome + File.separator + "customJarsLog4j.properties");
        logger = Logger.getLogger("opsActivitiesLogger");
    }

    public void getAllApps(Connection con, Properties prop) {
        logger.info("Inside getAllApps");

        try {
            String query1 = prop.getProperty("OPS_APPDECOMM_GETALLAPPS");

            PreparedStatement pst1 = con.prepareStatement(query1);
            logger.info("Executing query - " + pst1.toString());
            ResultSet rs1 = pst1.executeQuery();
            if (rs1.next()) {
                AppDetailsPojo t = new AppDetailsPojo();
                t.epName = rs1.getString(1);
                t.epKey = rs1.getString(2);
                t.secsysName = rs1.getString(3);
                t.secsysKey = rs1.getString(4);

                allApps.add(t);
            }
        } catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        } finally {
        }
    }

    public void updateTriggerChain (Connection con, Properties prop, String trigger) {
        HashMap<String, String> triggerData = new HashMap<>();

        try {
            ResultSet rs1=null;
            String triggerName;
            String cronExp;

            String selectSQL = prop.getProperty("OPS_APPDECOMM_GETALLTRIGGERCHAINS");
            selectSQL = selectSQL.replace("${jobname}",trigger);
            PreparedStatement pstmt = con.prepareStatement(selectSQL);

            logger.info("Executing statement: "+pstmt.toString());
            rs1=pstmt.executeQuery();
            logger.info("Statement executed successfully");

            while(rs1.next()) {
                triggerName = rs1.getString("trigger_name");
                java.sql.Blob myBlob = rs1.getBlob("job_data");
                cronExp = rs1.getString("CRON_EXPRESSION");
                InputStream input = myBlob.getBinaryStream();

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buf = new byte[1024];
                InputStream in = myBlob.getBinaryStream();
                int n = 0;
                while ((n=in.read(buf))>=0)
                {
                    baos.write(buf, 0, n);
                }
                in.close();

                byte[] bytes = baos.toByteArray();
                String strImportTriggerSet = new String(bytes, "UTF-8");
                String strImportOnFailure = new String(bytes, "UTF-8");
                strImportTriggerSet = strImportTriggerSet.replaceAll("\uFFFD", "\"").replaceAll("t\u25AF", "\"");
                strImportOnFailure = strImportTriggerSet.replaceAll("\uFFFD", "\"").replaceAll("t\u25AF", "\"");

                if(strImportTriggerSet.contains("jobnamelabelt")) {
                    strImportTriggerSet = strImportTriggerSet.substring(strImportTriggerSet.lastIndexOf("savtriggerorderformt"), strImportTriggerSet.indexOf("jobnamelabelt")).trim();
                }
                if(strImportTriggerSet.contains("cronexpressiont")) {
                    strImportTriggerSet = strImportTriggerSet.substring(strImportTriggerSet.lastIndexOf("savtriggerorderformt"), strImportTriggerSet.indexOf("cronexpressiont")).trim();
                }
                strImportTriggerSet=strImportTriggerSet.replace("savtriggerorderformt","");
                strImportTriggerSet=strImportTriggerSet.replace("%","");
                strImportTriggerSet=strImportTriggerSet.substring(0,strImportTriggerSet.length()-1).trim();
                //
                if(strImportOnFailure.contains("jobtriggernamesr")) {
                    strImportOnFailure = strImportOnFailure.substring(strImportOnFailure.lastIndexOf("onFailuret"), strImportOnFailure.indexOf("jobtriggernamesr")).trim();
                }
                strImportOnFailure=strImportOnFailure.replace("onFailuret","");
                strImportOnFailure=strImportOnFailure.replace("%","");
                strImportOnFailure=strImportOnFailure.substring(0,strImportOnFailure.length()-1).trim();

                logger.info("Trigger set before the change: " + strImportTriggerSet);
                strImportTriggerSet = strImportTriggerSet.replaceAll("," + trigger, "");
                strImportTriggerSet = strImportTriggerSet.replaceAll(trigger + ",", "");
                logger.info("Trigger set after the change: " + strImportTriggerSet);

                triggerData.put(triggerName, strImportTriggerSet);
                setTriggerChain(prop, triggerName, strImportTriggerSet, cronExp, strImportOnFailure);
            }
        } catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        } finally {
        }
    }

    private String callBearerToken(Properties prop) throws Exception{
        String token = "";
        try {
            AESCrypt aes=new AESCrypt();
            HttpUrlConn httpConn = new HttpUrlConn();
            String getOAuthUrl = prop.getProperty("URLPATH") + "api/login";
            JSONObject jsonRequest = new JSONObject();
            jsonRequest.put("username", prop.getProperty("APIUSER"));
            jsonRequest.put("password", aes.decryptIt(prop.getProperty("APIUSERPWD")));
            String urlParams = jsonRequest.toString();

            HttpURLConnection con1 = null;
            HttpURLConnection con2 = null;
            JSONObject getOAuthResponse = null;
            //logger.info(urlParams);

            //con2 = httpConn.sendPostHttps(getOAuthUrl, urlParams, null);
            con1 = httpConn.sendPostHttp(getOAuthUrl, urlParams, null);
            getOAuthResponse = httpConn.fetchJsonObject(con1);


            if (getOAuthResponse!=null) {
                token = getOAuthResponse.get("access_token").toString();
                //tokenObj.setOAuthToken(token);
                logger.info("Got token");

            } else {
                logger.error("Could not get the token :" + getOAuthResponse);
                throw new Exception("Could not get token");
            }
        } catch (Exception e) {
            logger.error("Exception: " + e.getMessage(), e);
            throw e;
        }
        return token;

    }
    
    private  Boolean setTriggerChain(Properties prop, String triggerName, String finalSet, String cronExp, String onFailure) {
        Boolean status = false;
        try {
            logger.info("Updating trigger chain : " + triggerName
                    + "\nFinal Sequence : " + finalSet
                    + "\nOn Failure : " + onFailure
                    + "\nCron Exp : " + cronExp);
            //String cronExp = prop.getProperty("GIADHOCTRIGGERCHAINCRON");
            String url = prop.getProperty("URLPATH") + "api/v5/createUpdateTrigger";
            HttpUrlConn httpConn = new HttpUrlConn();

            //String[] triggerSequenceList=triggerSequence.split(",");
            HttpsURLConnection con1 = null;
            JSONObject getConnectionResponse = null;
            JSONObject triggerchainjson = new JSONObject();
            JSONObject triggerchainjsonmain = new JSONObject();
            JSONArray triggerChainArr = new JSONArray();
            triggerchainjson.put("triggername", triggerName);
            triggerchainjson.put("jobname", "TriggerChainJob");
            triggerchainjson.put("jobgroup", "utility");
            triggerchainjson.put("cronexpression", cronExp);
            JSONObject triggerchainsubjson = new JSONObject();
            triggerchainsubjson.put("savtriggerorderform", finalSet);
            triggerchainsubjson.put("onFailureForm", onFailure);

            triggerchainjson.put("valueMap", triggerchainsubjson);

            triggerChainArr.add(0, triggerchainjson);

            triggerchainjsonmain.put("triggers", triggerChainArr);

            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            String token = callBearerToken(prop);
            httpConnectionHeaders.put("Authorization", "Bearer " + token);
            con1 = httpConn.sendPostHttps(url, triggerchainjsonmain.toString(), httpConnectionHeaders);

            getConnectionResponse = httpConn.fetchJsonObject(con1);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(getConnectionResponse.toString());

            String triggerchainstatus = (String) json.get("errorCode");
            String msg = (String) json.get("msg");
            logger.info("Trigger chain udpate status : " + triggerchainstatus);

            if (triggerchainstatus.equals("1")) {
                logger.error("Error while updating trigger chain - " + msg);
            } else {
                status = true;
            }
        } catch (Exception ex) {
            logger.error("Exception: " + ex.getMessage(), ex);
            status=false;
        } finally {
            return  status;
        }
    }
    public void triggerUpdate(Connection con, Properties prop, AppDetailsPojo app) {
        logger.info("Inside getAllApps");

        try {
            String query1 = prop.getProperty("OPS_APPDECOMM_GETALLTRIGGERS").replace("${secsyskey}", app.secsysKey);
            PreparedStatement pst1 = con.prepareStatement(query1);
            logger.info("Executing query - " + pst1.toString());
            ResultSet rs1 = pst1.executeQuery();
            while (rs1.next()) {
                updateTriggerChain(con, prop, rs1.getString(1));
                String delQry = prop.getProperty("OPS_APPDECOMM_DELETETRIGGER")
                        .replace("${triggername}", rs1.getString(1));
                handleQry(prop, con, delQry);
            }
        } catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        } finally {
        }
    }

    public void handleQry(Properties prop, Connection con, String qry) {
        logger.info("Inside handleQry");

        try {
            PreparedStatement pst = con.prepareStatement(qry);
            logger.info("Executing statement: "+pst.toString());
            int result = pst.executeUpdate();
            logger.info("Query execution status - " + result);

            con.commit();
        } catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
        finally {
        }
    }

    public void decommission(Connection con, Properties prop) {
        logger.info("***** Inside ApplicationDecommission->decommission *****");
        QueryHandlerApp qryHandler = new QueryHandlerApp();

        allApps.clear();
        getAllApps(con, prop);

        try {
           for (int k = 0; k < allApps.size(); k++) {
                AppDetailsPojo t = allApps.get(k);

                if(t != null) {
                    logger.info("#####################################################");
                    logger.info("Decommissioning Application - " + t.epName);
                    logger.info("#####################################################");

                    //Delete trigger and update trigger chain
                    triggerUpdate(con, prop, t);

                    //Run all changes to be done through queries
                    String qryName = "OPS_APPDECOMM";
                    logger.info("-----------------------------------------------------");
                    String noQrsStr = qryName + "_No_Of_Queries";
                    int noQrs = Integer.parseInt(prop.getProperty(noQrsStr));
                    String execOrder = prop.getProperty(qryName + "_Execution_Order");
                    if (noQrs != 0 && (execOrder != null || !execOrder.isEmpty())) {
                        logger.info("Number of queries in " + qryName + " - " + noQrs);
                        String[] order = execOrder.split(",");

                        if (noQrs == order.length) {
                            for (int i = 0; i < order.length; i++) {
                                logger.info("*******************************");
                                logger.info("Executing query no - " + order[i]);
                                String qryTypeStr = qryName + "_" + order[i] + "_Type";
                                if (prop.getProperty(qryTypeStr).equalsIgnoreCase("INSERT")) {
                                    qryHandler.handleInsertQry(prop, con,
                                            qryName + "_" + order[i], t.epKey, t.secsysKey);
                                } else if (prop.getProperty(qryTypeStr).equalsIgnoreCase("UPDATE")) {
                                    qryHandler.handleUpdateQry(prop, con,
                                            qryName + "_" + order[i], t.epKey, t.secsysKey);
                                } else if (prop.getProperty(qryTypeStr).equalsIgnoreCase("DELETE")) {
                                    qryHandler.handleDeleteQry(prop, con,
                                            qryName + "_" + order[i], t.epKey, t.secsysKey);
                                } else if (prop.getProperty(qryTypeStr).equalsIgnoreCase("AS-IS")) {
                                    qryHandler.handleQry(prop, con,
                                            qryName + "_" + order[i], t.epKey, t.secsysKey);
                                }
                            }
                        } else {
                            logger.error("Number of queries do not match with size of exec order.");
                        }
                    } else {
                        logger.error("Wrong configuration for " + qryName + ". Please check configurations!");
                    }
                } else {
                    logger.error("Invalid entry for app.");
                }
            }
        } catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
        finally {
        }
    }

}
