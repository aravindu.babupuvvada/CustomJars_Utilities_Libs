package com.SaviyntCustom.NEUnixRunRecons;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

/**
 * Created by Saviynt on 3/2/2017.
 */
public class HttpUrlConn {
    final String USER_AGENT = "Mozilla/5.0";
    private Logger logger = null;

    public HttpUrlConn() {
        String saviyntHome = System.getenv("SAVIYNT_HOME");
        //System.setProperty("LOG_PATH", saviyntHome);
        PropertyConfigurator.configure(saviyntHome + File.separator + "customJarsLog4j.properties");
        logger = Logger.getLogger("unixNEReconsLogger");
    }

    // HTTP GET request
    public void sendGet() throws Exception {
        try {
            String url = "";

            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            con.setRequestProperty("User-Agent", USER_AGENT);

            int responseCode = con.getResponseCode();
            logger.info("\nSending 'GET' request to URL : " + url);
            logger.info("Response Code : " + responseCode);

            if (responseCode == 200) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                logger.info(response.toString());
            } else {
                logger.info("Error: Response code obtained is " + responseCode);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public HttpsURLConnection sendGetHttps(String url,Map<String,String> headers) throws Exception {
        HttpsURLConnection con = null;

        try {
            URL obj = new URL(url);
            con = (HttpsURLConnection) obj.openConnection();
            //add request header
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            con.setRequestProperty("Accept", "application/json");

            if(headers!=null && headers.size()>0) {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    con.setRequestProperty(entry.getKey(), entry.getValue());
                }
            }

            con.setDoOutput(true);

        } catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        } finally {
            return con;
        }
    }

    public HttpURLConnection sendPostHttp(String url, String urlParameters,
                                          Map<String,String> headers) throws Exception {
        DataOutputStream wr = null;
        HttpURLConnection con = null;

        try {
            URL obj = new URL(url);

            con = (HttpURLConnection) obj.openConnection();

            //add request header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            con.setRequestProperty("Accept", "application/json");

            if(headers!=null && headers.size()>0) {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    con.setRequestProperty(entry.getKey(), entry.getValue());
                }
            }

            //String urlParameters = "ccn=&locale=&caller=&num=12345";

            // Send post request
            con.setDoOutput(true);
            //logger.info("HttpUrlConn: Calling post for the url.");
            wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);

            int responseCode = con.getResponseCode();
            //logger.info("Response Code : " + responseCode);
            //logger.info("HttpUrlConn: Response code =" + responseCode);

        } catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        } finally {
            try{
                if (wr!=null) {
                    wr.flush();
                    wr.close();
                }
            } catch (Exception e) {
                logger.error("Exception - " +  e.getMessage(), e);
            }
            return con;
        }
    }

    public HttpsURLConnection sendPutHttps(String url, String urlParameters,
                                           Map<String,String> headers) throws Exception {
        DataOutputStream wr = null;
        HttpsURLConnection con = null;
        try {
            URL obj = new URL(url);
            con = (HttpsURLConnection) obj.openConnection();
            con.setRequestMethod("PUT");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            con.setRequestProperty("Accept", "application/json");

            if(headers!=null && headers.size()>0) {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    con.setRequestProperty(entry.getKey(), entry.getValue());
                }
            }
            // Send post request
            con.setDoOutput(true);
            //logger.info("HttpUrlConn: Calling post for the url.");
            wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);

        } catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        } finally {
            try{
                if (wr!=null) {
                    wr.flush();
                    wr.close();
                }
            } catch (Exception e) {
                logger.error("Exception - " +  e.getMessage(), e);
            }
            return con;
        }
    }

    public HttpsURLConnection sendPostHttps(String url, String urlParameters,
                                            Map<String,String> headers) throws Exception {
        DataOutputStream wr = null;
        HttpsURLConnection con = null;

        try {
            URL obj = new URL(url);

            con = (HttpsURLConnection) obj.openConnection();

            //add request header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            con.setRequestProperty("Accept", "application/json");

            if(headers!=null && headers.size()>0) {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    con.setRequestProperty(entry.getKey(), entry.getValue());
                }
            }

            //String urlParameters = "ccn=&locale=&caller=&num=12345";

            // Send post request
            con.setDoOutput(true);
            //logger.info("HttpUrlConn: Calling post for the url.");
            wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);

            int responseCode = con.getResponseCode();
            //logger.info("Response Code : " + responseCode);
            //logger.info("HttpUrlConn: Response code =" + responseCode);

        } catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        } finally {
            try{
                if (wr!=null) {
                    wr.flush();
                    wr.close();
                }
            } catch (Exception e) {
                logger.error("Exception - " +  e.getMessage(), e);
            }
            return con;
        }
    }

    public int fetchResponseCode (HttpURLConnection con) {
        int responseCode = 0;
        try {
            responseCode = con.getResponseCode();
            logger.info("Response Code : " + responseCode);
        } catch (IOException e) {
            logger.error("Exception - " +  e.getMessage(), e);
        } finally {
            return responseCode;
        }
    }

    public StringBuffer fetchResponseString (HttpURLConnection con) {
        int responseCode = 0;
        BufferedReader in = null;
        StringBuffer response = null;
        try {
            responseCode = con.getResponseCode();
            if (responseCode == 200) {
                in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;

                response = new StringBuffer();

                logger.info("Response string is "+ response);
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                logger.info("Response string is "+ response);
                logger.info("HttpUrlConn: Got response");
            } else {
                logger.info("Cannot reach the url or bad arguments, response code is " + responseCode);
                logger.info("HttpUrlConn: Cannot reach the url or bad arguments, response code is " + responseCode);
            }

        } catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        } finally {
            try{
                if (in!=null) {
                    in.close();
                }
            } catch (Exception e) {
                logger.error("Exception - " +  e.getMessage(), e);
            }
            return response;
        }
    }


    public JSONArray fetchJsonArray(HttpURLConnection con) {
        int responseCode = 0;
        BufferedReader in = null;
        try {
            responseCode = con.getResponseCode();
            if (responseCode == 200) {
                in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                logger.info("response " + response);
                JSONArray jsonArray = parseJsonResponseArray(response.toString());
                logger.info("HttpUrlConn: Got Json response");
                return jsonArray;
            } else {
                logger.info("Cannot reach the url or bad arguments, response code is " + responseCode);
                logger.info("HttpUrlConn: Cannot reach the url or bad arguments, response code is " + responseCode);
                return null;
            }

        } catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);

            return null;
        } finally {
            try{
                if (in!=null) {
                    in.close();
                }
            } catch (Exception e) {
                logger.error("Exception - " +  e.getMessage(), e);
            }
        }
    }

    public JSONObject fetchJsonObject (HttpURLConnection con) {
        int responseCode = 0;
        BufferedReader in = null;
        try {
            responseCode = con.getResponseCode();
            if (responseCode == 200 || responseCode == 500) {
                in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                logger.info("response " + response);
                JSONObject jsonObject = parseJsonResponse(response.toString());
                logger.info("HttpUrlConn: Got Json response");
                return jsonObject;
            } else {
                logger.info("Cannot reach the url or bad arguments, response code is " + responseCode);
                logger.info("HttpUrlConn: Cannot reach the url or bad arguments, response code is " + responseCode);
                return null;
            }

        } catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);

            return null;
        } finally {
            try{
                if (in!=null) {
                    in.close();
                }
            } catch (Exception e) {
                logger.error("Exception - " +  e.getMessage(), e);
            }
        }
    }

    public int fetchResponseCode2 (HttpsURLConnection con) {
        int responseCode = 0;
        try {
            responseCode = con.getResponseCode();
            logger.info("Response Code : " + responseCode);
        } catch (IOException e) {
            logger.error("Exception - " +  e.getMessage(), e);
        } finally {
            return responseCode;
        }
    }

    public StringBuffer fetchResponseString2 (HttpsURLConnection con) {
        int responseCode = 0;
        BufferedReader in = null;
        StringBuffer response = null;
        try {
            responseCode = con.getResponseCode();
            if (responseCode == 200) {
                in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;

                response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                logger.info("HttpUrlConn: Got response " + response);
            } else {
                logger.info("Cannot reach the url or bad arguments, response code is " + responseCode);
                logger.info("HttpUrlConn: Cannot reach the url or bad arguments, response code is " + responseCode);
            }

        } catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        } finally {
            try{
                if (in!=null) {
                    in.close();
                }
            } catch (Exception e) {
                logger.error("Exception - " +  e.getMessage(), e);
            }
            return response;
        }
    }

    public JSONObject fetchJsonObject2 (HttpsURLConnection con) {
        int responseCode = 0;
        BufferedReader in = null;
        try {
            responseCode = con.getResponseCode();
            if (responseCode == 200) {
                in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                logger.info("Lam HttpUrlConn: Got Json response : " + response);
                JSONObject jsonObject = parseJsonResponse(response.toString());
                //logger.info("HttpUrlConn: Got Json response");
                return jsonObject;
            } else {
                logger.info("Cannot reach the url or bad arguments, response code is " + responseCode);
                logger.info("HttpUrlConn: Cannot reach the url or bad arguments, response code is " + responseCode);
                return null;
            }

        } catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);

            return  null;
        } finally {
            try{
                if (in!=null) {
                    in.close();
                }
            } catch (Exception e) {
                logger.error("Exception - " +  e.getMessage(), e);
            }
        }
    }

    public JSONArray parseJsonResponseArray(String JSONString) {
        JSONParser parser = new JSONParser();
        try {
            JSONArray jsonArray = null;
            if(JSONString!=null && JSONString.trim().length()>0) {
                jsonArray = (JSONArray) parser.parse(JSONString);
            } else {
                logger.info("HttpUrlConn: Json response is - " + JSONString);
                logger.info("HttpUrlConn: Json response is - "+JSONString);
            }
            return jsonArray;
        } catch (ParseException e) {
            logger.error("Exception - " +  e.getMessage(), e);

            return null;
        }
    }

    public JSONObject parseJsonResponse(String JSONString) {
        JSONParser parser = new JSONParser();
        try {
            JSONObject jsonObject = null;
            if(JSONString!=null && JSONString.trim().length()>0) {
                jsonObject = (JSONObject) parser.parse(JSONString);
            } else {
                logger.info("HttpUrlConn: Json response is - " + JSONString);
                logger.info("HttpUrlConn: Json response is - "+JSONString);
            }
            return jsonObject;
        } catch (ParseException e) {
            logger.error("Exception - " +  e.getMessage(), e);

            return null;
        }
    }
}
