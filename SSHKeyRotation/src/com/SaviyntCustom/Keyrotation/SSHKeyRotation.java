package com.SaviyntCustom.Keyrotation;

import com.sun.org.apache.xpath.internal.axes.FilterExprWalker;
import org.apache.commons.lang3.RandomStringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import com.jcraft.jsch.*;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class SSHKeyRotation {

    Logger logger = Logger.getLogger(SSHKeyRotation.class.getName());
    FileHandler fh;
    Connection dbConn = null;
    Statement dbStmt = null;
    Properties prop = new Properties();
    String jobstate;
    StringBuilder serversFailedList=new StringBuilder();
    StringBuilder serversSuccessList=new StringBuilder();
    HashMap<String,String> keylist = new HashMap<String,String>();
    public static Connection con;
    String comments;
    String finalPubKey;

    String presentPP;
    String presentKey;
    String presentUsername;
    String presentHost;
    Integer presentPort;
    Integer timesofrotation;

    Session session;
    private static org.apache.log4j.Logger LOGGER = null;

    public SSHKeyRotation() {
        String saviyntHome = System.getenv("SAVIYNT_HOME");
        //System.setProperty("LOG_PATH", saviyntHome);
        PropertyConfigurator.configure(saviyntHome + File.separator + "customJarsLog4j.properties");
        LOGGER = org.apache.log4j.Logger.getLogger("rotationLogger");
    }

    public static void main(String args []) {
        System.out.println("***** SSH Key Rotation start *****\n");

        SSHKeyRotation skr = new SSHKeyRotation();
        if (args.length == 1) {
            String[] arguments = args[0].split(",");
            ArrayList<String> servers = new ArrayList<>();
            LOGGER.info("Servers - " + args[0]);

            Map map=new HashMap();
            map.put("servers", args[0]);
            //map.put("reconday","5");

            SSHKeyRotation rot = new SSHKeyRotation();
            rot.rotateKey(map);
        }
        else
        {
            System.out.println("No argument supplied, rotating key for all servers");
            skr.beginRotation();
        }

    }

    public void rotateKey(Map servers)
    {
        LOGGER.info("***** SSH Key Rotation start *****\n");

        SSHKeyRotation skr = new SSHKeyRotation();
        if (servers != null & servers.containsKey("servers")) {
            String[] connections = ((String)servers.get("servers")).split(",");
            LOGGER.info("Servers - " + servers.get("servers"));

            if(connections != null && connections.length == 1 && connections[0].equalsIgnoreCase("ALLGI")) {
                LOGGER.info("Key rotation will be done for all GI servers");

                //skr.beginRotation();
                skr.specificServer(null, "ALLGI");

            } else if (connections != null && connections.length > 0){
                LOGGER.info("Key rotation will be done for "+ servers.get("servers"));

                ArrayList<String> connArray = new ArrayList<>();
                for (String a : connections) {
                    connArray.add(a);
                }
                skr.specificServer(connArray, null);
            }
        }
        else {
            LOGGER.info("SSHKeyRotation: No Argument set");

        }
    }

    public void specificServer(ArrayList<String> servers, String additional) {
        InputStream inputStream = null;
        AESCrypt aes = new AESCrypt();
        try {
            String saviyntHome = System.getenv("SAVIYNT_HOME");
            String propFileName = saviyntHome + File.separator + "keyRotationConfig.properties";
            inputStream = new FileInputStream(propFileName);
            prop.load(inputStream);
            populateTable(prop);

            ArrayList<String> serversL = null;
            if(additional != null && additional.equalsIgnoreCase("ALLGI")) {
                serversL = new ArrayList<String>();

                String giEndpointsSQL = prop.getProperty("ALLGIENDPOINTSQUERY");
                PreparedStatement pstmt = con.prepareStatement(giEndpointsSQL);
                LOGGER.info("Executing statement: " + pstmt.toString());
                ResultSet rs1 = pstmt.executeQuery();
                LOGGER.info("Statement executed successfully");

                while(rs1.next()) {
                    LOGGER.info("GI server ---name--- "+rs1.getString(1));
                    serversL.add(rs1.getString(1));
                }
            }
            if(serversL == null && servers != null) {
                serversL = servers;
            }

            for (String server : serversL) {
                LOGGER.info("Generating key for server " + server);
                genKey(prop, server, con, aes);
                LOGGER.info("Successfully Generated key for server " + server);
            }
            beginRotationperServer(serversL, con, prop, aes);
        }
        catch(Exception e) {
            LOGGER.error("Exception - " +  e.getMessage(), e);
        }

    }


    public void populateTable(Properties prop) throws SQLException
    {
        LOGGER.info("Start table updation for new endpoints, this will enable if endpoint was enabled and disable if the endpoint was disbaled");
        HashMap<String,String> newEndpointsMap = new HashMap<String, String>();
        String newEndpointsSQL = prop.getProperty("ENDPOINTQUERY");
        try {
            AESCrypt aes = new AESCrypt();
            Class.forName("com.mysql.jdbc.Driver");
            String DB_URL = prop.getProperty("dburl");
            String DB_USER = prop.getProperty("dbusername");
            String DB_PASSWORD = aes.decryptIt(prop.getProperty("dbpass"));
            con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            con.setAutoCommit(true);

            PreparedStatement pstmt = con.prepareStatement(newEndpointsSQL);
            LOGGER.info("Executing statement: " + pstmt.toString());
            ResultSet rs1 = pstmt.executeQuery();
            LOGGER.info("Statement executed successfully");

            while(rs1.next()) {
                LOGGER.info("Found new endpoint ---name--- "+rs1.getString(1) + " ---with status--- "+rs1.getString(2));
                newEndpointsMap.put(rs1.getString(1),rs1.getString(2));
            }

            String endpointInserts = "INSERT INTO keyrotationdetails(Endpointname,Status,oldpassphrase,newpassphrase,lastsuccessfullattempt,lastattempt,rotationtimes,comments)"+
                    "VALUES (?,?,?,?,?,?,0,?);";

            Calendar calendar = Calendar.getInstance();
            java.sql.Date updatedate = new java.sql.Date(calendar.getTime().getTime());

            for (Map.Entry<String,String> server: newEndpointsMap.entrySet()) {
                PreparedStatement preparedStatement = con.prepareStatement(endpointInserts);
                preparedStatement.setString(1, server.getKey());
                preparedStatement.setString(2, server.getValue());
                preparedStatement.setString(3, prop.getProperty("boardingpassphrase")); //move out
                preparedStatement.setString(4, prop.getProperty("boardingpassphrase"));// move out
                preparedStatement.setDate(5, null);
                preparedStatement.setDate(6, updatedate);
                preparedStatement.setDate(7, null);
                LOGGER.info("Executing statement: " + preparedStatement.toString());
                preparedStatement.executeUpdate();
                LOGGER.info("Statement executed successfully");
            }

            String updateEndpoints = "update keyrotationdetails set status = ? where Endpointname = ?";
            String selectAllStatus = prop.getProperty("CHECKENDPOINTSTATUS");

            PreparedStatement pstmt1 = con.prepareStatement(selectAllStatus);
            LOGGER.info("Executing statement: " + pstmt1.toString());
            ResultSet rs2 = pstmt1.executeQuery();
            LOGGER.info("Statement executed successfully");

            while (rs2.next()) {
                PreparedStatement preparedStatement1 = con.prepareStatement(updateEndpoints);
                preparedStatement1.setString(1,rs2.getString(2));
                preparedStatement1.setString(2,rs2.getString(1));
                LOGGER.info("Executing statement: " + preparedStatement1.toString());
                preparedStatement1.executeUpdate();
                LOGGER.info("Statement executed successfully");
            }
            //con.close();
        }
        catch (Exception e) {
            LOGGER.error("Exception - " +  e.getMessage(), e);
        }
    }

    public String generateCommonLangPassword() {
        String upperCaseLetters = RandomStringUtils.random(5, 65, 90, true, true);
        String lowerCaseLetters = RandomStringUtils.random(5, 97, 122, true, true);
        String numbers = RandomStringUtils.randomNumeric(4);
        //String specialChar = RandomStringUtils.random(2, 33, 47, false, false);
        String totalChars = RandomStringUtils.randomAlphanumeric(2);
        /*String combinedChars = upperCaseLetters.concat(lowerCaseLetters)
                .concat(numbers)
                .concat(specialChar)
                .concat(totalChars);*/
        String combinedChars = upperCaseLetters.concat(lowerCaseLetters)
                .concat(numbers)
                .concat(totalChars);
        List<Character> pwdChars = combinedChars.chars()
                .mapToObj(c -> (char) c)
                .collect(Collectors.toList());
        Collections.shuffle(pwdChars);
        String password = pwdChars.stream()
                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
                .toString();
        return password;
    }


    public void genKey(Properties prop,String hostname,Connection con,AESCrypt aes)
    {
        JSch jsch = new JSch();
        String passphrase = generateCommonLangPassword();
        LOGGER.info("Passphrase generated: "+passphrase);
        KeyPair keyPair = null;
        String empty = "";
        String setpassphraseindb="update keyrotationdetails set newpassphrase = ?, lastattempt = now() where Endpointname = ?";
        try {
            String privatekeyfilepath = prop.getProperty("basedir") + hostname + "_new";
            String publickeyfilepath = prop.getProperty("basedir") + hostname + "_new.pub";
            File privatekeyFile = new File(privatekeyfilepath);
            File publickeyFile = new File(publickeyfilepath);
            keyPair = KeyPair.genKeyPair(jsch, KeyPair.RSA, Integer.parseInt(prop.getProperty("keysize")));
            keyPair.setPassphrase(passphrase);
            keyPair.writePrivateKey(privatekeyFile.getAbsolutePath());
            keyPair.writePublicKey(publickeyFile.getAbsolutePath(),prop.getProperty("keylabel") );
            keylist.put(hostname,publickeyfilepath);

            Calendar calendar = Calendar.getInstance();
            java.sql.Date updatedate = new java.sql.Date(calendar.getTime().getTime());

            PreparedStatement pstmt = con.prepareStatement(setpassphraseindb);
            pstmt = con.prepareStatement(setpassphraseindb);
            pstmt.setString(1,aes.encryptIt(passphrase).trim());
            //pstmt.setDate(2,updatedate);
            pstmt.setString(2,hostname);
            LOGGER.info("Executing statement: " + pstmt.toString());
            pstmt.executeUpdate();
            LOGGER.info("Statement executed successfully");

            /*PreparedStatement pstmt1 = con.prepareStatement(setnewkey);
            pstmt1 = con.prepareStatement(setnewkey);
            pstmt1.setString(1,aes.encryptIt(passphrase));
            pstmt1.setString(2,hostname);
            LOGGER.info("Executing statement: " + pstmt1.toString());
            pstmt1.executeUpdate();
            LOGGER.info("Statement executed successfully");*/

            keyPair.dispose();
        }
        catch (Exception e) {
            LOGGER.error("Exception - " +  e.getMessage(), e);
        }
    }
    public boolean revertToOldKey () {
        boolean status = false;
        try {
            List<String> commandList = new ArrayList<String>();
            commandList.add("cp -f /home/"+presentUsername+"/.ssh/authorized_keys_bkp /home/"+presentUsername+"/.ssh/authorized_keys");

            StringBuilder resp = new StringBuilder();
            StringBuilder errResp = new StringBuilder();
            runCommands(session, commandList, resp, errResp);

            if (resp.length() >= 0 && errResp.length() == 0) {
                LOGGER.info("SUCCESSFULLY executed commands on unix instance");
                status = true;
            } else {
                comments = comments + "Execution of commands FAILED on unix instance..." + " | ";
                LOGGER.info("Execution of commands FAILED on unix instance...");
            }
        } catch (Exception e) {
            LOGGER.error("Exception - " +  e.getMessage(), e);
            comments = comments + e.getMessage() + " | ";
        } finally {
            return  status;
        }
    }
    public void beginRotationperServer(ArrayList<String> servers,Connection con,Properties prop,AESCrypt aes)
    {
        String connectionDetails = "select attributevalue from externalconnattvalue extr join externalconnection ec on extr.connectiontype = ec.externalconnectionkey where ec.connectionname = ? and attributekey in ('HOST_NAME','PORT_NUMBER','USERNAME','PEM_KEY_FILE');";
        String getoldpassphrase = "select oldpassphrase,rotationtimes from keyrotationdetails where Endpointname = ?";
        String hostname = "";
        String port = "";
        String username = "";
        String passPhrase = "";
        String pemKeyFilePath = "";

        for(String server : servers) {
            comments = "";
            presentKey = null;
            presentPP = null;

            try {
                PreparedStatement pstmt = con.prepareStatement(connectionDetails);
                pstmt = con.prepareStatement(connectionDetails);
                pstmt.setString(1,server);
                LOGGER.info("Executing statement: " + pstmt.toString());
                ResultSet rs1 = pstmt.executeQuery();
                LOGGER.info("Statement executed successfully");

                if (rs1.next()) {
                    hostname = rs1.getString(1);
                    rs1.next();
                    port = rs1.getString(1);
                    rs1.next();
                    username = rs1.getString(1);
                    rs1.next();
                    pemKeyFilePath = rs1.getString(1);

                    PreparedStatement pstmt1 = con.prepareStatement(getoldpassphrase);
                    pstmt1 = con.prepareStatement(getoldpassphrase);
                    pstmt1.setString(1,server);
                    LOGGER.info("Executing statement: " + pstmt1.toString());
                    ResultSet rs2 = pstmt1.executeQuery();
                    LOGGER.info("Statement executed successfully");
                    rs2.next();
                    passPhrase = aes.decryptIt(rs2.getString(1));
                    timesofrotation = Integer.parseInt(rs2.getString(2));
                    finalPubKey = null;

                    JSch jsch = new JSch();
                    //LOGGER.info("pemKeyFilePath =" + pemKeyFilePath);
                    presentKey = pemKeyFilePath;
                    presentPP = passPhrase;
                    presentUsername = username;
                    presentHost = hostname;
                    presentPort = Integer.parseInt(port);

                    jsch.addIdentity(pemKeyFilePath, passPhrase);
                    session = jsch.getSession(username, hostname, Integer.parseInt(port));
                    LOGGER.info("Username - " + username + "\n"
                            + "PEM file - " + pemKeyFilePath + "\n"
                            + "Passphrase - " + passPhrase);

                    session.setConfig("StrictHostKeyChecking", "no");
                    if (runCommandsOnInstance(session,username,server,prop)) {
                        if(checkConnection(prop,server,con,aes)) {
                            timesofrotation++;
                            updateTable(server,timesofrotation,con);

                            Path pubKeyFilePath = Paths.get(prop.getProperty("basedir") + server + "_new.pub");
                            Path presentPubKeyFilePath = Paths.get(prop.getProperty("basedir") + server + ".pub");
                            if(Files.exists(presentPubKeyFilePath)) {
                                Files.delete(presentPubKeyFilePath);
                            }
                            Files.move(pubKeyFilePath,presentPubKeyFilePath);
                        } else {
                            Path pubKeyFilePath = Paths.get(prop.getProperty("basedir") + server + "_new.pub");
                            Files.delete(pubKeyFilePath);
                        }
                    }
                }
            } catch(Exception e) {
                comments = comments + e.getMessage() + " | ";
                LOGGER.error("Exception - " +  e.getMessage(), e);
            } finally {
                LOGGER.info("Disconnecting the session.");
                session.disconnect();
                LOGGER.info("Disconnected the session.");

                if(!comments.equals("")) {
                    updateTable(server,comments,con);
                }
            }
        }
    }


    public void updateTable(String server,Integer times,Connection con)
    {
        String qry = "update keyrotationdetails set oldpassphrase=newpassphrase,rotationtimes= ?," +
                "lastattempt=now(),lastsuccessfullattempt=now(),comments='Successfully rotated key',publickey=? " +
                "where Endpointname = ?";

        try {
            Calendar calendar = Calendar.getInstance();
            java.sql.Date updatedate = new java.sql.Date(calendar.getTime().getTime());
            PreparedStatement pstmt = con.prepareStatement(qry);
            pstmt = con.prepareStatement(qry);
            pstmt.setInt(1,times);
            //pstmt.setDate(2,updatedate);
            //pstmt.setDate(3,updatedate);
            pstmt.setString(2,finalPubKey);
            pstmt.setString(3,server);
            LOGGER.info("Executing statement: " + pstmt.toString());
            pstmt.executeUpdate();
            LOGGER.info("Statement executed successfully");

        }
        catch (Exception e) {
            LOGGER.error("Exception - " +  e.getMessage(), e);
        }

    }

    public void updateTable(String server,String comments,Connection con)
    {
        String qry = "update keyrotationdetails set comments = ? where Endpointname = ?";

        try {
            Calendar calendar = Calendar.getInstance();
            java.sql.Date updatedate = new java.sql.Date(calendar.getTime().getTime());
            PreparedStatement pstmt = con.prepareStatement(qry);
            pstmt = con.prepareStatement(qry);
            pstmt.setString(1,comments);
            pstmt.setString(2,server);
            LOGGER.info("Executing statement: " + pstmt.toString());
            pstmt.executeUpdate();
            LOGGER.info("Statement executed successfully");

        }
        catch (Exception e) {
            LOGGER.error("Exception - " +  e.getMessage(), e);
        }
    }

    public boolean checkConnection(Properties prop,String server,Connection con,AESCrypt aes)
    {
        boolean status = false;
        Path presentKeyFilePath = Paths.get(prop.getProperty("basedir") + server + "_" + timesofrotation);
        Path finalKeyFilePath = Paths.get(prop.getProperty("basedir") + server + "_" + (timesofrotation+1));
        Path newKeyFilePath = Paths.get(prop.getProperty("basedir") + server + "_new");
        HashMap<String, String> templateAttrs = new HashMap<>();
        HttpUrlConn httpConn = new HttpUrlConn();
        HttpsURLConnection con1 = null;
        JSONObject getConnectionResponse = null;
        JSONObject connectionjson = null;
        String url=prop.getProperty("URLPATH")+"api/v5/testConnection";
        Map<String, String> httpConnectionHeaders = new HashMap<String, String>();

        try {
            Files.move(newKeyFilePath, finalKeyFilePath, StandardCopyOption.REPLACE_EXISTING);

            templateAttrs.put("connectiontype","Unix");
            templateAttrs.put("saveconnection","Y");
            templateAttrs.put("systemname",server);
            templateAttrs.put("connectionName",server);
            templateAttrs.put("PEM_KEY_FILE",prop.getProperty("basedir") + server + "_" + (timesofrotation+1));
            String sqlselect = "Select newpassphrase from keyrotationdetails where Endpointname = ?";
            String passPhrase;

            PreparedStatement pstmt1 = con.prepareStatement(sqlselect);
            pstmt1 = con.prepareStatement(sqlselect);
            pstmt1.setString(1,server);
            LOGGER.info("Executing statement: " + pstmt1.toString());
            ResultSet rs2 = pstmt1.executeQuery();
            LOGGER.info("Statement executed successfully");
            rs2.next();
            passPhrase = aes.decryptIt(rs2.getString(1));
            templateAttrs.put("PASSPHRASE",passPhrase);

            Boolean retry = true;
            String connectionUpdateStatus = null;

            while (retry) {
                TokenPojo token = new TokenPojo();
                String outhToken = token.callBearerToken(prop);
                connectionjson = new JSONObject(templateAttrs);
                LOGGER.info("connectionjson : " + connectionjson.toString());
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                con1 = httpConn.sendPostHttps(url, connectionjson.toString(), httpConnectionHeaders);
                int code = con1.getResponseCode();
                if (code != 504) {
                    retry = false;
                } else {
                    LOGGER.info("Rotation failed with timeout error. Retrying...");
                }
                getConnectionResponse = httpConn.fetchJsonObject(con1);
                connectionUpdateStatus = null;
                if (getConnectionResponse != null) {
                    JSONParser parser1 = new JSONParser();
                    JSONObject json1 = (JSONObject) parser1.parse(getConnectionResponse.toString());
                    if (json1 != null) {
                        connectionUpdateStatus = (String) json1.get("msg");
                    }
                    LOGGER.info("Connection update status : " + connectionUpdateStatus);
                }
            }

            if(connectionUpdateStatus!=null && connectionUpdateStatus.equalsIgnoreCase("Connection Successful")) {
                status=true;
                if(Files.exists(presentKeyFilePath)) {
                    Files.delete(presentKeyFilePath);
                }
            } else {
                LOGGER.info("Updating connection failed for " + server + ".");
                comments = comments + "Updating connection failed. Status - " + connectionUpdateStatus + " | ";

                if(revertToOldKey()) {
                    if(Files.exists(finalKeyFilePath)) {
                        Files.delete(finalKeyFilePath);
                    }

                    templateAttrs.put("connectiontype", "Unix");
                    templateAttrs.put("saveconnection", "Y");
                    templateAttrs.put("systemname", server);
                    templateAttrs.put("connectionName", server);
                    templateAttrs.put("PEM_KEY_FILE", presentKey);
                    templateAttrs.put("PASSPHRASE", presentPP);

                    connectionjson = new JSONObject(templateAttrs);
                    LOGGER.info("connectionjson : " + connectionjson.toString());
                    con1 = httpConn.sendPostHttps(url, connectionjson.toString(), httpConnectionHeaders);
                    getConnectionResponse = httpConn.fetchJsonObject(con1);
                    JSONParser parser2 = new JSONParser();
                    JSONObject json2 = (JSONObject) parser2.parse(getConnectionResponse.toString());

                    String connectionRollbackStatus = (String) json2.get("msg");
                    LOGGER.info("Connection rollback status : " + connectionRollbackStatus);
                    if (connectionRollbackStatus != null && connectionRollbackStatus.equalsIgnoreCase("Connection Successful")) {
                    } else {
                        comments = comments + "Rolling back connection failed. Status - " + connectionRollbackStatus + " | ";
                    }
                } else {
                    comments = comments + "Rolling back key failed. Connection will be in failed state. | ";
                }
            }
        }
        catch (Exception e) {
            comments = comments + "Exception occured while updating connection. Ex - " + e.getMessage() + " | ";
            LOGGER.error("Exception - " +  e.getMessage(), e);

            if(revertToOldKey()) {
                if (Files.exists(finalKeyFilePath)) {
                    Files.delete(finalKeyFilePath);
                }

                templateAttrs.put("connectiontype", "Unix");
                templateAttrs.put("saveconnection", "Y");
                templateAttrs.put("systemname", server);
                templateAttrs.put("connectionName", server);
                templateAttrs.put("PEM_KEY_FILE", presentKey);
                templateAttrs.put("PASSPHRASE", presentPP);

                connectionjson = new JSONObject(templateAttrs);
                LOGGER.info("connectionjson : " + connectionjson.toString());
                con1 = httpConn.sendPostHttps(url, connectionjson.toString(), httpConnectionHeaders);
                getConnectionResponse = httpConn.fetchJsonObject(con1);
                JSONParser parser2 = new JSONParser();
                JSONObject json2 = (JSONObject) parser2.parse(getConnectionResponse.toString());

                String connectionRollbackStatus = (String) json2.get("msg");
                LOGGER.info("Connection rollback status(E) : " + connectionRollbackStatus);
                if (connectionRollbackStatus != null && connectionRollbackStatus.equalsIgnoreCase("Connection Successful")) {
                } else {
                    comments = comments + "Rolling back connection failed. Status - " + connectionRollbackStatus + " | ";
                }
            }
        }
        finally {
            return status;
        }
    }

    public List<String> getPushKeyCommands(String username,String server,Properties prop){
        List<String> commandList = new ArrayList<String>();
        //commandList.add("sudo echo '"+signedPubKey+"' > /etc/ssh/trusted-user-ca-keys.pem");
        //commandList.add("sudo sed -i '/TrustedUserCAKeys \\/etc\\/ssh\\/trusted-user-ca-keys.pem/d' /etc/ssh/sshd_config");
        //commandList.add("sudo echo -e '\nTrustedUserCAKeys /etc/ssh/trusted-user-ca-keys.pem' >> /etc/ssh/sshd_config");
        //commandList.add("sudo service ssh restart");
        //commandList.add("sudo service sshd restart");
        String key ="";
        String presentPubKey ="";
        String finalkey = "";
        try {

            String path = prop.getProperty("basedir") + server + "_new.pub";
            key = new String(Files.readAllBytes(Paths.get(path)));

            String presentPubKeyPath = prop.getProperty("basedir") + server + ".pub";
            File presentPubKeyFile = new File(presentPubKeyPath);
            if(presentPubKeyFile.exists()) {
                presentPubKey = new String(Files.readAllBytes(Paths.get(presentPubKeyPath)));
            } else {
                presentPubKey = prop.getProperty("boardingpubkey");
            }
            presentPubKey = presentPubKey.replace("/","\\/");
            presentPubKey = presentPubKey.replaceAll("\r", "").replaceAll("\n", "");

            finalkey = prop.getProperty("keyheader") + " " + key + " " + prop.getProperty("keyowner");
            finalkey = finalkey.replaceAll("\r", "").replaceAll("\n", "");
            finalPubKey = finalkey;
            //LOGGER.info("echo '"+finalkey+"' >> /home/"+username+"/.ssh/authorized_keys");
            //commandList.add("sed -i '/"+presentPubKey+"/d' /home/"+username+"/.ssh/authorized_keys");
            commandList.add("cp -f /home/"+username+"/.ssh/authorized_keys /home/"+username+"/.ssh/authorized_keys_bkp");
            commandList.add("sed '/"+presentPubKey+"/d' /home/"+username+"/.ssh/authorized_keys > /home/"+username+"/.ssh/temp");
            commandList.add("mv /home/"+username+"/.ssh/temp /home/"+username+"/.ssh/authorized_keys");
            commandList.add("echo '"+finalkey+"' >> /home/"+username+"/.ssh/authorized_keys");
            commandList.add("chmod 600 /home/"+username+"/.ssh/authorized_keys /home/"+username+"/.ssh/authorized_keys_bkp");

            LOGGER.info("Got push key Command list.");
        }
        catch (Exception e) {
            LOGGER.error("Exception - " +  e.getMessage(), e);
        }
        return commandList;
    }

    public boolean runCommandsOnInstance(Session session,String username,String server,Properties prop){
        boolean status = false;
        Channel channel = null;
        StringBuilder resp = new StringBuilder();
        StringBuilder errResp = new StringBuilder();
        List<String> commandList = new ArrayList<String>();
        try{
            //commandList = getPushKeyCommands(username,server,prop);
            LOGGER.info("Establishing Connection...");
            session.connect();
            LOGGER.info("Connection established to instances...");

            commandList = getPushKeyCommands(username,server,prop);
            if(!commandList.isEmpty()) {
                runCommands(session, commandList, resp, errResp);

                if (resp.length() >= 0 && errResp.length() == 0) {
                    LOGGER.info("SUCCESSFULLY executed commands on unix instance");
                    status = true;
                } else {
                    comments = comments + "Execution of commands FAILED on unix instance..." + " | ";
                    LOGGER.info("Execution of commands FAILED on unix instance...");
                }
            }

        } catch (Exception e){
            comments = comments + e.getMessage() + " | ";
            LOGGER.error("Exception - " +  e.getMessage(), e);
            status = false;
        } finally {
        }
        LOGGER.info("exit runCommandsOnInstance");
        return status;
    }


    public void runCommands(Session session, List<String> commandList, StringBuilder resp, StringBuilder errResp) {
        LOGGER.info("inside runCommands");
        String line = null;
        BufferedReader br = null;
        BufferedReader errBr = null;
        Channel channel = null;
        for (String command : commandList) {
            try {
                channel = session.openChannel("exec");
                LOGGER.info("Unix Provisioning Service command = " + command);
                //success flow
                InputStream commandOutput = channel.getInputStream();
                br = new BufferedReader(new InputStreamReader(commandOutput));
                //error flow
                ((ChannelExec) channel).setErrStream(System.err);
                InputStream errorStream = ((ChannelExec) channel).getErrStream();
                errBr = new BufferedReader(new InputStreamReader(errorStream));

                ((ChannelExec) channel).setCommand(command);
                channel.connect();
                LOGGER.info("Channel Connected..");

                while ((line = br.readLine()) != null) {
                    LOGGER.info("success response ####");
                    LOGGER.info("o/p : " + line);
                    resp.append(line);
                }

                while ((line = errBr.readLine()) != null) {
                    LOGGER.info("failure response ####");
                    LOGGER.info("o/p : " + line);
                    errResp.append(line);
                    comments = comments + line + " | ";
                }

            } catch (Exception e) {
                comments = comments + e.getMessage() + " | ";
                LOGGER.error("Exception - " +  e.getMessage(), e);
            } finally {
                LOGGER.info("going to disconnect channel....");
                channel.disconnect();
                LOGGER.info("Channel Disconnected.");
            }
        }
    }

    public void startRotation(Properties prop)
    {
        LOGGER.info("Starting to generate keys");
        ArrayList<String> servers = new ArrayList<>();
        try
        {
            AESCrypt aes = new AESCrypt();
            Class.forName("com.mysql.jdbc.Driver");
            String DB_URL = prop.getProperty("dburl");
            String DB_USER = prop.getProperty("dbusername");
            String DB_PASSWORD = aes.decryptIt(prop.getProperty("dbpass"));
            con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

            String candidateSelection = "select Endpointname from keyrotationdetails where status = 1;";
            PreparedStatement pstmt = con.prepareStatement(candidateSelection);
            pstmt = con.prepareStatement(candidateSelection);
            LOGGER.info("Executing statement: " + pstmt.toString());
            ResultSet rs1 = pstmt.executeQuery();
            LOGGER.info("Statement executed successfully");

            LOGGER.info("Candidate Selection done");
            while(rs1.next()) {
                servers.add(rs1.getString(1));
            }

            for(String server : servers) {
                LOGGER.info("Generating key for server " + server);
                genKey(prop,server,con,aes);
                LOGGER.info("Successfully Generated key for server " + server);
            }
            
            beginRotationperServer(servers,con,prop,aes);
        }
        catch (Exception e) {
            LOGGER.error("Exception - " +  e.getMessage(), e);
        }
    }

    public void beginRotation()
    {
        LOGGER.info("Start SSHKey Rotation");
        try
        {
            LocalDateTime ldt = LocalDateTime.now();
            ZonedDateTime zdt = ZonedDateTime.of(ldt, ZoneId.systemDefault());
            ZonedDateTime gmt = zdt.withZoneSameInstant(ZoneId.of("GMT"));
            Timestamp startdate = Timestamp.valueOf(gmt.toLocalDateTime());

            InputStream inputStream = null;
            AESCrypt aes = new AESCrypt();
            String saviyntHome = System.getenv("SAVIYNT_HOME");
            //String saviyntHome = "//Users/arpitberi/Documents/saviynt/Conf";
            String propFileName = saviyntHome + File.separator + "keyRotationConfig.properties";
            inputStream = new FileInputStream(propFileName);
            prop.load(inputStream);
            populateTable(prop);
            startRotation(prop);
        }
        catch (Exception e) {
            LOGGER.error("Exception - " +  e.getMessage(), e);
            jobstate = "failure";
        }
        LOGGER.info("Done Rotating keys");
    }
}
