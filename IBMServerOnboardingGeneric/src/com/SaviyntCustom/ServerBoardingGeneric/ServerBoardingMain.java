package com.SaviyntCustom.ServerBoardingGeneric;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import javax.net.ssl.HttpsURLConnection;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Level;

/**
 * Created by Saviynt on 7/17/2018.
 */
public class ServerBoardingMain {

    FileHandler fh;
    Connection dbConn = null;
    Statement dbStmt = null;
    Properties prop = new Properties();
    String jobstate;
    StringBuilder requestsFailedList=new StringBuilder();
    StringBuilder requestsSuccessList=new StringBuilder();
    private Logger logger = null;

    public ServerBoardingMain() {
        String saviyntHome = System.getenv("SAVIYNT_HOME");
        //System.setProperty("LOG_PATH", saviyntHome);
        PropertyConfigurator.configure(saviyntHome + File.separator + "customJarsLog4j.properties");
        logger = Logger.getLogger("boardingGenLogger");
    }

    public static void getPendingRequests(String outhToken,Properties prop)
    {

        try {
            String url=prop.getProperty("URLPATH")+"api/v5/getPendingRequests";
            com.SaviyntCustom.ServerBoardingGeneric.HttpUrlConn httpConn = new com.SaviyntCustom.ServerBoardingGeneric.HttpUrlConn();
            HashMap<String,String> requestsList=new HashMap<>();
            HashMap<String,String> requestorList=new HashMap<>();
            HttpsURLConnection con1 = null;
            HttpURLConnection con2 = null;
            JSONObject getPendingRequestsResponse = null;
            JSONObject jsonRequest = new JSONObject();
            JSONArray secSystemArray=new JSONArray();
            secSystemArray.add(0,prop.getProperty("SECURITYSYSTEM"));
            jsonRequest.put("securitysystem",secSystemArray);
            jsonRequest.put("endpoint",secSystemArray);
            jsonRequest.put("max",prop.getProperty("MAXREQUESTS"));
            jsonRequest.put("assignee",prop.getProperty("ASSIGNEE"));

            // add header
            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();

            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            httpConnectionHeaders.put("Content-Type", "application/json");
            httpConnectionHeaders.put("SAVUSERNAME", prop.getProperty("SAVUSERNAME"));
            String urlParams = jsonRequest.toString();

            System.out.println("\n ***** Reading Pending request : " + url + ". Input : " + urlParams);
            con2 = httpConn.sendPostHttps(url, jsonRequest.toString(), httpConnectionHeaders);
            getPendingRequestsResponse = httpConn.fetchJsonObject(con2);

            //con2 = httpConn.sendPostHttps(url, jsonRequest.toString(), httpConnectionHeaders);
            //getPendingRequestsResponse = httpConn.fetchJsonObject(con2);


            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(getPendingRequestsResponse.toString());

            Long totalRequests = (Long)json.get("total");
            JSONArray requestDetails = (JSONArray)json.get("results");

            if(totalRequests != null) {
                //int totalreq = Integer.parseInt(totalRequests);
                System.out.println("Total pending requests " + totalRequests);

                for (int i = 0; i < totalRequests; i++) {
                    boolean process = false;
                    JSONObject req = (JSONObject) requestDetails.get(i);

                    Long requestkey = (Long) req.get("requestkey");
                    //System.out.println("The requestkey is " + requestkey);
                    String requestid = (String) req.get("requestid");
                    String endpoints = (String) req.get("endpoints");

                    /*
                        String requestor=(String)req.get("requestor");
                        requestsList.put(String.valueOf(requestkey), requestid);
                        Pattern pattern1 = Pattern.compile("\\((.*?)\\)");
                        Matcher matcher1 = pattern1.matcher(requestor);
                        String requestorappr = "";
                        while (matcher1.find()) {
                            requestorappr = matcher1.group(1);
                            String emailapprover = getUserDetails(requestorappr, "email", outhToken, requestid,prop);
                            ccEmailRecipients.append(emailapprover + ",");
                        }
                    */
                    if(endpoints.contains(prop.getProperty("SECURITYSYSTEM"))) {
                        requestsList.put(String.valueOf(requestkey), requestid);
                    }
                    //getRequestDetails(requestkey,requesteeusername,endpoints,outhToken,ldapEntitlements);
                }

                ServerBoardingHandler userObj = new ServerBoardingHandler();
                userObj.processBoardingRequests(prop, requestsList, outhToken);
            }

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }


    }

    public static String getUserDetails(String username,String filter,String outhToken,String requestid,Properties prop)
    {
        String filterresult ="";
        try {
            //String url=prop.getProperty("URLPATH")+"api/v5/user?q=systemusername:"+column[0];
            String url=prop.getProperty("URLPATH")+"api/v5/getUser";
            com.SaviyntCustom.ServerBoardingGeneric.HttpUrlConn httpConn = new HttpUrlConn();

            HttpsURLConnection con1 = null;
            HttpURLConnection con2 = null;
            JSONObject getuserDetails = null;
            JSONObject jsonRequest = new JSONObject();

            JSONArray subReportArr = new JSONArray();

            JSONObject subReport1 = new JSONObject();
            subReport1.put("username",username);
            //put subreport object to array
            subReportArr.add(subReport1);

            jsonRequest.put("filtercriteria",subReport1);

            // add header
            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();

            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            httpConnectionHeaders.put("Content-Type", "application/json");
            httpConnectionHeaders.put("SAVUSERNAME", prop.getProperty("SAVUSERNAME"));

            //logger.info("Sending 'POST' request to URL : " + url);
            con2 = httpConn.sendPostHttps(url,jsonRequest.toString(),httpConnectionHeaders);
            getuserDetails = httpConn.fetchJsonObject(con2);

            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(getuserDetails.toString());
            String message = (String)json.get("msg");

            if(message.equalsIgnoreCase("Successful"))
            {
                //logger.info("user found");
                JSONArray userresults = (JSONArray)json.get("userlist");
                JSONObject userInnerresults = (JSONObject)userresults.get(0);
                //String cp10 = (String)userInnerresults.get("customproperty24");
                filterresult = (String)userInnerresults.get(filter);
            }
            else
            {
                //logger.info("user not found");
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            return filterresult;
        }
    }


    public static void main(String args[]) {
        ServerBoardingMain t=new ServerBoardingMain();
        t.startBoarding();
    }

    public  void startBoarding()
    {
        logger.info("External code start");
        try {
            //java.sql.Timestamp startdate = new java.sql.Timestamp(new java.util.Date().getTime());
            LocalDateTime ldt = LocalDateTime.now();
            ZonedDateTime zdt = ZonedDateTime.of(ldt, ZoneId.systemDefault());
            ZonedDateTime gmt = zdt.withZoneSameInstant(ZoneId.of("GMT"));
            Timestamp startdate = Timestamp.valueOf(gmt.toLocalDateTime());

            InputStream inputStream = null;
            com.SaviyntCustom.ServerBoardingGeneric.AESCrypt aes = new AESCrypt();
            String saviyntHome = System.getenv("SAVIYNT_HOME");
            String propFileName = saviyntHome + File.separator + "configurationsServerOnboardingGeneric.properties";
            inputStream = new FileInputStream(propFileName);
            prop.load(inputStream);
            com.SaviyntCustom.ServerBoardingGeneric.TokenPojo token = new TokenPojo();
            String outhToken = token.callBearerToken(prop);
            getPendingRequests(outhToken,prop);
            //create connection
            //createConnection(prop,outhToken);



            //store status of job
            /*
            String db_url = prop.getProperty("db_url");
            String user = prop.getProperty("user");
            String pass = prop.getProperty("pass");
            pass = aes.decryptIt(pass.trim());

            DatabaseConnector db = new DatabaseConnector();
            dbConn = db.getDatabaseConnection(db_url, user, pass);
            dbStmt = db.getDbCreateStatement(dbConn);
            LocalDateTime ldt1 = LocalDateTime.now();
            ZonedDateTime zdt1 = ZonedDateTime.of(ldt1, ZoneId.systemDefault());
            ZonedDateTime gmt1 = zdt1.withZoneSameInstant(ZoneId.of("GMT"));
            Timestamp enddate = Timestamp.valueOf(gmt1.toLocalDateTime());
            if(jobstate==null || jobstate=="")
                jobstate="success";

            String jobendsql = "insert into ecmimportjob(JOBSTARTDATE,JOBENDDATE,JOBNAME,SYSTEMNAME,SAVRESPONSE,COMENTS) values(?,?,?,?,?,?)" ;
            PreparedStatement pst;
            PreparedStatement pstupdate;
            pst = dbConn.prepareStatement(jobendsql);
            pst.setTimestamp(1, startdate);
            pst.setTimestamp(2, enddate);
            pst.setString(3,"ServerOnboardingJobCustom");
            pst.setString(4,"test");


            pst.setString(6,"[Success : "+requestsSuccessList.toString().replaceFirst(".$", "")+"## Failure : "+requestsFailedList.toString().replaceFirst(".$", "")+"]");
            try {
                pst.executeUpdate();
            }
            catch (SQLException se)
            {
                se.printStackTrace();
            }
            */
        } catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
            jobstate="failure";
            //logger.info("Error in Jar:"+e.getMessage());
            //logger.log(Level.SEVERE, e.getMessage(), e);
        } finally {
            try {
                if (dbConn != null)
                    dbConn.close();
                fh.close();
            } catch (Exception e) {
                //logger.log(Level.SEVERE, e.getMessage(), e);
            }
            try {
                if (dbStmt != null)
                    dbStmt.close();
            }
                catch (Exception e) {
                //logger.log(Level.SEVERE, e.getMessage(), e);
            }
            logger.info("External Code done");
            //logger.info("External Code Ended");
            try {
                if (fh!=null) {
                    fh.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("Exception - " +  e.getMessage(), e);
                jobstate="failure";
            }
        }
    }
}
