package com.SaviyntCustom.ServerBoardingGeneric;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException;

import java.sql.*;

/**
 * Creates DB connection from properties file
 */

public class DataBaseConnection {
    //private static Statement stmt;
    public Connection conn;

    DataBaseConnection(String url, String user, String pass, String jdbc_driver){
        try{
            Class.forName(jdbc_driver);
            conn = DriverManager.getConnection(url, user, pass);
            //stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(-1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet execute_query(String query) throws SQLException{
        //System.out.println(query);
        Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_UPDATABLE);
        ResultSet rs = null;
        try{

            rs = stmt.executeQuery(query);
        }
        catch (MySQLIntegrityConstraintViolationException e){
            e.printStackTrace();
        }
        catch(MySQLSyntaxErrorException e){
            e.printStackTrace();
        }
        return rs;
    }

    public int execute_update(String query) throws SQLException{
        //System.out.println(query);
        Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_UPDATABLE);
        int rs = 0;
        try{
            rs = stmt.executeUpdate(query);
        }
        catch (MySQLIntegrityConstraintViolationException e){
            e.printStackTrace();
        }
        catch(MySQLSyntaxErrorException e){
            e.printStackTrace();
        }
        return rs;
    }



    void close(){
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        conn.close();
    }
}
