package com.SaviyntCustom.ServerBoardingGeneric;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.*;
import java.util.*;
import java.util.Date;

/**
 * Created by Saviynt on 3/6/2017.
 */
public class AESCrypt {


    public static String decryptIt(String strToDecrypt) {
        byte[] key = "S@v!ynt_s@V!YNt_".getBytes();
        //byte[] key = "thrinathtransfer".getBytes();
        if(strToDecrypt !=null ) {
            try {

                Cipher cipher  = Cipher.getInstance("AES/ECB/PKCS5PADDING");
                final SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
                cipher.init(Cipher.DECRYPT_MODE, secretKey);
                final String decryptedString = new String(cipher.doFinal(Base64.decodeBase64(strToDecrypt)));
                return decryptedString;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    public static String encryptIt(String strToEncrypt) {
        byte[] key = "S@v!ynt_s@V!YNt_".getBytes();
        //byte[] key = "thrinathtransfer".getBytes();
        try {
            // Test with CBC or CTR mode.

            Cipher cipher  = Cipher.getInstance("AES/ECB/PKCS5Padding");
            final SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            final String encryptedString = Base64.encodeBase64String(cipher.doFinal(strToEncrypt.getBytes()));
            return encryptedString;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public  static void checkRecontriggerChains() {
        try {
            Connection con;
            Class.forName("com.mysql.jdbc.Driver");
            String DB_URL = "jdbc:mysql://127.0.0.1:11017/saviyntaws?allowMultiQueries=true";
            String DB_USER = "saviyntuser";
            String DB_PASSWORD = "$aviyntclouddb1";
            //String JDBC_DRIVER = prop.getProperty("JDBC_DRIVER");
            System.out.println("Selected schema: " + DB_URL);
            con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

            String selectSQL = "select trigger_name,job_data from qrtz_triggers where trigger_name in " +
                    "('GI_RECON_OOB_SYNC_SOX','GI_RECON_OOB_SYNC_DAY1','GI_RECON_OOB_SYNC_DAY2','GI_RECON_OOB_SYNC_DAY3'," +
                    "'GI_RECON_OOB_SYNC_DAY4','GI_RECON_OOB_SYNC_DAY5','GI_RECON_OOB_SYNC_DAY6'," +
                    "'GI_RECON_OOB_SYNC_DAY7') and job_name='TriggerChainJob';";
            PreparedStatement pstmt = con.prepareStatement(selectSQL);
            System.out.println("Executing statement: "+pstmt.toString());
            ResultSet rs3=pstmt.executeQuery();
            HashMap<String, String> triggerData = new HashMap<>();
            HashMap<String, String> finalTriggerData = new HashMap<>();

            while(rs3.next()) {
                String triggerName = rs3.getString("trigger_name");
                java.sql.Blob myBlob = rs3.getBlob("job_data");
                InputStream input = myBlob.getBinaryStream();

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buf = new byte[1024];

                InputStream in = myBlob.getBinaryStream();

                int n = 0;
                while ((n=in.read(buf))>=0)
                {
                    baos.write(buf, 0, n);
                }
                in.close();

                byte[] bytes = baos.toByteArray();
                String strImport = new String(bytes, "UTF-8");
                strImport = strImport.replaceAll("\uFFFD", "\"").replaceAll("t\u25AF", "\"");
                if(strImport.contains("jobnamelabelt")) {
                    strImport = strImport.substring(strImport.lastIndexOf("savtriggerorderformt"), strImport.indexOf("jobnamelabelt")).trim();
                }
                if(strImport.contains("cronexpressiont")) {
                    strImport = strImport.substring(strImport.lastIndexOf("savtriggerorderformt"), strImport.indexOf("cronexpressiont")).trim();
                }
                strImport=strImport.replace("savtriggerorderformt","");
                strImport=strImport.substring(0,strImport.length()-1).trim();

                triggerData.put(triggerName, strImport);
            }


            selectSQL = "select TRIGGER_NAME, ep.ENDPOINTNAME, ep.CUSTOMPROPERTY32 as 'RECONDAY' from qrtz_triggers qt\n" +
                    "join endpoints ep on (qt.TRIGGER_NAME = concat('ACCOUNTS_',substring_index(ep.ENDPOINTNAME,'.',1)) or qt.TRIGGER_NAME = concat('ACCOUNTS_',substring_index(ep.ENDPOINTNAME,'.',1),'_SERVER') or qt.TRIGGER_NAME = concat('GI_UNIX_SERVER_',substring_index(ep.ENDPOINTNAME,'.',1))) and ep.CUSTOMPROPERTY22='giserver' and ep.STATUS=1\n" +
                    "and TRIGGER_NAME not like '%access%';";
            System.out.println("Executing query - " + selectSQL);
            Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery(selectSQL);

            System.out.println("Statement executed successfully");
            int j=1;
            System.out.println("----------------------------------------");

            while (rs.next()) {
                String trigger = rs.getString("TRIGGER_NAME");
                String reconday = rs.getString("RECONDAY");
                String chains = "";
                int i=0;

                for (Map.Entry<String, String> data : triggerData.entrySet()) {
                    if(data.getValue().contains(trigger)) {
                        if(!data.getKey().endsWith(reconday)) {
                            triggerData.put(data.getKey(),
                                    data.getValue().replace(trigger + ",", ""));
                        }
                        i++;
                        chains = chains +  data.getKey() + ", ";
                    }

                }
                if(i>1) {
                    System.out.println(j + ")");

                    System.out.println(rs.getString("ENDPOINTNAME") + ", " +
                            rs.getString("TRIGGER_NAME") + ", Recon day - " +
                            rs.getString("RECONDAY") +
                            ". \nTrigger is present in " + i + " trigger chains. Chains are: " +
                            chains + "\n----------------------------------------");
                    j++;
                }
            }

            System.out.println("************************************************");

            for (Map.Entry<String, String> data : triggerData.entrySet()) {
                System.out.println(data.getKey() + " =  " + data.getValue());

            }

        }catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public  void testAddDynAttrs() {
        try {
            Connection con;
            Class.forName("com.mysql.jdbc.Driver");
            String DB_URL = "jdbc:mysql://127.0.0.1:11017/saviyntaws?allowMultiQueries=true";
            String DB_USER = "saviyntuser";
            String DB_PASSWORD = "$aviyntclouddb1";
            //String JDBC_DRIVER = prop.getProperty("JDBC_DRIVER");
            System.out.println("Selected schema: " + DB_URL);
            con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

            Properties prop = new Properties();
            InputStream inputStream = null;
            AESCrypt aes = new AESCrypt();
            String saviyntHome = System.getenv("SAVIYNT_HOME");
            String propFileName = saviyntHome + File.separator + "configurationsServerOnboardingGeneric.properties";
            inputStream = new FileInputStream(propFileName);
            prop.load(inputStream);
            TokenPojo token = new TokenPojo();
            String outhToken = token.callBearerToken(prop);

            addDynamicAttributes("dpywdcu1fad01.w3-969.ibm.com", con, prop);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public  boolean addDynamicAttributes(String connectionname, Connection con, Properties prop)
    {
        boolean status = false;
        int counter = 0;

        System.out.println("\nInside addDynamicAttributes...");

        try
        {
            String query1 ="select endpointkey,customproperty1 from endpoints where endpointname = ?";
            PreparedStatement pst = con.prepareStatement(query1);
            pst.setString(1,connectionname);
            ResultSet rs = pst.executeQuery();
            rs.next();
            String endpointkey = rs.getString(1);
            String osType = rs.getString(2);

            String defaultpg = null;
            if(osType!=null && osType.toUpperCase().contains("AIX")) {
                defaultpg = "staff";
            } else {
                defaultpg = "users";
            }
            String dynAttrs = prop.getProperty("DYNAMIC_ATTRIBUTES");

            if(endpointkey!=null && osType!=null && dynAttrs != null && !dynAttrs.isEmpty())
            {
                dynAttrs = dynAttrs.replace("${endpointname}","'"+connectionname+"'");
                dynAttrs = dynAttrs.replace("${endpointkey}",endpointkey);
                dynAttrs = dynAttrs.replace("${defaultpg}","'"+defaultpg+"'");
                System.out.println("DYNAMIC_ATTRIBUTES - " + dynAttrs);
                PreparedStatement pstmt = con.prepareStatement(dynAttrs);
                pstmt = con.prepareStatement(dynAttrs);
                System.out.println("Executing statement: " + pstmt.toString());
                //ResultSet rs1 = pstmt.executeQuery();
                pstmt.execute();
                System.out.println("Statement executed successfully");
                status =true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();

        }
        finally {
            return status;
        }

    }

    
    public static void main (String args[]) {

    //String a = encryptIt("$aviynt$AW$1");
    //String a1 = encryptIt("BHXU3Rm8LXC5henD");
    //String a2 = encryptIt("6a8bTAeBRLHKSQf6");

    //PROD
    String a = encryptIt("$aviyntclouddb1");
    String a1 = encryptIt("tD4-S2PW-~Q*Hwbf");
    String a2 = encryptIt("vmvD8httcBWZHbSY");
    String a3 = encryptIt("Accesshub@123456789");

    //String a3 = encryptIt("Welcome2ibm@123");
    //String a3 = encryptIt("BHXU3Rm8LXC5henD");

    //System.out.println(a);
    //System.out.println(a1);
    //System.out.println(a2);
    //System.out.println(a3);

    String b = decryptIt("m0mhDw0eNY2FempwZ5pyuLPyTDSeC1javBA5IYhg9sA=");
    //System.out.println(b);

    /*
      String  accountsReconTriggerName = null;
      String data = "bldbz172095.cloud.dst.ibm.com";
      if (data.matches(".*[a-zA-Z]+.*")) {
          accountsReconTriggerName = "ACCOUNTS_"+
                  ((data.split("\\."))[0]).toUpperCase();
      } else {
          accountsReconTriggerName = "ACCOUNTS_"+
                  data.toUpperCase();
      }
      System.out.println("accountsReconTriggerName = "+accountsReconTriggerName);
        */
      /*String a = "GI_CLEAR_ACCOUNTID,ACCOUNTS_MOPBZ5187,ACCOUNTS_B20AEIIAMEP01T,ACCOUNTS_B20AEIIAMEP02T,ACCOUNTS_B19LEIIAMEP01T,ACCOUNTS_B20ZEIIAMEP01T,ACCOUNTS_MOPBZP174227,ACCOUNTS_CNWBZP1191_SERVER,Accounts_b01aeibirt002_server,ACCOUNTS_B03AVI17003880_SERVER,ACCOUNTS_B03AVI17003881_SERVER,ACCOUNTS_B03AVI18318930_SERVER,ACCOUNTS_TSTXGSAND1_SERVER, ACCOUNTS_TSTXGSAND2_SERVER,ACCOUNTS_B01LVI19510640_SERVER,ACCOUNTS_B01LVI21562660_SERVER,ACCOUNTS_TSTLGSADM1_SERVER,ACCOUNTS_TSTLGSADR1_SERVER,GI_UNIX_ACCOUNT_CORRELATION,GI_ACCOUNTS_CP30_MAXAGE";
      String b[] = a.split(",", 2);

      System.out.println("Value1 = "+b[0]);
      System.out.println("Value2 = "+b[1]);*/

      /*
      String soxList = "IBM SPI,ePayment Information,IBM Crown Jewel,Subject to SOX (financially significant asset)";
      String[] soxListA = null;
      if(soxList != null) {
          soxListA = soxList.toUpperCase().split(",");
      }

      if(StringUtils.indexOfAny("TEST, IBM SPI, TEST1,TEST".toUpperCase(),soxListA)!=-1) {
          System.out.println("Inside");
      }
      */

      /*
      HashMap<String, String> triggerData = new HashMap<>();
      triggerData.put("GI_RECON_OOB_SYNC_DAY1","GI_CLEAR_ACCOUNTID,GI_BACKUPACCOUNTS_DAY1,GI_OOBREMOVECP9CP45,ACCOUNTS_B01AEIND004,ACCOUNTS_B20ZEIIAMEP01T,ACCOUNTS_B19LEIIAMEP01T,ACCOUNTS_B20AEIIAMEP02T,ACCOUNTS_B20AEIIAMEP01T,ACCOUNTS_B03AVI17003880_SERVER,ACCOUNTS_B03AVI17003881_SERVER,ACCOUNTS_B03AVI18318930_SERVER,ACCOUNTS_B01LVI19510640_SERVER,ACCOUNTS_B01LVI21562660_SERVER,ACCOUNTS_TSTXGSAND1_SERVER,GI_UNIX_ACCOUNT_CORRELATION,GI_ACCOUNTS_CP30_MAXAGE,GI_ORPHAN_GROUP_OWNERSHIP_ASSIGNMENT,GI_UPDATEINACTIVEENTGROUPS,UNIX_ENT_TO_ROLE,GI_OOB_SYNC_DAY1,IAM_ENTITLEMENT_UPDATE,REVOKEOOBACCESS,REVOKEOOBANALYTICS");
      triggerData.put("GI_RECON_OOB_SYNC_DAY2","GI_CLEAR_ACCOUNTID,GI_BACKUPACCOUNTS_DAY2,GI_OOBREMOVECP9CP45,ACCOUNTS_TSTXGSAND2_SERVER,ACCOUNTS_TSTXGSASD1_SERVER,ACCOUNTS_TSTXGSASD2_SERVER,ACCOUNTS_TSTXGSATM1_SERVER,ACCOUNTS_TSTLGSADM1_SERVER,ACCOUNTS_TSTLGSADR1_SERVER,ACCOUNTS_B23ACIWAS05_SERVER,ACCOUNTS_B01AEIBIRT002_SERVER,GI_UNIX_SERVER_B01AEIAPP005,GI_UNIX_SERVER_B01AEIAPP006,GI_UNIX_SERVER_B01AEILDM003,GI_UNIX_ACCOUNT_CORRELATION,GI_ACCOUNTS_CP30_MAXAGE,GI_ORPHAN_GROUP_OWNERSHIP_ASSIGNMENT,GI_UPDATEINACTIVEENTGROUPS,UNIX_ENT_TO_ROLE,GI_OOB_SYNC_DAY2,IAM_ENTITLEMENT_UPDATE,REVOKEOOBACCESS,REVOKEOOBANALYTICS");
      triggerData.put("GI_RECON_OOB_SYNC_DAY3","GI_CLEAR_ACCOUNTID,GI_BACKUPACCOUNTS_DAY3,GI_OOBREMOVECP9CP45,ACCOUNTS_G41P-R-00000239,ACCOUNTS_BEJXGSAND1,ACCOUNTS_A31XGSAND2,ACCOUNTS_A31XGSAND1,ACCOUNTS_B19ACIRDB001ST,ACCOUNTS_G42P-R-00000279,ACCOUNTS_G41P-R-00000278,ACCOUNTS_G41P-A-00000082,GI_UNIX_SERVER_B01AEIND001,GI_UNIX_SERVER_B01AEIND002,GI_UNIX_SERVER_B01AEINDM002,GI_UNIX_SERVER_B01AEINIM002,GI_UNIX_SERVER_B01AEILDM004,GI_UNIX_SERVER_B01AEIRDB004,GI_UNIX_SERVER_B01AEIRDB005,GI_UNIX_SERVER_B01AEIWAS005,GI_UNIX_SERVER_B01AEIWAS006,GI_UNIX_SERVER_B01AEIWAS007,GI_UNIX_SERVER_B01AEIWAS008,GI_UNIX_SERVER_B01AEIWAS009,GI_UNIX_ACCOUNT_CORRELATION,GI_ACCOUNTS_CP30_MAXAGE,GI_ORPHAN_GROUP_OWNERSHIP_ASSIGNMENT,GI_UPDATEINACTIVEENTGROUPS,UNIX_ENT_TO_ROLE,GI_OOB_SYNC_DAY3,IAM_ENTITLEMENT_UPDATE,REVOKEOOBACCESS,REVOKEOOBANALYTICS");
      triggerData.put("GI_RECON_OOB_SYNC_DAY4","GI_CLEAR_ACCOUNTID,GI_BACKUPACCOUNTS_DAY4,GI_OOBREMOVECP9CP45,GI_UNIX_SERVER_B01AEIWAS010,GI_UNIX_SERVER_B01AEIWAS011,GI_UNIX_SERVER_B01AEIWAS012,GI_UNIX_SERVER_B01AEIWAS013,GI_UNIX_SERVER_B01AEIWAS014,ACCOUNTS_B01AEIWAS001,ACCOUNTS_B01AEIWAS003,ACCOUNTS_B01AEILDM002,ACCOUNTS_B01AEIRDB002,ACCOUNTS_B01AEIWAS004,ACCOUNTS_B01AEIRDB003,ACCOUNTS_B01AEIWAS002,GI_UNIX_ACCOUNT_CORRELATION,GI_ACCOUNTS_CP30_MAXAGE,GI_ORPHAN_GROUP_OWNERSHIP_ASSIGNMENT,GI_UPDATEINACTIVEENTGROUPS,UNIX_ENT_TO_ROLE,GI_OOB_SYNC_DAY4,IAM_ENTITLEMENT_UPDATE,REVOKEOOBACCESS,REVOKEOOBANALYTICS");
      triggerData.put("GI_RECON_OOB_SYNC_DAY5","GI_CLEAR_ACCOUNTID,GI_BACKUPACCOUNTS_DAY5,GI_OOBREMOVECP9CP45,ACCOUNTS_B01AEIRDB001,ACCOUNTS_B01AEINDM001,ACCOUNTS_B01AEILDM001,ACCOUNTS_B01AEIBIRT001,ACCOUNTS_IGAPLDAP02,ACCOUNTS_IGAPITIM02,ACCOUNTS_IGAPIDB202,ACCOUNTS_IGAPIDB201,ACCOUNTS_IGAPITIM01,ACCOUNTS_IGAPLDAP01,ACCOUNTS_IGATITIM03,ACCOUNTS_IGATITIM02,GI_UNIX_ACCOUNT_CORRELATION,GI_ACCOUNTS_CP30_MAXAGE,GI_ORPHAN_GROUP_OWNERSHIP_ASSIGNMENT,GI_UPDATEINACTIVEENTGROUPS,UNIX_ENT_TO_ROLE,GI_OOB_SYNC_DAY5,IAM_ENTITLEMENT_UPDATE,REVOKEOOBACCESS,REVOKEOOBANALYTICS");
      triggerData.put("GI_RECON_OOB_SYNC_DAY6","GI_CLEAR_ACCOUNTID,GI_BACKUPACCOUNTS_DAY6,GI_OOBREMOVECP9CP45,ACCOUNTS_IGATITIM01,ACCOUNTS_B01AEIAPP003,ACCOUNTS_B01AEIND003,ACCOUNTS_B01AEIAPP004,ACCOUNTS_B01AEIAPP001,ACCOUNTS_B01AEIAPP002,ACCOUNTS_B23ACIRDB010,ACCOUNTS_B23ACIWAS04,ACCOUNTS_B23ACIRDB009,ACCOUNTS_B23ACIRDB008,ACCOUNTS_B23ACIWAS03,GI_UNIX_ACCOUNT_CORRELATION,GI_ACCOUNTS_CP30_MAXAGE,GI_ORPHAN_GROUP_OWNERSHIP_ASSIGNMENT,GI_UPDATEINACTIVEENTGROUPS,UNIX_ENT_TO_ROLE,GI_OOB_SYNC_DAY6,IAM_ENTITLEMENT_UPDATE,REVOKEOOBACCESS,REVOKEOOBANALYTICS");
      triggerData.put("GI_RECON_OOB_SYNC_DAY7","GI_CLEAR_ACCOUNTID,GI_BACKUPACCOUNTS_DAY7,GI_OOBREMOVECP9CP45,ACCOUNTS_B12CXCN0P0131,ACCOUNTS_B12CXCN0P0130,ACCOUNTS_DEEHQDS040IBMXM,ACCOUNTS_DEEHQAP031IBMXM,ACCOUNTS_DEEHQDS041IBMXM,ACCOUNTS_DEEHQAP040IBMXM,ACCOUNTS_DEEHQAP030IBMXM,ACCOUNTS_DEEHQAP041IBMXM,GI_UNIX_ACCOUNT_CORRELATION,GI_ACCOUNTS_CP30_MAXAGE,GI_ORPHAN_GROUP_OWNERSHIP_ASSIGNMENT,GI_UPDATEINACTIVEENTGROUPS,UNIX_ENT_TO_ROLE,GI_OOB_SYNC_DAY7,IAM_ENTITLEMENT_UPDATE,REVOKEOOBACCESS,REVOKEOOBANALYTICS");
      String triggerChainList = triggerData.get("GI_RECON_OOB_SYNC_DAY1");
      int min = StringUtils.countMatches(triggerChainList,"ACCOUNTS_");
      String finalTriggerName = "GI_RECON_OOB_SYNC_DAY1";

      for (Map.Entry<String, String> data : triggerData.entrySet()) {
          int count = StringUtils.countMatches(data.getValue(), "ACCOUNTS_");

          if (count < min) {
              min = count;
              triggerChainList = data.getValue();
              finalTriggerName = data.getKey();
          }

          System.out.println("data.getKey() = "+data.getKey());
          System.out.println("count = "+count);
          System.out.println("min = "+min);
      }

      System.out.println("finalTriggerName = "+finalTriggerName);
    */

      //checkRecontriggerChains();

        AESCrypt aes = new AESCrypt();
        aes.testAddDynAttrs();
  }
}
