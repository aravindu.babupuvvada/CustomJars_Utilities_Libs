package com.SaviyntCustom.ReconOOBSyncWin;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Reconcilation {
	private Logger logger = null;

	public static void main(String[] args) {
		Map map=new HashMap();  
	    map.put("sox","yes");
	    map.put("reconday","1");
	    
	    Reconcilation sync = new Reconcilation();
	    sync.sync(map);
	}
	public Reconcilation() {
		String saviyntHome = System.getenv("SAVIYNT_HOME");
		//System.setProperty("LOG_PATH", saviyntHome);
		PropertyConfigurator.configure(saviyntHome + File.separator + "customJarsLog4j.properties");
		logger = Logger.getLogger("oobsyncWinLogger");
	}

    public void sync(Map data) {
        logger.info("***** Validate and Sync Start *****");
        
		String reconAccAndAccAttrsQry = null;
		String deleteAccHistQry = null;
		String accHistoryQry = null;
       
		Connection con=null;
		
		Statement actHistStmt=null;
		ResultSet actHRs=null;

		Statement accAndAttrStmt = null;
		ResultSet reconAccAndAccAttrsData = null;
		
		Statement actHistDelStmt=null;

		PreparedStatement arsStmt=null;
		PreparedStatement actUpdate=null;
		PreparedStatement actStatusUpd=null;

		InputStream inputStream = null;
		
		try{
			String sox = null;
			String reconday = null;
			
	        if (data != null & (data.containsKey("sox") || data.containsKey("reconday"))) {
	            sox = ((String)data.get("sox"));
	            reconday = ((String)data.get("reconday"));
	            
	            logger.info("sox - " + sox);
	            logger.info("reconday - " + reconday);
	
	        }
	        
			/*Date today = Calendar.getInstance().getTime();
	    	
			Calendar c = Calendar.getInstance();
			c.setTime(today);
			int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);*/
			
	    	Properties prop = new Properties();	        
	    	String saviyntHome = System.getenv("SAVIYNT_HOME");
			logger.info("SAVIYNT_HOME - " + saviyntHome + "\n");
	    	String propFileName = saviyntHome.concat("/SAMConfig.properties");
	    	inputStream = new FileInputStream(propFileName);
	        prop.load(inputStream);
			Class.forName("com.mysql.jdbc.Driver");
			logger.info("DBURL - " + prop.getProperty("DBURL"));
			logger.info("DBUSER - " + prop.getProperty("DBUSER"));

			logger.info("Connecting to - " + prop.getProperty("DBURL"));

			AESCrypt aes = new AESCrypt();
			String pass=aes.decryptIt(prop.getProperty("DBPASS"));
		    //LOGGER.info("pass"+pass+"\n");
			if(con==null) {
				con=DriverManager.getConnection(prop.getProperty("DBURL"),prop.getProperty("DBUSER"),pass);
			}
			con.setAutoCommit(true);
			logger.info("DB connection successful.");

			String conditionQuery = null;
	        if (sox != null && sox.equalsIgnoreCase("yes")) {
	        	conditionQuery = prop.getProperty("GIWIN_SOXCONDQUERY");
	        } else if ((sox != null && sox.equalsIgnoreCase("no")) 
	        		&& (reconday != null && !reconday.isEmpty())) {
	        	if(reconday.equalsIgnoreCase("adhoc")) {
					conditionQuery = prop.getProperty("GIWIN_ADHOCCONDQUERY");
				} else {
					conditionQuery = prop.getProperty("GIWIN_NONSOXCONDQUERY");
					conditionQuery = conditionQuery.replace("${reconday}", "'" + reconday + "'");
				}
	        }
	        
	        if(conditionQuery != null && !conditionQuery.isEmpty()) {
	        	
	        	reconAccAndAccAttrsQry = prop.getProperty("GIWIN_RECONACCSANDATTR");
				reconAccAndAccAttrsQry = reconAccAndAccAttrsQry.replace("${condquery}",conditionQuery);
	        	accHistoryQry = prop.getProperty("GIWIN_ACCSHIST");
				accHistoryQry = accHistoryQry.replace("${condquery}",conditionQuery);
				deleteAccHistQry = prop.getProperty("GIWIN_DELACCSHIST");
				deleteAccHistQry = deleteAccHistQry.replace("${condquery}",conditionQuery);

				logger.info("reconAccAndAccAttrsQry - " + reconAccAndAccAttrsQry);
				logger.info("accHistoryQry - " + accHistoryQry);
				logger.info("deleteAccHistQry - " + deleteAccHistQry);

				accAndAttrStmt=con.createStatement();
			    reconAccAndAccAttrsData=accAndAttrStmt.executeQuery(reconAccAndAccAttrsQry);
	
			    actHistStmt=con.createStatement();
			    actHRs=actHistStmt.executeQuery(accHistoryQry);
			    
			    arsStmt=con.prepareStatement(prop.getProperty("GIWIN_ARSTASK"));
			    
			    actUpdate=con.prepareStatement("update accounts set customproperty45='Yes' where accountkey=?");
			    actStatusUpd=con.prepareStatement("update accounts set customproperty39=? where accountkey=?");
			    
			    /* --the below code is to check in account_attributes table ---  */ 
			    while(reconAccAndAccAttrsData.next()) {
					int accountkey = reconAccAndAccAttrsData.getInt(1); 
					String name = reconAccAndAccAttrsData.getString(2);
					String status = reconAccAndAccAttrsData.getString(3);
					String gecos = reconAccAndAccAttrsData.getString(4);
					int endpointkey = reconAccAndAccAttrsData.getInt(5);
					String accounttype = reconAccAndAccAttrsData.getString(6);
					int sskey = reconAccAndAccAttrsData.getInt(7);
					int userkey = reconAccAndAccAttrsData.getInt(8);
					String reconciledGecos = reconAccAndAccAttrsData.getString(9);
					
					 if((status.equalsIgnoreCase("1") || status.equalsIgnoreCase("Active")) 
							 && (null!=gecos && null!=reconciledGecos && !reconciledGecos.equalsIgnoreCase(gecos))) {
						 logger.info("GECOS mismatch found. Account - " + accountkey);
						 logger.info("Reconciled GECOS - " + reconciledGecos);
						 logger.info("Accounts GECOS - " + gecos);
						 
						 actUpdate.setInt(1, accountkey);
						 arsStmt.setInt(1, accountkey);
						 arsStmt.setString(2, name);
						 arsStmt.setString(3, "Created by sync job for GECOS mismatch - Windows");
						 arsStmt.setInt(4, endpointkey);
						 arsStmt.setInt(5, sskey);
						 arsStmt.setInt(6, 12);
						 arsStmt.setInt(7, userkey);
					     int arows=actUpdate.executeUpdate();
						 int rows=arsStmt.executeUpdate();
						 logger.info("Acc Rows updated = " + arows + ", GECOS sync task created = " + rows);
					 }
					
					 while(actHRs.next()) {
						 if(accountkey==actHRs.getInt(1)) {
							if((status.equalsIgnoreCase("1") || status.equalsIgnoreCase("Active")) 
									&& (null!=accounttype && (accounttype.equalsIgnoreCase("Personal") || accounttype.equalsIgnoreCase("System"))) ) {
								 if((actHRs.getString(3)).equalsIgnoreCase("InActive") || (actHRs.getString(3)).equalsIgnoreCase("0") 
										|| (actHRs.getString(3)).equalsIgnoreCase("2")) {
									logger.info("Status mismatch found. Account - " + accountkey);
									logger.info("Reconciled Status - " + status);
									logger.info("Account Status - " + actHRs.getString(3));
									
									arsStmt.setInt(1, accountkey);
									arsStmt.setString(2, name);
									arsStmt.setString(3, "Created by sync job for STATUS mismatch - Windows");
									arsStmt.setInt(4, endpointkey);
									arsStmt.setInt(5, sskey);
									arsStmt.setInt(6, 14);
									arsStmt.setInt(7, userkey);
									
									int rows=arsStmt.executeUpdate();
									logger.info("Status sync task created - " + rows);
								 }
							}
				    		
							/*if((status.equalsIgnoreCase("1") || status.equalsIgnoreCase("Active")) 
									&& (null!=accounttype && accounttype.equalsIgnoreCase("System")) ) {
								if((actHRs.getString(3)).equalsIgnoreCase("InActive") || (actHRs.getString(3)).equalsIgnoreCase("0") 
										|| (actHRs.getString(3)).equalsIgnoreCase("2") ) { 
									Date objDate = new Date();
									String strDateFormat = "dd-MMM-yyyy"; 
									SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat);
									
									logger.info("");
									LOGGER.info("System account - " + accountkey 
											+  " activated on = " + objSDF.format(objDate));
									
									actStatusUpd.setString(1,"Enabled on:"+objSDF.format(objDate));
									actStatusUpd.setInt(2,accountkey);
									int srows=actStatusUpd.executeUpdate();
									LOGGER.info("ARows updated="+srows+"\n");
							     }
							}*/
				    		
				    		if((status.equalsIgnoreCase("2") || status.equalsIgnoreCase("InActive") || status.equalsIgnoreCase("0")) 
				    				&& (null!=accounttype && (accounttype.equalsIgnoreCase("Personal") || accounttype.equalsIgnoreCase("System"))) ) {
				    			if((actHRs.getString(3)).equalsIgnoreCase("Active") || (actHRs.getString(3)).equalsIgnoreCase("1")) { 
									Date objDate = new Date();
									String strDateFormat = "dd-MMM-yyyy"; 
									SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat);

									logger.info("Account - " + accountkey
											+  " deactivated on - " + objSDF.format(objDate));
									
									actStatusUpd.setString(1,"Disabled on:"+objSDF.format(objDate));
									
									actStatusUpd.setInt(2,accountkey);
									int srows=actStatusUpd.executeUpdate();
									logger.info("ARows updated - "+srows+"\n");
								}
			    			}
				    	}
				    }
				    actHRs.first();
				    //LOGGER.info("Checking........" ); 
				}
			    //Delete the processed records
			    actHistDelStmt=con.createStatement();
			    actHistDelStmt.executeUpdate(deleteAccHistQry);
			    logger.info("Deleted processed records from Account History table" );
			    
	        }


	        if(!checkOOBNestedGroups(con, prop)) {
				logger.info("There was some error in running OOB check for nested groups.");
			}

		} catch(Exception e) {
			logger.error("Exception - " +  e.getMessage(), e);
		}
		finally {
			try {
				if(con!=null)		
					con.close();
				if(actHistStmt!=null)
					actHistStmt.close();
				if(accAndAttrStmt!=null)
					accAndAttrStmt.close();
				if(actHRs!=null)
					actHRs.close();
				if(arsStmt!=null)
					arsStmt.close();
				if(actUpdate!=null)
					actUpdate.close();
				logger.info("***** Validate and Sync End *****\n\n");
			} catch(Exception e) {
				logger.error("Exception - " +  e.getMessage(), e);

			}
		}
	}

	public boolean checkOOBNestedGroups(Connection con, Properties prop) {
		boolean status = false;

		logger.info("Inside getOOBNestedGroups");

		try {
			String getNestedGrpStr = prop.getProperty("GIWIN_GETOOBNESTEDGROUPS");

			if(getNestedGrpStr != null && !getNestedGrpStr.isEmpty()) {
				logger.info("getNestedGrpStr - " + getNestedGrpStr);
				PreparedStatement pstmt = con.prepareStatement(getNestedGrpStr);

				pstmt = con.prepareStatement(getNestedGrpStr);
				logger.info("Executing statement: " + pstmt.toString());
				ResultSet rs = pstmt.executeQuery();
				logger.info("Statement executed successfully");
				while (rs.next()) {
					Integer roleKey = rs.getInt(1);
					Integer parentEnt = rs.getInt(2);
					Integer childEnt = rs.getInt(3);
					Integer epKey = rs.getInt(4);
					Integer ssKey = rs.getInt(5);
					Integer userkey = rs.getInt(6);

					PreparedStatement arsChildStmt=con.prepareStatement(prop.getProperty("GIWIN_ARSTASK_REMOVECHILDGRP"));

					if(roleKey!=null && parentEnt!=null && childEnt!=null
							&& epKey!=null && ssKey!=null && userkey!=null) {
						//Create the remove child ent task

						arsChildStmt.setInt(1, childEnt);
						arsChildStmt.setString(2, "Created by custom OOB job for Nested Group - Windows");
						arsChildStmt.setInt(3, epKey);
						arsChildStmt.setInt(4, ssKey);
						arsChildStmt.setInt(5, roleKey);
						arsChildStmt.setInt(6, userkey);
						logger.info("Executing statement: " + arsChildStmt.toString());

						int rows=arsChildStmt.executeUpdate();
						logger.info("Remove child OOB group task created - " + rows);
					}
				}
				status =true;
			}
		}
		catch (Exception e) {
			logger.error("Exception - " +  e.getMessage(), e);
		}
		finally {
			return status;
		}
	}
}
