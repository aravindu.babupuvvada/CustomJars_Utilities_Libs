package com.accesshub.job;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.net.ssl.HttpsURLConnection;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class WinTransferOwnership {
	private Logger logger = null;

	public WinTransferOwnership() {
		String saviyntHome = System.getenv("SAVIYNT_HOME");
		//System.setProperty("LOG_PATH", saviyntHome);
		PropertyConfigurator.configure(saviyntHome + File.separator + "customJarsLog4j.properties");
		logger = Logger.getLogger("winTransferLogger");
	}

	public void main(String args[]) {
		WinTransferOwnership t=new WinTransferOwnership();
		t.start();
	}

	private void start() {
		// TODO Auto-generated method stub
		Connection con=null;
		PreparedStatement updtUserAccs=null;
		PreparedStatement updtAccAttrs=null;
		PreparedStatement updtAccEnts=null;
		PreparedStatement failup1=null;
		PreparedStatement failup2=null;
		Statement taskStmt=null;
		PreparedStatement winTaskStmt=null;
		Statement targetTask=null;
		Statement origTask=null;
		
		PreparedStatement updtTransferTask=null;
		PreparedStatement createWinTaskStmt=null;
		InputStream inputStream = null;
		ResultSet transferEndTasks=null;
		
		ResultSet winDetails=null;
		ResultSet winTasks=null;
		try{  
	    	Properties prop = new Properties();	        
	    	String saviyntHome = System.getenv("SAVIYNT_HOME");
	    	logger.info("Start Processing...\n");

	    	logger.info("Saviynt Home---> "+saviyntHome+"\n");
	    	String propFileName = saviyntHome.concat("/SAMConfig.properties");
	    //	String propFileName = "./externalconfig.properties";
	    	logger.info("Found the configs.\n");
	    	inputStream = new FileInputStream(propFileName);
	        prop.load(inputStream);
			Class.forName("com.mysql.jdbc.Driver");  
			logger.info("Before connection"+"\n");
		    logger.info("DBURL - " + prop.getProperty("DBURL"));
			logger.info("DBUSER - " + prop.getProperty("DBUSER"));
			
			AESCrypt aes = new AESCrypt();
			String pass=aes.decryptIt(prop.getProperty("DBPASS"));
			if(con==null) {
				con = DriverManager.getConnection(prop.getProperty("DBURL"), prop.getProperty("DBUSER"), pass);
			}
			con.setAutoCommit(true);
			logger.info("After connection");

			taskStmt=con.createStatement();
		    transferEndTasks=taskStmt.executeQuery(prop.getProperty("WINTRANSFER_SELECTTRANTASKS"));
			targetTask=con.createStatement();
			winTasks=targetTask.executeQuery(prop.getProperty("WINTRANSFER_SERVERTASKS"));

			winTaskStmt=con.prepareStatement(prop.getProperty("WINTRANSFER_SELECTSERVERDETAILS"));
			createWinTaskStmt=con.prepareStatement(prop.getProperty("WINTRANSFER_CREATESERVERTASKS"));
		    updtUserAccs=con.prepareStatement(prop.getProperty("WINTRANSFER_UPDATEUSERACCS"));
		    updtAccAttrs=con.prepareStatement(prop.getProperty("WINTRANSFER_UPDATEACCS"));
		    updtTransferTask=con.prepareStatement(prop.getProperty("WINTRANSFER_UPDATETRANTASKS"));
		    updtAccEnts=con.prepareStatement(prop.getProperty("WINTRANSFER_UPDATEAE1"));

		    String success = "";
		    
		    TokenPojo token = new TokenPojo();
		    String outhToken = "";
		    while(winTasks.next()) {
		    	logger.info("There are Server tasks present. Processing...");
		    	int transferTaskid=winTasks.getInt("parenttask");
		    	int serverTaskStatus=winTasks.getInt("status");
		    	if(serverTaskStatus==3) {
		    		updtAccEnts.setInt(1, transferTaskid);
		    		updtAccEnts.setInt(2, transferTaskid);
		    		int too = updtAccEnts.executeUpdate();
		    		if(too!=0) {
		    			outhToken = token.callBearerToken(prop);
		    			logger.info("Got the token before compeleting task - " + outhToken);
		    		    success=provisionTask(outhToken,transferTaskid,prop);
		    	        logger.info("Transfer account ownership is completed - " + success  );
		    		} else {
		    			logger.info("Transfer account ownership is NOT completed" );
		    		}
		    	}
		    	if(serverTaskStatus==4 || serverTaskStatus==8) {
		    		failup1=con.prepareStatement(prop.getProperty("WINTRANSFER_ROLLBACKACCS"));
		    		failup1.setInt(1, transferTaskid);
		    		failup1.setInt(2, transferTaskid);
		    		failup1.setInt(3, transferTaskid);
		    		int act=failup1.executeUpdate();

		    		failup2=con.prepareStatement(prop.getProperty("WINTRANSFER_ROLLBACKUSERACCS"));
		    		failup2.setInt(1, transferTaskid);
		    		failup2.setInt(2, transferTaskid);
		    		int ua=failup2.executeUpdate();

					origTask=con.createStatement();
					boolean b=origTask.execute(prop.getProperty("WINTRANSFER_SETTRANTASKFAILED")+transferTaskid);
		    		logger.info("Transfer account ownership is completed with failed status"+ b+":" +act +":"+ua );
		    	}
		    }
		    
		    while(transferEndTasks.next()) {
		    	int tTaskkey=transferEndTasks.getInt("taskkey");
		    	String winActName=transferEndTasks.getString("ENTITLEMENT_VALUEKEY");
		    	logger.info("Transfer ownership endpoint task key - " + tTaskkey );

				winTaskStmt.setInt(1, tTaskkey);
				winDetails=winTaskStmt.executeQuery();

		 	    if(winDetails.next()) {
		 	    	int winAccount=winDetails.getInt("entitlementid");
		 	    	String targetEndpoint=winDetails.getString("customproperty2");
		 	    	String accountType=winDetails.getString("customproperty3");
		 	    	logger.info("entitlementid (Account key) - " + winAccount);
		 	    	logger.info("customproperty2 (Target endpoint) - " + targetEndpoint );
		 	    	logger.info("customproperty3 (Account type) - " + accountType );
		 	    //	String connStatus=getConnection(outhToken,targetEndpoint,prop);
		 	    	
		 	    	String connStatus="success";
		 	    	if(connStatus=="success") {
		 	    		logger.info("Not checking the conncetion");
		 	    		updtUserAccs.setInt(1, tTaskkey);
		 	    		updtUserAccs.setInt(2, tTaskkey);
		 	    		int up1 = updtUserAccs.executeUpdate();
		 	    		updtAccAttrs.setInt(1, tTaskkey);
		 	    		updtAccAttrs.setInt(2, tTaskkey);
		 	    		updtAccAttrs.setInt(3, tTaskkey);
		 	    		int up2 = updtAccAttrs.executeUpdate();
		 	    		int value = 0;
		 	    		if(up1!=0 && up2!=0) {
		 	    			createWinTaskStmt.setInt(1, tTaskkey);
		 	    		    value =createWinTaskStmt.executeUpdate();
		 	    		}
		 	    		logger.info("No of tasks created - " + value);
		 	    		if(value!=0) {
		 	    			logger.info("Created server task for - " + tTaskkey);
		 	    			updtTransferTask.setInt(1, tTaskkey);
							logger.info("Executing - " + updtTransferTask.toString());
							int tasks=updtTransferTask.executeUpdate();
		 	    			logger.info("Transfer task updated to in progress status - " + tasks);
		 	    		} else {
		 	    			if(accountType!=null && accountType.equalsIgnoreCase("FIREFIGHTERID")) {
		 	    				outhToken = token.callBearerToken(prop);
		 	    				logger.info("Got the token before compeleting FFID related task - " + outhToken);
				    		    success=provisionTask(outhToken,tTaskkey,prop);
				 	    	}
		 	    			logger.info("Inserted record failed for FFID acc.");
		 	    		}
		 	    	}
 		    	}
		    }
		} catch(SQLException sqle) {
			sqle.printStackTrace();
			System.out.println(sqle.getMessage());
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		finally {
	    	logger.info("End Processing...\n");

			try {
				if(con!=null)
					con.close();
				if(taskStmt!=null)
					taskStmt.close();
				} catch(SQLException sqle) {
					System.out.println(sqle.getErrorCode()+sqle.getMessage());
					sqle.printStackTrace();
				}
			}
		}
	
	  
	/* Currently the below method is not used */
	public String getConnection(String outhToken,String cp2,Properties prop)
	    {
	       // String URLPATH="https://ibm-dev2.idaccesshub.com/ECM/";
	        String retvalue="failed";
	        try {
	            String url=prop.getProperty("URLPATH")+"api/v5/testConnection";
	            HttpsURLConnection con1 = null;
	            HttpURLConnection con2 = null;
	            JSONObject getConnectionResponse = null;
	            JSONObject jsonRequest = new JSONObject();
	            jsonRequest.put("connectiontype","Unix");
	            jsonRequest.put("saveconnection","Y");
	            jsonRequest.put("systemname",cp2);
	            jsonRequest.put("connectionName",cp2);
	            
	            HttpUrlConn httpConn = new HttpUrlConn();

	            // add header
	            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();

	            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
	            httpConnectionHeaders.put("Content-Type", "application/json");
	            httpConnectionHeaders.put("TRANSFERUSER", prop.getProperty("TRANSFERUSER"));
	            String urlParams = jsonRequest.toString();

	            logger.info("TRANSFEROWNERSHIP:Sending 'POST' request to URL : " + url);
	            con2 = httpConn.sendPostHttps(url, jsonRequest.toString(), httpConnectionHeaders);
	            getConnectionResponse = httpConn.fetchJsonObject(con2);

	            JSONParser parser = new JSONParser();
	            JSONObject json = (JSONObject) parser.parse(getConnectionResponse.toString());
	            String message = (String)json.get("msg");


	            logger.info("TRANSFEROWNERSHIP:Message is : "+message);
	            if(message.contains("Connection Successful"))
	            {
	              return retvalue="success";
	            }
	            else
	            {
	                logger.info("TRANSFEROWNERSHIP:Connection failed to Unix endpoint");
	                return "failed";
	            }

	        }
	        catch(Exception e)
	        {
	            e.printStackTrace();
	        }
	       
	        return retvalue; 
	    }

	   public String provisionTask(String outhToken,int taskID,Properties prop)
	    {
	      //  String URLPATH="https://ibm-dev2.idaccesshub.com/ECM/";
	        String retvalue="failed";
	        try {
	            String url=prop.getProperty("URLPATH")+"api/v5/completetask";
	            HttpsURLConnection con1 = null;
	            HttpURLConnection con2 = null;
	            JSONObject getTaskResponse = null;
	            JSONObject jsonRequest = new JSONObject();
	            jsonRequest.put("taskid",Integer.toString(taskID));
	            jsonRequest.put("provisioning","true");
	            	            
	            HttpUrlConn httpConn = new HttpUrlConn();

	            // add header
	            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();

	            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
	            httpConnectionHeaders.put("Content-Type", "application/json");
	            httpConnectionHeaders.put("TRANSFERUSER", prop.getProperty("TRANSFERUSER"));
	            String urlParams = jsonRequest.toString();

	            logger.info("TRANSFEROWNERSHIP:Sending 'POST' request to URL : " + url + "\n" +
	            					"Input: " + jsonRequest.toString());
	            con2 = httpConn.sendPostHttps(url, jsonRequest.toString(), httpConnectionHeaders);
	            getTaskResponse = httpConn.fetchJsonObject(con2);

	            JSONParser parser = new JSONParser();
	            JSONObject json = (JSONObject) parser.parse(getTaskResponse.toString());
	            String message = (String)json.get("message");
	            String errorcode=(String)json.get("errorCode");


	            logger.info("TRANSFEROWNERSHIP:Completetask method return value : "+message);
	            if(message.contains("Success"))
	            {
	              return retvalue="success";
	            }
	            else
	            {
	                logger.info("TRANSFEROWNERSHIP:completeTask method failed "+ errorcode);
	                return "failed";
	            }

	        }
	        catch(Exception e)
	        {
	            e.printStackTrace();
	        }
	       
	        return retvalue; 
	    }

}
