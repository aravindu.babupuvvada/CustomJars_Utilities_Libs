package com.SaviyntCustom.WinPasswordRotation;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.*;
import java.util.*;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.net.ssl.HttpsURLConnection;

public class PasswordRotation {

    Logger logger = null;
    Properties prop = new Properties();
    public static Connection con;
    String comments;
    String finalPubKey;

    String newPwd;
    String presentPwd;
    String presentUsername;
    String presentHost;
    String presentServiceUrl;
    String presentDomain;
    String presentChangePwdJson;
    Integer timesofrotation;

    HashMap<String,String> pwdData = new HashMap<String, String>();

    public PasswordRotation() {
        String saviyntHome = System.getenv("SAVIYNT_HOME");
        //System.setProperty("LOG_PATH", saviyntHome);
        PropertyConfigurator.configure(saviyntHome + File.separator + "customJarsLog4j.properties");
        logger = Logger.getLogger("winRotationLogger");
    }

    public static void main(String args []) {
        System.out.println("***** Win Password Rotation start *****\n");

        PasswordRotation skr = new PasswordRotation();
        if (args.length == 1) {
            String[] arguments = args[0].split(",");
            ArrayList<String> servers = new ArrayList<>();
            System.out.println("Servers - " + args[0]);

            Map map=new HashMap();
            map.put("servers", args[0]);
            skr.rotatePassword(map);
        }
        else {
            System.out.println("No argument supplied, rotating password for all servers");
            Map map=new HashMap();
            map.put("servers", "tpydalsiam309.w3-969.ibm.com|Zd3hMz9DgYpYVmNZfV7mmg==");
            skr.rotatePassword(map);

            //map.put("daysafter", "10");
            //skr.rotatePasswordDaily(map);
        }

    }
    public void rotatePasswordDaily(Map data) {
        try {
            logger.info("***** Windows Password Rotation (Daily) start *****\n");

            String daysAfter = null;

            if (data != null & data.containsKey("daysafter")) {
                daysAfter = ((String) data.get("daysafter"));

                logger.info("daysAfter - " + daysAfter);
            } else {
                daysAfter = "80";
            }

            setup();

            Map servers=new HashMap();
            String serversCsv = "";
            String giEndpointsSQL = prop.getProperty("WIN_ENDPOINTSPWDCHANGE");
            giEndpointsSQL=giEndpointsSQL.replace("${days}",daysAfter);
            PreparedStatement pstmt = con.prepareStatement(giEndpointsSQL);
            logger.info("Executing statement: " + pstmt.toString());
            ResultSet rs1 = pstmt.executeQuery();
            logger.info("Statement executed successfully");

            while(rs1.next()) {
                //logger.info("Password will be changed for - " + rs1.getString(1));
                serversCsv = serversCsv + rs1.getString(1) + ",";
            }
            serversCsv = serversCsv.substring(0, serversCsv.length() - 1);
            logger.info("Password will be changed for - " + serversCsv);

            servers.put("servers", serversCsv);
            rotatePassword(servers);

        } catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        } finally {
            logger.info("***** Windows Password Rotation (Daily) end *****\n");
        }
    }

    public void rotatePassword(Map servers) {
        logger.info("***** Windows Password Rotation start *****\n");

        if (servers != null & servers.containsKey("servers")) {
            String[] connections = ((String)servers.get("servers")).split(",");
            logger.info("Servers - " + servers.get("servers"));

            setup();

            if(connections != null && connections.length == 1
                    && connections[0].equalsIgnoreCase("ALLWINDOWS")) {
                logger.info("Key rotation will be done for all GI servers");

                //skr.beginRotation();
                specificServer(null, "ALLWINDOWS");

            } else if (connections != null && connections.length > 0){
                logger.info("Key rotation will be done for "+ servers.get("servers"));

                ArrayList<String> connArray = new ArrayList<>();
                for (String a : connections) {
                    String[] serPwd = a.split("\\|");
                    if( (serPwd.length > 1) && !serPwd[1].isEmpty() ) {
                        pwdData.put(serPwd[0], serPwd[1]);
                    } else {
                        logger.info("Password is not set or empty. Password stored in AH DB will be used.");
                    }
                    connArray.add(serPwd[0]);
                }
                specificServer(connArray, null);
            }
        } else {
            logger.info("WinPasswordRotation: No Argument set");
        }
        logger.info("***** Windows Password Rotation end *****\n");

    }

    public void setup () {
        try {
            InputStream inputStream = null;
            String saviyntHome = System.getenv("SAVIYNT_HOME");
            String propFileName = saviyntHome + File.separator + "winPwdRotationConfig.properties";
            inputStream = new FileInputStream(propFileName);
            prop.load(inputStream);

            AESCrypt aes = new AESCrypt();
            Class.forName("com.mysql.jdbc.Driver");
            String DB_URL = prop.getProperty("dburl");
            String DB_USER = prop.getProperty("dbusername");
            String DB_PASSWORD = aes.decryptIt(prop.getProperty("dbpass"));
            logger.info("Connecting to DB - " + DB_URL);
            con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            con.setAutoCommit(true);
        } catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
    }
    public void specificServer(ArrayList<String> servers, String additional) {
        AESCrypt aes = new AESCrypt();
        try {
            populateTable(prop);

            ArrayList<String> serversL = null;
            if(additional != null && additional.equalsIgnoreCase("ALLWINDOWS")) {
                serversL = new ArrayList<String>();

                String giEndpointsSQL = prop.getProperty("ALLWINDOWSENDPOINTSQUERY");
                PreparedStatement pstmt = con.prepareStatement(giEndpointsSQL);
                logger.info("Executing statement: " + pstmt.toString());
                ResultSet rs1 = pstmt.executeQuery();
                logger.info("Statement executed successfully");

                while(rs1.next()) {
                    logger.info("GI Windows server ---name--- "+rs1.getString(1));
                    serversL.add(rs1.getString(1));
                }
            }
            if(serversL == null && servers != null) {
                serversL = servers;
            }

            for (String server : serversL) {
                logger.info("Generating key for server " + server);
                genPassword(prop, server, con, aes);
                logger.info("Successfully Generated key for server " + server);
            }
            beginRotationperServer(serversL, con, prop, aes);
        }
        catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }

    }

    public void populateTable(Properties prop) throws SQLException {
        logger.info("Start table updation for new endpoints, this will enable if endpoint was enabled and disable if the endpoint was disbaled");
        HashMap<String,String> newEndpointsMap = new HashMap<String, String>();
        String newEndpointsSQL = prop.getProperty("WIN_ENDPOINTQUERY");
        try {
            PreparedStatement pstmt = con.prepareStatement(newEndpointsSQL);
            logger.info("Executing statement: " + pstmt.toString());
            ResultSet rs1 = pstmt.executeQuery();
            logger.info("Statement executed successfully");

            while(rs1.next()) {
                logger.info("Found new endpoint ---name--- "+rs1.getString(1) + " --with status--- "+rs1.getString(2));
                newEndpointsMap.put(rs1.getString(1),rs1.getString(2));
            }

            String endpointInserts = prop.getProperty("WIN_INSERTROTATIONENTRY");

            Calendar calendar = Calendar.getInstance();
            java.sql.Date updatedate = new java.sql.Date(calendar.getTime().getTime());

            for (Map.Entry<String,String> server: newEndpointsMap.entrySet()) {
                PreparedStatement preparedStatement = con.prepareStatement(endpointInserts);
                preparedStatement.setString(1, server.getKey());
                preparedStatement.setString(2, server.getValue());
                preparedStatement.setString(3, prop.getProperty("boardingpassword")); //move out
                preparedStatement.setString(4, prop.getProperty("boardingpassword"));// move out
                preparedStatement.setDate(5, null);
                preparedStatement.setDate(6, updatedate);
                preparedStatement.setDate(7, null);
                logger.info("Executing statement: " + preparedStatement.toString());
                preparedStatement.executeUpdate();
                logger.info("Statement executed successfully");
            }

            String updateEndpoints = prop.getProperty("WIN_UPDATEEPSTATUS");
            String selectAllStatus = prop.getProperty("WIN_CHECKENDPOINTSTATUS");

            PreparedStatement pstmt1 = con.prepareStatement(selectAllStatus);
            logger.info("Executing statement: " + pstmt1.toString());
            ResultSet rs2 = pstmt1.executeQuery();
            logger.info("Statement executed successfully");

            while (rs2.next()) {
                PreparedStatement preparedStatement1 = con.prepareStatement(updateEndpoints);
                preparedStatement1.setString(1,rs2.getString(2));
                preparedStatement1.setString(2,rs2.getString(1));
                logger.info("Executing statement: " + preparedStatement1.toString());
                preparedStatement1.executeUpdate();
                logger.info("Statement executed successfully");
            }
            //con.close();
        }
        catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
    }

    public void genPassword(Properties prop, String hostname, Connection con, AESCrypt aes) {
        PasswordGenerator pg = new PasswordGenerator();
        String pwd = pg.generateStrongPassword(
                Integer.parseInt(prop.getProperty("WIN_PASSWORDLEN")), prop.getProperty("WIN_PASSSPECIALCHARS"));
        logger.info("Password generated: "+pwd);

        String setpassphraseindb = prop.getProperty("WIN_UPDATENEWPWD");

        try {
            Calendar calendar = Calendar.getInstance();
            java.sql.Date updatedate = new java.sql.Date(calendar.getTime().getTime());

            PreparedStatement pstmt = con.prepareStatement(setpassphraseindb);
            pstmt = con.prepareStatement(setpassphraseindb);
            pstmt.setString(1,aes.encryptIt(pwd).trim());
            //pstmt.setDate(2,updatedate);
            pstmt.setString(2,hostname);
            logger.info("Executing statement: " + pstmt.toString());
            pstmt.executeUpdate();
            logger.info("Statement executed successfully");
        }
        catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
    }
    public boolean revertToOldPwd(String server) {
        logger.info("Inside revertToOldPwd");
        //boolean status = false;
        //Since windows is throwing error while reverting back, this method always returns true for now
        boolean status = true;
        try {
            if(changePassword(prop, server, newPwd, presentPwd)) {
                status = true;
            } else {
                logger.error("Rollback password failed for - " + server);
            }
        } catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
            comments = comments + e.getMessage() + " | ";
        } finally {
            return  status;
        }
    }

    public boolean changePassword(Properties prop, String server, String oldPassword, String newPassword) {
        boolean status=false;

        logger.info("\n***** Changing password for connection - "+server+" *****\n");

        try {
            String url=prop.getProperty("WIN_CONNMS_CHANGEPWD_URL");
            HttpUrlConn httpConn = new HttpUrlConn();

            HttpsURLConnection con1 = null;
            JSONObject getApiResponse = null;
            JSONParser parser = new JSONParser();
            JSONObject json;
            String changePwdStatus = null;

            TokenPojo token = new TokenPojo();
            String outhToken = token.callV6BearerToken(prop);

            JSONObject changePwdJsonMain = new JSONObject();
            JSONObject dataJson =new JSONObject();
            dataJson.put("userName", presentUsername);
            dataJson.put("hostName", presentHost);
            dataJson.put("serviceURL", presentServiceUrl);
            dataJson.put("domain", presentDomain);
            dataJson.put("password", oldPassword);
            dataJson.put("ChangeCredentialsJSON", presentChangePwdJson);
            dataJson.put("accountName", presentUsername);
            dataJson.put("randomPassword", newPassword);
            dataJson.put("usessl", prop.getProperty("WIN_USESSL"));
            JSONObject configDataJson =new JSONObject();
            configDataJson.put("userName", presentUsername);
            configDataJson.put("hostName", presentHost);
            configDataJson.put("serviceURL", presentServiceUrl);
            configDataJson.put("domain", presentDomain);
            configDataJson.put("password", oldPassword);
            configDataJson.put("ChangeCredentialsJSON", presentChangePwdJson);
            configDataJson.put("accountName", presentUsername);
            configDataJson.put("randomPassword", newPassword);
            configDataJson.put("usessl", prop.getProperty("WIN_USESSL"));

            changePwdJsonMain.put("data", dataJson);
            changePwdJsonMain.put("configData", configDataJson);

            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            httpConnectionHeaders.put("TENANT_ID", "DEFAULT");
            httpConnectionHeaders.put("Content-Type", "application/json");
            logger.info("changePwdJsonMain - " + changePwdJsonMain.toString());
            con1 = httpConn.sendPostHttps(url, changePwdJsonMain.toString(), httpConnectionHeaders);
            int code = con1.getResponseCode();

            getApiResponse = httpConn.fetchJsonObject(con1);
            if(getApiResponse != null) {
                json = (JSONObject) parser.parse(getApiResponse.toString());
                changePwdStatus = (String) json.get("message");
            }
            logger.info("Change Password API status : " + changePwdStatus);

            if(changePwdStatus!=null
                    && changePwdStatus.equalsIgnoreCase("success")) {
                    status = true;
            } else {
                logger.info("Change password failed for - " + server);
            }

        } catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
        finally {
            return status;
        }
    }


    public void beginRotationperServer(ArrayList<String> servers,Connection con,
                                       Properties prop,AESCrypt aes) {
        String connectionDetails = prop.getProperty("WIN_GETCONNDETAILS");
        String getPasswordDetails = prop.getProperty("WIN_GETPWD");

        String hostname = "";
        String serviceUrl = "";
        String username = "";
        String domain = "";
        String oldp = "";
        String newp = "";
        String changePwdJson = "";

        for(String server : servers) {
            comments = "";
            presentPwd = null;

            try {
                PreparedStatement pstmt = con.prepareStatement(connectionDetails);
                pstmt.setString(1,server);
                logger.info("Executing statement: " + pstmt.toString());
                ResultSet rs1 = pstmt.executeQuery();
                logger.info("Statement executed successfully");

                if (rs1.next()) {
                    domain = rs1.getString(1);
                    rs1.next();
                    hostname = rs1.getString(1);
                    rs1.next();
                    serviceUrl = rs1.getString(1);
                    rs1.next();
                    username = rs1.getString(1);
                    changePwdJson = prop.getProperty("WIN_CHANGEPWDJSON");

                    PreparedStatement pstmt1 = con.prepareStatement(getPasswordDetails);
                    pstmt1 = con.prepareStatement(getPasswordDetails);
                    pstmt1.setString(1,server);
                    logger.info("Executing statement: " + pstmt1.toString());
                    ResultSet rs2 = pstmt1.executeQuery();
                    logger.info("Statement executed successfully");
                    rs2.next();
                    oldp = aes.decryptIt(rs2.getString(1));
                    newp = aes.decryptIt(rs2.getString(2));
                    timesofrotation = Integer.parseInt(rs2.getString(3));
                    finalPubKey = null;

                    newPwd = newp;
                    if(pwdData.get(hostname) != null) {
                        presentPwd = aes.decryptItInput(pwdData.get(hostname));
                    } else {
                        presentPwd = oldp;
                    }
                    presentUsername = username;
                    presentHost = hostname;
                    presentServiceUrl = serviceUrl;
                    presentDomain = domain;
                    presentChangePwdJson = changePwdJson;

                    if (changePassword(prop, server, presentPwd, newPwd)) {
                        if(checkConnection(prop,server,con,aes)) {
                            timesofrotation++;
                            updateTableSuccess(server,timesofrotation,con);
                        } else {
                            //comments = "Error while updating connection password.";
                            logger.error("Change password failed for - " + server);
                        }
                    } else {
                        logger.error("Error while updating password using MS API.");
                        comments = "Error while updating password using MS API.";
                    }
                }
            } catch(Exception e) {
                comments = comments + e.getMessage() + " | ";
                logger.error("Exception - " +  e.getMessage(), e);
            } finally {
                if(!comments.equals("")) {
                    updateTableError(server,comments,con);
                }
            }
        }
    }

    public void updateTableSuccess(String server,Integer times,Connection con) {
        String qry = prop.getProperty("WIN_UDPATEROTATIONDETAILS");

        try {
            Calendar calendar = Calendar.getInstance();
            java.sql.Date updatedate = new java.sql.Date(calendar.getTime().getTime());
            PreparedStatement pstmt = con.prepareStatement(qry);
            pstmt = con.prepareStatement(qry);
            pstmt.setInt(1,times);
            //pstmt.setDate(2,updatedate);
            //pstmt.setDate(3,updatedate);
            pstmt.setString(2,server);
            logger.info("Executing statement: " + pstmt.toString());
            pstmt.executeUpdate();
            logger.info("Statement executed successfully");
        }
        catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
    }

    public void updateTableError(String server,String comments,Connection con) {
        String qry = prop.getProperty("WIN_UPDATECOMMENTS");

        try {
            Calendar calendar = Calendar.getInstance();
            java.sql.Date updatedate = new java.sql.Date(calendar.getTime().getTime());
            PreparedStatement pstmt = con.prepareStatement(qry);
            pstmt = con.prepareStatement(qry);
            pstmt.setString(1,comments);
            pstmt.setString(2,server);
            logger.info("Executing statement: " + pstmt.toString());
            pstmt.executeUpdate();
            logger.info("Statement executed successfully");
        }
        catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
    }

    public boolean checkConnection(Properties prop,String server,Connection con,AESCrypt aes) {
        boolean status = false;
        HashMap<String, String> templateAttrs = new HashMap<>();
        HttpUrlConn httpConn = new HttpUrlConn();
        HttpsURLConnection con1 = null;
        JSONObject getConnectionResponse = null;
        JSONObject connectionjson = null;
        String url = prop.getProperty("URLPATH") + "api/v5/testConnection";
        Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
        String newPwd = "";

        try {
            templateAttrs.put("connectiontype","WINDOWS");
            templateAttrs.put("saveconnection","Y");
            templateAttrs.put("systemname",server); // should be hostname
            templateAttrs.put("connectionName",server);
            templateAttrs.put("MSCONNECTORVERSION","WINDOWS/1.0");

            String sqlselect = prop.getProperty("WIN_GETNEWPWD");
            PreparedStatement pstmt1 = con.prepareStatement(sqlselect);
            pstmt1 = con.prepareStatement(sqlselect);
            pstmt1.setString(1,server);
            logger.info("Executing statement: " + pstmt1.toString());
            ResultSet rs2 = pstmt1.executeQuery();
            logger.info("Statement executed successfully");
            rs2.next();
            newPwd = aes.decryptIt(rs2.getString(1));
            templateAttrs.put("password",newPwd);

            Boolean retry = true;
            String connectionUpdateStatus = null;

            while (retry) {
                TokenPojo token = new TokenPojo();
                String outhToken = token.callBearerToken(prop);
                connectionjson = new JSONObject(templateAttrs);
                logger.info("connectionjson : " + connectionjson.toString());
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                con1 = httpConn.sendPostHttps(url, connectionjson.toString(), httpConnectionHeaders);
                int code = con1.getResponseCode();
                if (code != 504) {
                    retry = false;
                } else {
                    logger.info("Connection update failed with timeout error. Retrying...");
                }
                getConnectionResponse = httpConn.fetchJsonObject(con1);
                connectionUpdateStatus = null;
                if (getConnectionResponse != null) {
                    JSONParser parser1 = new JSONParser();
                    JSONObject json1 = (JSONObject) parser1.parse(getConnectionResponse.toString());
                    if (json1 != null) {
                        connectionUpdateStatus = (String) json1.get("msg");
                    }
                    logger.info("Connection update status : " + connectionUpdateStatus);
                }
            }

            if(connectionUpdateStatus!=null && connectionUpdateStatus.equalsIgnoreCase("Connection Successful")) {
                status=true;
            } else {
                logger.info("Updating connection failed for " + server + ".");
                comments = comments + "Updating connection failed. Status - " + connectionUpdateStatus + " | ";

                if(revertToOldPwd(server)) {
                    templateAttrs.put("connectiontype", "WINDOWS");
                    templateAttrs.put("saveconnection", "Y");
                    templateAttrs.put("systemname", server);
                    templateAttrs.put("connectionName", server);
                    templateAttrs.put("password", presentPwd);

                    connectionjson = new JSONObject(templateAttrs);
                    logger.info("connectionjson : " + connectionjson.toString());
                    con1 = httpConn.sendPostHttps(url, connectionjson.toString(), httpConnectionHeaders);
                    getConnectionResponse = httpConn.fetchJsonObject(con1);
                    JSONParser parser2 = new JSONParser();
                    JSONObject json2 = (JSONObject) parser2.parse(getConnectionResponse.toString());

                    String connectionRollbackStatus = (String) json2.get("msg");
                    logger.info("Connection rollback status : " + connectionRollbackStatus);
                    if (connectionRollbackStatus != null && connectionRollbackStatus.equalsIgnoreCase("Connection Successful")) {
                    } else {
                        comments = comments + "Rolling back connection failed. Status - " + connectionRollbackStatus + " | ";
                    }
                } else {
                    comments = comments + "Rolling back password failed. Connection will be in failed state. | ";
                }
            }
        }
        catch (Exception e) {
            comments = comments + "Exception occured while updating connection. Ex - " + e.getMessage() + " | ";
            logger.error("Exception - " +  e.getMessage(), e);

            if(revertToOldPwd(server)) {
                templateAttrs.put("connectiontype", "Unix");
                templateAttrs.put("saveconnection", "Y");
                templateAttrs.put("systemname", server);
                templateAttrs.put("connectionName", server);
                templateAttrs.put("password", presentPwd);

                connectionjson = new JSONObject(templateAttrs);
                logger.info("connectionjson : " + connectionjson.toString());
                con1 = httpConn.sendPostHttps(url, connectionjson.toString(), httpConnectionHeaders);
                getConnectionResponse = httpConn.fetchJsonObject(con1);
                JSONParser parser2 = new JSONParser();
                JSONObject json2 = (JSONObject) parser2.parse(getConnectionResponse.toString());

                String connectionRollbackStatus = (String) json2.get("msg");
                logger.info("Connection rollback status(E) : " + connectionRollbackStatus);
                if (connectionRollbackStatus != null && connectionRollbackStatus.equalsIgnoreCase("Connection Successful")) {
                } else {
                    comments = comments + "Rolling back connection failed. Status - " + connectionRollbackStatus + " | ";
                }
            }
        }
        finally {
            return status;
        }
    }
}
