package com.SaviyntCustom.GiAdhocTriggerChain;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.JSONObject;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

public class TokenPojo {
    private String oAuthToken = "";
    private static Logger LOGGER = null;

    public TokenPojo() {
        String saviyntHome = System.getenv("SAVIYNT_HOME");
        //System.setProperty("LOG_PATH", saviyntHome);
        PropertyConfigurator.configure(saviyntHome + File.separator + "customJarsLog4j.properties");
        LOGGER = Logger.getLogger("adhocLogger");
    }

    public String getOAuthToken() {
        return oAuthToken;
    }

    public void setOAuthToken(String token) {
        this.oAuthToken = token;
    }

    public static String decode(String strToDecrypt) {
        byte[] key = "S@v!ynt_s@V!YNt_".getBytes();
        if(strToDecrypt !=null ) {
            try {

                Cipher cipher  = Cipher.getInstance("AES/ECB/PKCS5PADDING");
                final SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
                cipher.init(Cipher.DECRYPT_MODE, secretKey);
                final String decryptedString = new String(cipher.doFinal(org.apache.commons.codec.binary.Base64.decodeBase64(strToDecrypt)));
                return decryptedString;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static void main(String[] args)
    {
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
            LOGGER.info("Saviynt code start : "+timeStamp);
            InputStream inputStream = null;
            String saviyntHome = System.getenv("SAVIYNT_HOME");
            LOGGER.info(saviyntHome);

            //load gdituserimport.properties and GDITSaviyntUserMapping.properties files
            String propFileName = saviyntHome + File.separator + "gdituserimport.properties";
            String mappingpropFileName = saviyntHome + File.separator + "GDITSaviyntUserMapping.properties";
            Properties prop = new Properties();
            inputStream = new FileInputStream(propFileName);
            prop.load(inputStream);

            //get the Saviynt oauth token to call REST APIs
            TokenPojo token = new TokenPojo();
            String outhToken = token.callBearerToken(prop);
            LOGGER.info("outhToken is "+outhToken);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }


    public String callBearerToken(Properties prop) throws Exception{
        String token = "";
        try {
            AESCrypt aes=new AESCrypt();
            HttpUrlConn httpConn = new HttpUrlConn();
            String getOAuthUrl = prop.getProperty("URLPATH") + "api/login";
            JSONObject jsonRequest = new JSONObject();
            jsonRequest.put("username", prop.getProperty("SAVUSERNAME"));
            jsonRequest.put("password", aes.decryptIt(prop.getProperty("SAVPASSWORD")));
            String urlParams = jsonRequest.toString();

            HttpURLConnection con1 = null;
            HttpURLConnection con2 = null;
            JSONObject getOAuthResponse = null;

            //con2 = httpConn.sendPostHttps(getOAuthUrl, urlParams, null);
            con1 = httpConn.sendPostHttp(getOAuthUrl, urlParams, null);
            getOAuthResponse = httpConn.fetchJsonObject(con1);


            if (getOAuthResponse!=null) {
                token = getOAuthResponse.get("access_token").toString();
                //tokenObj.setOAuthToken(token);
                LOGGER.info("Got token");

            } else {
                LOGGER.info("Could not get the token :" + getOAuthResponse);

                throw new Exception("Could not get token");

            }


        } catch (Exception e) {
            LOGGER.error("Exception: " + e.getMessage(), e);
            throw e;
        }
        return token;
    }

}

