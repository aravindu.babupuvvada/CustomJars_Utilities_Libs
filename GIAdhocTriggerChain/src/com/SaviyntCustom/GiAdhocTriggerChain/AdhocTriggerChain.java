package com.SaviyntCustom.GiAdhocTriggerChain;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.net.ssl.HttpsURLConnection;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.*;
import java.util.stream.Collectors;

public class AdhocTriggerChain {
    private static Logger LOGGER = null;

    Properties prop = new Properties();
    public static Connection con;

    public static void main(String args [])
    {
        System.out.println("***** SSH Key Rotation start *****\n");

        AdhocTriggerChain skr = new AdhocTriggerChain();
        if (args.length == 1) {
            String[] arguments = args[0].split(",");
            ArrayList<String> servers = new ArrayList<>();
            //System.out.println("Servers - " + args[0]);

            Map map=new HashMap();
            map.put("servers", "tpydalsiam304.sl.bluecloud.ibm.com");
            map.put("servertype", "windows");

            AdhocTriggerChain rot = new AdhocTriggerChain();
            rot.setAdhocTriggerChain(map);

        }
        else {
            System.out.println("No argument supplied");
        }
    }

    public AdhocTriggerChain() {
        String saviyntHome = System.getenv("SAVIYNT_HOME");
        //System.setProperty("LOG_PATH", saviyntHome);
        PropertyConfigurator.configure(saviyntHome + File.separator + "customJarsLog4j.properties");
        LOGGER = Logger.getLogger("adhocLogger");
    }

    private String join(List<String> namesList) {
        return String.join(",", namesList
                .stream()
                .map(name -> ("'" + name + "'"))
                .collect(Collectors.toList()));
    }

    public void setAdhocTriggerChain(Map input)
    {
        LOGGER.info("***** Set Adhoc trigger chain start *****\n");
        InputStream inputStream = null;

        if (input != null && (input.containsKey("servers") && input.containsKey("servertype"))) {
            String serverType = ((String)input.get("servertype")).trim();
            String servers = ((String)input.get("servers")).trim();
            String[] adhocServers = servers.split(",");
            String cp22 = null;
            LOGGER.info("Server Type - " + serverType);
            LOGGER.info("Servers - " + servers);

            if( (serverType!=null &&
                    (serverType.equalsIgnoreCase("unix")
                    || serverType.equalsIgnoreCase("windows"))
                )
                && adhocServers.length>0 ) {
                try {
                    if(serverType.equalsIgnoreCase("unix")) {
                        cp22 = "giserver";
                    } else {
                        cp22 = "giwindowsserver";
                    }
                    List<String> serversList = Arrays.asList(adhocServers);

                    String saviyntHome = System.getenv("SAVIYNT_HOME");
                    String propFileName = saviyntHome + File.separator + "SAMConfig.properties";
                    inputStream = new FileInputStream(propFileName);
                    prop.load(inputStream);

                    AESCrypt aes = new AESCrypt();
                    Class.forName("com.mysql.jdbc.Driver");
                    String DB_URL = prop.getProperty("DBURL");
                    String DB_USER = prop.getProperty("DBUSER");
                    String DB_PASSWORD = aes.decryptIt(prop.getProperty("DBPASS"));
                    con = DriverManager.getConnection( DB_URL, DB_USER, DB_PASSWORD);
                    con.setAutoCommit(true);

                    /*
                    String giEndpointsSQL = "select ENDPOINTNAME, CUSTOMPROPERTY32 from endpoints where CUSTOMPROPERTY22='giserver' " +
                            "and status=1 and endpointname in (" + join(serversList) + ");";
                    PreparedStatement pstmt = con.prepareStatement(giEndpointsSQL);
                    LOGGER.info("Executing statement: " + pstmt.toString());
                    ResultSet rs1 = pstmt.executeQuery();
                    LOGGER.info("Statement executed successfully");
                    while(rs1.next()) {
                        LOGGER.info("GI server ---name--- "+rs1.getString(1));
                    }
                    */

                    String uptQuery = prop.getProperty("GIADHOCCLEARCP");
                    uptQuery = uptQuery.replace("${servertype}", cp22);
                    PreparedStatement pstmt = con.prepareStatement(uptQuery);

                    LOGGER.info("Executing statement: " + pstmt.toString());
                    int result = pstmt.executeUpdate();
                    LOGGER.info("Statement executed successfully");
                    LOGGER.info("CP for Adhoc is cleared for : " + result + " Servers.");

                    String uptQuery1 = prop.getProperty("GIADHOCSETCP");
                    uptQuery1 = uptQuery1.replace("${servertype}", cp22);
                    uptQuery1 = uptQuery1.replace("${servers}", join(serversList));
                    PreparedStatement pstmt1 = con.prepareStatement(uptQuery1);

                    LOGGER.info("Executing statement: " + pstmt1.toString());
                    int result1 = pstmt1.executeUpdate();
                    LOGGER.info("Statement executed successfully");
                    LOGGER.info("CP for Adhoc is set for : " + result1 + " Servers.");

                    /*
                    String selectSQL = prop.getProperty("GIADHOCTRIGGERCHAINGETNAME").
                            replace("${servers}", join(serversList));
                    LOGGER.info("Executing query - " + selectSQL);
                    PreparedStatement pstmt3 = con.prepareStatement(selectSQL);
                    ResultSet rs2 = pstmt3.executeQuery();
                    LOGGER.info("Statement executed successfully");

                    String triggers = "";
                    while (rs2.next()) {
                        String trigger = rs2.getString("TRIGGER_NAME");
                        String endpoint = rs2.getString("ENDPOINTNAME");

                        LOGGER.info(endpoint + " - " + trigger);
                        triggers += trigger + ",";
                    }
                    if(!triggers.isEmpty()) {
                        String finalTriggerSet = prop.getProperty("GIADHOCTRIGGERCHAINPRE")
                                + triggers + prop.getProperty("GIADHOCTRIGGERCHAINPOST");
                        LOGGER.info("Final trigger chain - " + finalTriggerSet);

                        setTriggerChain(prop, prop.getProperty("GIADHOCTRIGGERCHAIN"), finalTriggerSet);
                    } else {
                        LOGGER.error("No recon triggers. Trigger chain is not updated");
                    }
                    */
                } catch (Exception ex) {
                    LOGGER.error("Exception: " + ex.getMessage(), ex);
                    ex.printStackTrace();
                }
            }
        }
        else {
            LOGGER.error("No Argument set");
        }
        LOGGER.info("***** Set Adhoc trigger chain end *****\n\n\n");
    }

    private String callBearerToken(Properties prop) throws Exception{
        String token = "";
        try {
            AESCrypt aes=new AESCrypt();
            HttpUrlConn httpConn = new HttpUrlConn();
            String getOAuthUrl = prop.getProperty("URLPATH") + "api/login";
            JSONObject jsonRequest = new JSONObject();
            jsonRequest.put("username", prop.getProperty("APIUSER"));
            jsonRequest.put("password", aes.decryptIt(prop.getProperty("APIUSERPWD")));
            String urlParams = jsonRequest.toString();

            HttpURLConnection con1 = null;
            HttpURLConnection con2 = null;
            JSONObject getOAuthResponse = null;
            //LOGGER.info(urlParams);

            //con2 = httpConn.sendPostHttps(getOAuthUrl, urlParams, null);
            con1 = httpConn.sendPostHttp(getOAuthUrl, urlParams, null);
            getOAuthResponse = httpConn.fetchJsonObject(con1);


            if (getOAuthResponse!=null) {
                token = getOAuthResponse.get("access_token").toString();
                //tokenObj.setOAuthToken(token);
                LOGGER.info("Got token");

            } else {
                LOGGER.error("Could not get the token :" + getOAuthResponse);
                throw new Exception("Could not get token");
            }
        } catch (Exception e) {
            LOGGER.error("Exception: " + e.getMessage(), e);
            throw e;
        }
        return token;

    }


    private  Boolean setTriggerChain(Properties prop, String triggerName, String finalSet) {
        Boolean status = false;
        try {
            LOGGER.info("Updating trigger chain : " + triggerName
                    + ".\nFinal Sequence : " + finalSet);
            String cronExp = prop.getProperty("GIADHOCTRIGGERCHAINCRON");
            String url = prop.getProperty("URLPATH") + "api/v5/createUpdateTrigger";
            HttpUrlConn httpConn = new HttpUrlConn();

            //String[] triggerSequenceList=triggerSequence.split(",");
            HttpsURLConnection con1 = null;
            JSONObject getConnectionResponse = null;
            JSONObject triggerchainjson = new JSONObject();
            JSONObject triggerchainjsonmain = new JSONObject();
            JSONArray triggerChainArr = new JSONArray();
            triggerchainjson.put("triggername", triggerName);
            triggerchainjson.put("jobname", "TriggerChainJob");
            triggerchainjson.put("jobgroup", "utility");
            triggerchainjson.put("cronexpression", cronExp);
            JSONObject triggerchainsubjson = new JSONObject();
            triggerchainsubjson.put("savtriggerorderform", finalSet);
            triggerchainsubjson.put("onFailureForm", "Continue");

            triggerchainjson.put("valueMap", triggerchainsubjson);

            triggerChainArr.add(0, triggerchainjson);

            triggerchainjsonmain.put("triggers", triggerChainArr);

            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            String token = callBearerToken(prop);
            httpConnectionHeaders.put("Authorization", "Bearer " + token);
            con1 = httpConn.sendPostHttps(url, triggerchainjsonmain.toString(), httpConnectionHeaders);

            getConnectionResponse = httpConn.fetchJsonObject(con1);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(getConnectionResponse.toString());

            String triggerchainstatus = (String) json.get("errorCode");
            String msg = (String) json.get("msg");
            LOGGER.info("Trigger chain udpate status : " + triggerchainstatus);

            if (triggerchainstatus.equals("1")) {
                LOGGER.error("Error while updating trigger chain - " + msg);
            } else {
                status = true;
            }
        } catch (Exception ex) {
            LOGGER.error("Exception: " + ex.getMessage(), ex);
            status=false;
        } finally {
            return  status;
        }
    }
}
