package com.SaviyntCustom.CustomQuery;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class ExecuteQuery {
	Logger logger = null;
	Properties prop = new Properties();
	public String outhToken;
	public static Connection con;

	public static void main(String[] args) {
		Map map=new HashMap();
		map.put("name","TEST");
		//map.put("name","SERVER_PREPROV");

	    ExecuteQuery qry = new ExecuteQuery();
		qry.execute(map);
	}
	public ExecuteQuery() {
		String saviyntHome = System.getenv("SAVIYNT_HOME");
		//System.setProperty("LOG_PATH", saviyntHome);
		PropertyConfigurator.configure(saviyntHome + File.separator + "customJarsLog4j.properties");
		logger = Logger.getLogger("customQueryLogger");
	}

	public void provServerExecute1(Map data) {
		execute(data);
	}
	public void provServerExecute2(Map data) {
		execute(data);
	}
	public void reconGIExecute1(Map data) {
		execute(data);
	}
	public void reconGIExecute2(Map data) {
		execute(data);
	}
	public void reconGIExecute3(Map data) {
		execute(data);
	}
	public void reconGIExecute4(Map data) {
		execute(data);
	}
	public void reconGIExecute5(Map data) {
		execute(data);
	}
	public void reconGIExecute6(Map data) {
		execute(data);
	}
	public void reconGIExecute7(Map data) {
		execute(data);
	}
	public void reconGIExecuteSox(Map data) {
		execute(data);
	}
	public void reconNEExecute1(Map data) {
		execute(data);
	}
	public void reconNEExecute2(Map data) {
		execute(data);
	}
	public void reconNEExecute3(Map data) {
		execute(data);
	}
	public void reconNEExecute4(Map data) {
		execute(data);
	}
	public void reconNEExecute5(Map data) {
		execute(data);
	}
	public void reconNEExecute6(Map data) {
		execute(data);
	}
	public void reconNEExecute7(Map data) {
		execute(data);
	}
	public void reconNEExecuteSox(Map data) {
		execute(data);
	}
	public void reconWindowsExecute1(Map data) {
		execute(data);
	}
	public void reconWindowsExecute2(Map data) {
		execute(data);
	}
	public void reconWindowsExecute3(Map data) {
		execute(data);
	}
	public void reconWindowsExecute4(Map data) {
		execute(data);
	}
	public void reconWindowsExecute5(Map data) {
		execute(data);
	}
	public void reconWindowsExecute6(Map data) {
		execute(data);
	}
	public void reconWindowsExecute7(Map data) {
		execute(data);
	}
	public void reconWindowsExecuteSox(Map data) {
		execute(data);
	}
	public void reconGBExecute1(Map data) {
		execute(data);
	}
	public void reconGBExecute2(Map data) {
		execute(data);
	}
	public void reconGBExecute3(Map data) {
		execute(data);
	}
	public void reconGBExecute4(Map data) {
		execute(data);
	}
	public void reconGBExecute5(Map data) {
		execute(data);
	}
	public void reconGBExecute6(Map data) {
		execute(data);
	}
	public void reconGBExecute7(Map data) {
		execute(data);
	}
	public void reconGBExecuteSox(Map data) {
		execute(data);
	}
	public void triggerChainExecute1(Map data) {
		execute(data);
	}
	public void triggerChainExecute2(Map data) {
		execute(data);
	}
	public void triggerChainExecute3(Map data) {
		execute(data);
	}
	public void triggerChainExecute4(Map data) {
		execute(data);
	}
	public void triggerChainExecute5(Map data) {
		execute(data);
	}
	public void triggerChainExecute6(Map data) {
		execute(data);
	}
	public void triggerChainExecute7(Map data) {
		execute(data);
	}
	public void triggerChainExecute8(Map data) {
		execute(data);
	}
	public void triggerChainExecute9(Map data) {
		execute(data);
	}
	public void triggerChainExecute10(Map data) {
		execute(data);
	}
	public void execute1(Map data) {
		execute(data);
	}
	public void execute2(Map data) {
		execute(data);
	}
	public void execute3(Map data) {
		execute(data);
	}
	public void execute4(Map data) {
		execute(data);
	}
	public void execute5(Map data) {
		execute(data);
	}
	public void execute6(Map data) {
		execute(data);
	}
	public void execute7(Map data) {
		execute(data);
	}
	public void execute8(Map data) {
		execute(data);
	}
	public void execute9(Map data) {
		execute(data);
	}
	public void execute10(Map data) {
		execute(data);
	}

	public void setup() {
		try {
			InputStream inputStream = null;
			String saviyntHome = System.getenv("SAVIYNT_HOME");
			String propFileName = saviyntHome + File.separator + "SAMConfig.properties";
			inputStream = new FileInputStream(propFileName);
			prop.load(inputStream);

			AESCrypt aes = new AESCrypt();
			Class.forName("com.mysql.jdbc.Driver");
			String DB_URL = prop.getProperty("DBURL");
			String DB_USER = prop.getProperty("DBUSER");
			String DB_PASSWORD = aes.decryptIt(prop.getProperty("DBPASS"));
			logger.info("Connecting to DB - " + DB_URL);
			con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
			con.setAutoCommit(false);
		} catch(Exception e) {
			logger.error("Exception - " +  e.getMessage(), e);
		}
	}

    public void execute(Map data) {
        logger.info("***** Execute Query Start *****");
		String qryName = null;
		QueryHandler qryHandler = new QueryHandler();

		try {
			setup();
			if (data != null & (data.containsKey("name"))) {
				qryName = ((String)data.get("name"));
				logger.info("qryName - " + qryName);
			}

			if(qryName != null) {
				String noQrsStr = qryName + "_No_Of_Queries";
				int noQrs = Integer.parseInt(prop.getProperty(noQrsStr));
				String execOrder = prop.getProperty(qryName + "_Execution_Order");
				if( noQrs != 0 && (execOrder != null || !execOrder.isEmpty())) {
					logger.info("Number of queries in " + qryName + " - " + noQrs);
					String[] order = execOrder.split(",");

					if(noQrs == order.length) {
						for (int i = 0; i < order.length; i++) {
							logger.info("*******************************");
							logger.info("Executing query no - " + order[i]);
							String qryTypeStr = qryName + "_" + order[i] + "_Type";
							if (prop.getProperty(qryTypeStr).equalsIgnoreCase("INSERT")) {
								qryHandler.handleInsertQry(prop, con, qryName + "_" + order[i]);
							} else if (prop.getProperty(qryTypeStr).equalsIgnoreCase("UPDATE")) {
								qryHandler.handleUpdateQry(prop, con, qryName + "_" + order[i]);
							} else if (prop.getProperty(qryTypeStr).equalsIgnoreCase("DELETE")) {
								qryHandler.handleDeleteQry(prop, con, qryName + "_" + order[i]);
							} else if (prop.getProperty(qryTypeStr).equalsIgnoreCase("AS-IS")) {
								qryHandler.handleQry(prop, con, qryName + "_" + order[i]);
							}
						}
					} else {
						logger.error("Number of queries do not match with size of exec order.");
					}
				} else {
					logger.error("Wrong configuration for " + qryName + ". Please check configurations!");
				}
			}
		} catch(Exception e) {
			logger.error("Exception - " +  e.getMessage(), e);
		}
		finally {
			try {
				if(con!=null) {
					con.close();
				}
				logger.info("***** Execute Query End *****\n\n");
			} catch(Exception e) {
				logger.error("Exception - " +  e.getMessage(), e);
			}
		}
	}
}
