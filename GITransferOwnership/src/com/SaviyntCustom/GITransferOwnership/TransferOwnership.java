package com.SaviyntCustom.GITransferOwnership;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.net.ssl.HttpsURLConnection;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class TransferOwnership {
	private Logger logger = null;

	public TransferOwnership() {
		String saviyntHome = System.getenv("SAVIYNT_HOME");
		//System.setProperty("LOG_PATH", saviyntHome);
		PropertyConfigurator.configure(saviyntHome + File.separator + "customJarsLog4j.properties");
		logger = Logger.getLogger("transferLogger");
	}

	public static void main(String args[]) {
		TransferOwnership t=new TransferOwnership();
		t.processTransfer();
	}

	private void processTransfer() {
		Properties prop = new Properties();
		InputStream inputStream = null;
		Connection con= null;

		try {
			String saviyntHome = System.getenv("SAVIYNT_HOME");
			logger.info("*************** Transfer Ownership Start *************************");

			logger.info("SAVIYNT_HOME - " + saviyntHome + "\n");
			String propFileName = saviyntHome.concat("/SAMConfig.properties");
			inputStream = new FileInputStream(propFileName);
			prop.load(inputStream);
			Class.forName("com.mysql.jdbc.Driver");
			logger.info("DBURL - " + prop.getProperty("DBURL"));
			logger.info("DBUSER - " + prop.getProperty("DBUSER"));

			logger.info("Connecting to - " + prop.getProperty("DBURL"));

			AESCrypt aes = new AESCrypt();
			String pass = aes.decryptIt(prop.getProperty("DBPASS"));
			if (con == null) {
				con = DriverManager.getConnection(prop.getProperty("DBURL"), prop.getProperty("DBUSER"), pass);
			}
			con.setAutoCommit(true);
			logger.info("DB connection successful.");

			processUnixTasks(prop, con);
			processWindowsTasks(prop, con);
		} catch (Exception e) {
			logger.error("Exception - " +  e.getMessage(), e);
		} finally {
			try {
				if(con!=null)
					con.close();
			} catch(SQLException e) {
				logger.error("Exception - " +  e.getMessage(), e);
			}
			logger.info("*************** Transfer Ownership End *************************");
		}
	}

	private void processUnixTasks(Properties prop, Connection con) {
		logger.info("-------------------- Processing UNIX tasks --------------------");

		PreparedStatement updtUserAccs = null;
		PreparedStatement updtAccAttrs = null;
		PreparedStatement updtAccEnts = null;
		PreparedStatement failup1 = null;
		PreparedStatement failup2 = null;
		PreparedStatement unixTaskStmt= null;
		PreparedStatement updtTransferTask= null;
		PreparedStatement createUnixTaskStmt= null;
		Statement transferTaskStmt= null;
		Statement targetTask= null;
		Statement origTask= null;

		ResultSet transferEndTasks= null;
		ResultSet unixDetails= null;
		ResultSet unixTasks= null;

		try {
			transferTaskStmt = con.createStatement();
			transferEndTasks = transferTaskStmt.executeQuery(prop.getProperty("GITRANSFER_SELECTTRANTASKS_UNIX"));
			targetTask = con.createStatement();
			unixTasks = targetTask.executeQuery(prop.getProperty("GITRANSFER_SERVERTASKS_UNIX"));

			unixTaskStmt = con.prepareStatement(prop.getProperty("GITRANSFER_SELECTSERVERDETAILS"));
			createUnixTaskStmt = con.prepareStatement(prop.getProperty("GITRANSFER_CREATESERVERTASKS_UNIX"));
			updtUserAccs = con.prepareStatement(prop.getProperty("GITRANSFER_UPDATEUSERACCS"));
			updtAccAttrs = con.prepareStatement(prop.getProperty("GITRANSFER_UPDATEACCS_UNIX"));
			updtTransferTask = con.prepareStatement(prop.getProperty("GITRANSFER_UPDATETRANTASKS"));
			updtAccEnts = con.prepareStatement(prop.getProperty("GITRANSFER_UPDATEAE1_UNIX"));

			String success = "";

			TokenPojo token = new TokenPojo();
			String outhToken = "";

			logger.info("Checking if there are any server tasks to be checked. ");
			while(unixTasks.next()) {
				int transferTaskid = unixTasks.getInt("parenttask");
				int serverTaskStatus = unixTasks.getInt("status");
				int serverTaskKey = unixTasks.getInt("taskkey");

				logger.info("Unix Task Key - " + serverTaskKey
						+ ", Transfer Task Key - " + transferTaskid);

				if(serverTaskStatus==3) {
					logger.info("Task is in complete state.");

					updtAccEnts.setInt(1, transferTaskid);
					updtAccEnts.setInt(2, transferTaskid);
					int too = updtAccEnts.executeUpdate();
					if(too!=0) {
						outhToken = token.callBearerToken(prop);
						logger.info("Got the token. Completing the transfer task as completed.");
						success = provisionTask(outhToken,transferTaskid,prop);
						logger.info("Transfer account ownership is completed - " + success  );
					} else {
						logger.info("Updating account failed. Transfer account ownership is NOT completed" );
					}
				}
				if(serverTaskStatus==4 || serverTaskStatus==8) {
					logger.info("Task is not in complete state. Status - " + serverTaskStatus);
					logger.info("Rolling back Account changes.");
					failup1 = con.prepareStatement(prop.getProperty("GITRANSFER_ROLLBACKACCS"));
					failup1.setInt(1, transferTaskid);
					failup1.setInt(2, transferTaskid);
					failup1.setInt(3, transferTaskid);
					int act = failup1.executeUpdate();

					logger.info("Rolling back User-Account changes.");
					failup2= con.prepareStatement(prop.getProperty("GITRANSFER_ROLLBACKUSERACCS"));
					failup2.setInt(1, transferTaskid);
					failup2.setInt(2, transferTaskid);
					int ua = failup2.executeUpdate();

					if(act!=0 && ua!=0) {
						origTask = con.createStatement();
						boolean b = origTask.execute(prop.getProperty("GITRANSFER_SETTRANTASKFAILED")
								+ transferTaskid);
						logger.info("Transfer account ownership is completed with failed status." +
								" Status of updates - " + b + " : " + act + " : " + ua);
					} else {
						logger.info("Rolling back account changes failed. Not marking transfer task as failed." +
								" Status of updates - " + act + " : " + ua);
					}
				}
			}

			logger.info("Checking if there are any Transfer tasks to be processed. ");
			while(transferEndTasks.next()) {
				int tTaskkey = transferEndTasks.getInt("taskkey");
				String winActName = transferEndTasks.getString("ENTITLEMENT_VALUEKEY");
				logger.info("Processing Transfer ownership endpoint task key - " + tTaskkey );

				unixTaskStmt.setInt(1, tTaskkey);
				unixDetails = unixTaskStmt.executeQuery();

				if(unixDetails.next()) {
					int winAccount = unixDetails.getInt("entitlementid");
					String targetEndpoint = unixDetails.getString("customproperty2");
					String accountType = unixDetails.getString("customproperty3");
					logger.info("entitlementid (Account key) - " + winAccount);
					logger.info("customproperty2 (Target endpoint) - " + targetEndpoint );
					logger.info("customproperty3 (Account type) - " + accountType );
					//	String connStatus=getConnection(outhToken,targetEndpoint,prop);

					String connStatus="success";
					if(connStatus=="success") {
						logger.info("Not checking the conncetion.");
						updtUserAccs.setInt(1, tTaskkey);
						updtUserAccs.setInt(2, tTaskkey);
						int up1 = updtUserAccs.executeUpdate();
						updtAccAttrs.setInt(1, tTaskkey);
						updtAccAttrs.setInt(2, tTaskkey);
						updtAccAttrs.setInt(3, tTaskkey);
						int up2 = updtAccAttrs.executeUpdate();
						int value = 0;
						if(up1!=0 && up2!=0) {
							createUnixTaskStmt.setInt(1, tTaskkey);
							value =createUnixTaskStmt.executeUpdate();
						}
						logger.info("No of server tasks created - " + value);
						if(value!=0) {
							logger.info("Created server task for - " + tTaskkey);
							updtTransferTask.setInt(1, tTaskkey);
							//logger.info("Executing - " + updtTransferTask.toString());
							int tasks=updtTransferTask.executeUpdate();
							logger.info("Transfer task updated to in progress status - " + tasks);
						} else {
							if(accountType!=null && accountType.equalsIgnoreCase("FIREFIGHTERID")) {
								outhToken = token.callBearerToken(prop);
								logger.info("Got the token before compeleting FFID related task. Taskkey - " + tTaskkey);
								success = provisionTask(outhToken,tTaskkey,prop);
							}
							logger.info("Inserted record failed for FFID acc.");
						}
					}
				}
			}
		} catch(Exception e) {
			logger.error("Exception - " +  e.getMessage(), e);
		}
		finally {
			logger.info("Unix tasks processing completed.");
			logger.info("------------------------------------------------------------------");
		}
	}

	private void processWindowsTasks(Properties prop, Connection con) {
		logger.info("-------------------- Processing Windows tasks --------------------");

		PreparedStatement updtUserAccs = null;
		PreparedStatement updtAccAttrs = null;
		PreparedStatement updtAccEnts = null;
		PreparedStatement failup1 = null;
		PreparedStatement failup2 = null;
		PreparedStatement winTaskStmt= null;
		PreparedStatement updtTransferTask= null;
		PreparedStatement createWinTaskStmt= null;
		Statement transferTaskStmt= null;
		Statement targetTask= null;
		Statement origTask= null;

		ResultSet transferEndTasks= null;
		ResultSet winDetails= null;
		ResultSet winTasks= null;

		try {
			transferTaskStmt = con.createStatement();
			transferEndTasks = transferTaskStmt.executeQuery(prop.getProperty("GITRANSFER_SELECTTRANTASKS_WIN"));
			targetTask = con.createStatement();
			winTasks = targetTask.executeQuery(prop.getProperty("GITRANSFER_SERVERTASKS_WIN"));

			winTaskStmt = con.prepareStatement(prop.getProperty("GITRANSFER_SELECTSERVERDETAILS"));
			createWinTaskStmt = con.prepareStatement(prop.getProperty("GITRANSFER_CREATESERVERTASKS_WIN"));
			updtUserAccs = con.prepareStatement(prop.getProperty("GITRANSFER_UPDATEUSERACCS"));
			updtAccAttrs = con.prepareStatement(prop.getProperty("GITRANSFER_UPDATEACCS_WIN"));
			updtTransferTask = con.prepareStatement(prop.getProperty("GITRANSFER_UPDATETRANTASKS"));
			updtAccEnts = con.prepareStatement(prop.getProperty("GITRANSFER_UPDATEAE1_WIN"));

			String success = "";

			TokenPojo token = new TokenPojo();
			String outhToken = "";

			logger.info("Checking if there are any server tasks to be checked. ");
			while(winTasks.next()) {
				int transferTaskid = winTasks.getInt("parenttask");
				int serverTaskStatus = winTasks.getInt("status");
				int serverTaskKey = winTasks.getInt("taskkey");

				logger.info("Win Task Key - " + serverTaskKey
						+ ", Transfer Task Key - " + transferTaskid);

				if(serverTaskStatus==3) {
					logger.info("Task is in complete state.");
					updtAccEnts.setInt(1, transferTaskid);
					updtAccEnts.setInt(2, transferTaskid);
					int too = updtAccEnts.executeUpdate();
					if(too!=0) {
						outhToken = token.callBearerToken(prop);
						logger.info("Got the token. Completing the transfer task as completed.");
						success = provisionTask(outhToken,transferTaskid,prop);
						logger.info("Transfer account ownership is completed - " + success  );
					} else {
						logger.info("Updating account failed. Transfer account ownership is NOT completed" );
					}
				}
				if(serverTaskStatus==4 || serverTaskStatus==8) {
					logger.info("Task is not in complete state. Status - " + serverTaskStatus);
					logger.info("Rolling back Account changes.");
					failup1 = con.prepareStatement(prop.getProperty("GITRANSFER_ROLLBACKACCS"));
					failup1.setInt(1, transferTaskid);
					failup1.setInt(2, transferTaskid);
					failup1.setInt(3, transferTaskid);
					int act = failup1.executeUpdate();

					logger.info("Rolling back User-Account changes.");
					failup2= con.prepareStatement(prop.getProperty("GITRANSFER_ROLLBACKUSERACCS"));
					failup2.setInt(1, transferTaskid);
					failup2.setInt(2, transferTaskid);
					int ua = failup2.executeUpdate();

					if(act!=0 && ua!=0) {
						origTask = con.createStatement();
						boolean b = origTask.execute(prop.getProperty("GITRANSFER_SETTRANTASKFAILED")
								+ transferTaskid);
						logger.info("Transfer account ownership is completed with failed status." +
								" Status of updates - " + b + " : " + act + " : " + ua);
					} else {
						logger.info("Rolling back account changes failed. Not marking transfer task as failed." +
								" Status of updates - " + act + " : " + ua);
					}
				}
			}

			logger.info("Checking if there are any Transfer tasks to be processed. ");
			while(transferEndTasks.next()) {
				int tTaskkey = transferEndTasks.getInt("taskkey");
				String winActName = transferEndTasks.getString("ENTITLEMENT_VALUEKEY");
				logger.info("Processing Transfer ownership endpoint task key - " + tTaskkey );

				winTaskStmt.setInt(1, tTaskkey);
				winDetails = winTaskStmt.executeQuery();

				if(winDetails.next()) {
					int winAccount = winDetails.getInt("entitlementid");
					String targetEndpoint = winDetails.getString("customproperty2");
					String accountType = winDetails.getString("customproperty3");
					logger.info("entitlementid (Account key) - " + winAccount);
					logger.info("customproperty2 (Target endpoint) - " + targetEndpoint );
					logger.info("customproperty3 (Account type) - " + accountType );
					//	String connStatus=getConnection(outhToken,targetEndpoint,prop);

					String connStatus="success";
					if(connStatus=="success") {
						logger.info("Not checking the conncetion.");
						updtUserAccs.setInt(1, tTaskkey);
						updtUserAccs.setInt(2, tTaskkey);
						int up1 = updtUserAccs.executeUpdate();
						updtAccAttrs.setInt(1, tTaskkey);
						updtAccAttrs.setInt(2, tTaskkey);
						updtAccAttrs.setInt(3, tTaskkey);
						int up2 = updtAccAttrs.executeUpdate();
						int value = 0;
						if(up1!=0 && up2!=0) {
							createWinTaskStmt.setInt(1, tTaskkey);
							value =createWinTaskStmt.executeUpdate();
						}
						logger.info("No of server tasks created - " + value);
						if(value!=0) {
							logger.info("Created server task for - " + tTaskkey);
							updtTransferTask.setInt(1, tTaskkey);
							//logger.info("Executing - " + updtTransferTask.toString());
							int tasks=updtTransferTask.executeUpdate();
							logger.info("Transfer task updated to in progress status - " + tasks);
						} else {
							if(accountType!=null && accountType.equalsIgnoreCase("FIREFIGHTERID")) {
								outhToken = token.callBearerToken(prop);
								logger.info("Got the token before compeleting FFID related task. Taskkey - " + tTaskkey);
								success = provisionTask(outhToken,tTaskkey,prop);
							}
							logger.info("Inserted record failed for FFID acc.");
						}
					}
				}
			}
		} catch(Exception e) {
			logger.error("Exception - " +  e.getMessage(), e);
		}
		finally {
			logger.info("Windows tasks processing completed.");
			logger.info("------------------------------------------------------------------");
		}
	}

	/* Currently the below method is not used */
	public String getConnection(String outhToken,String cp2,Properties prop)
	{
		// String URLPATH="https://ibm-dev2.idaccesshub.com/ECM/";
		String retvalue="failed";
		try {
			String url=prop.getProperty("URLPATH")+"api/v5/testConnection";
			HttpsURLConnection con1 = null;
			HttpURLConnection con2 = null;
			JSONObject getConnectionResponse = null;
			JSONObject jsonRequest = new JSONObject();
			jsonRequest.put("connectiontype","Unix");
			jsonRequest.put("saveconnection","Y");
			jsonRequest.put("systemname",cp2);
			jsonRequest.put("connectionName",cp2);

			HttpUrlConn httpConn = new HttpUrlConn();

			// add header
			Map<String, String> httpConnectionHeaders = new HashMap<String, String>();

			httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
			httpConnectionHeaders.put("Content-Type", "application/json");
			httpConnectionHeaders.put("TRANSFERUSER", prop.getProperty("TRANSFERUSER"));
			String urlParams = jsonRequest.toString();

			logger.info("Sending 'POST' request to URL : " + url);
			con2 = httpConn.sendPostHttps(url, jsonRequest.toString(), httpConnectionHeaders);
			getConnectionResponse = httpConn.fetchJsonObject(con2);

			JSONParser parser = new JSONParser();
			JSONObject json = (JSONObject) parser.parse(getConnectionResponse.toString());
			String message = (String)json.get("msg");

			logger.info("Message is : "+message);
			if(message.contains("Connection Successful")) {
				return retvalue="success";
			}
			else {
				logger.info("Connection failed to Unix endpoint");
				return "failed";
			}
		} catch(Exception e) {
			logger.error("Exception - " +  e.getMessage(), e);
		}
		return retvalue;
	}

	public String provisionTask(String outhToken,int taskID,Properties prop)
	{
		//  String URLPATH="https://ibm-dev2.idaccesshub.com/ECM/";
		String retvalue="failed";
		try {
			String url=prop.getProperty("URLPATH")+"api/v5/completetask";
			HttpsURLConnection con1 = null;
			HttpURLConnection con2 = null;
			JSONObject getTaskResponse = null;
			JSONObject jsonRequest = new JSONObject();
			jsonRequest.put("taskid",Integer.toString(taskID));
			jsonRequest.put("provisioning","true");

			HttpUrlConn httpConn = new HttpUrlConn();

			// add header
			Map<String, String> httpConnectionHeaders = new HashMap<String, String>();

			httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
			httpConnectionHeaders.put("Content-Type", "application/json");
			httpConnectionHeaders.put("TRANSFERUSER", prop.getProperty("TRANSFERUSER"));
			String urlParams = jsonRequest.toString();

			logger.info("Sending 'POST' request to URL : " + url + "\n" +
					"Input: " + jsonRequest.toString());
			con2 = httpConn.sendPostHttps(url, jsonRequest.toString(), httpConnectionHeaders);
			getTaskResponse = httpConn.fetchJsonObject(con2);

			JSONParser parser = new JSONParser();
			JSONObject json = (JSONObject) parser.parse(getTaskResponse.toString());
			String message = (String)json.get("message");
			String errorcode=(String)json.get("errorCode");


			logger.info("Completetask method return value : "+message);
			if(message.contains("Success")) {
				return retvalue="success";
			} else {
				logger.info("completeTask method failed "+ errorcode);
				return "failed";
			}

		} catch(Exception e) {
			logger.error("Exception - " +  e.getMessage(), e);
		}
		return retvalue;
	}

	public String provisionTaskNew(String outhToken,int taskID,Properties prop)
	{
		//  String URLPATH="https://ibm-dev2.idaccesshub.com/ECM/";
		String retvalue="failed";
		try {
			String url=prop.getProperty("URLPATH")+"api/v5/completetask";
			HttpsURLConnection con1 = null;
			HttpURLConnection con2 = null;
			JSONObject getTaskResponse = null;
			JSONObject jsonRequest = new JSONObject();
			JSONArray taskArr = new JSONArray();
			JSONObject taskDetails = new JSONObject();
			taskDetails.put("taskid", String.valueOf(taskID));
			taskDetails.put("provisioningComments", "Transfer Complete");
			taskArr.add(0, taskDetails);
			jsonRequest.put("taskkeytocomplete", taskArr);
			HttpUrlConn httpConn = new HttpUrlConn();

			// add header
			Map<String, String> httpConnectionHeaders = new HashMap<String, String>();

			httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
			httpConnectionHeaders.put("Content-Type", "application/json");
			httpConnectionHeaders.put("TRANSFERUSER", prop.getProperty("TRANSFERUSER"));
			String urlParams = jsonRequest.toString();

			logger.info("Sending 'POST' request to URL : " + url + "\n" +
					"Input: " + jsonRequest.toString());
			con2 = httpConn.sendPostHttps(url, jsonRequest.toString(), httpConnectionHeaders);
			getTaskResponse = httpConn.fetchJsonObject(con2);

			JSONParser parser = new JSONParser();
			JSONObject json = (JSONObject) parser.parse(getTaskResponse.toString());
			String message = (String)json.get("message");
			String errorcode=(String)json.get("errorCode");


			logger.info("Completetask method return value : "+message);
			if(message.contains("Success")) {
				return retvalue="success";
			} else {
				logger.info("completeTask method failed "+ errorcode);
				return "failed";
			}

		} catch(Exception e) {
			logger.error("Exception - " +  e.getMessage(), e);
		}
		return retvalue;
	}

}
