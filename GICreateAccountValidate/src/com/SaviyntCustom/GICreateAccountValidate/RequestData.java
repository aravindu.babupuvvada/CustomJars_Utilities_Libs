package com.SaviyntCustom.GICreateAccountValidate;

import java.util.Date;

public class RequestData {
    private String combination;
    private String accName;
    private String epName;
    private Date requestDate;
    private String stage;
    private String reqKey;
    private String reqID;
    private String raKey;
    private String aaKey;

    public String getRaKey() {
        return raKey;
    }

    public void setRaKey(String raKey) {
        this.raKey = raKey;
    }

    public String getAaKey() {
        return aaKey;
    }

    public void setAaKey(String aaKey) {
        this.aaKey = aaKey;
    }

    public String getReqID() {
        return reqID;
    }

    public void setReqID(String reqID) {
        this.reqID = reqID;
    }

    public String getReqKey() {
        return reqKey;
    }

    public void setReqKey(String reqKey) {
        this.reqKey = reqKey;
    }

        public String getCombination() {
        return combination;
    }

    public void setCombination(String combination) {
        this.combination = combination;
    }

    public String getAccName() {
        return accName;
    }

    public void setAccName(String accName) {
        this.accName = accName;
    }

    public String getEpName() {
        return epName;
    }

    public void setEpName(String epName) {
        this.epName = epName;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }



    public RequestData(String combination, String accName, String epName, Date requestDate, String stage,
                       String reqKey, String reqID, String raKey, String aaKey) {
        this.combination = combination;
        this.accName = accName;
        this.epName = epName;
        this.requestDate = requestDate;
        this.stage = stage;
        this.reqKey = reqKey;
        this.reqID = reqID;
        this.raKey = raKey;
        this.aaKey = aaKey;
    }

}
