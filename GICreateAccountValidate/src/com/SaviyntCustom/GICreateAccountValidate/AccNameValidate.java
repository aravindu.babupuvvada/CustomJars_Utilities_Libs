package com.SaviyntCustom.GICreateAccountValidate;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class AccNameValidate {

    Logger logger = null;
    Properties prop = new Properties();
    public String outhToken;
    public static Connection con;
    HashMap<String, RequestData> unixValRequests = new HashMap<>();
    HashMap<String, RequestData> unixOtherRequests = new HashMap<>();
    HashMap<String, RequestData> winValRequests = new HashMap<>();
    HashMap<String, RequestData> winOtherRequests = new HashMap<>();

    public AccNameValidate() {
        String saviyntHome = System.getenv("SAVIYNT_HOME");
        //System.setProperty("LOG_PATH", saviyntHome);
        PropertyConfigurator.configure(saviyntHome + File.separator + "customJarsLog4j.properties");
        logger = Logger.getLogger("giCreateAccountValidateLogger");
    }

    public static void main(String args []) {
        System.out.println("***** GI create account validation start *****\n");

        AccNameValidate uu = new AccNameValidate();
        uu.startValidate();

    }

    public void startValidate() {
        logger.info("***** GI create account validation start *****\n");

        try {
            setup();
            processUnixRequests();
            processWinRequests();
        } catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        } finally {
            logger.info("***** GI create account validation end *****\n");
        }
    }

    public void processWinRequests() {
        logger.info("Start - processWinRequests");

        fetchAllWinRequests();
        validateWinRequests();

        logger.info("End - processWinRequests");
    }

    public void validateWinRequests() {
        logger.info("Start - validateWinRequests");

        //logger.info(valRequests.toString());
        //logger.info(otherRequests.toString());

        /*Map<String, RequestData> valRequestsSorted = new TreeMap<>(Collections.reverseOrder());
        valRequestsSorted.putAll(valRequests);
        Map<String, RequestData> otherRequestsSorted = new TreeMap<>(Collections.reverseOrder());
        otherRequestsSorted.putAll(otherRequests);*/
        Map<String, RequestData> valRequestsSorted = new TreeMap<>(winValRequests);
        //Map<String, RequestData> otherRequestsSorted = new TreeMap<>(otherRequests);

        logger.info("Requests in prevalidation stage - " + valRequestsSorted.toString());
        for (Map.Entry<String, RequestData> req : valRequestsSorted.entrySet()) {
            String reqKeyT = req.getKey();
            String reqIdT = req.getValue().getReqID();

            if (winValRequests.containsKey(reqKeyT)) {
                //Check the new requests which are still in intial stage and reject if there are any dups
                String retVal1 = hasDupNewRequestsWin(reqKeyT);
                if (retVal1 != null && !retVal1.isEmpty()) {
                    String[] allReqs = retVal1.split(",");
                    for (int i = 0; i < allReqs.length; i++) {
                        //Reject all recent requests and retain the oldest
                        //Post reject remove the entry from the list
                        String reqId1T = winValRequests.get(allReqs[i]).getReqID();
                        String aaKey1T = winValRequests.get(allReqs[i]).getAaKey();
                        approveRejectRequest(prop, allReqs[i], reqId1T,
                                "2", prop.getProperty("REJECTCOMMENTS") + "Req ID - " + reqIdT,
                                aaKey1T);
                        logger.info("Removing the request from the list since its a dup of req - "
                                + reqKeyT +". Req Key Removed - " + allReqs[i]);
                        winValRequests.remove(allReqs[i]);
                    }
                }

                //Check all other in progress requests
                String retVal = hasDupOtherRequestsWin(reqKeyT);
                if (retVal != null && !retVal.isEmpty()) {
                    String aaKeyT = winValRequests.get(reqKeyT).getAaKey();
                    String reqId1T = winOtherRequests.get(retVal).getReqID();
                    //Reject the request and proceed with next request
                    logger.info("Removing the new request from the list since its a dup of old req - "
                            + retVal +". Req Key Removed - " + reqKeyT);
                    approveRejectRequest(prop, reqKeyT, reqIdT,
                            "2", prop.getProperty("REJECTCOMMENTS") + "Req ID - " + reqId1T,
                            aaKeyT);
                    winValRequests.remove(reqKeyT);
                }
            }
        }
        for (Map.Entry<String, RequestData> req : winValRequests.entrySet()) {
            String aaKeyT = req.getValue().getAaKey();
            approveRejectRequest(prop, req.getKey(), req.getValue().getReqID(),
                    "1", prop.getProperty("APPROVECOMMENTS"), aaKeyT);
        }

        logger.info("End - validateWinRequests");
    }

    public String hasDupNewRequestsWin(String reqKey) {
        String retVal = "";
        String combo = winValRequests.get(reqKey).getCombination();
        for (Map.Entry<String, RequestData> req : winValRequests.entrySet()) {
            if(!reqKey.equalsIgnoreCase(req.getKey())) {
                String combo1 = req.getValue().getCombination();
                if (combo.equalsIgnoreCase(combo1)) {
                    if(retVal.isEmpty()) {
                        retVal = req.getKey();
                    } else {
                        retVal = req.getKey() + "," + retVal;
                    }
                }
            }
        }
        return retVal;
    }

    public String hasDupOtherRequestsWin(String reqKey) {
        String retVal = "";
        RequestData requestData = winValRequests.get(reqKey);
        String combo = requestData.getCombination();
        for (Map.Entry<String, RequestData> req : winOtherRequests.entrySet()) {
            String combo1 = req.getValue().getCombination();
            if(combo.equalsIgnoreCase(combo1)) {
                retVal = req.getKey();
                break;
            }
        }
        return retVal;
    }

    public void fetchAllWinRequests() {

        logger.info("Start - fetchAllWinRequests");

        try {
            String query1 = prop.getProperty("GIWINCREATEACC_GETALLREQUESTS");
            PreparedStatement pst = con.prepareStatement(query1);

            logger.info("Executing statement: " + pst.toString());
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                String combo = rs.getString(1);
                String accName = rs.getString(2);
                String epName = rs.getString(3);
                Date reqDate = rs.getDate(4);
                String stage = rs.getString(5);
                String reqKey = rs.getString(6);
                String reqID = rs.getString(7);
                String raKey = rs.getString(8);
                String aaKey = rs.getString(9);

                RequestData req = new RequestData(combo, accName, epName, reqDate, stage,
                        reqKey, reqID, raKey, aaKey);
                if(stage.equalsIgnoreCase(prop.getProperty("GICREATEACC_PREVALIDATIONSTEP"))) {
                    winValRequests.put(reqKey,req);
                } else {
                    winOtherRequests.put(reqKey,req);
                }
            }
        } catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }

        logger.info("End - fetchAllWinRequests");
    }

    public void processUnixRequests() {
        logger.info("Start - processUnixRequests");

        fetchAllUnixRequests();
        validateUnixRequests();

        logger.info("End - processUnixRequests");
    }

    public void validateUnixRequests() {
        logger.info("Start - validateUnixRequests");

        //logger.info(valRequests.toString());
        //logger.info(otherRequests.toString());

        /*Map<String, RequestData> valRequestsSorted = new TreeMap<>(Collections.reverseOrder());
        valRequestsSorted.putAll(valRequests);
        Map<String, RequestData> otherRequestsSorted = new TreeMap<>(Collections.reverseOrder());
        otherRequestsSorted.putAll(otherRequests);*/
        Map<String, RequestData> valRequestsSorted = new TreeMap<>(unixValRequests);
        //Map<String, RequestData> otherRequestsSorted = new TreeMap<>(otherRequests);

        logger.info("Requests in prevalidation stage - " + valRequestsSorted.toString());
        for (Map.Entry<String, RequestData> req : valRequestsSorted.entrySet()) {
            String reqKeyT = req.getKey();
            String reqIdT = req.getValue().getReqID();
            if (unixValRequests.containsKey(reqKeyT)) {
                //Check the new requests which are still in intial stage and reject if there are any dups
                String retVal1 = hasDupNewRequestsUnix(reqKeyT);
                if (retVal1 != null && !retVal1.isEmpty()) {
                    String[] allReqs = retVal1.split(",");
                    for (int i = 0; i < allReqs.length; i++) {
                        //Reject all recent requests and retain the oldest
                        //Post reject remove the entry from the list
                        String reqId1T = unixValRequests.get(allReqs[i]).getReqID();
                        String aaKey1T = unixValRequests.get(allReqs[i]).getAaKey();
                        approveRejectRequest(prop, allReqs[i], reqId1T,
                                "2", prop.getProperty("REJECTCOMMENTS") + "Req ID - " + reqIdT,
                                aaKey1T);
                        logger.info("Removing the request from the list since its a dup of req - "
                                + reqKeyT +". Req Key Removed - " + allReqs[i]);
                        unixValRequests.remove(allReqs[i]);
                    }
                }

                //Check all other in progress requests
                String retVal = hasDupOtherRequestsUnix(reqKeyT);
                if (retVal != null && !retVal.isEmpty()) {
                    String aaKeyT = unixValRequests.get(reqKeyT).getAaKey();
                    String reqId1T = unixOtherRequests.get(retVal).getReqID();
                    //Reject the request and proceed with next request
                    logger.info("Removing the new request from the list since its a dup of old req - "
                            + retVal +". Req Key Removed - " + reqKeyT);
                    approveRejectRequest(prop, reqKeyT, reqIdT,
                            "2", prop.getProperty("REJECTCOMMENTS") + "Req ID - " + reqId1T,
                            aaKeyT);
                    unixValRequests.remove(reqKeyT);
                }
            }
        }
        for (Map.Entry<String, RequestData> req : unixValRequests.entrySet()) {
            String aaKeyT = req.getValue().getAaKey();
            approveRejectRequest(prop, req.getKey(), req.getValue().getReqID(),
                    "1", prop.getProperty("APPROVECOMMENTS"), aaKeyT);
        }

        logger.info("End - validateUnixRequests");
    }

    public String hasDupNewRequestsUnix(String reqKey) {
        String retVal = "";
        String combo = unixValRequests.get(reqKey).getCombination();
        for (Map.Entry<String, RequestData> req : unixValRequests.entrySet()) {
            if(!reqKey.equalsIgnoreCase(req.getKey())) {
                String combo1 = req.getValue().getCombination();
                if (combo.equalsIgnoreCase(combo1)) {
                    if(retVal.isEmpty()) {
                        retVal = req.getKey();
                    } else {
                        retVal = req.getKey() + "," + retVal;
                    }
                }
            }
        }
        return retVal;
    }

    public String hasDupOtherRequestsUnix(String reqKey) {
        String retVal = "";
        RequestData requestData = unixValRequests.get(reqKey);
        String combo = requestData.getCombination();
        for (Map.Entry<String, RequestData> req : unixOtherRequests.entrySet()) {
            String combo1 = req.getValue().getCombination();
            if(combo.equalsIgnoreCase(combo1)) {
                retVal = req.getKey();
                break;
            }
        }
        return retVal;
    }

    public void fetchAllUnixRequests() {

        logger.info("Start - fetchAllUnixRequests");

        try {
            String query1 = prop.getProperty("GIUNIXCREATEACC_GETALLREQUESTS");
            PreparedStatement pst = con.prepareStatement(query1);

            logger.info("Executing statement: " + pst.toString());
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                String combo = rs.getString(1);
                String accName = rs.getString(2);
                String epName = rs.getString(3);
                Date reqDate = rs.getDate(4);
                String stage = rs.getString(5);
                String reqKey = rs.getString(6);
                String reqID = rs.getString(7);
                String raKey = rs.getString(8);
                String aaKey = rs.getString(9);

                RequestData req = new RequestData(combo, accName, epName, reqDate, stage,
                                    reqKey, reqID, raKey, aaKey);
                if(stage.equalsIgnoreCase(prop.getProperty("GICREATEACC_PREVALIDATIONSTEP"))) {
                    unixValRequests.put(reqKey,req);
                } else {
                    unixOtherRequests.put(reqKey,req);
                }
            }
        } catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }

        logger.info("End - fetchAllUnixRequests");
    }

    public void setup () {
        try {
            InputStream inputStream = null;
            String saviyntHome = System.getenv("SAVIYNT_HOME");
            String propFileName = saviyntHome + File.separator + "SAMConfig.properties";
            inputStream = new FileInputStream(propFileName);
            prop.load(inputStream);

            AESCrypt aes = new AESCrypt();
            Class.forName("com.mysql.jdbc.Driver");
            String DB_URL = prop.getProperty("DBURL");
            String DB_USER = prop.getProperty("DBUSER");
            String DB_PASSWORD = aes.decryptIt(prop.getProperty("DBPASS"));
            logger.info("Connecting to DB - " + DB_URL);
            con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            con.setAutoCommit(true);
        } catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
    }

    public void getPendingRequests(Properties prop)
    {
        try {
            String url=prop.getProperty("APIURL")+"api/v5/getPendingRequests";
            HttpUrlConn httpConn = new HttpUrlConn();
            HashMap<String,String> requestsList=new HashMap<>();
            HashMap<String,String> requestorList=new HashMap<>();
            HttpURLConnection con1 = null;
            JSONObject getPendingRequestsResponse = null;
            JSONObject jsonRequest = new JSONObject();
            JSONArray reqTypeArray=new JSONArray();
            reqTypeArray.add(0,"CREATEUSER");
            jsonRequest.put("requesttype",reqTypeArray);
            jsonRequest.put("max",prop.getProperty("MAXREQUESTS"));
            jsonRequest.put("assignee",prop.getProperty("ASSIGNEE"));

            TokenPojo token = new TokenPojo();
            outhToken = token.callBearerToken(prop);

            // add header
            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();

            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            httpConnectionHeaders.put("Content-Type", "application/json");
            httpConnectionHeaders.put("SAVUSERNAME", prop.getProperty("SAVUSERNAME"));
            String urlParams = jsonRequest.toString();

            logger.info("\n ***** Reading Pending request : " + url + ". Input : " + urlParams);
            con1 = httpConn.sendPostHttps(url, jsonRequest.toString(), httpConnectionHeaders);
            getPendingRequestsResponse = httpConn.fetchJsonObject(con1);
            logger.info("getPendingRequestsResponse - " + getPendingRequestsResponse.toString());

            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(getPendingRequestsResponse.toString());

            Long totalRequests = (Long)json.get("total");
            JSONArray requestDetails = (JSONArray)json.get("results");

            if(totalRequests != null) {
                //int totalreq = Integer.parseInt(totalRequests);
                logger.info("Total pending requests " + totalRequests);

                for (int i = 0; i < totalRequests; i++) {
                    boolean process = false;
                    JSONObject req = (JSONObject) requestDetails.get(i);

                    Long requestkey = (Long) req.get("requestkey");
                    //logger.info("The requestkey is " + requestkey);
                    String requestid = (String) req.get("requestid");
                    //String endpoints = (String) req.get("endpoints");

                    requestsList.put(String.valueOf(requestkey), requestid);
                    //getRequestDetails(requestkey,requesteeusername,endpoints,outhToken,ldapEntitlements);
                }

                //processRequests(prop, requestsList);
            }

        } catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
    }

    public void approveRejectRequest(Properties prop, String reqKey, String reqID,
                                     String action, String comments, String aaKey)
    {
        try {
            String query1 = prop.getProperty("GICREATEACC_UDPATECOMMENTS");
            query1 = query1.replace("${comments}", comments);
            query1 = query1.replace("${aaapprkey}", aaKey);
            PreparedStatement pst = con.prepareStatement(query1);
            logger.info("Executing statement: " + pst.toString());
            pst.executeUpdate();
            logger.info("Statement executed successfully");

            String url=prop.getProperty("URLPATH")+"api/v5/approveRejectRequest";
            HttpUrlConn httpConn = new HttpUrlConn();
            HttpURLConnection con = null;
            JSONObject getApiResponse = null;
            String apiStatus = null;
            JSONParser parser = new JSONParser();
            JSONObject json = null;
            JSONObject jsonRequest = new JSONObject();
            jsonRequest.put("requestkey",reqKey);
            jsonRequest.put("requestid",reqID);
            jsonRequest.put("reqaction",action);
            jsonRequest.put("approver",prop.getProperty("GICREATEACC_ASSIGNEE"));
            jsonRequest.put("comments",comments);

            // add header
            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();

            TokenPojo token = new TokenPojo();
            outhToken = token.callBearerToken(prop);

            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            httpConnectionHeaders.put("Content-Type", "application/json");
            String urlParams = jsonRequest.toString();

            logger.info("\n ***** Reading Pending request : " + url + ". Input : " + urlParams);
            con = httpConn.sendPostHttps(url, jsonRequest.toString(), httpConnectionHeaders);
            int code = con.getResponseCode();
            if(code == 401) {
                refreshToken(prop);
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                con = httpConn.sendPostHttps(url, jsonRequest.toString(), httpConnectionHeaders);
            }

            getApiResponse = httpConn.fetchJsonObject(con);
            if (getApiResponse != null) {
                json = (JSONObject) parser.parse(getApiResponse.toString());
                apiStatus = (String) json.get("message");
            }
            logger.info("getApiResponse - " + getApiResponse.toString());
            logger.info("API status : " + apiStatus);

            if ((json != null)
                    && (apiStatus != null && apiStatus.equalsIgnoreCase("SUCCESS"))) {
                logger.info("Calling API to approve/reject is successful. Req ID - " + reqID
                            + ". Action - " + action);
            } else {
                logger.info("Calling API to approve/reject failed. Req ID - " + reqID
                        + ". Action - " + action);
            }
        } catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
    }


    public void refreshToken(Properties prop) {
        try {
            TokenPojo token = new TokenPojo();
            outhToken = token.callBearerToken(prop);
        } catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
    }

    String getKey (HashMap<String,String> dynAttrsMapping, String value) {
        for (Map.Entry<String,String> entry : dynAttrsMapping.entrySet()) {
            if (Objects.equals(value, entry.getValue())) {
                return entry.getKey();
            }
        }
        return  null;
    }

    public boolean compareAttrDetails (HashMap<String, String> ahReqData, HashMap<String, String> bpData) {
        logger.info("Inside compareUserDetails.");
        boolean status = true;

        try {
            for (Map.Entry<String, String> bpUserAtt : bpData.entrySet()) {
                String ahUserAtt = ahReqData.get(bpUserAtt.getKey());
                logger.info("Attr = " + bpUserAtt.getKey() + ", BP data = " + bpUserAtt.getValue() +
                        ", AH req data - " + ahUserAtt);

                if(bpUserAtt.getKey().equalsIgnoreCase(prop.getProperty("USERCREATE_MANAGERATTR"))) {
                    if(!bpUserAtt.getValue().contains(ahUserAtt)) {
                        status = false;
                        break;
                    }
                } else if(!ahUserAtt.equalsIgnoreCase(bpUserAtt.getValue())) {
                    status = false;
                    break;
                }
            }
        }
        catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
            status = false;
        } finally {
            return status;
        }
    }
}
