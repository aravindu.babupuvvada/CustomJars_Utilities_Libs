package com.SaviyntCustom.ServerOnboardingGI;
import java.time.Duration;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class MethodPollerTest<T> {

    Duration pollDurationSec;
    Long pollIntervalMillis;

    private Supplier<T> pollMethod = null;
    private Predicate<T> pollResultPredicate = null;

    public MethodPollerTest() {
    }

    public MethodPollerTest<T> poll(Duration pollDurationSec, Long pollIntervalMillis) {
        this.pollDurationSec = pollDurationSec;
        this.pollIntervalMillis = pollIntervalMillis;
        return this;
    }

    public MethodPollerTest<T> method(Supplier<T> supplier) {
        pollMethod = supplier;
        return this;
    }

    public MethodPollerTest<T> until(Predicate<T> predicate) {
        pollResultPredicate = predicate;
        return this;
    }

    public T execute()
    {
        // TODO: Validate that poll, method and until have been called.

        T result = null;
        long start = System.currentTimeMillis();
        long last = start;
        boolean pollSucceeded = false;
        // TODO: Add check on poll duration
        // TODO: Use poll interval

        while (!pollSucceeded) {
            result = pollMethod.get();
            pollSucceeded = pollResultPredicate.test(result);

            if (!pollSucceeded) {
                long current = System.currentTimeMillis();
                if (Math.abs(last - current)/1000 >= pollDurationSec.getSeconds()) {
                    break;
                } else {
                    try {
                        Thread.sleep(pollIntervalMillis);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return result;
    }
}