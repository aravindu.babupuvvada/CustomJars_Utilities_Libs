package com.accesshub.job;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Reconcilation {
	private static Logger LOGGER = null;

	public static void main(String[] args) {
		Map map=new HashMap();  
	    map.put("sox","no");
	    map.put("reconday","adhoc");
	    
	    Reconcilation sync = new Reconcilation();
	    sync.sync(map);
		
        System.out.println("***** In MAIN *****\n");
	
	}
	public Reconcilation() {
		String saviyntHome = System.getenv("SAVIYNT_HOME");
		//System.setProperty("LOG_PATH", saviyntHome);
		PropertyConfigurator.configure(saviyntHome + File.separator + "customJarsLog4j.properties");
		LOGGER = Logger.getLogger("oobsyncLogger");
	}

    public void sync(Map data) {
        LOGGER.info("***** Validate and Sync Start *****");
        
		String reconAccAndAccAttrsQry = null;
		String deleteAccHistQry = null;
		String accHistoryQry = null;
       
		Connection con=null;
		
		Statement actHistStmt=null;
		ResultSet actHRs=null;

		Statement accAndAttrStmt = null;
		ResultSet reconAccAndAccAttrsData = null;
		
		Statement actHistDelStmt=null;

		PreparedStatement arsStmt=null;
		PreparedStatement actUpdate=null;
		PreparedStatement actStatusUpd=null;

		InputStream inputStream = null;
		
		try{
			String sox = null;
			String reconday = null;
			
	        if (data != null & (data.containsKey("sox") || data.containsKey("reconday"))) {
	            sox = ((String)data.get("sox"));
	            reconday = ((String)data.get("reconday"));
	            
	            LOGGER.info("sox - " + sox);
	            LOGGER.info("reconday - " + reconday);
	
	        }
	        
			/*Date today = Calendar.getInstance().getTime();
	    	
			Calendar c = Calendar.getInstance();
			c.setTime(today);
			int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);*/
			
	    	Properties prop = new Properties();	        
	    	String saviyntHome = System.getenv("SAVIYNT_HOME");
	    	LOGGER.info("Saviynt ---> " + saviyntHome);
	    	String propFileName = saviyntHome.concat("/externalconfig.properties");
	    	LOGGER.info("Found the configs.");
	    	inputStream = new FileInputStream(propFileName);
	        prop.load(inputStream);
			Class.forName("com.mysql.jdbc.Driver");  
		    LOGGER.info("DBURL - " + prop.getProperty("DBURL"));
			LOGGER.info("DBUSER - " + prop.getProperty("DBUSER"));
			
			AESCrypt aes = new AESCrypt();
			String pass=aes.decryptIt(prop.getProperty("DBPASS"));
		    //LOGGER.info("pass"+pass+"\n");
			if(con==null) {
				con=DriverManager.getConnection("jdbc:mysql://"+prop.getProperty("DBURL"),prop.getProperty("DBUSER"),pass);
			}
	        
	        String conditionQuery = null;
	        if (sox != null && sox.equalsIgnoreCase("yes")) {
	        	conditionQuery = prop.getProperty("SOXCONDQUERY");
	        } else if ((sox != null && sox.equalsIgnoreCase("no")) 
	        		&& (reconday != null && !reconday.isEmpty())) {
	        	if(reconday.equalsIgnoreCase("adhoc")) {
					conditionQuery = prop.getProperty("ADHOCCONDQUERY");
				} else {
					conditionQuery = prop.getProperty("NONSOXCONDQUERY");
					conditionQuery = conditionQuery.replace("${reconday}", "'" + reconday + "'");
				}
	        }
	        
	        if(conditionQuery != null && !conditionQuery.isEmpty()) {
	        	
	        	reconAccAndAccAttrsQry = 
	        			"select a.accountkey,a.name,a.status,a.customproperty26,a.endpointkey,a.accounttype,ep.SECURITYSYSTEMKEY,ua.USERKEY,aa.attribute_value " +
	        			"from endpoints ep " +
	        			"join accounts a on a.endpointkey=ep.endpointkey " +
	        			"join account_attributes aa on aa.accountkey=a.accountkey and aa.attribute_name='comments' " +
	        			"join user_accounts ua on a.accountkey=ua.accountkey " +
	        			"where ep.endpointkey in " +
	        			"(select ep.endpointkey from CONFIGURATION c " +
	        			"JOIN endpoints ep ON substring_index(c.name,'_',1)=ep.endpointkey AND ep.customproperty22 = 'giserver' " +
	        			"where  C.NAME LIKE '%_OUTOFBANDACTION' AND C.CONFIGDATA IS NOT NULL AND C.CONFIGDATA != '' " +
	        			"and " + conditionQuery + " and ep.status=1) " +
	        			"order by a.accountkey;";
	        	deleteAccHistQry = 
	        			"delete from accounts_history_temp where endpointkey in " +
	        			"(select ep.endpointkey from CONFIGURATION c " +
	        			"JOIN endpoints ep ON substring_index(c.name,'_',1)=ep.endpointkey AND ep.customproperty22 = 'giserver' " +
	        			"where  C.NAME LIKE '%_OUTOFBANDACTION' AND C.CONFIGDATA IS NOT NULL AND C.CONFIGDATA != '' " +
	        			"and " + conditionQuery + " and ep.status=1);";
	        	accHistoryQry = 
	        			"select accountkey,name,status,customproperty26 from accounts_history_temp " +
	        			"where endpointkey in " +
	        			"(select ep.endpointkey from CONFIGURATION c " +
	        			"JOIN endpoints ep ON substring_index(c.name,'_',1)=ep.endpointkey AND ep.customproperty22 = 'giserver' " +
	        			"where  C.NAME LIKE '%_OUTOFBANDACTION' AND C.CONFIGDATA IS NOT NULL AND C.CONFIGDATA != '' " +
	        			"and " + conditionQuery + " and ep.status=1) " +
	    			    "order by accountkey;";
	        	
				LOGGER.info("Connection Established to - " + prop.getProperty("DBURL"));
				LOGGER.info("reconAccAndAccAttrsQry - " + reconAccAndAccAttrsQry);
				LOGGER.info("deleteAccHistQry - " + deleteAccHistQry);
				LOGGER.info("accHistoryQry - " + accHistoryQry);

				accAndAttrStmt=con.createStatement();
			    reconAccAndAccAttrsData=accAndAttrStmt.executeQuery(reconAccAndAccAttrsQry);
	
			    actHistStmt=con.createStatement();
			    actHRs=actHistStmt.executeQuery(accHistoryQry);
			    
			    arsStmt=con.prepareStatement("INSERT INTO arstasks (`ACCOUNTKEY`,`ACCOUNTNAME`,`COMMENTS`,`ENDPOINT`,`OWNERTYPE`,"
			    				+ " `OWNERKEY`,`SECURITYSYSTEM`,`SOURCE`,`STATUS`,`TASKDATE`,`TASKTYPE`,"
			    		        + "`UPADTEUSER`,`USERKEY`) VALUES(?,?,?,?,1,1,?,'OOB',1,now(),?,1,?)");
			    
			    actUpdate=con.prepareStatement("update accounts set customproperty45='Yes' where accountkey=?");
			    actStatusUpd=con.prepareStatement("update accounts set customproperty39=? where accountkey=?");
			    
			    /* --the below code is to check in account_attributes table ---  */ 
			    while(reconAccAndAccAttrsData.next()) {
					int accountkey=reconAccAndAccAttrsData.getInt(1); 
					String name=reconAccAndAccAttrsData.getString(2);
					String status=reconAccAndAccAttrsData.getString(3);
					String gecos=reconAccAndAccAttrsData.getString(4);
					int endpointkey=reconAccAndAccAttrsData.getInt(5);
					String accounttype=reconAccAndAccAttrsData.getString(6);
					int sskey=reconAccAndAccAttrsData.getInt(7);
					int userkey=reconAccAndAccAttrsData.getInt(8);
					String reconciledGecos=reconAccAndAccAttrsData.getString(9);
					
					 if((status.equalsIgnoreCase("1") || status.equalsIgnoreCase("Active")) 
							 && (null!=reconciledGecos && !reconciledGecos.equalsIgnoreCase(gecos))) {
						 LOGGER.info("GECOS mismatch found. Account - " + accountkey);
						 LOGGER.info("Reconciled GECOS - " + reconciledGecos);
						 LOGGER.info("Accounts GECOS - " + gecos);
						 
						 actUpdate.setInt(1, accountkey);
						 arsStmt.setInt(1, accountkey);
						 arsStmt.setString(2, name);
						 arsStmt.setString(3, "Created by sync job for GECOS mismatch");
						 arsStmt.setInt(4, endpointkey);
						 arsStmt.setInt(5, sskey);
						 arsStmt.setInt(6, 12);
						 arsStmt.setInt(7, userkey);
					     int arows=actUpdate.executeUpdate();
						 int rows=arsStmt.executeUpdate();
						 LOGGER.info("ARows updated="+arows+" Rows updated="+rows+"\n");
					 }
					
					 while(actHRs.next()) {
						 if(accountkey==actHRs.getInt(1)) {
							if((status.equalsIgnoreCase("1") || status.equalsIgnoreCase("Active")) 
									&& (null!=accounttype && (accounttype.equalsIgnoreCase("Personal") || accounttype.equalsIgnoreCase("System"))) ) {
								 if((actHRs.getString(3)).equalsIgnoreCase("InActive") || (actHRs.getString(3)).equalsIgnoreCase("0") 
										|| (actHRs.getString(3)).equalsIgnoreCase("2")) {
									LOGGER.info("Status mismatch found. Account - " + accountkey);
									LOGGER.info("Reconciled Status - " + status);
									LOGGER.info("Account Status - " + actHRs.getString(3));
									
									arsStmt.setInt(1, accountkey);
									arsStmt.setString(2, name);
									arsStmt.setString(3, "Created by sync job for STATUS mismatch");
									arsStmt.setInt(4, endpointkey);
									arsStmt.setInt(5, sskey);
									arsStmt.setInt(6, 14);
									arsStmt.setInt(7, userkey);
									
									int rows=arsStmt.executeUpdate();
									LOGGER.info("Rows updated - "+rows+"\n");
								 }
							}
				    		
							/*if((status.equalsIgnoreCase("1") || status.equalsIgnoreCase("Active")) 
									&& (null!=accounttype && accounttype.equalsIgnoreCase("System")) ) {
								if((actHRs.getString(3)).equalsIgnoreCase("InActive") || (actHRs.getString(3)).equalsIgnoreCase("0") 
										|| (actHRs.getString(3)).equalsIgnoreCase("2") ) { 
									Date objDate = new Date();
									String strDateFormat = "dd-MMM-yyyy"; 
									SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat);
									
									System.out.println("");
									LOGGER.info("System account - " + accountkey 
											+  " activated on = " + objSDF.format(objDate));
									
									actStatusUpd.setString(1,"Enabled on:"+objSDF.format(objDate));
									actStatusUpd.setInt(2,accountkey);
									int srows=actStatusUpd.executeUpdate();
									LOGGER.info("ARows updated="+srows+"\n");
							     }
							}*/
				    		
				    		if((status.equalsIgnoreCase("2") || status.equalsIgnoreCase("InActive") || status.equalsIgnoreCase("0")) 
				    				&& (null!=accounttype && (accounttype.equalsIgnoreCase("Personal") || accounttype.equalsIgnoreCase("System"))) ) {
				    			if((actHRs.getString(3)).equalsIgnoreCase("Active") || (actHRs.getString(3)).equalsIgnoreCase("1")) { 
									Date objDate = new Date();
									String strDateFormat = "dd-MMM-yyyy"; 
									SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat);

									LOGGER.info("Account - " + accountkey
											+  " deactivated on - " + objSDF.format(objDate));
									
									actStatusUpd.setString(1,"Disabled on:"+objSDF.format(objDate));
									
									actStatusUpd.setInt(2,accountkey);
									int srows=actStatusUpd.executeUpdate();
									LOGGER.info("ARows updated - "+srows+"\n");
								}
			    			}
				    	}
				    }
				    actHRs.first();
				    //LOGGER.info("Checking........" ); 
				}
			    //Delete the processed records
			    actHistDelStmt=con.createStatement();
			    actHistDelStmt.executeUpdate(deleteAccHistQry);
			    LOGGER.info("Deleted processed records from Account History table" );
			    
	        }
		} catch(Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage(), e);
		}
		finally {
			try {
				if(con!=null)		
					con.close();
				if(actHistStmt!=null)
					actHistStmt.close();
				if(accAndAttrStmt!=null)
					accAndAttrStmt.close();
				if(actHRs!=null)
					actHRs.close();
				if(arsStmt!=null)
					arsStmt.close();
				if(actUpdate!=null)
					actUpdate.close();
				LOGGER.info("***** Validate and Sync End *****\n\n");
			} catch(SQLException sqle) {
				LOGGER.error(sqle.getErrorCode() + " - " + sqle.getMessage(), sqle);
				sqle.printStackTrace();
			}
		}
	}
}
