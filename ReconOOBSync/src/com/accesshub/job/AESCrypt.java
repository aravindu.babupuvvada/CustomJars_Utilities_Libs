package com.accesshub.job;


import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

/**
 * Created by Saviynt on 3/6/2017.
 */
public class AESCrypt {

    private static final String ALGORITHM = "AES";
    private static final String KEY = "thrinathtransfer";

    
    public static <encryptedValue64> String encryptIt(String value) {
        Key key = null;
        try {
            key = generateKey();
            Cipher cipher = Cipher.getInstance(AESCrypt.ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte [] encryptedByteValue = cipher.doFinal(value.getBytes("utf-8"));
			String encryptedValue64 = new Base64().encodeToString(encryptedByteValue);
            return encryptedValue64;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String decryptIt(String value) {
        try {
            Key key = generateKey();
            Cipher cipher = Cipher.getInstance(AESCrypt.ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte [] decryptedValue64 = new Base64().decode(value);
            byte [] decryptedByteValue = cipher.doFinal(decryptedValue64);
            String decryptedValue = new String(decryptedByteValue,"utf-8");
            return decryptedValue;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("error in getting the string message "+e.toString());
            
            return null;
        }
    }

    private static Key generateKey() throws Exception {
        Key key = new SecretKeySpec(AESCrypt.KEY.getBytes(), AESCrypt.ALGORITHM);
        return key;
    }

 /* public static void main (String args[]) {
   // public static void getEncryptedValue(String a) {
      //  String a = encryptIt("ezuwJsSe3");

        String b = decryptIt("qxNs3IEAeNibfW7gJg5mv5swn9rWczCXcHZkduj++7M=");
      //  System.out.println(a);
        System.out.println(b);
    }
   */ 
    
}
