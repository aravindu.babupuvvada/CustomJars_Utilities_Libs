package com.SaviyntCustom.SAMTransferAccounts;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashSet;

public class ExcelWriter {

    public void writeExcel(HashSet<Account> listAccs, HashSet<Group> listGrps, String excelFilePath) throws IOException {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheetA = workbook.createSheet("Accounts");
        createAccountsHeaderRow (sheetA);

        if(listAccs!=null && !listAccs.isEmpty()) {
            int rowCount = 1;

            for (Account acc : listAccs) {
                Row row = sheetA.createRow(rowCount++);
                writeAccount(acc, row, sheetA);
            }
        }
        Sheet sheetG = workbook.createSheet("Groups");
        createGroupsHeaderRow (sheetG);

        if(listGrps!=null && !listGrps.isEmpty()) {

            int rowCount = 1;

            for (Group grp : listGrps) {
                Row row = sheetG.createRow(rowCount++);
                writeGroup(grp, row, sheetG);
            }
        }

        try (FileOutputStream outputStream = new FileOutputStream(excelFilePath)) {
            workbook.write(outputStream);
        }
    }

    private void createAccountsHeaderRow(Sheet sheet) {

        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        Font font = sheet.getWorkbook().createFont();
        font.setBold(true);
        font.setFontHeightInPoints((short) 12);
        cellStyle.setFont(font);
        cellStyle.setBorderBottom(BorderStyle.THIN);
        cellStyle.setBorderLeft(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.THIN);
        cellStyle.setBorderTop(BorderStyle.THIN);

        Row row = sheet.createRow(0);
        Cell cellAccName = row.createCell(1);

        cellAccName.setCellStyle(cellStyle);
        cellAccName.setCellValue("Account Name");

        Cell cellServer = row.createCell(2);
        cellServer.setCellStyle(cellStyle);
        cellServer.setCellValue("Server");

        Cell cellAccStatus = row.createCell(3);
        cellAccStatus.setCellStyle(cellStyle);
        cellAccStatus.setCellValue("Account Status");

        Cell cellcurrOwner = row.createCell(4);
        cellcurrOwner.setCellStyle(cellStyle);
        cellcurrOwner.setCellValue("Current Owner");
    }

    private void createGroupsHeaderRow(Sheet sheet) {

        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        Font font = sheet.getWorkbook().createFont();
        font.setBold(true);
        font.setFontHeightInPoints((short) 12);
        cellStyle.setFont(font);
        cellStyle.setBorderBottom(BorderStyle.THIN);
        cellStyle.setBorderLeft(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.THIN);
        cellStyle.setBorderTop(BorderStyle.THIN);

        Row row = sheet.createRow(0);
        Cell cellAccName = row.createCell(1);

        cellAccName.setCellStyle(cellStyle);
        cellAccName.setCellValue("Group Name");

        Cell cellServer = row.createCell(2);
        cellServer.setCellStyle(cellStyle);
        cellServer.setCellValue("Server");

        Cell cellcurrOwner = row.createCell(3);
        cellcurrOwner.setCellStyle(cellStyle);
        cellcurrOwner.setCellValue("Current Owner");
    }

    private void writeAccount(Account acc, Row row, Sheet sheet) {
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        Font font = sheet.getWorkbook().createFont();
        font.setFontHeightInPoints((short) 11);
        cellStyle.setFont(font);
        cellStyle.setBorderBottom(BorderStyle.THIN);
        cellStyle.setBorderLeft(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.THIN);
        cellStyle.setBorderTop(BorderStyle.THIN);

        Cell cell = row.createCell(1);
        cell.setCellValue(acc.getAccName());
        cell.setCellStyle(cellStyle);

        cell = row.createCell(2);
        cell.setCellValue(acc.getEndpoint());
        cell.setCellStyle(cellStyle);

        cell = row.createCell(3);
        cell.setCellValue(acc.getAccStatus());
        cell.setCellStyle(cellStyle);

        cell = row.createCell(4);
        cell.setCellValue(acc.getCurrOwner());
        cell.setCellStyle(cellStyle);
    }

    private void writeGroup(Group grp, Row row, Sheet sheet) {
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        Font font = sheet.getWorkbook().createFont();
        font.setFontHeightInPoints((short) 11);
        cellStyle.setFont(font);
        cellStyle.setBorderBottom(BorderStyle.THIN);
        cellStyle.setBorderLeft(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.THIN);
        cellStyle.setBorderTop(BorderStyle.THIN);

        Cell cell = row.createCell(1);
        cell.setCellValue(grp.getGrpName());
        cell.setCellStyle(cellStyle);

        cell = row.createCell(2);
        cell.setCellValue(grp.getEndpoint());
        cell.setCellStyle(cellStyle);

        cell = row.createCell(3);
        cell.setCellValue(grp.getCurrOwner());
        cell.setCellStyle(cellStyle);

    }
}
