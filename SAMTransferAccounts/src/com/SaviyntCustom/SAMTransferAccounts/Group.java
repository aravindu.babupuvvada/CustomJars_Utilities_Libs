package com.SaviyntCustom.SAMTransferAccounts;

public class Group {
    private String grpName;
    private String entValKey;
    private String endpointKey;
    private String endpoint;
    private String currOwner;

    public Group() {
    }

    public Group(String grpName, String entValKey, String endpointKey, String endpoint, String currOwner) {
        this.grpName = grpName;
        this.entValKey = entValKey;
        this.endpointKey = endpointKey;
        this.endpoint = endpoint;
        this.currOwner = currOwner;
    }

    public String getGrpName() {
        return grpName;
    }

    public void setGrpName(String grpName) {
        this.grpName = grpName;
    }

    public String getEntValKey() {
        return entValKey;
    }

    public void setEntValKey(String entValKey) {
        this.entValKey = entValKey;
    }

    public String getEndpointKey() {
        return endpointKey;
    }

    public void setEndpointKey(String endpointKey) {
        this.endpointKey = endpointKey;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getCurrOwner() {
        return currOwner;
    }

    public void setCurrOwner(String currOwner) {
        this.currOwner = currOwner;
    }
// getters and setters
}