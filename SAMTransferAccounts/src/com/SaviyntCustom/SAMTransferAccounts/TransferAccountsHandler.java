package com.SaviyntCustom.SAMTransferAccounts;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class TransferAccountsHandler {
    public Connection con;
    Logger logger = null;
    public String outhToken;

    public HashMap<String, String> errInputFiles = new HashMap<>();
    public HashMap<String, String> errInputLines = new HashMap<>();

    public TransferAccountsHandler() {
        String saviyntHome = System.getenv("SAVIYNT_HOME");
        //System.setProperty("LOG_PATH", saviyntHome);
        PropertyConfigurator.configure(saviyntHome + File.separator + "customJarsLog4j.properties");
        logger = org.apache.log4j.Logger.getLogger("transferAccsLogger");
    }

    public void processTransferRequests(Properties prop, HashMap<String,String> requestsList,
                                        HashMap<String,String> requestorList, String outhTokenIn) throws IOException, SQLException {
        // update sql
        ResultSet rs3 = null;
        String selectSQL = "";
        HashSet<String> transferRequests = new HashSet<String>();
        outhToken = outhTokenIn;
        HashMap<Integer,TransferAccountsStatusPojo> transferAccObkeys=new HashMap<>();

        try {
            Class.forName("com.mysql.jdbc.Driver");
            String DB_URL = prop.getProperty("DBURL");
            String DB_USER = prop.getProperty("DBUSER");
            String DB_PASSWORD = prop.getProperty("DBPASS");
            //String JDBC_DRIVER = prop.getProperty("JDBC_DRIVER");
            logger.info("Selected schema: " + DB_URL);
            con = DriverManager.getConnection(DB_URL, DB_USER, (new AESCrypt()).decryptIt(DB_PASSWORD.trim()));
            con.setAutoCommit(true);

            String pendingRequests = requestsList.keySet().stream().map(Object::toString).collect(Collectors.joining(","));
            HashMap<String, ArsRequestAttachmentPojo> pendingTransferRequestkeys = new HashMap<>();
            //HashMap<String,ServerOnboardingStatusPojo> processedRequestkeys=new HashMap<>();
            HashMap<String, String> serverNames = new HashMap<>();
            HashMap<String, String> boarded = new HashMap<>();
            ArsRequestAttachmentPojo reqOb = null;

            if (pendingRequests != null && !pendingRequests.equals("")) {
                String[] bbRequests = pendingRequests.split(",");
                for (int i = 0; i < bbRequests.length; i++) {
                    reqOb = new ArsRequestAttachmentPojo();
                    reqOb.requestkey = bbRequests[i];
                    reqOb.requestID = requestsList.get(bbRequests[i]);
                    reqOb.requestor = requestorList.get(bbRequests[i]);
                    pendingTransferRequestkeys.put(bbRequests[i], reqOb);
                    if (!transferRequests.contains(bbRequests[i])) {
                        transferRequests.add(bbRequests[i]);
                    }
                }
                selectSQL = "SELECT ars_requestkey,fileattached FROM arsrequest_attachments " +
                        "WHERE ars_requestkey in (" + pendingRequests + ") order by arsrequest_attachmentskey desc";
                logger.info("Executing query - " + selectSQL);
                Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                        ResultSet.CONCUR_UPDATABLE);
                rs3 = stmt.executeQuery(selectSQL);

                logger.info("Statement executed successfully");

                while (rs3.next()) {
                    String[] blobdataList = null;
                    //String[] blobdataOwnerList = null;
                    java.sql.Blob myBlob = rs3.getBlob("fileattached");
                    String ars_requestkey = rs3.getString("ars_requestkey");

                    if (myBlob != null) {
                        InputStream input = myBlob.getBinaryStream();
                        String blobdata = new BufferedReader(new InputStreamReader(input)).lines().collect(java.util.stream.Collectors.joining("\n"));
                        String[] blobdataList1 = blobdata.split("\n");
                        if (blobdataList1.length > 0) {
                            if (blobdataList1[0].contains(prop.getProperty("SAMTRANSFER_IDENTITYFILE_KEYWORD"))) {
                                blobdataList = blobdataList1;
                            }
                        }
                    }

                    //if (blobdataList != null || blobdataOwnerList != null)
                    if (blobdataList != null) {
                        if (!pendingTransferRequestkeys.containsKey(ars_requestkey)) {
                            reqOb = new ArsRequestAttachmentPojo();
                            reqOb.requestkey = ars_requestkey;
                            pendingTransferRequestkeys.put(ars_requestkey, reqOb);
                        } else {
                            reqOb = pendingTransferRequestkeys.get(ars_requestkey);
                        }
                        if (blobdataList != null) {
                            reqOb.blobdataList = blobdataList;
                        }
                        /*if (blobdataOwnerList != null) {
                            reqOb.blobdataOwnerList = blobdataOwnerList;
                        }*/
                    }
                }
            }

            for (Map.Entry<String, ArsRequestAttachmentPojo> entry : pendingTransferRequestkeys.entrySet()) {
                String requestkey = entry.getKey();
                boolean reqProceed = false;
                ArsRequestAttachmentPojo transferAccsObArs = entry.getValue();
                logger.info("\n\n***** Processing RequestID = " + requestsList.get(requestkey)
                        + ". RequestKey = " + requestkey + " *****\n");

                String[] blobdataList = transferAccsObArs.blobdataList;
                HashMap<Integer, String> headercolumnMapping = new HashMap();

                if (validateInput(prop, requestkey, blobdataList)) {
                    reqProceed = true;
                }

                if (reqProceed) {
                    int index = 0;
                    String[] headerInfo = blobdataList[0].split(",");
                    for (String headerStr : headerInfo) {
                        headerStr = headerStr.trim();
                        headercolumnMapping.put(index, headerStr);
                        logger.info("Header : " + headerStr);
                        index++;
                    }
                    /*
                    // Check and send pre transfer email for all users in the request
                    String xlsFileName =  prop.getProperty("SAMTRANSFER_TRANSFERPATH") + requestsList.get(requestkey) + "_PreTransferReport.xlsx";
                    Path xlsFilePath = Paths.get(xlsFileName);
                    if(!Files.exists(xlsFilePath)) {
                        sendEmailPreTransfer(prop, con, transferAccsObArs);
                    } else {
                        logger.info("PRE_TRANSFER report has already been sent for req id - " + requestsList.get(requestkey));
                    }
                    */

                    for (int i = 1; i < blobdataList.length; i++) {
                        String[] dataInfo = blobdataList[i].split(",", -1);

                        //Check if data has the same no of attrs or columns
                        if (dataInfo.length > 0 && dataInfo.length == headerInfo.length) {
                            //TRIM all input values just to remove any white spaces
                            for (int j = 0; j < dataInfo.length; j++)
                                dataInfo[j] = dataInfo[j].trim();

                            String currOwner = dataInfo[0];
                            String newOwner = dataInfo[1];
                            String busJustification = dataInfo[2];
                            //String action = dataInfo[3];
                            //String reportEmail = dataInfo[4];
                            String serverType = dataInfo[3];


                            HashMap<String, String> lastStatusList = findStatusDetails(con, entry.getKey(),
                                    currOwner,newOwner);
                            String completed = lastStatusList.get("completed");
                            TransferAccountsStatusPojo transferAccsOb = null;
                            transferAccsOb = new TransferAccountsStatusPojo();
                            transferAccsOb.requestID = transferAccsObArs.requestID;
                            transferAccsOb.requestor = transferAccsObArs.requestor;
                            transferAccsOb.requestKey = transferAccsObArs.requestkey;
                            transferAccsOb.currOwner = currOwner;
                            transferAccsOb.newOwner = newOwner;
                            transferAccsOb.businessJustification = busJustification;
                            //transferAccsOb.action = action;
                            //transferAccsOb.reportEmail = reportEmail;
                            transferAccsOb.serverType = serverType;

                            //logger.info(completed);

                            if(completed.equalsIgnoreCase("NOT_PRESENT"))  {
                                logger.info("Report does not exist. Processing the transfer.");

                                HashSet<Account> allAccs = new HashSet<>();
                                HashSet<Group> allGrps = new HashSet<>();
                                //Validate input
                                String error = checkMandatory(prop, headercolumnMapping,
                                        dataInfo, headerInfo, transferAccsOb.requestor);
                                if (error.length() > 0) {
                                    logger.info("Error in data. Setting transfer as complete.");
                                    logger.error("Error - " + error);
                                    transferAccsOb.totalAccounts =  0;
                                    transferAccsOb.accKeys = "";
                                    transferAccsOb.totalGroups = 0;
                                    transferAccsOb.grpKeys = "";
                                    transferAccsOb.totalSuccessTasks = 0;
                                    transferAccsOb.complete = true;
                                    transferAccsOb.totalTasks = 0;
                                    transferAccsOb.reportEmailSent = true;
                                    transferAccsOb.comments.append("ERROR - " + error);
                                    transferAccsOb.taskKeys = "";
                                    transferAccsOb.failedTaskKeys = "";

                                    insertTransferStatusTable(con,transferAccsOb);
                                } else {
                                    logger.info("Validation successful. Processing the transfer.");

                                    //Validation successful. Proceed with transfer
                                    //Get all accounts for the current owner and initiate transfer for each account
                                    String query1 = prop.getProperty("SAMTRANSFER_GETALLACCOUNTS");
                                    String cp22 = getCP22(prop,transferAccsOb.serverType);
                                    query1 = query1.replace("${currowner}", transferAccsOb.currOwner);
                                    query1 = query1.replace("${servertype}", cp22);

                                    PreparedStatement pst = con.prepareStatement(query1);

                                    logger.info("Executing statement: " + pst.toString());
                                    ResultSet rs = pst.executeQuery();

                                    int totalAccs = 0;
                                    String accKeys = "";
                                    while (rs.next()) {
                                        if(!accKeys.equalsIgnoreCase("")) {
                                            accKeys = accKeys + "_";
                                        }
                                        totalAccs++;
                                        String accKey = rs.getString(1);
                                        String accName = rs.getString(2);
                                        String accStatusT = rs.getString(3);
                                        String accStatus = "";
                                        if(accStatusT.equalsIgnoreCase("1") || accStatusT.equalsIgnoreCase("Manually Provisioned")) {
                                            accStatus = "Active";
                                        } else {
                                            accStatus = "Inactive";
                                        }
                                        String accType = rs.getString(4);
                                        String epKey = rs.getString(5);
                                        String ssKey = rs.getString(6);
                                        String epName = rs.getString(7);
                                        logger.info(accKey + " - " + accName + " - " + accStatus + " - " + accType
                                                + " - " + epKey + " - " + ssKey + " - " + epName);

                                        Account temp = new Account(accName,accKey,accStatus,accType,
                                                        epKey,ssKey,epName,transferAccsOb.currOwner);
                                        allAccs.add(temp);

                                        accKeys = accKeys + accKey;
                                    }

                                    //Get all groups for the current owner and initiate transfer for each account
                                    String query2 = prop.getProperty("SAMTRANSFER_GETALLGROUPS");
                                    query2 = query2.replace("${currowner}", transferAccsOb.currOwner);
                                    query2 = query2.replace("${servertype}", cp22);

                                    PreparedStatement pst1 = con.prepareStatement(query2);

                                    logger.info("Executing statement: " + pst1.toString());
                                    ResultSet rs1 = pst1.executeQuery();

                                    int totalGrps = 0;
                                    String grpKeys = "";
                                    while (rs1.next()) {
                                        totalGrps++;
                                        if(!grpKeys.equalsIgnoreCase("")) {
                                            grpKeys = grpKeys + "_";
                                        }
                                        String grpName = rs1.getString(1);
                                        String entValKey = rs1.getString(2);
                                        String epKey = rs1.getString(3);
                                        String epName = rs1.getString(4);
                                        logger.info(grpName + " - " + entValKey + " - " + epKey + " - "  + epName);

                                        Group temp = new Group(grpName,entValKey,
                                                epKey,epName,transferAccsOb.currOwner);
                                        allGrps.add(temp);
                                        grpKeys = grpKeys + entValKey;
                                    }
                                    int totalTasks = 0;

                                    /*
                                    String xlsPath = prop.getProperty("SAMTRANSFER_TRANSFERPATH");
                                    String xlsFileName = transferAccsOb.requestKey + "_"
                                            + transferAccsOb.currOwner + "_" + transferAccsOb.newOwner + "_PreTransferReport.xlsx";
                                    ExcelWriter excel = new ExcelWriter();
                                    excel.writeExcel(allAccs, allGrps, xlsPath + xlsFileName);
                                    SendEmail seConn = new SendEmail();
                                    String sub = prop.getProperty("SAMTRANSFER_EMAILSUBJECT");
                                    sub = sub.replace("${currowner}", transferAccsOb.currOwner);
                                    sub = sub.replace("${newowner}", transferAccsOb.newOwner);
                                    sub = sub.replace("${when}", "PRE-TRANSFER");
                                    seConn.sendEmail(prop, sub, "Test",
                                            transferAccsOb.reportEmail, null, xlsPath + xlsFileName);
                                     */

                                    if (allAccs != null && !allAccs.isEmpty()) {
                                        Iterator<Account> it = allAccs.iterator();
                                        while (it.hasNext()) {
                                            Account acc = it.next();

                                            boolean status = changeAccOwnership(prop, con, transferAccsOb, acc);
                                            if(status) {
                                                totalTasks++;
                                            }
                                        }
                                    }
                                    if (allGrps != null && !allGrps.isEmpty()) {
                                        Iterator<Group> it = allGrps.iterator();
                                        while (it.hasNext()) {
                                            Group grp = it.next();

                                            boolean status = changeGrpOwnership(prop, con, transferAccsOb, grp);
                                            if(status) {
                                            }
                                        }
                                    }

                                    // If its only for report OR total accs=0, set complete=true
                                    // If total accs=0, need to send email to requestor?
                                    transferAccsOb.totalAccounts =  totalAccs;
                                    transferAccsOb.accKeys = accKeys;
                                    transferAccsOb.totalGroups = totalGrps;
                                    transferAccsOb.grpKeys = grpKeys;
                                    transferAccsOb.totalSuccessTasks = 0;
                                    if(totalAccs==0 && totalGrps==0) {
                                        transferAccsOb.complete = true;
                                        transferAccsOb.comments.append("There are no accounts or groups to be transferred.");
                                    } else {
                                        transferAccsOb.complete = false;
                                        transferAccsOb.comments.append("Transfer of Accounts and Groups have " +
                                                "been done and tasks have been created to update GECOS");
                                    }
                                    transferAccsOb.totalTasks = totalTasks;
                                    transferAccsOb.reportEmailSent = true;

                                    insertTransferStatusTable(con,transferAccsOb);
                                    //if((accKeys != null && !accKeys.isEmpty()) &&
                                    //        (grpKeys != null && !grpKeys.isEmpty())) {
                                    //    sendEmailPostTransfer(prop, con, transferAccsOb);
                                    //}

                                    logger.info("Total Accounts - " + totalAccs);
                                    logger.info("Total Groups - " + totalGrps);
                                }
                            }
                            else {
                                logger.info("Transfer record present. Checking transfer status.");
                                int totalaccounts = Integer.parseInt(lastStatusList.get("totalaccounts"));
                                int totalgroups = Integer.parseInt(lastStatusList.get("totalgroups"));
                                int totaltasks = Integer.parseInt(lastStatusList.get("totaltasks"));
                                int taskscompleted = Integer.parseInt(lastStatusList.get("taskscompleted"));
                                int reportemailsent = Integer.parseInt(lastStatusList.get("reportemailsent"));
                                int emailsent = Integer.parseInt(lastStatusList.get("emailsent"));
                                String accKeys = lastStatusList.get("acckeys");
                                String grpkeys = lastStatusList.get("grpkeys");
                                String comments = lastStatusList.get("comments");
                                String taskKeys = lastStatusList.get("taskkeys");
                                String failedTaskKeys = lastStatusList.get("failedtaskkeys");
                                HashSet<Task> allTasks = new HashSet<>();
                                transferAccsOb.totalAccounts = totalaccounts;
                                transferAccsOb.totalTasks=totaltasks;
                                transferAccsOb.totalSuccessTasks=taskscompleted;
                                transferAccsOb.totalGroups=totalgroups;
                                transferAccsOb.reportEmailSent=(reportemailsent==1?true:false);
                                transferAccsOb.comments.append(comments);
                                transferAccsOb.complete=(completed.equalsIgnoreCase("1")?true:false);
                                transferAccsOb.taskKeys=taskKeys;
                                transferAccsOb.failedTaskKeys=failedTaskKeys;
                                transferAccsOb.accKeys=accKeys;
                                transferAccsOb.grpKeys=grpkeys;

                                if(completed.equalsIgnoreCase("0")) {
                                    if (totalaccounts != totaltasks) {
                                        logger.info("No. of Accounts does not match with No. of tasks.");

                                        // Parse all acc keys and see if tasks are missing for any accounts
                                        String accKeyA[] = accKeys.split("_");
                                        List<String> accKeysList = Arrays.asList(accKeyA);

                                        String query1 = prop.getProperty("SAMTRANSFER_GETALLACCOUNTS_SELECTED");
                                        String cp22 = getCP22(prop,transferAccsOb.serverType);
                                        query1 = query1.replace("${currowner}", transferAccsOb.currOwner);
                                        query1 = query1.replace("${servertype}", cp22);
                                        query1 = query1.replace("${accsList}", join(accKeysList));

                                        PreparedStatement pst = con.prepareStatement(query1);

                                        logger.info("Executing statement: " + pst.toString());
                                        ResultSet rs = pst.executeQuery();

                                        int totalRemAccs = 0;
                                        HashSet<Account> remAccs = new HashSet<>();
                                        HashSet<Group> remGrps = new HashSet<>();

                                        while (rs.next()) {
                                            totalRemAccs++;
                                            String accKey = rs.getString(1);
                                            String accName = rs.getString(2);
                                            String accStatusT = rs.getString(3);
                                            String accStatus = "";
                                            if(accStatusT.equalsIgnoreCase("1") || accStatusT.equalsIgnoreCase("Manually Provisioned")) {
                                                accStatus = "Active";
                                            } else {
                                                accStatus = "Inactive";
                                            }
                                            String accType = rs.getString(4);
                                            String epKey = rs.getString(5);
                                            String ssKey = rs.getString(6);
                                            String epName = rs.getString(7);
                                            logger.info(accKey + " - " + accName + " - " + accStatus + " - " + accType
                                                    + " - " + epKey + " - " + ssKey + " - " + epName);

                                            Account temp = new Account(accName,accKey,accStatus,accType,
                                                    epKey,ssKey,epName,transferAccsOb.currOwner);
                                            remAccs.add(temp);
                                        }

                                        int remTotalTasks =0;

                                        if (remAccs != null && !remAccs.isEmpty()) {
                                            Iterator<Account> it = remAccs.iterator();
                                            while (it.hasNext()) {
                                                Account acc = it.next();
                                                boolean status = changeAccOwnership(prop, con, transferAccsOb, acc);
                                                if(status) {
                                                    remTotalTasks++;
                                                }
                                            }
                                        }

                                        String grpKeyA[] = grpkeys.split("_");
                                        List<String> grpKeysList = Arrays.asList(grpKeyA);

                                        //Get all groups for the current owner and initiate transfer for each account
                                        String query2 = prop.getProperty("SAMTRANSFER_GETALLGROUPS_SELECTED");
                                        query2 = query2.replace("${currowner}", transferAccsOb.currOwner);
                                        query2 = query2.replace("${servertype}", cp22);
                                        query2 = query2.replace("${grpsList}", join(grpKeysList));

                                        PreparedStatement pst1 = con.prepareStatement(query2);

                                        logger.info("Executing statement: " + pst1.toString());
                                        ResultSet rs1 = pst1.executeQuery();

                                        int totalRemGrps = 0;
                                        int totalRemGrpsProcessed = 0;
                                        while (rs1.next()) {
                                            totalRemGrps++;

                                            String grpName = rs1.getString(1);
                                            String entValKey = rs1.getString(2);
                                            String epKey = rs1.getString(3);
                                            String epName = rs1.getString(4);
                                            logger.info(grpName + " - " + entValKey + " - " + epKey + " - "  + epName);

                                            Group temp = new Group(grpName,entValKey,
                                                    epKey,epName,transferAccsOb.currOwner);
                                            remGrps.add(temp);
                                        }

                                        if (remGrps != null && !remGrps.isEmpty()) {
                                            Iterator<Group> it = remGrps.iterator();
                                            while (it.hasNext()) {
                                                Group grp = it.next();

                                                boolean status = changeGrpOwnership(prop, con, transferAccsOb, grp);
                                                if(status) {
                                                    totalRemGrpsProcessed++;
                                                }
                                            }
                                        }
                                        if(totalRemGrps>0 && totalRemGrpsProcessed>0) {
                                            logger.info("There were few groups not processed " +
                                                    "properly in the previous run. Total groups processed now: " + totalRemGrpsProcessed);
                                            transferAccsOb.comments.append(" | There were few groups not processed " +
                                                    "properly in the previous run. Total groups processed now: " + totalRemGrpsProcessed);
                                        }

                                        if(transferAccsOb.totalTasks + remTotalTasks <= transferAccsOb.totalAccounts
                                            && remTotalTasks>0) {
                                            transferAccsOb.totalTasks = transferAccsOb.totalTasks + remTotalTasks;

                                            logger.info("There were few accounts not processed " +
                                                    "properly in the previous run. Total accounts processed now: " + remTotalTasks);
                                            transferAccsOb.comments.append(" | There were few accounts not processed " +
                                                    "properly in the previous run. Total accounts processed now: " + remTotalTasks);

                                            updateTransferStatusTable(con,transferAccsOb);
                                        }
                                    } else {
                                        // Check if any tasks are completed. If all tasks are processed, send email and
                                        // mark complete = true
                                        if(totaltasks != taskscompleted) {
                                            logger.info("Tasks present for all accounts. Checking task status.");

                                            String query1 = prop.getProperty("SAMTRANSFER_GETALLTASKS");
                                            String cp22 = getCP22(prop,transferAccsOb.serverType);
                                            query1 = query1.replace("${reqkey}", transferAccsOb.requestKey);
                                            query1 = query1.replace("${newowner}", transferAccsOb.newOwner);
                                            query1 = query1.replace("${currowner}", transferAccsOb.currOwner);
                                            query1 = query1.replace("${busjustification}", transferAccsOb.businessJustification);
                                            query1 = query1.replace("${servertype}", cp22);

                                            PreparedStatement pst = con.prepareStatement(query1);

                                            logger.info("Executing statement: " + pst.toString());
                                            ResultSet rs = pst.executeQuery();

                                            int totalTasksRecord = 0;
                                            int totalCompletedTasks = 0;
                                            String failedTaskKeysL = "";
                                            String taskKeysL = "";
                                            while (rs.next()) {
                                                totalTasksRecord++;
                                                String taskKey = rs.getString(1);
                                                String accTKey = rs.getString(2);
                                                String epKey = rs.getString(3);
                                                String tStatus = rs.getString(4);
                                                String accTName = rs.getString(5);
                                                String epName = rs.getString(6);

                                                logger.info(taskKey + " - " + accTName + " - " +
                                                        accTKey + " - " + epKey + " - " + epName + " - " + tStatus);

                                                Task temp = new Task(taskKey,accTName,accTKey,epKey,epName,tStatus);
                                                allTasks.add(temp);

                                                if (!taskKeysL.equalsIgnoreCase("")) {
                                                    taskKeysL = taskKeysL + "_";
                                                }
                                                taskKeysL = taskKeysL + taskKey;

                                                if(!tStatus.equalsIgnoreCase("1")) {
                                                    totalCompletedTasks++;

                                                    if(tStatus.equalsIgnoreCase("4") ||
                                                            tStatus.equalsIgnoreCase("8")) {
                                                        if (!failedTaskKeysL.equalsIgnoreCase("")) {
                                                            failedTaskKeysL = failedTaskKeysL + "_";
                                                        }
                                                        failedTaskKeysL = failedTaskKeysL + taskKey;
                                                    }
                                                }
                                            }
                                            if(totaltasks != totalTasksRecord) {
                                                logger.info("There are few tasks missing. Tasks missing - " +
                                                        (totaltasks - totalTasksRecord));
                                                transferAccsOb.comments.append(" | There are few tasks missing. Tasks missing - " +
                                                        (totaltasks - totalTasksRecord));
                                            }
                                            if(totalCompletedTasks == totaltasks) {
                                                logger.info("All tasks are processed. Marking transfer as complete.");

                                                transferAccsOb.totalSuccessTasks = totaltasks;
                                                // 1. Send email to beneficiary for successful transfers and
                                                sendEmailToOwner(prop, con, transferAccsOb);
                                                // 2. Send post report email
                                                //sendEmailPostTransfer(prop, con, transferAccsOb);

                                                transferAccsOb.reportEmailSent = true;
                                                transferAccsOb.failedTaskKeys = failedTaskKeysL;
                                                transferAccsOb.taskKeys = taskKeysL;
                                                transferAccsOb.emailSent = true;
                                                transferAccsOb.complete = true;
                                                transferAccsOb.comments.append(" | Transfer Complete.");
                                            } else {
                                                logger.info("There are "+ (totaltasks-totalCompletedTasks) + " tasks pending. Updating status.");

                                                transferAccsOb.totalSuccessTasks = totalCompletedTasks;
                                                transferAccsOb.failedTaskKeys = failedTaskKeysL;
                                                transferAccsOb.taskKeys = taskKeysL;
                                                transferAccsOb.complete = false;
                                                transferAccsOb.emailSent = false;
                                            }
                                            updateTransferStatusTable(con, transferAccsOb);
                                        } else {
                                            if(emailsent == 0) {
                                                //Send email to beneficiary for successful transfers and
                                                //requestor for failed transfers
                                                sendEmailToOwner(prop, con, transferAccsOb);

                                                transferAccsOb.emailSent = true;
                                                transferAccsOb.complete = true;

                                                updateTransferStatusTable(con, transferAccsOb);
                                            }
                                        }
                                    }
                                    //If not complete, pls check if total accounts=total tasks AND total tasks=total successful tasks
                                    //If total accounts dont match with total tasks, change ownership and create tasks for remaining
                                    //If all tasks are completed, send email based on task status to new owner and requestor
                                    //Set complete=true once all tasks were processed and email was sent
                                } else {
                                    logger.info("Transfer has been processed for the user."
                                            + " Old owner - " + transferAccsOb.currOwner + ", "
                                            + "New Owner - " + transferAccsOb.newOwner);
                                }
                            }

                            transferAccObkeys.put(i, transferAccsOb);
                        }
                    }

                    //For all entries within the request

                    //Check if request was already processed and just need to check
                    // tasks and send notification if its complete. Check completion for each identity transfer and sent email

                    //If new request, For all lines in the requests, carry out transfer activities
                    //Add entries to DB for each transfer
                } // end of all steps

                logger.info("End of all steps");

                //Do we need an email in the every run to requestor with status of all transfers?
            }
            if (!transferRequests.isEmpty()) {
                //check and update requests
                //update invalid requests

                if (!checkAndUpdateRequestSendEmail(prop, con, transferRequests)) {
                    logger.info("Error in updating requests. ");
                }
                updateRequestInvalidData(prop,con);
            }
        } catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        } finally {
            try {
                if (rs3 != null) {
                    rs3.close();
                }
            } catch (SQLException e) {
                logger.error("Exception - " +  e.getMessage(), e);
            }
        }
    }

    public void sendEmailPostTransfer(Properties prop, Connection con, TransferAccountsStatusPojo transferAccsOb) {
        try {
            logger.info("Inside sendEmailPostTransfer...");
            HashSet<Account> allAccsNew = new HashSet<>();
            HashSet<Group> allGrpsNew = new HashSet<>();
            List<String> accKeysList = Arrays.asList(transferAccsOb.accKeys.split("_"));
            List<String> grpKeysList = Arrays.asList(transferAccsOb.grpKeys.split("_"));

            //Get all accounts for the current owner and initiate transfer for each account
            String query1 = prop.getProperty("SAMTRANSFER_GETALLACCOUNTS_SELECTED");
            String cp22 = getCP22(prop,transferAccsOb.serverType);
            query1 = query1.replace("${currowner}", transferAccsOb.newOwner);
            query1 = query1.replace("${servertype}", cp22);
            query1 = query1.replace("${accsList}", join(accKeysList));

            PreparedStatement pst = con.prepareStatement(query1);

            logger.info("Executing statement: " + pst.toString());
            ResultSet rs = pst.executeQuery();

            int totalAccs = 0;
            while (rs.next()) {
                totalAccs++;
                String accKey = rs.getString(1);
                String accName = rs.getString(2);
                String accStatusT = rs.getString(3);
                String accStatus = "";
                if (accStatusT.equalsIgnoreCase("1") || accStatusT.equalsIgnoreCase("Manually Provisioned")) {
                    accStatus = "Active";
                } else {
                    accStatus = "Inactive";
                }
                String accType = rs.getString(4);
                String epKey = rs.getString(5);
                String ssKey = rs.getString(6);
                String epName = rs.getString(7);
                logger.info(accKey + " - " + accName + " - " + accStatus + " - " + accType
                        + " - " + epKey + " - " + ssKey + " - " + epName);

                Account temp = new Account(accName, accKey, accStatus, accType,
                        epKey, ssKey, epName, transferAccsOb.currOwner);
                allAccsNew.add(temp);
            }

            //Get all groups for the current owner and initiate transfer for each account
            String query2 = prop.getProperty("SAMTRANSFER_GETALLGROUPS_SELECTED");
            query2 = query2.replace("${currowner}", transferAccsOb.newOwner);
            query2 = query2.replace("${servertype}", cp22);
            query2 = query2.replace("${grpsList}", join(grpKeysList));

            PreparedStatement pst1 = con.prepareStatement(query2);

            logger.info("Executing statement: " + pst1.toString());
            ResultSet rs1 = pst1.executeQuery();

            int totalGrps = 0;
            while (rs1.next()) {
                totalGrps++;
                String grpName = rs1.getString(1);
                String entValKey = rs1.getString(2);
                String epKey = rs1.getString(3);
                String epName = rs1.getString(4);
                logger.info(grpName + " - " + entValKey + " - " + epKey + " - " + epName);

                Group temp = new Group(grpName, entValKey,
                        epKey, epName, transferAccsOb.currOwner);
                allGrpsNew.add(temp);
            }
            int totalTasks = 0;

            String xlsPath = prop.getProperty("SAMTRANSFER_TRANSFERPATH");
            String xlsFileName =transferAccsOb.requestKey + "_"
                    + transferAccsOb.currOwner + "_" + transferAccsOb.newOwner + "_PostTransfer.xlsx";
            ExcelWriter excel = new ExcelWriter();
            excel.writeExcel(allAccsNew, allGrpsNew, xlsPath + xlsFileName);
            SendEmail seConn = new SendEmail();
            String sub = prop.getProperty("SAMTRANSFER_EMAILSUBJECT");
            sub = sub.replace("${currowner}", transferAccsOb.currOwner);
            sub = sub.replace("${newowner}", transferAccsOb.newOwner);
            sub = sub.replace("${when}", "POST-TRANSFER");
            seConn.sendEmail(prop, sub, "Test",
                    "mahesh.shivananjappa@saviynt.com", null, xlsPath + xlsFileName);
        } catch (Exception e) {
            logger.error("Error sending post transfer email Exception - " +  e.getMessage(), e);
        }
    }

    public void sendEmailPreTransfer(Properties prop, Connection con, ArsRequestAttachmentPojo transferAccsObArs) {
        try {
            logger.info("Inside sendEmailPreTransfer...");
            HashSet<Account> allAccsNew = new HashSet<>();
            HashSet<Group> allGrpsNew = new HashSet<>();
            String[] blobdataList = transferAccsObArs.blobdataList;
            List<String> currOwnerList = new ArrayList<String>();
            String serverType = "";
            String reportEmail = "";
            String requestID = transferAccsObArs.requestID;

            String[] headerInfo = blobdataList[0].split(",");
            for (int i = 1; i < blobdataList.length; i++) {
                String[] dataInfo = blobdataList[i].split(",", -1);

                //Check if data has the same no of attrs or columns
                if (dataInfo.length > 0 && dataInfo.length == headerInfo.length) {
                    currOwnerList.add(dataInfo[0].trim());
                    reportEmail = dataInfo[4].trim();
                    serverType = dataInfo[5].trim();
                }
            }


            //Get all accounts for the current owner and initiate transfer for each account
            String query1 = prop.getProperty("SAMTRANSFER_GETALLACCOUNTS_ALLUSERS");
            String cp22 = getCP22(prop,serverType);
            query1 = query1.replace("${currowner}", join(currOwnerList));
            query1 = query1.replace("${servertype}", cp22);

            PreparedStatement pst = con.prepareStatement(query1);

            logger.info("Executing statement: " + pst.toString());
            ResultSet rs = pst.executeQuery();

            int totalAccs = 0;
            while (rs.next()) {
                totalAccs++;
                String accKey = rs.getString(1);
                String accName = rs.getString(2);
                String accStatusT = rs.getString(3);
                String accStatus = "";
                if (accStatusT.equalsIgnoreCase("1") || accStatusT.equalsIgnoreCase("Manually Provisioned")) {
                    accStatus = "Active";
                } else {
                    accStatus = "Inactive";
                }
                String accType = rs.getString(4);
                String epKey = rs.getString(5);
                String ssKey = rs.getString(6);
                String epName = rs.getString(7);
                String currOwner = rs.getString(8);
                logger.info(accKey + " - " + accName + " - " + accStatus + " - " + accType
                        + " - " + epKey + " - " + ssKey + " - " + epName + " - " + currOwner);

                Account temp = new Account(accName, accKey, accStatus, accType,
                        epKey, ssKey, epName, currOwner);
                allAccsNew.add(temp);
            }

            //Get all groups for the current owner and initiate transfer for each account
            String query2 = prop.getProperty("SAMTRANSFER_GETALLGROUPS_ALLUSERS");
            query2 = query2.replace("${currowner}", join(currOwnerList));
            query2 = query2.replace("${servertype}", cp22);

            PreparedStatement pst1 = con.prepareStatement(query2);

            logger.info("Executing statement: " + pst1.toString());
            ResultSet rs1 = pst1.executeQuery();

            int totalGrps = 0;
            while (rs1.next()) {
                totalGrps++;
                String grpName = rs1.getString(1);
                String entValKey = rs1.getString(2);
                String epKey = rs1.getString(3);
                String epName = rs1.getString(4);
                String currOwner = rs1.getString(5);
                logger.info(grpName + " - " + entValKey + " - " + epKey + " - " + epName + " - " + currOwner);

                Group temp = new Group(grpName, entValKey,
                        epKey, epName, currOwner);
                allGrpsNew.add(temp);
            }
            int totalTasks = 0;

            String xlsPath = prop.getProperty("SAMTRANSFER_TRANSFERPATH");
            String xlsFileName =requestID + "_PreTransferReport.xlsx";
            ExcelWriter excel = new ExcelWriter();
            excel.writeExcel(allAccsNew, allGrpsNew, xlsPath + xlsFileName);
            SendEmail seConn = new SendEmail();
            String sub = prop.getProperty("SAMTRANSFER_EMAILSUBJECT_PRE");
            sub = sub.replace("${reqid}", requestID);
            seConn.sendEmail(prop, sub, "Test",
                    reportEmail, null, xlsPath + xlsFileName);
        } catch (Exception e) {
            logger.error("Error sending pre transfer email Exception - " +  e.getMessage(), e);
        }
    }

    public void sendEmailToOwner(Properties prop, Connection con,
                                 TransferAccountsStatusPojo transferAccsOb) {
        try {
            logger.info("Inside sendEmailToOwner...");
            String subject = "";
            String body = "";

            //Get all groups for the current owner and initiate transfer for each account
            String query1 = prop.getProperty("SAMTRANSFER_EMAILTEMPLATE_OWNER");
            PreparedStatement pst1 = con.prepareStatement(query1);
            logger.info("Executing statement: " + pst1.toString());
            ResultSet rs1 = pst1.executeQuery();

            if (rs1.next()) {
                subject = rs1.getString(1);
                body = rs1.getString(2);
            } else {
                subject = prop.getProperty("SAMTRANSFER_EMAILSUBJECT_OWNER");
                body = prop.getProperty("SAMTRANSFER_EMAILBODY_OWNER");
            }

            String newOwnerEmail = getEmail(con, transferAccsOb.newOwner);
            String oldOwnerEmail = getEmail(con, transferAccsOb.currOwner);

            body = body.replace("${olduserdetails}", oldOwnerEmail + ", " + transferAccsOb.currOwner);
            body = body.replace("${newuserdetails}", newOwnerEmail + ", " + transferAccsOb.newOwner);

            /*
            BufferedReader reader = new BufferedReader(new FileReader(bodyContentFile));
            StringBuilder stringBuilder = new StringBuilder();
            char[] buffer = new char[10];
            while (reader.read(buffer) != -1) {
                stringBuilder.append(new String(buffer));
                buffer = new char[10];
            }
            reader.close();
            String body = stringBuilder.toString();


            // Send email to owner which contains all successful transfers
            String data = "";
            data = data + "<table><tr><td>Account Name</td><td>Server</td><td>Task Key</td><td>Status</td></tr> ";
            // Send email to requestor regarding task failures
            if (allTasks != null && !allTasks.isEmpty()) {
                Iterator<Task> it = allTasks.iterator();
                while (it.hasNext()) {
                    Task task = it.next();
                    if(task.getStatus().equalsIgnoreCase("3")) {
                        data = data + "<tr>";
                        data = data + "<td>" + task.getAccName() + "</td>"
                                + "<td>" + task.getEndpointName() + "</td>"
                                + "<td>" + task.getTaskKey() + "</td>"
                                + "<td>" + task.getStatus() + "</td>";
                        data = data + "</tr> ";
                    }
                }
            }
            data = data + "</table>";

            body = body.replace("${data}", data);*/

            SendEmail seConn = new SendEmail();
            seConn.sendEmail(prop, subject, body, newOwnerEmail + "," + oldOwnerEmail,
                    null, null);

        } catch (Exception e) {
            logger.error("Error sending post transfer email Exception - " +  e.getMessage(), e);
        }
    }

    private String join(List<String> namesList) {
        return String.join(",", namesList
                .stream()
                .map(name -> ("'" + name + "'"))
                .collect(Collectors.toList()));
    }

    public boolean changeAccOwnership (Properties prop, Connection con,
                                            TransferAccountsStatusPojo transferAccsOb, Account acc) {
        boolean retVal = true;
        logger.info("Inside changeAccOwnership...");

        try {

            if(acc.getAccType() != null) {
                //Insert the update account task to update GECOS
                String insertTaskQry = prop.getProperty("SAMTRANSFER_INSERTUPDTASK");
                insertTaskQry = insertTaskQry.replace("${reqkey}", transferAccsOb.requestKey);
                insertTaskQry = insertTaskQry.replace("${secsyskey}", acc.getSsKey());
                insertTaskQry = insertTaskQry.replace("${newowner}", transferAccsOb.newOwner);
                insertTaskQry = insertTaskQry.replace("${currowner}", transferAccsOb.currOwner);
                insertTaskQry = insertTaskQry.replace("${busjustification}", transferAccsOb.businessJustification);
                insertTaskQry = insertTaskQry.replace("${acckey}", acc.getAccKey());
                //logger.info(insertTaskQry);

                PreparedStatement pstmt1 = con.prepareStatement(insertTaskQry.toString());
                logger.info("Executing statement: " + pstmt1.toString());
                int result = pstmt1.executeUpdate();
                logger.info("Statement executed successfully");
                if (result != 0) {
                    logger.info("GECOS update Task created for Acc - " + acc.getAccKey());

                    //Change ownership
                    String updAccQry = prop.getProperty("SAMTRANSFER_UPDATEACCOWNER");
                    updAccQry = updAccQry.replace("${newowner}", transferAccsOb.newOwner);
                    updAccQry = updAccQry.replace("${currowner}", acc.getCurrOwner());
                    updAccQry = updAccQry.replace("${acckey}", acc.getAccKey());
                    //logger.info(updAccQry);

                    pstmt1 = con.prepareStatement(updAccQry.toString());
                    logger.info("Executing statement: " + pstmt1.toString());
                    result = pstmt1.executeUpdate();
                    logger.info("Statement executed successfully");
                    if (result != 0) {
                        logger.info("Ownership update successful for Acc - " + acc.getAccKey());

                        //Change CPs based on acc type
                        String updCPQry = "";
                        if (acc.getAccType().equalsIgnoreCase("SYSTEM") ||
                                acc.getAccType().equalsIgnoreCase("FIREFIGHTERID")) {
                            updCPQry = prop.getProperty("SAMTRANSFER_UPDATECPSYS");
                            updCPQry = updCPQry.replace("${newowner}", transferAccsOb.newOwner);
                            updCPQry = updCPQry.replace("${acckey}", acc.getAccKey());
                            updCPQry = updCPQry.replace("${acctype}", "S");
                            //logger.info(updCPQry);
                        } else {
                            updCPQry = prop.getProperty("SAMTRANSFER_UPDATECPPER");
                            updCPQry = updCPQry.replace("${newowner}", transferAccsOb.newOwner);
                            updCPQry = updCPQry.replace("${acckey}", acc.getAccKey());
                            updCPQry = updCPQry.replace("${acctype}", "I");
                            //logger.info(updCPQry);
                        }

                        pstmt1 = con.prepareStatement(updCPQry.toString());
                        logger.info("Executing statement: " + pstmt1.toString());
                        result = pstmt1.executeUpdate();
                        logger.info("Statement executed successfully");
                        if (result != 0) {
                            logger.info("CPs update successful for Acc - " + acc.getAccKey());
                        } else {
                            logger.info("CPs update failed for Acc - " + acc.getAccKey());
                        }
                    } else {
                        logger.info("Ownership update failed for Acc - " + acc.getAccKey());
                    }
                } else {
                    logger.info("GECOS update Task creation failed for Acc - " + acc.getAccKey());
                }
            }
            //Get all groups
        } catch (Exception e) {
            retVal = false;
            logger.error("Exception - " +  e.getMessage(), e);
        } finally {
            return retVal;
        }
    }

    public boolean changeGrpOwnership (Properties prop, Connection con,
                                       TransferAccountsStatusPojo transferAccsOb, Group grp) {
        boolean retVal = true;
        logger.info("Inside changeGrpOwnership...");

        try {
            //Change grp ownership
            String updGrpQry = prop.getProperty("SAMTRANSFER_UPDATEGRPOWNER");
            updGrpQry = updGrpQry.replace("${newowner}", transferAccsOb.newOwner);
            updGrpQry = updGrpQry.replace("${entvalkey}", grp.getEntValKey());
            updGrpQry = updGrpQry.replace("${currowner}", grp.getCurrOwner());
            //logger.info(updGrpQry);

            PreparedStatement pstmt1 = con.prepareStatement(updGrpQry.toString());
            logger.info("Executing statement: " + pstmt1.toString());
            int result = pstmt1.executeUpdate();
            logger.info("Statement executed successfully");
            if (result != 0) {
                logger.info("Group ownership update successful for Group - " + grp.getEntValKey());
            } else {
                logger.info("Group ownership update failed for Group - " + grp.getEntValKey());
            }

        } catch (Exception e) {
            retVal = false;
            logger.error("Exception - " +  e.getMessage(), e);
        } finally {
            return retVal;
        }
    }

    public String getCP22 (Properties prop, String serverType) {
        String serverTypeGI = prop.getProperty("SAMTRANSFER_SERVERTYPEGI");
        String cp22GI = prop.getProperty("SAMTRANSFER_CP22GI");
        String serverTypeNE = prop.getProperty("SAMTRANSFER_SERVERTYPENE");
        String cp22NE = prop.getProperty("SAMTRANSFER_CP22NE");
        String serverTypeGB = prop.getProperty("SAMTRANSFER_SERVERTYPEGB");
        String cp22GB = prop.getProperty("SAMTRANSFER_CP22GB");

        if (serverType.equalsIgnoreCase(serverTypeGI)) {
            return cp22GI;
        } else if (serverType.equalsIgnoreCase(serverTypeNE)) {
            return cp22NE;
        } else if (serverType.equalsIgnoreCase(serverTypeGB)) {
            return cp22GB;
        }
        return "";
    }

    public  String getEmail(Connection con, String username) {
        String email="";
        logger.info("Inside getEmail...");

        try {
            ResultSet rs=null;
            String selectSQL = "SELECT EMAIL FROM USERS WHERE USERNAME = ? ";

            PreparedStatement pstmt = con.prepareStatement(selectSQL);
            pstmt=con.prepareStatement(selectSQL);
            pstmt.setString(1,username);

            logger.info("Executing statement: "+pstmt.toString());
            rs=pstmt.executeQuery();
            logger.info("Statement executed successfully");

            while(rs.next()) {
                email = rs.getString("EMAIL");
            }
        }
        catch(SQLException se) {
            se.printStackTrace();
        }
        finally {
            return email;
        }
    }

    public  HashMap<String,String> findStatusDetails(Connection con, String reqkey,String currowner, String newowner) {
        logger.info("Inside findStatusDetails...");

        String totalaccounts="0";
        String totalgroups="0";
        String totaltasks="0";
        String taskscompleted="0";
        String completed="NOT_PRESENT";
        String reportemailsent="0";
        String emailsent="0";
        String comments = "";
        String accKeys = "";
        String grpKeys = "";
        String taskKeys = "";
        String failedTaskKeys = "";

        HashMap<String,String> llist=new HashMap<>();
        llist.put("totalaccounts",totalaccounts);
        llist.put("totalgroups",totalgroups);
        llist.put("totaltasks",totaltasks);
        llist.put("taskscompleted",taskscompleted);
        llist.put("completed",completed);
        llist.put("reportemailsent",reportemailsent);
        llist.put("emailsent",emailsent);
        llist.put("comments",comments);
        llist.put("acckeys",accKeys);
        llist.put("grpkeys",grpKeys);
        llist.put("taskkeys",taskKeys);
        llist.put("failedtaskkeys",failedTaskKeys);

        try {
            ResultSet rs=null;
            String selectSQL = "SELECT * FROM samtransferaccountsstatus " +
                    "WHERE arsrequestkey = ? and currowner=? and newowner=?";

            PreparedStatement pstmt = con.prepareStatement(selectSQL);
            pstmt=con.prepareStatement(selectSQL);
            pstmt.setString(1,reqkey);
            pstmt.setString(2,currowner);
            pstmt.setString(3,newowner);

            logger.info("Executing statement: "+pstmt.toString());
            rs=pstmt.executeQuery();
            logger.info("Statement executed successfully");

            while(rs.next()) {
                totalaccounts = rs.getString("totalaccounts");
                totalgroups = rs.getString("totalgroups");
                totaltasks = rs.getString("totaltasks");
                taskscompleted = rs.getString("taskscompleted");
                completed = rs.getString("completed");
                reportemailsent = rs.getString("reportemailsent");
                emailsent = rs.getString("emailsent");
                comments = rs.getString("comments");
                accKeys = rs.getString("acckeys");
                grpKeys = rs.getString("grpkeys");
                taskKeys = rs.getString("taskkeys");
                failedTaskKeys = rs.getString("failedtaskkeys");

                llist.put("totalaccounts",totalaccounts);
                llist.put("totalgroups",totalgroups);
                llist.put("totaltasks",totaltasks);
                llist.put("taskscompleted",taskscompleted);
                llist.put("completed",completed);
                llist.put("reportemailsent",reportemailsent);
                llist.put("emailsent",emailsent);
                llist.put("comments",comments);
                llist.put("acckeys",accKeys);
                llist.put("grpkeys",grpKeys);
                llist.put("taskkeys",taskKeys);
                llist.put("failedtaskkeys",failedTaskKeys);
            }
        }
        catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
        finally {
            return llist;
        }
    }

    public void insertTransferStatusTable(Connection con, TransferAccountsStatusPojo transferAccOb) {
        logger.info("Inside insertTransferStatusTable...");

        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            ResultSet rs=null;
            String selectSQL = "INSERT INTO samtransferaccountsstatus(arsrequestkey,updatedate,currowner,newowner,totalaccounts,acckeys,totalgroups,grpkeys,totaltasks,taskscompleted,comments,completed,reportemailsent,emailsent,taskkeys,servertype) values(?,now(),?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

            PreparedStatement pstmt = con.prepareStatement(selectSQL);
            pstmt=con.prepareStatement(selectSQL);
            pstmt.setString(1,transferAccOb.requestKey);
            pstmt.setString(2,transferAccOb.currOwner);
            pstmt.setString(3, transferAccOb.newOwner);
            pstmt.setInt(4, transferAccOb.totalAccounts);
            pstmt.setString(5, transferAccOb.accKeys);
            pstmt.setInt(6, transferAccOb.totalGroups);
            pstmt.setString(7, transferAccOb.grpKeys);
            pstmt.setInt(8, transferAccOb.totalTasks);
            pstmt.setInt(9, transferAccOb.totalSuccessTasks);
            pstmt.setString(10, transferAccOb.comments.toString());
            if(transferAccOb.complete) {
                pstmt.setInt(11, 1);
            } else {
                pstmt.setInt(11, 0);
            }
            if(transferAccOb.reportEmailSent) {
                pstmt.setInt(12, 1);
            } else {
                pstmt.setInt(12, 0);
            }
            if(transferAccOb.emailSent) {
                pstmt.setInt(13, 1);
            } else {
                pstmt.setInt(13, 0);
            }
            pstmt.setString(14, transferAccOb.failedTaskKeys);
            pstmt.setString(15, transferAccOb.serverType);

            logger.info("Executing statement: "+pstmt.toString());
            pstmt.execute();
            logger.info("Statement executed successfully");
        }
        catch(SQLException e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
    }

    public void updateTransferStatusTable(Connection con, TransferAccountsStatusPojo transferAccOb) {
        logger.info("Inside updateTransferStatusTable...");

        try {
            String selectSQL = "UPDATE samtransferaccountsstatus SET totaltasks=?,taskscompleted=?,comments=?,completed=?,reportemailsent=?,emailsent=?,updatedate=now(),taskkeys=?,failedtaskkeys=? where arsrequestkey=? and currowner=? and newowner=?; ";

            PreparedStatement pstmt = con.prepareStatement(selectSQL);
            pstmt=con.prepareStatement(selectSQL);
            pstmt.setInt(1,transferAccOb.totalTasks);
            pstmt.setInt(2,transferAccOb.totalSuccessTasks);
            pstmt.setString(3, transferAccOb.comments.toString());
            if(transferAccOb.complete) {
                pstmt.setInt(4, 1);
            } else {
                pstmt.setInt(4, 0);
            }
            if(transferAccOb.reportEmailSent) {
                pstmt.setInt(5, 1);
            } else {
                pstmt.setInt(5, 0);
            }
            if(transferAccOb.emailSent) {
                pstmt.setInt(6, 1);
            } else {
                pstmt.setInt(6, 0);
            }
            pstmt.setString(7, transferAccOb.taskKeys);
            pstmt.setString(8, transferAccOb.failedTaskKeys);

            pstmt.setString(9, transferAccOb.requestKey);
            pstmt.setString(10, transferAccOb.currOwner);
            pstmt.setString(11, transferAccOb.newOwner);

            logger.info("Executing statement: "+pstmt.toString());
            pstmt.executeUpdate();
            logger.info("Statement executed successfully");
        }
        catch(SQLException e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
    }


    public Integer getKeybyValue (Map <Integer,String> map, String value) {
        for (Map.Entry<Integer, String> entry : map.entrySet()) {
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }

    public String checkMandatory(Properties prop,HashMap<Integer,String> headerMap,String [] data,
                                 String[] headerInfo, String requestor)
    {
        logger.info("Inside Check Manadatory Attributes...");
        String [] attrs = prop.getProperty("SAMTRANSFER_MANDATORYATTRIBUTES").split(",");
        String error = "";
        String currOwner = null;
        String newOwner = null;
        String serverType = null;

        int fe = 0;

        try {
            if (data.length == headerInfo.length) {

                if (attrs.length > 1) {
                    for (int i = 0; i < attrs.length; i++) {
                        int key = getKeybyValue(headerMap, attrs[i]);
                        logger.info("Checking Key position : " + key + "value of header : " + attrs[i] + "value of data : " + data[key]);
                        if (data[key] == null) {
                            error = error + attrs[i] + ",";
                            logger.info("Failed : Mandatory Attributes Missing :" + error);
                            fe++;
                        } else {
                            if (attrs[i].equalsIgnoreCase("Current Owner")) {
                                currOwner = data[key];
                            } else if (attrs[i].equalsIgnoreCase("New Owner")) {
                                newOwner = data[key];
                            } else if (attrs[i].equalsIgnoreCase("Server Type")) {
                                serverType = data[key];
                            }
                        }
                    }
                    if (error.equals("") && currOwner != null && newOwner != null) {

                        String query1 = prop.getProperty("SAMTRANSFER_VALIDATEOWNERS");
                        query1 = query1.replace("${currowner}", currOwner);
                        query1 = query1.replace("${newowner}", newOwner);
                        PreparedStatement pst = con.prepareStatement(query1);

                        logger.info("Executing statement: " + pst.toString());
                        ResultSet rs = pst.executeQuery();

                        if (rs.next()) {
                            logger.info("Valid Users and user type");
                        } else {
                            error = "Invalid Users.";
                        }
                    }
                    if (error.equals("")) {
                        logger.info("Validating server types...");

                        String validServerTypes = prop.getProperty("SAMTRANSFER_VALIDSERVERTYPES");

                        if (!validServerTypes.contains(serverType)) {
                            error = "Invalid Server Type.";
                        } else {
                            String serverTypeGI = prop.getProperty("SAMTRANSFER_SERVERTYPEGI");
                            String serverTypeNE = prop.getProperty("SAMTRANSFER_SERVERTYPENE");
                            String serverTypeGB = prop.getProperty("SAMTRANSFER_SERVERTYPEGB");
                            String stypeQry = "";
                            PreparedStatement pst = null;

                            if (serverType.equalsIgnoreCase(serverTypeGI)) {
                                stypeQry = prop.getProperty("SAMTRANSFER_CHECKREQUESTOR_GI");
                                stypeQry = stypeQry.replace("${requestor}", requestor);
                                pst = con.prepareStatement(stypeQry);
                                logger.info("Executing statement: " + pst.toString());

                                ResultSet rs = pst.executeQuery();

                                if (rs.next()) {
                                } else {
                                    error = "Invalid Requestor (GI).";
                                }
                            } else if (serverType.equalsIgnoreCase(serverTypeNE)) {
                                stypeQry = prop.getProperty("SAMTRANSFER_CHECKREQUESTOR_NE");
                                stypeQry = stypeQry.replace("${requestor}", requestor);
                                pst = con.prepareStatement(stypeQry);
                                logger.info("Executing statement: " + pst.toString());

                                ResultSet rs = pst.executeQuery();

                                if (rs.next()) {
                                } else {
                                    error = "Invalid Requestor (NE).";
                                }
                            } else if (serverType.equalsIgnoreCase(serverTypeGB)) {
                                stypeQry = prop.getProperty("SAMTRANSFER_CHECKREQUESTOR_GB");
                                stypeQry = stypeQry.replace("${requestor}", requestor);
                                pst = con.prepareStatement(stypeQry);
                                logger.info("Executing statement: " + pst.toString());

                                ResultSet rs = pst.executeQuery();

                                if (rs.next()) {
                                } else {
                                    error = "Invalid Requestor (GB).";
                                }
                            }
                        }
                    }
                }
            } else {
                error = "Invalid number of input data (number of columns not matching).";
            }
        } catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
            error = "Error in checking Mandatory input";
        } finally {
            return error;
        }
    }

    public boolean isValidEmail(String email)
    {
        logger.info("Validating email...");

        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return pat.matcher(email).matches();
    }

    public boolean validateInput(Properties prop,String requestKey,
                                 String [] identityData)
    {
        boolean status = false;
        String fileErr = null;
        String lineErr = null;

        logger.info("Inside validateInput...");

        if (identityData == null) {
            fileErr = "Input file is missing in the request. Please check and try again!";
            logger.info("Input file is missing in the request.");
        } else if (identityData != null && identityData.length > 0) {
            String[] headerInfo = identityData[0].split(",");

            if (headerInfo != null) {
                String[] headerNeeded = prop.getProperty("SAMTRANSFER_IDENTITYFILE_HEADERS").split(",");
                int headerNeededCnt = Integer.parseInt(prop.getProperty("SAMTRANSFER_IDENTITYFILE_HEADERS_COUNT"));
                boolean wrongHeader = false;
                int i;
                if(headerInfo.length == headerNeededCnt) {
                    for (i = 0; i < headerNeededCnt; i++) {
                        headerInfo[i] = headerInfo[i].trim();
                        if(!headerInfo[i].equalsIgnoreCase(headerNeeded[i])) {
                            wrongHeader = true;
                            break;
                        }
                    }
                    if(wrongHeader) {
                        fileErr = "Error in Input file headers. (" + headerNeeded[i]
                                + ") not found. Please check and try again!";
                    }
                } else {
                    fileErr = "Error in Input file headers. Please check and try again!";
                    logger.info("Error in Input file headers.");
                }
            } else {
                fileErr = "Error in Input file headers. Please check and try again!";
                logger.info("Error in Input file headers.");
            }

            if (fileErr == null) {
                status = true;
                int i;
                lineErr = "";
                //logger.info("Total lines - " + identityData.length);
                //logger.info("Total header count - " + headerInfo.length);
                for (i = 1; i < identityData.length; i++) {
                    String[] dataInfo = identityData[i].split(",", -1);
                    //logger.info("Line # - " + i + " Length - " + dataInfo.length);

                    if (dataInfo.length != headerInfo.length) {
                        lineErr = lineErr + identityData[i] + "<br>";
                    }
                }
                if (lineErr != null) {
                    errInputLines.put(requestKey, lineErr);
                }
            }
        } else {
            fileErr = "Invalid Input file. Please check and try again!";
            logger.info("Invalid Input file.");
        }
        if (fileErr != null) {
            errInputFiles.put(requestKey, fileErr);
        }
        return status;
    }

    public boolean checkAndUpdateRequestSendEmail(Properties prop, Connection con, HashSet<String> transferKeys) {
        boolean res=true;
        logger.info("Inside checkAndUpdateRequestSendEmail...");
        logger.info("Transfer Requests - "+ transferKeys);

        try {
            if(transferKeys!=null && !transferKeys.isEmpty()) {
                Iterator<String> it = transferKeys.iterator();
                while(it.hasNext()){
                    String reqKey = it.next();
                    boolean allComplete = false;
                    logger.info("RequestKey - "+reqKey);

                    String selectSQL = prop.getProperty("SAMTRANSFER_CHECKTRANSFERSTATUS");
                    selectSQL = selectSQL.replace("${requestkey}",reqKey);

                    PreparedStatement pstmt = con.prepareStatement(selectSQL);
                    pstmt = con.prepareStatement(selectSQL);
                    logger.info("Executing statement: " + pstmt.toString());
                    ResultSet rs3 = pstmt.executeQuery();
                    logger.info("Statement executed successfully");

                    while (rs3.next()) {
                        allComplete = true;
                        String currowner = (String) rs3.getString("currowner");
                        String newowner = (String) rs3.getString("newowner");
                        String failedTaskKeys = (String) rs3.getString("failedtaskkeys");
                        Integer completed = (Integer) rs3.getInt("completed");
                        if (currowner != null &&  newowner != null && completed != null ) {
                            if(completed == 0) {
                                allComplete=false;
                                break;
                            }
                        }
                    }
                    if(allComplete) {
                        //Send email to requestor with the data.

                        //logger.info("ALL SUCCESSFUL");

                        String uptQuery = prop.getProperty("SAMTRANSFER_UPDATEREQUEST");
                        uptQuery = uptQuery.replace("${requestkey}",reqKey);
                        uptQuery = uptQuery.replace("${arstatus}", "3");
                        uptQuery = uptQuery.replace("${comments}", "Completed by Transfer utility");
                        PreparedStatement pstmt1 = con.prepareStatement(uptQuery);

                        logger.info("Executing statement: " + pstmt1.toString());
                        int result = pstmt1.executeUpdate();
                        logger.info("Statement executed successfully");
                    }
                }
            }
        }
        catch(SQLException e) {
            logger.error("Exception - " +  e.getMessage(), e);
            res=false;
        }
        finally {
            return res;
        }
    }

    public boolean updateRequestInvalidData(Properties prop,Connection con) {
        boolean res=true;
        logger.info("Inside updateRequestInvalidData...");
        logger.info("inputErr - "+ errInputFiles.toString());

        try {
            if(errInputFiles !=null && !errInputFiles.isEmpty()) {
                for (Map.Entry<String, String> err : errInputFiles.entrySet()) {
                    String uptQuery = prop.getProperty("SAMTRANSFER_UPDATEREQUEST");
                    uptQuery = uptQuery.replace("${requestkey}", err.getKey());
                    uptQuery = uptQuery.replace("${arstatus}", "6");
                    uptQuery = uptQuery.replace("${comments}", err.getValue());

                    PreparedStatement pstmt1 = con.prepareStatement(uptQuery);

                    logger.info("Executing statement: " + pstmt1.toString());
                    int result = pstmt1.executeUpdate();
                    logger.info("Statement executed successfully");
                }
            }
        }
        catch(SQLException e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
        finally
        {
            return res;
        }
    }
}
