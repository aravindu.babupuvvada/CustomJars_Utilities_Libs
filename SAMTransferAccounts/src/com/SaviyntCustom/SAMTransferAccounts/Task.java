package com.SaviyntCustom.SAMTransferAccounts;

public class Task {
    private String taskKey;
    private String accName;
    private String accKey;
    private String endpointKey;
    private String endpointName;
    private String status;

    public Task() {
    }

    public Task(String taskKey, String accName, String accKey, String endpointKey, String endpointName, String status) {
        this.taskKey = taskKey;
        this.accKey = accKey;
        this.accKey = accKey;
        this.endpointKey = endpointKey;
        this.endpointName = endpointName;
        this.status = status;
    }

    public String getTaskKey() {
        return taskKey;
    }

    public String getAccName() {
        return accName;
    }

    public void setAccName(String accName) {
        this.accName = accName;
    }

    public String getEndpointName() {
        return endpointName;
    }

    public void setEndpointName(String endpointName) {
        this.endpointName = endpointName;
    }

    public void setTaskKey(String taskKey) {
        this.taskKey = taskKey;
    }

    public String getAccKey() {
        return accKey;
    }

    public void setAccKey(String accKey) {
        this.accKey = accKey;
    }

    public String getEndpointKey() {
        return endpointKey;
    }

    public void setEndpointKey(String endpointKey) {
        this.endpointKey = endpointKey;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}