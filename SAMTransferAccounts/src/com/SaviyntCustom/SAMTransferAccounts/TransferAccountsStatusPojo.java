package com.SaviyntCustom.SAMTransferAccounts;

public class TransferAccountsStatusPojo {
    String requestKey;
    String requestID;
    String requestor;
    String currOwner;
    String newOwner;
    //String action;
    String businessJustification;
    //String reportEmail;
    String serverType;
    int error;
    StringBuilder comments=new StringBuilder();
    int totalAccounts;
    String accKeys;
    int totalGroups;
    String grpKeys;
    int totalTasks;
    int totalSuccessTasks;
    boolean proceed;
    boolean complete;
    boolean emailSent;
    boolean reportEmailSent;
    String  taskKeys;
    String failedTaskKeys;
    String emailBody;
}
