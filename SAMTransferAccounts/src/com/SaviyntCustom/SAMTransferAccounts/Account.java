package com.SaviyntCustom.SAMTransferAccounts;

public class Account {
    private String accName;
    private String accKey;
    private String accStatus;
    private String accType;
    private String endpointKey;
    private String ssKey;
    private String endpoint;
    private String currOwner;

    public Account() {
    }

    public Account(String accName, String accKey, String accStatus, String accType,
                   String endpointKey, String ssKey, String endpoint, String currOwner) {
        this.accName = accName;
        this.accKey = accKey;
        this.accStatus = accStatus;
        this.accType = accType;
        this.endpointKey = endpointKey;
        this.ssKey = ssKey;
        this.endpoint = endpoint;
        this.currOwner = currOwner;
    }

    public String getAccName() {
        return accName;
    }

    public void setAccName(String accName) {
        this.accName = accName;
    }

    public String getAccType() {
        return accType;
    }

    public void setAccType(String accType) {
        this.accType = accType;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getCurrOwner() {
        return currOwner;
    }

    public void setCurrOwner(String currOwner) {
        this.currOwner = currOwner;
    }

    public String getAccKey() {
        return accKey;
    }

    public void setAccKey(String accKey) {
        this.accKey = accKey;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }

    public String getEndpointKey() {
        return endpointKey;
    }

    public void setEndpointKey(String endpointKey) {
        this.endpointKey = endpointKey;
    }

    public String getSsKey() {
        return ssKey;
    }

    public void setSsKey(String ssKey) {
        this.ssKey = ssKey;
    }
// getters and setters
}