package com.SaviyntCustom.FedrampUserUpdate;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class AESCrypt {
    public static String decryptIt(String strToDecrypt) {
        byte[] key = "S@v!ynt_s@V!YNt_".getBytes();
        //byte[] key = "thrinathtransfer".getBytes();
        if(strToDecrypt !=null ) {
            try {

                Cipher cipher  = Cipher.getInstance("AES/ECB/PKCS5PADDING");
                final SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
                cipher.init(Cipher.DECRYPT_MODE, secretKey);
                final String decryptedString = new String(cipher.doFinal(Base64.decodeBase64(strToDecrypt)));
                return decryptedString;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    public static String encryptIt(String strToEncrypt) {
        byte[] key = "S@v!ynt_s@V!YNt_".getBytes();
        //byte[] key = "thrinathtransfer".getBytes();
        try {
            // Test with CBC or CTR mode.

            Cipher cipher  = Cipher.getInstance("AES/ECB/PKCS5Padding");
            final SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            final String encryptedString = Base64.encodeBase64String(cipher.doFinal(strToEncrypt.getBytes()));
            return encryptedString;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }


    public static void main (String args[]) {
        String b = decryptIt("UbTenyTwfRKWSigeYF82FLPyTDSeC1javBA5IYhg9sA=");
        //System.out.println(a);
        System.out.println(b);
    }
}
