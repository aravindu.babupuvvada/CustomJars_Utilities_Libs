package com.SaviyntCustom.FedrampUserUpdate;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.sql.*;
import java.util.*;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.net.ssl.HttpsURLConnection;

public class UserUpdate {

    Logger logger = null;
    Properties prop = new Properties();

    HashMap<String, HashMap> frAllUsers = new HashMap<>();
    HashMap<String, String> userAttrsMapping = new HashMap<>();
    HashMap<String, String> cpsMapping = new HashMap<>();

    public UserUpdate() {
        String saviyntHome = System.getenv("SAVIYNT_HOME");
        //System.setProperty("LOG_PATH", saviyntHome);
        PropertyConfigurator.configure(saviyntHome + File.separator + "customJarsLog4j.properties");
        logger = Logger.getLogger("fedUserUpdateLogger");
    }

    public static void main(String args []) {
        System.out.println("***** Fedramp user update start *****\n");

        UserUpdate uu = new UserUpdate();
        uu.startUpdate();

    }

    public void startUpdate() {
        logger.info("***** Fedramp user update start *****\n");
        InputStream inputStream = null;

        try {
            String saviyntHome = System.getenv("SAVIYNT_HOME");
            String propFileName = saviyntHome + File.separator + "fedrampUserUpdateConfig.properties";
            inputStream = new FileInputStream(propFileName);
            prop.load(inputStream);

            // Fetch all active fedramp Users
            if(fetchAllFedrampUsers(prop)) {
                /*
                for (Map.Entry<String, HashMap> set : frAllUsers.entrySet()) {
                    HashMap<String,String> userDetails = set.getValue();
                    //logger.info(set.getKey() + " = " + set.getValue());

                    for (Map.Entry<String, String> user : userDetails.entrySet()) {
                        logger.info(user.getKey() + " = " + user.getValue());
                    }
                    logger.info("---------------------------------------------------------------");
                }
                */
                fetchEdDataAndProcess(prop);
            } else {
                logger.error("Error while reading Fedramp user details.");
            }
        } catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
    }

    public boolean fetchAllFedrampUsers(Properties prop) {
        boolean status=false;
        logger.info("***** Fetching all Fedramp users *****");

        try {
            for (String cpKey : prop.stringPropertyNames()) {
                String labelVal = prop.getProperty(cpKey);
                if (cpKey.startsWith("AHDB_CP_MAPPING_")) {
                    cpsMapping.put(labelVal, cpKey.replace("AHDB_CP_MAPPING_", ""));
                }
            }
            //logger.info("cpsMapping - " + cpsMapping.toString());

            String url = prop.getProperty("APIURL") + "api/v5/getUser?max=10000";
            HttpUrlConn httpConn = new HttpUrlConn();

            HttpsURLConnection con = null;
            JSONObject getApiResponse = null;
            JSONParser parser = new JSONParser();
            JSONObject json = null;
            String getUserStatus = null;
            String getUserCount = null;
            JSONArray userDetails = null;

            TokenPojo token = new TokenPojo();
            String outhToken = token.callBearerToken(prop);

            JSONObject inputJson =new JSONObject();
            inputJson.put("userQuery", prop.getProperty("USERFETCHQUERY"));
            String [] arrayStr=prop.getProperty("USERFIELDS").split(",");
            JSONArray respFieldsJSONArray = new JSONArray();
            for (String s: arrayStr){
                respFieldsJSONArray.add(s);
            }
            //System.out.println(respFieldsJSONArray.toString());
            inputJson.put("responsefields", respFieldsJSONArray);

            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            httpConnectionHeaders.put("Content-Type", "application/json");
            logger.info("inputJson - " + inputJson.toString());
            con = httpConn.sendPostHttps(url, inputJson.toString(), httpConnectionHeaders);
            int code = con.getResponseCode();

            getApiResponse = httpConn.fetchJsonObject(con);
            if(getApiResponse != null) {
                logger.info("Response JSON - " + getApiResponse.toString());
                json = (JSONObject) parser.parse(getApiResponse.toString());
                getUserStatus = (String) json.get("msg");
                getUserCount = (String) json.get("displaycount");
            }
            logger.info("Get User API status : " + getUserStatus);

            if( (json!=null)
                && (getUserStatus!=null && getUserStatus.equalsIgnoreCase("Successful"))
                && (getUserCount!=null && !getUserCount.equalsIgnoreCase("0")) ) {
                logger.info("Parsing all user details.");

                HashMap<String, String> frUserDetails = null;
                userDetails = (JSONArray) json.get("userdetails");
                if(userDetails!=null) {
                    for (int i = 0; i < userDetails.size(); i++) {
                        JSONObject user = (JSONObject) userDetails.get(i);
                        frUserDetails = new HashMap<>();

                        String keyUser = (String) user.get(prop.getProperty("FRUSERNAME"));
                        Iterator<String> keysItr = user.keySet().iterator();
                        while(keysItr.hasNext()) {
                            String key = keysItr.next();
                            String value = (String) user.get(key).toString();

                            //key = key.replace(" ","").toLowerCase();
                            String cpKey = key;
                            if(cpsMapping.containsKey(key)) {
                                cpKey = cpsMapping.get(key);
                            }
                            frUserDetails.put(cpKey, value);
                        }
                        for (String s: arrayStr) {
                            if(!frUserDetails.containsKey(s)) {
                                frUserDetails.put(s,"");
                            }
                        }
                        if(frUserDetails!=null && keyUser!=null) {
                            logger.info("frUserDetails - " + frUserDetails.toString());
                            frAllUsers.put(keyUser, frUserDetails);
                        }
                        status = true;
                        /*
                        System.out.println(user.get("firstname"));
                        System.out.println(user.get("displayname"));
                        System.out.println(user.get("userKey"));
                        System.out.println(user.get("email"));
                        System.out.println(user.get("username"));
                        System.out.println(user.get("lastname"));
                        */
                    }
                }
            } else {
                logger.error("Get User API Failed.");
            }
        } catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        } finally {
            return status;
        }
    }

    public boolean createUserUpdateRequest(Properties prop, String username) {
        boolean status=false;
        logger.info("Inside createUserUpdateRequest.");

        try {
            String url = prop.getProperty("APIURL") + "api/v5/updateUserRequest";
            HttpUrlConn httpConn = new HttpUrlConn();

            HttpsURLConnection con = null;
            JSONObject getApiResponse = null;
            JSONParser parser = new JSONParser();
            JSONObject json = null;
            String userUpdateStatus = null;
            String requestId = null;

            TokenPojo token = new TokenPojo();
            String outhToken = token.callBearerToken(prop);

            JSONObject inputJson =new JSONObject();
            inputJson.put("updateuser", prop.getProperty("SAVUSERNAME"));
            inputJson.put("username", username);
            inputJson.put("statuskey", 0);

            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            httpConnectionHeaders.put("Content-Type", "application/json");
            logger.info("inputJson - " + inputJson.toString());
            con = httpConn.sendPutHttps(url, inputJson.toString(), httpConnectionHeaders);
            int code = con.getResponseCode();

            getApiResponse = httpConn.fetchJsonObject(con);
            if(getApiResponse != null) {
                json = (JSONObject) parser.parse(getApiResponse.toString());
                userUpdateStatus = (String) json.get("msg");
                requestId = (String) json.get("requestid");
            }
            logger.info("Update User Request API status : " + userUpdateStatus);

            if( (json!=null)
                    && (userUpdateStatus!=null && userUpdateStatus.equalsIgnoreCase("success"))
                    && requestId!=null ) {
                status = true;
                logger.info("User Update Request has been created successfully for "
                        + username + ". Request ID - " + requestId + ".");
            } else {
                logger.info("User Update Request Failed. Username - " + username);
            }
        } catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        } finally {
            return status;
        }
    }

    public boolean userUpdateRequestPresent(Properties prop, String username) {
        boolean status=false;
        logger.info("Inside userUpdateRequestPresent.");

        try {
            String url = prop.getProperty("APIURL") + "api/v5/fetchRequestHistory";
            HttpUrlConn httpConn = new HttpUrlConn();

            HttpsURLConnection con = null;
            JSONObject getApiResponse = null;
            JSONParser parser = new JSONParser();
            JSONObject json = null;
            String fetchReqHistStatus = null;
            Long totalCnt = null;

            TokenPojo token = new TokenPojo();
            String outhToken = token.callBearerToken(prop);

            JSONObject inputJson =new JSONObject();
            inputJson.put("username", prop.getProperty("SAVUSERNAME"));
            JSONArray statusArr = new JSONArray();
            statusArr.add("open");
            inputJson.put("status", statusArr);
            inputJson.put("requestedfor", username);
            JSONArray reqTypeArr = new JSONArray();
            reqTypeArr.add("UPDATEUSER");
            inputJson.put("requesttype", reqTypeArr);

            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            httpConnectionHeaders.put("Content-Type", "application/json");
            logger.info("url - " + url + ".\n inputJson - " + inputJson.toString());
            con = httpConn.sendPostHttps(url, inputJson.toString(), httpConnectionHeaders);
            int code = con.getResponseCode();

            getApiResponse = httpConn.fetchJsonObject(con);
            if(getApiResponse != null) {
                json = (JSONObject) parser.parse(getApiResponse.toString());
                fetchReqHistStatus = (String) json.get("msg");
                totalCnt = (Long) json.get("totalcount");
            }
            logger.info("Fetch Request Histoy API status : " + fetchReqHistStatus);

            if( (json!=null)
                    && (fetchReqHistStatus!=null && fetchReqHistStatus.equalsIgnoreCase("Successful"))
                    && (totalCnt!=null && totalCnt > 0) ) {
                status = true;
                logger.info("There are pending user update request. Username - " + username);
            } else {
                logger.info("There are no user update request for  " + username);
            }
        } catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
            logger.info("Error occured while checking if user update request is present.");
            status = true;
        } finally {
            return status;
        }
    }


    public boolean updateUser(Properties prop, HashMap<String, String> userDetails) {
        boolean status=false;
        logger.info("Inside updateUser.");

        try {
            String url = prop.getProperty("APIURL") + "api/v5/updateUser";
            HttpUrlConn httpConn = new HttpUrlConn();

            HttpsURLConnection con = null;
            JSONObject getApiResponse = null;
            JSONParser parser = new JSONParser();
            JSONObject json = null;
            String userUpdateStatus = null;
            String errorCode = null;

            TokenPojo token = new TokenPojo();
            String outhToken = token.callBearerToken(prop);

            JSONObject inputJson =new JSONObject();
            for (Map.Entry<String, String> user : userDetails.entrySet()) {
                //logger.info(user.getKey() + " = " + user.getValue());
                inputJson.put(user.getKey(), user.getValue());
            }
            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            httpConnectionHeaders.put("Content-Type", "application/json");
            logger.info("inputJson - " + inputJson.toString());
            con = httpConn.sendPostHttps(url, inputJson.toString(), httpConnectionHeaders);
            int code = con.getResponseCode();

            getApiResponse = httpConn.fetchJsonObject(con);
            if(getApiResponse != null) {
                json = (JSONObject) parser.parse(getApiResponse.toString());
                userUpdateStatus = (String) json.get("message");
                errorCode = (String) json.get("errorCode");
            }
            logger.info("Update User API status : " + userUpdateStatus);

            if( (json!=null)
                    && (userUpdateStatus!=null && userUpdateStatus.toUpperCase().contains("SUCCESS"))
                    && (errorCode!=null && errorCode.equalsIgnoreCase("0")) ) {
                status = true;
                logger.info("Update user success.");
            } else {
                logger.info("Update user Failed.");
            }
        } catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        } finally {
            return status;
        }
    }


    public void fetchEdDataAndProcess(Properties prop) {
        logger.info("Inside fetchEdDataAndProcess.");

        for (String attrKey : prop.stringPropertyNames()) {
            String ahvalue = prop.getProperty(attrKey);
            if (attrKey.startsWith("AHDB_MAPPING_")) {
                userAttrsMapping.put(attrKey.replace("AHDB_MAPPING_", "").toLowerCase(), ahvalue);
            }
        }
        HashMap<String, String> bpUserAttrs = new HashMap<>();

        for (Map.Entry<String, HashMap> set : frAllUsers.entrySet()) {
            try {
                HashMap<String, String> ahUserAttrs = set.getValue();
                String frUsername = set.getKey();
                String bpUsername = ahUserAttrs.get(prop.getProperty("BPUSERNAME"));

                String url = prop.getProperty("BPGETUSERAPI");
                url=url.replace("${bpusername}",bpUsername);
                bpUserAttrs.clear();

                logger.info("***** Validating data for Fedramp username - " + frUsername
                                + ", BP username - " + bpUsername + " *****");

                HttpUrlConn httpConn = new HttpUrlConn();
                HttpsURLConnection con = null;
                JSONObject getApiResponse = null;
                JSONParser parser = new JSONParser();
                JSONObject json = null;
                int code = 0;

                Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
                httpConnectionHeaders.put("Content-Type", "application/json");
                logger.info("BP URL - " + url);
                con = httpConn.sendGetHttps(url, httpConnectionHeaders);
                code = con.getResponseCode();

                logger.info("Response code from BP URL - " + code);

                getApiResponse = httpConn.fetchJsonObject(con);
                if(getApiResponse != null) {
                    json = (JSONObject) parser.parse(getApiResponse.toString());
                }

                //testing
                //code = 200;
                //json = (JSONObject) parser.parse(
                //        new FileReader(System.getenv("SAVIYNT_HOME") + File.separator + "s1.txt"));

                if(code==200 && json!=null) {
                    // A JSON object. Key value pairs are unordered. JSONObject supports java.util.Map interface.
                    JSONObject searchObject = (JSONObject) json.get("search");
                    JSONObject returnObject = (JSONObject) searchObject.get("return");
                    String message = (String) returnObject.get("message");
                    Long count = (Long) returnObject.get("count");

                    if ((message != null && message.equalsIgnoreCase("success"))
                            && (count != null && count == 1)) {
                        //Success flow
                        JSONArray entries = (JSONArray) searchObject.get("entry");
                        if (entries != null) {
                            Iterator<JSONObject> iterator = entries.iterator();
                            if (iterator.hasNext()) {
                                //Read the user attributes
                                JSONArray attributeArr = (JSONArray) iterator.next().get("attribute");
                                if (attributeArr != null) {
                                    //Parse through all attributes and read only required ones
                                    Iterator<JSONObject> iterator1 = attributeArr.iterator();
                                    while (iterator1.hasNext()) {
                                        JSONObject attObject = (JSONObject) iterator1.next();
                                        String attName = (String) attObject.get("name");
                                        //Compare the attribute with configured list
                                        String[] edFields = prop.getProperty("EDFIELDS_TOUPDATE").toLowerCase().split(",");
                                        if (Arrays.asList(edFields).contains(attName.toLowerCase())) {
                                            //Store the key and value to map to compare with data from AH DB
                                            JSONArray attValArr = (JSONArray) attObject.get("value");
                                            if (attValArr != null) {
                                                Iterator<String> iterator2 = attValArr.iterator();
                                                if (iterator2.hasNext()) {
                                                    if (userAttrsMapping.containsKey(attName.toLowerCase())) {
                                                        String val = iterator2.next();
                                                        logger.info("BP attr = " + attName + ", Value = " + val
                                                                + ". AH mapped attr = " + userAttrsMapping.get(attName.toLowerCase()));
                                                        bpUserAttrs.put(userAttrsMapping.get(attName.toLowerCase()), val);
                                                        if (attName.equalsIgnoreCase("callupName")) {
                                                            String names[] = val.split(",");
                                                            bpUserAttrs.put("firstname", names[1].trim());
                                                            bpUserAttrs.put("lastname", names[0].trim());
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //Completed reading all attributes. Compare the value and see update is needed
                        if(!compareUserDetails(ahUserAttrs, bpUserAttrs)) {
                            logger.info("AH and BP data do not match. Updating user data in AH.");
                            bpUserAttrs.put(prop.getProperty("FRUSERNAME"), frUsername);
                            if(updateUser(prop, bpUserAttrs)) {
                                logger.info("Updated user attributes. User - " + frUsername);
                            } else {
                                logger.error("Update user attributes failed. User - " + frUsername);
                            }
                        } else {
                            logger.info("AH and BP data are same. Not updating user data in AH.");
                        }

                    } else {
                        //No record or no response. Create update user request to set status as inactive
                        if(!userUpdateRequestPresent(prop,frUsername)) {
                            if (createUserUpdateRequest(prop, frUsername)) {
                                logger.info("User update request has been created successfully for " + frUsername);
                            } else {
                                logger.error("Error while creating User update request for " + frUsername);
                            }
                        } else {
                            logger.info("User Update request already present for the user." +
                                    " Not creating the request. Username - " + frUsername);
                        }
                    }
                } else {
                    //Error while reading data. Nothing to be done.
                    logger.error("Error while reading Blue page data for user - " + frUsername + ".");
                }
            } catch(Exception e) {
                logger.error("Exception - " +  e.getMessage(), e);
            }
        }
    }

    public boolean compareUserDetails (HashMap<String, String> ahData, HashMap<String, String> bpData) {
        logger.info("Inside compareUserDetails.");
        boolean status = true;

        try {
            for (Map.Entry<String, String> bpUserAtt : bpData.entrySet()) {
                logger.info("BP data - Mapped Attr = " + bpUserAtt.getKey() + ", Value = " + bpUserAtt.getValue());
                if(ahData.containsKey(bpUserAtt.getKey())) {
                    String ahUserAtt = ahData.get(bpUserAtt.getKey());
                    logger.info("AH data - Attr = " + bpUserAtt.getKey() + ", Value = " + ahUserAtt);
                    if(!ahUserAtt.equals(bpUserAtt.getValue())) {
                        status = false;
                        break;
                    }
                }
            }
        }
        catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        } finally {
            return status;
        }
    }
}
