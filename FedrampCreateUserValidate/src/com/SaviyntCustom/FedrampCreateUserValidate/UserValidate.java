package com.SaviyntCustom.FedrampCreateUserValidate;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.*;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.net.ssl.HttpsURLConnection;

public class UserValidate {

    Logger logger = null;
    Properties prop = new Properties();
    public String outhToken;

    HashMap<String, HashMap> frAllUsers = new HashMap<>();

    public UserValidate() {
        String saviyntHome = System.getenv("SAVIYNT_HOME");
        //System.setProperty("LOG_PATH", saviyntHome);
        PropertyConfigurator.configure(saviyntHome + File.separator + "customJarsLog4j.properties");
        logger = Logger.getLogger("fedUserCreateValidateLogger");
    }

    public static void main(String args []) {
        System.out.println("***** Fedramp user create validation start *****\n");

        UserValidate uu = new UserValidate();
        uu.startUpdate();

    }

    public void startUpdate() {
        logger.info("***** Fedramp user create validation start *****\n");
        InputStream inputStream = null;

        try {
            String saviyntHome = System.getenv("SAVIYNT_HOME");
            String propFileName = saviyntHome + File.separator + "fedrampUserUpdateConfig.properties";
            inputStream = new FileInputStream(propFileName);
            prop.load(inputStream);

            /*
            // Fetch all active fedramp Users
            if(fetchAllFedrampUsers(prop)) {
                fetchEdDataAndProcess(prop);
            } else {
                logger.error("Error while reading Fedramp user details.");
            }
            */
            getPendingRequests(prop);
        } catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
    }

    public void getPendingRequests(Properties prop)
    {
        try {
            String url=prop.getProperty("APIURL")+"api/v5/getPendingRequests";
            HttpUrlConn httpConn = new HttpUrlConn();
            HashMap<String,String> requestsList=new HashMap<>();
            HashMap<String,String> requestorList=new HashMap<>();
            HttpURLConnection con1 = null;
            JSONObject getPendingRequestsResponse = null;
            JSONObject jsonRequest = new JSONObject();
            JSONArray reqTypeArray=new JSONArray();
            reqTypeArray.add(0,"CREATEUSER");
            jsonRequest.put("requesttype",reqTypeArray);
            jsonRequest.put("max",prop.getProperty("MAXREQUESTS"));
            jsonRequest.put("assignee",prop.getProperty("ASSIGNEE"));

            TokenPojo token = new TokenPojo();
            outhToken = token.callBearerToken(prop);

            // add header
            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();

            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            httpConnectionHeaders.put("Content-Type", "application/json");
            httpConnectionHeaders.put("SAVUSERNAME", prop.getProperty("SAVUSERNAME"));
            String urlParams = jsonRequest.toString();

            logger.info("\n ***** Reading Pending request : " + url + ". Input : " + urlParams);
            con1 = httpConn.sendPostHttps(url, jsonRequest.toString(), httpConnectionHeaders);
            getPendingRequestsResponse = httpConn.fetchJsonObject(con1);
            logger.info("getPendingRequestsResponse - " + getPendingRequestsResponse.toString());

            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(getPendingRequestsResponse.toString());

            Long totalRequests = (Long)json.get("total");
            JSONArray requestDetails = (JSONArray)json.get("results");

            if(totalRequests != null) {
                //int totalreq = Integer.parseInt(totalRequests);
                logger.info("Total pending requests " + totalRequests);

                for (int i = 0; i < totalRequests; i++) {
                    boolean process = false;
                    JSONObject req = (JSONObject) requestDetails.get(i);

                    Long requestkey = (Long) req.get("requestkey");
                    //logger.info("The requestkey is " + requestkey);
                    String requestid = (String) req.get("requestid");
                    //String endpoints = (String) req.get("endpoints");

                    requestsList.put(String.valueOf(requestkey), requestid);
                    //getRequestDetails(requestkey,requesteeusername,endpoints,outhToken,ldapEntitlements);
                }

                processRequests(prop, requestsList);
            }

        } catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
    }

    public void approveRejectRequest(Properties prop, Map.Entry<String, String> req, String action, String comments)
    {
        try {
            String url=prop.getProperty("APIURL")+"api/v5/approveRejectRequest";
            HttpUrlConn httpConn = new HttpUrlConn();
            HttpURLConnection con = null;
            JSONObject getApiResponse = null;
            String apiStatus = null;
            JSONParser parser = new JSONParser();
            JSONObject json = null;
            JSONObject jsonRequest = new JSONObject();
            jsonRequest.put("requestkey",req.getKey());
            jsonRequest.put("requestid",req.getValue());
            jsonRequest.put("reqaction",action);
            jsonRequest.put("approver",prop.getProperty("ASSIGNEE"));
            jsonRequest.put("comments",comments);

            // add header
            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();

            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            httpConnectionHeaders.put("Content-Type", "application/json");
            String urlParams = jsonRequest.toString();

            logger.info("\n ***** Reading Pending request : " + url + ". Input : " + urlParams);
            con = httpConn.sendPostHttps(url, jsonRequest.toString(), httpConnectionHeaders);
            int code = con.getResponseCode();
            if(code == 401) {
                refreshToken(prop);
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                con = httpConn.sendPostHttps(url, jsonRequest.toString(), httpConnectionHeaders);
            }

            getApiResponse = httpConn.fetchJsonObject(con);
            if (getApiResponse != null) {
                json = (JSONObject) parser.parse(getApiResponse.toString());
                apiStatus = (String) json.get("message");
            }
            logger.info("getApiResponse - " + getApiResponse.toString());
            logger.info("API status : " + apiStatus);

            if ((json != null)
                    && (apiStatus != null && apiStatus.equalsIgnoreCase("SUCCESS"))) {
                logger.info("Calling API to approve/reject is successful. Req ID - " + req.getValue()
                            + ". Action - " + action);
            } else {
                logger.info("Calling API to approve/reject failed. Req ID - " + req.getValue()
                        + ". Action - " + action);
            }
        } catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
    }


    public void refreshToken(Properties prop) {
        try {
            TokenPojo token = new TokenPojo();
            outhToken = token.callBearerToken(prop);
        } catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        }
    }

    public boolean processRequests(Properties prop, HashMap<String,String> requestsList) {
        boolean status=false;
        logger.info("Inside processRequests.");

        try {
            HashMap<String, String> dynAttrsMapping = new HashMap<>();
            for (String attrkey : prop.stringPropertyNames()) {
                String edfield = prop.getProperty(attrkey);
                if (attrkey.startsWith("USERCREATE_DYNATTR_")) {
                    dynAttrsMapping.put(attrkey.replace("USERCREATE_DYNATTR_", "").trim(), edfield.trim());
                }
            }
            //logger.info("dynAttrsMapping - " + dynAttrsMapping.toString());

            String url = prop.getProperty("APIURL") + "api/v5/fetchRequestHistoryDetails";
            HttpUrlConn httpConn = new HttpUrlConn();

            HttpsURLConnection con = null;
            JSONObject getApiResponse = null;
            JSONParser parser = new JSONParser();
            JSONObject json = null;
            String getReqDetailsStatus = null;

            HashMap<String, String> reqDynAttrs = new HashMap<>();

            for (Map.Entry<String, String> req : requestsList.entrySet()) {
                String reqId = req.getValue();
                String reqKey = req.getKey();

                reqDynAttrs.clear();

                String urlParam = "requestkey=" + reqKey;
                logger.info("Calling to fetch req details. URL - " + url
                            + ". Input - " + urlParam);

                Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                httpConnectionHeaders.put("Content-Type", "application/x-www-form-urlencoded");
                con = httpConn.sendPostHttps(url, urlParam, httpConnectionHeaders);
                int code = con.getResponseCode();
                if(code == 401) {
                    refreshToken(prop);
                    httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                    con = httpConn.sendPostHttps(url, urlParam, httpConnectionHeaders);
                }

                getApiResponse = httpConn.fetchJsonObject(con);
                if (getApiResponse != null) {
                    json = (JSONObject) parser.parse(getApiResponse.toString());
                    getReqDetailsStatus = (String) json.get("msg");
                }
                logger.info("getApiResponse - " + getApiResponse.toString());
                logger.info("Get Request Details API status : " + getReqDetailsStatus);

                if ((json != null)
                        && (getReqDetailsStatus != null && getReqDetailsStatus.equalsIgnoreCase("SUCCESS"))) {
                    logger.info("Parsing all user details.");
                    JSONObject requestHistoryDetails = (JSONObject) json.get("requestHistoryDetails");
                    JSONArray dynAttrs = (JSONArray) requestHistoryDetails.get("dynamicattributes");

                    String bpUsername = null;

                    if (dynAttrs != null) {
                        logger.info("dynAttrs - " + dynAttrs.toString());

                        Iterator<JSONObject> it = dynAttrs.iterator();
                        while (it.hasNext()) {
                            JSONObject attObject = (JSONObject) it.next();
                            String attName = (String) attObject.get("name");
                            String attVal = (String) attObject.get("value");
                            if (dynAttrsMapping.containsKey(attName)) {
                                reqDynAttrs.put(attName,attVal);
                            } else if (prop.getProperty("USERCREATE_BPUSERNAME").equalsIgnoreCase(attName)) {
                                bpUsername = attVal;
                            }
                        }
                    }

                    checkAndProcessRequest(prop, req, dynAttrsMapping, bpUsername, reqDynAttrs);

                } else {
                    logger.error("Get Request Details API Failed.");
                }
            }
        } catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
        } finally {
            return status;
        }
    }
    String getKey (HashMap<String,String> dynAttrsMapping, String value) {
        for (Map.Entry<String,String> entry : dynAttrsMapping.entrySet()) {
            if (Objects.equals(value, entry.getValue())) {
                return entry.getKey();
            }
        }
        return  null;
    }

    public boolean checkAndProcessRequest(Properties prop, Map.Entry<String,String> req, HashMap<String,String> dynAttrsMapping,
                                          String bpUsername, HashMap<String,String> reqDynAttrs) {
        boolean status=false;
        logger.info("Inside checkAndProcessRequest.");

        try {
            //Get ED data and compare
            HashMap<String, String> bpData = new HashMap<>();
            String url = prop.getProperty("BPGETUSERAPI");
            url=url.replace("${bpusername}",bpUsername);

            HttpUrlConn httpConn = new HttpUrlConn();
            HttpsURLConnection con = null;
            JSONObject getApiResponse = null;
            JSONParser parser = new JSONParser();
            JSONObject json = null;
            int code = 0;


            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            httpConnectionHeaders.put("Content-Type", "application/json");
            logger.info("BP URL - " + url);
            con = httpConn.sendGetHttps(url, httpConnectionHeaders);
            code = con.getResponseCode();

            logger.info("Response code from BP URL - " + code);

            getApiResponse = httpConn.fetchJsonObject(con);
            if(getApiResponse != null) {
                json = (JSONObject) parser.parse(getApiResponse.toString());
            }

            //testing
            //code = 200;
            //json = (JSONObject) parser.parse(
            //                  new FileReader(System.getenv("SAVIYNT_HOME") + File.separator + "s1.txt"));

            if(code==200 && json!=null) {
                // A JSON object. Key value pairs are unordered. JSONObject supports java.util.Map interface.
                JSONObject searchObject = (JSONObject) json.get("search");
                JSONObject returnObject = (JSONObject) searchObject.get("return");
                String message = (String) returnObject.get("message");
                Long count = (Long) returnObject.get("count");

                if ((message != null && message.equalsIgnoreCase("success"))
                        && (count != null && count == 1)) {
                    //Success flow
                    JSONArray entries = (JSONArray) searchObject.get("entry");
                    if (entries != null) {
                        Iterator<JSONObject> iterator = entries.iterator();
                        if (iterator.hasNext()) {
                            //Read the user attributes
                            JSONArray attributeArr = (JSONArray) iterator.next().get("attribute");
                            if (attributeArr != null) {
                                //Parse through all attributes and read only required ones
                                Iterator<JSONObject> iterator1 = attributeArr.iterator();
                                while (iterator1.hasNext()) {
                                    JSONObject attObject = (JSONObject) iterator1.next();
                                    String attName = (String) attObject.get("name");
                                    if (dynAttrsMapping.containsValue(attName)) {
                                        //Store the key and value to map to compare with data from AH DB
                                        JSONArray attValArr = (JSONArray) attObject.get("value");
                                        String dynAttrKey = getKey(dynAttrsMapping, attName);
                                        String dynAttrVal = reqDynAttrs.get(dynAttrKey);
                                        if (attValArr != null) {
                                            Iterator<String> iterator2 = attValArr.iterator();
                                            if (iterator2.hasNext()) {
                                                String bpVal = iterator2.next();
                                                logger.info("BP value = " + bpVal + ", Dyn attr val = " + dynAttrVal);
                                                bpData.put(dynAttrKey, bpVal);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //Completed reading all attributes. Compare the value and approve/reject
                    if(!compareAttrDetails(reqDynAttrs, bpData)) {
                        logger.info("AH and BP data do not match. Rejecting request.");
                        approveRejectRequest(prop, req, "2", prop.getProperty("REJECTCOMMENTS"));
                    } else {
                        logger.info("AH and BP data are same. Approving request.");
                        approveRejectRequest(prop, req, "1", prop.getProperty("APPROVECOMMENTS"));

                    }
                } else {
                    //No record or no response. Reject the request
                    logger.info("No record found. Rejecting the request.");
                    approveRejectRequest(prop, req, "2", prop.getProperty("REJECTCOMMENTS"));
                }
            } else {
                //Error while reading data. Nothing to be done.
                logger.error("Error while reading Blue page data for user - " + bpUsername + ".");
            }
        } catch(Exception e) {
            logger.error("Error while reading Blue page data for user - " + bpUsername + ".");
            logger.error("Exception - " +  e.getMessage(), e);
        } finally {
            return status;
        }
    }


    public boolean compareAttrDetails (HashMap<String, String> ahReqData, HashMap<String, String> bpData) {
        logger.info("Inside compareUserDetails.");
        boolean status = true;

        try {
            for (Map.Entry<String, String> bpUserAtt : bpData.entrySet()) {
                String ahUserAtt = ahReqData.get(bpUserAtt.getKey());
                logger.info("Attr = " + bpUserAtt.getKey() + ", BP data = " + bpUserAtt.getValue() +
                        ", AH req data - " + ahUserAtt);

                if(bpUserAtt.getKey().equalsIgnoreCase(prop.getProperty("USERCREATE_MANAGERATTR"))) {
                    if(!bpUserAtt.getValue().contains(ahUserAtt)) {
                        status = false;
                        break;
                    }
                } else if(!ahUserAtt.equalsIgnoreCase(bpUserAtt.getValue())) {
                    status = false;
                    break;
                }
            }
        }
        catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
            status = false;
        } finally {
            return status;
        }
    }
}
