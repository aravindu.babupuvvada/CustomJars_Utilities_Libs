package com.SaviyntCustom.GIWinRunRecons;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.net.ssl.HttpsURLConnection;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

class ReconThread implements Runnable {

    private List<String> epNames;
    private String servertype;
    private Properties prop;
    private Connection con;
    private final int id;

    private Logger logger = null;
    public String stepData;

    public ReconThread(int id, List<String> epNames, String servertype, Properties prop, Connection con) {
        this.id = id;
        this.epNames = epNames;
        this.servertype = servertype;
        this.prop = prop;
        this.con = con;

        String saviyntHome = System.getenv("SAVIYNT_HOME");
        //System.setProperty("LOG_PATH", saviyntHome);
        PropertyConfigurator.configure(saviyntHome + File.separator + "customJarsLog4j.properties");
        logger = Logger.getLogger("winReconsLogger");
    }

    public ReconThread(int id, List<String> epNames, Properties prop, Connection con) {
        this.id = id;
        this.epNames = epNames;
        //this.servertype = servertype;
        this.prop = prop;
        this.con = con;

        String saviyntHome = System.getenv("SAVIYNT_HOME");
        //System.setProperty("LOG_PATH", saviyntHome);
        PropertyConfigurator.configure(saviyntHome + File.separator + "customJarsLog4j.properties");
        logger = Logger.getLogger("winReconsLogger");
    }

    private String join(List<String> namesList) {
        return String.join(",", namesList
                .stream()
                .map(name -> ("'" + name + "'"))
                .collect(Collectors.toList()));
    }

    @Override
    // This function will be executed in parallel
    public void run() {
        try {
            String selectSQL = prop.getProperty("RUNRECON_GIWIN_GETTRIGGERNAMES").
                    replace("${servers}", join(epNames));
            logger.info("(Thread - " + id + ") Executing query - " + selectSQL);
            PreparedStatement pstmt3 = con.prepareStatement(selectSQL);
            ResultSet rs2 = pstmt3.executeQuery();
            logger.info("(Thread - " + id + ") Statement executed successfully");

            MethodPollerTest<String> poller = new MethodPollerTest<>();

            while (rs2.next()) {
                String trigger = rs2.getString("TRIGGER_NAME");
                String endpoint = rs2.getString("ENDPOINTNAME");

                logger.info("(Thread - " + id + ") " + endpoint + " - " + trigger);

                JSONObject accimportsubjson = new JSONObject();
                JSONArray secSysArr = new JSONArray();
                secSysArr.add(0, endpoint);
                accimportsubjson.put("securitysystems", secSysArr);
                accimportsubjson.put("connectionname", endpoint);
                accimportsubjson.put("fullorincremental", "full");
                //accimportsubjson.put("accountsoraccess", "accounts");

                if (runTrigger(prop, trigger, "FullReconMSJob", "ECFConnector", accimportsubjson)) {
                    logger.info("(Thread - " + id + ") Started " + trigger + ". Start date - " + stepData);
                    Thread.sleep(Long.parseLong(prop.getProperty("RUNRECON_GIWIN_pollIntervalMillis")));
                    String triggerstatus = poller.poll(Duration.ofSeconds(Long.parseLong(
                            prop.getProperty("RUNRECON_GIWIN_pollDurationSec"))), Long.parseLong(prop.getProperty("RUNRECON_GIWIN_pollIntervalMillis"))).method(()
                            ->
                            checkTriggerStatusDB(prop, con,  endpoint, trigger, "FullReconMSJob", "ECFConnector", stepData, "Step3AccountsImport"))
                            .until(s -> s.startsWith("COMPLETED") || s.startsWith("FAILED"))
                            .execute();

                    logger.info("(Thread - " + id + ") Server - " + endpoint + ". Trigger - " + trigger + " job status : " + triggerstatus);
                    if (triggerstatus.equalsIgnoreCase("COMPLETED")) {
                    } else if (triggerstatus.equalsIgnoreCase("FAILED")) {
                    } else {
                    }
                } else {
                    logger.error("Running trigger " + trigger + " for server " + endpoint + " failed.");
                }
            }
        } catch (Exception e) {
            logger.error("(Thread - " + id + ") Exception - " +  e.getMessage(), e);
        }
    }

    public boolean runTrigger(Properties prop, String triggername, String jobName,
                              String jobgroup, JSONObject valueMap)
    {
        boolean status=false;
        try {
            TokenPojo token = new TokenPojo();
            String outhToken = token.callBearerToken(prop);

            String url=prop.getProperty("URLPATH")+"api/v5/runJobTrigger";
            HttpUrlConn httpConn = new HttpUrlConn();

            //String[] triggerSequenceList=triggerSequence.split(",");
            HttpsURLConnection con1 = null;
            JSONObject getConnectionResponse = null;
            JSONObject triggerJson =new JSONObject();
            triggerJson.put("triggername",triggername);
            triggerJson.put("jobname",jobName);
            triggerJson.put("jobgroup",jobgroup);
            triggerJson.put("createJobIfDoesNotExist","false");

            if(valueMap!=null) {
                triggerJson.put("valueMap",valueMap);
            }

            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            con1 = httpConn.sendPostHttps(url, triggerJson.toString(), httpConnectionHeaders);
            int code = con1.getResponseCode();

            getConnectionResponse = httpConn.fetchJsonObject(con1);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(getConnectionResponse.toString());

            String triggerchainstatus = (String)json.get("errorCode");
            String msg = (String)json.get("msg");
            logger.info("(Thread - " + id + ") Trigger chain status : "+triggerchainstatus);

            if(triggerchainstatus.equals("1")){
                stepData = null;
            } else {
                stepData = (String)json.get("timestamp");
                status = true;
            }

        }
        catch(Exception e) {
            logger.error("(Thread - " + id + ") Exception - " +  e.getMessage(), e);
            status=false;
        } finally {
            return status;
        }
    }

    public String checkTriggerStatusDB(Properties prop,Connection connection,
                                       String connectionname,String triggerName,
                                       String jobName, String jobgroup,String startDate,String stepName) {
        String status="CONTINUE";
        try {
            logger.info("(Thread - " + id + ") Inside checkTriggerStatusDB - "+triggerName);

            if(jobName.equalsIgnoreCase("SapImportJob")) {
                ResultSet rs1=null;
                String selectSQL1 = "select job.JOBID,job.COMENTS,job.SAVRESPONSE,job.JOBSTARTDATE,job.JOBENDDATE,log.LOGDATAASXML from ecmimportjob job left join importlog log on job.jobid = log.jobid where job.jobname = ? and (job.EXTERNALCONNECTION = ? || job.SYSTEMNAME = ? ) and job.JOBSTARTDATE >= STR_TO_DATE( ? , '%Y-%m-%d %T' ) and job.UPDATEUSER = ? limit 1;";
                PreparedStatement pstmt = con.prepareStatement(selectSQL1);
                pstmt=con.prepareStatement(selectSQL1);
                pstmt.setString(1,jobName);
                pstmt.setString(2,connectionname);
                pstmt.setString(3,connectionname);
                pstmt.setString(4,startDate);
                pstmt.setString(5,prop.getProperty("SAVUSERNAME"));

                logger.info("(Thread - " + id + ") Executing statement: "+pstmt.toString());
                rs1=pstmt.executeQuery();
                logger.info("(Thread - " + id + ") Statement executed successfully");

                if(rs1.next() != false) {
                    String savresponse = rs1.getString("SAVRESPONSE");
                    String logdataasxml = rs1.getString("LOGDATAASXML");
                    String jobstartdate = rs1.getString("JOBSTARTDATE");
                    String jobenddate = rs1.getString("JOBENDDATE");

                    logger.info("(Thread - " + id + ") Response - "+savresponse + ".\nLogXML - " + logdataasxml
                            + ".\nJob Start Date - " + jobstartdate+ ".\nJob End Date - " + jobenddate);

                    if(jobenddate!=null && !jobenddate.equals(jobstartdate) && logdataasxml!=null) {
                        if (savresponse.equalsIgnoreCase("Success") &&
                                !( (logdataasxml.contains("<Groups_Updated>0</Groups_Updated>") && logdataasxml.contains("<Groups_Created>0</Groups_Created>"))
                                        || (logdataasxml.contains("<Accounts_Updated>0</Accounts_Updated>") && logdataasxml.contains("<Accounts_Created>0</Accounts_Created>")) ) ) {
                            status = "COMPLETED";
                        } else if (savresponse.equalsIgnoreCase("Success") &&
                                (logdataasxml.contains("<Groups_Updated>0</Groups_Updated>") || logdataasxml.contains("<Accounts_Updated>0</Accounts_Updated>"))) {
                            status = "FAILED";
                        } else if (!savresponse.equalsIgnoreCase("Success")){
                            status = "FAILED";
                        }
                    }
                } else {
                    logger.info("(Thread - " + id + ") Job has not started yet");
                }
            } else if(jobName.equalsIgnoreCase("FullReconMSJob")) {
                ResultSet rs1=null;
                String selectSQL1 = "select job.JOBID,job.COMENTS,job.SAVRESPONSE,job.JOBSTARTDATE,job.JOBENDDATE,log.LOGDATAASXML from ecmimportjob job left join importlog log on job.jobid = log.jobid where job.jobname = ? and (job.EXTERNALCONNECTION = ? || job.SYSTEMNAME = ? ) and job.JOBSTARTDATE >= STR_TO_DATE( ? , '%Y-%m-%d %T' ) limit 1;";
                PreparedStatement pstmt = con.prepareStatement(selectSQL1);
                pstmt=con.prepareStatement(selectSQL1);
                pstmt.setString(1,jobName);
                pstmt.setString(2,connectionname);
                pstmt.setString(3,connectionname);
                pstmt.setString(4,startDate);

                logger.info("(Thread - " + id + ") Executing statement: "+pstmt.toString());
                rs1=pstmt.executeQuery();
                logger.info("(Thread - " + id + ") Statement executed successfully");

                if(rs1.next() != false) {
                    String savresponse = rs1.getString("SAVRESPONSE");
                    String logdataasxml = rs1.getString("LOGDATAASXML");
                    String jobstartdate = rs1.getString("JOBSTARTDATE");
                    String jobenddate = rs1.getString("JOBENDDATE");

                    logger.info("(Thread - " + id + ") Response - "+savresponse + ".\nLogXML - " + logdataasxml
                            + ".\nJob Start Date - " + jobstartdate+ ".\nJob End Date - " + jobenddate);

                    if(jobenddate!=null && logdataasxml!=null) {
                        if (savresponse.equalsIgnoreCase("Success") &&
                                !logdataasxml.contains("<jobDone>false</jobDone>")) {
                            status = "COMPLETED";
                        } else {
                            status = "FAILED";
                        }
                    }
                } else {
                    logger.info("(Thread - " + id + ") Job has not started yet");
                }
            } else if(jobName.equalsIgnoreCase("CustomQueryJob")) {
                ResultSet rs2=null;
                String selectSQL2 = "select JOBID,COMENTS,SAVRESPONSE,JOBSTARTDATE,JOBENDDATE from ecmimportjob where COMENTS like '%jobtriggername:${triggerName}%' and jobname = ? and JOBSTARTDATE >= STR_TO_DATE( ? , '%Y-%m-%d %T' ) and UPDATEUSER = ? limit 1;";
                selectSQL2=selectSQL2.replace("${triggerName}",triggerName);

                PreparedStatement pstmt2 = con.prepareStatement(selectSQL2);
                pstmt2=con.prepareStatement(selectSQL2);
                pstmt2.setString(1,jobName);
                pstmt2.setString(2,startDate);
                pstmt2.setString(3,prop.getProperty("SAVUSERNAME"));

                logger.info("(Thread - " + id + ") Executing statement: "+pstmt2.toString());
                rs2=pstmt2.executeQuery();
                logger.info("(Thread - " + id + ") Statement executed successfully");

                if(rs2.next() != false) {
                    String savresponse = rs2.getString("SAVRESPONSE");
                    String jobstartdate = rs2.getString("JOBSTARTDATE");
                    String jobenddate = rs2.getString("JOBENDDATE");

                    logger.info("(Thread - " + id + ") Response - "+savresponse +  ".\nJob Start Date - " + jobstartdate+ ".\nJob End Date - " + jobenddate);

                    if (savresponse.equalsIgnoreCase("Success")) {
                        status = "COMPLETED";
                    } else if (!savresponse.equalsIgnoreCase("Success")){
                        status = "FAILED";
                    }
                } else {
                    logger.info("(Thread - " + id + ") Job has not started yet");
                }
            } else if(jobName.equalsIgnoreCase("TriggerChainJob")) {
                ResultSet rs3=null;
                String selectSQL3 = "select JOBID,COMENTS,SAVRESPONSE,JOBSTARTDATE,JOBENDDATE from ecmimportjob where jobname = ? and JOBSTARTDATE >= STR_TO_DATE( ? , '%Y-%m-%d %T' ) and UPDATEUSER = ? limit 1;";

                PreparedStatement pstmt3 = con.prepareStatement(selectSQL3);
                pstmt3=con.prepareStatement(selectSQL3);
                pstmt3.setString(1,jobName);
                pstmt3.setString(2,startDate);
                pstmt3.setString(3,prop.getProperty("SAVUSERNAME"));

                logger.info("(Thread - " + id + ") Executing statement: "+pstmt3.toString());
                rs3=pstmt3.executeQuery();
                logger.info("(Thread - " + id + ") Statement executed successfully");

                if(rs3.next() != false) {
                    String savresponse = rs3.getString("SAVRESPONSE");
                    String jobstartdate = rs3.getString("JOBSTARTDATE");
                    String jobenddate = rs3.getString("JOBENDDATE");

                    logger.info("(Thread - " + id + ") Response - "+savresponse +  ".\nJob Start Date - " + jobstartdate+ ".\nJob End Date - " + jobenddate);

                    if ((jobenddate!=null && !jobenddate.equals(jobstartdate))
                            && savresponse.equalsIgnoreCase("Success")) {
                        status = "COMPLETED";
                    } else if (!savresponse.equalsIgnoreCase("Success")){
                        status = "FAILED";
                    }
                } else {
                    logger.info("(Thread - " + id + ") Job has not started yet");
                }
            } else {
            }
        }
        catch(Exception e) {
            logger.error("(Thread - " + id + ") Exception - " +  e.getMessage(), e);
            status="FAILED";
        } finally {
            return status;
        }
    }

}
