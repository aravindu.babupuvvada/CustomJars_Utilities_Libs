package com.SaviyntCustom.GIWinRunRecons;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

public class Recons {
	private Logger logger = null;

	public static void main(String[] args) {
		Map map=new HashMap();  
	    //map.put("servertype","GIWIN");
	    map.put("reconday","1");
	    
	    Recons allrecon = new Recons();
		allrecon.runAllRecons(map);
		
        //System.out.println("***** In MAIN *****\n");
	}
	public Recons() {
		String saviyntHome = System.getenv("SAVIYNT_HOME");
		//System.setProperty("LOG_PATH", saviyntHome);
		PropertyConfigurator.configure(saviyntHome + File.separator + "customJarsLog4j.properties");
		logger = Logger.getLogger("winReconsLogger");
	}

    public void runAllRecons(Map data) {
        logger.info("***** Run all GI Win recons method Start *****");
        
		String getAllServersQry = null;
		Connection con=null;
		
		Statement getAllServersStmt=null;
		ResultSet allServers=null;

		InputStream inputStream = null;
		
		try{
			//String servertype = null;
			String reconday = null;
			
	        //if (data != null & (data.containsKey("servertype") || data.containsKey("reconday"))) {
			if (data != null & data.containsKey("reconday")) {
				//servertype = ((String)data.get("servertype"));
	            reconday = ((String)data.get("reconday"));
	            
	            //logger.info("servertype - " + servertype);
	            logger.info("reconday - " + reconday);
	        }

	    	Properties prop = new Properties();	        
	    	String saviyntHome = System.getenv("SAVIYNT_HOME");
			logger.info("SAVIYNT_HOME - " + saviyntHome + "\n");
	    	String propFileName = saviyntHome.concat("/SAMConfig.properties");
	    	inputStream = new FileInputStream(propFileName);
	        prop.load(inputStream);
			Class.forName("com.mysql.jdbc.Driver");
			logger.info("DBURL - " + prop.getProperty("DBURL"));
			logger.info("DBUSER - " + prop.getProperty("DBUSER"));

			logger.info("Connecting to - " + prop.getProperty("DBURL"));

			AESCrypt aes = new AESCrypt();
			String pass=aes.decryptIt(prop.getProperty("DBPASS"));
		    //LOGGER.info("pass"+pass+"\n");
			if(con==null) {
				con=DriverManager.getConnection(prop.getProperty("DBURL"),prop.getProperty("DBUSER"),pass);
			}
			con.setAutoCommit(true);
			logger.info("DB connection successful.");

			/*
			String conditionQuery = null;
	        if (servertype != null && servertype.equalsIgnoreCase("GIWIN")) {
	        	if(reconday!=null && reconday.equalsIgnoreCase("adhoc")) {
					conditionQuery = prop.getProperty("RUNRECON_GIWIN_ADHOC");
				} else {
					conditionQuery = prop.getProperty("RUNRECON_GIWIN_NONADHOC");
					conditionQuery = conditionQuery.replace("${reconday}", "'" + reconday + "'");
				}
	        } else if (servertype != null && servertype.equalsIgnoreCase("GIUNIX")) {
				if(reconday!=null && reconday.equalsIgnoreCase("adhoc")) {
					conditionQuery = prop.getProperty("RUNRECON_GIUNIX_ADHOC");
				} else {
					conditionQuery = prop.getProperty("RUNRECON_GIUNIX_NONADHOC");
					conditionQuery = conditionQuery.replace("${reconday}", "'" + reconday + "'");
				}
			} else if (servertype != null && servertype.equalsIgnoreCase("NE")) {
				if(reconday!=null && reconday.equalsIgnoreCase("adhoc")) {
					conditionQuery = prop.getProperty("RUNRECON_NE_ADHOC");
				} else {
					conditionQuery = prop.getProperty("RUNRECON_NE_NONADHOC");
					conditionQuery = conditionQuery.replace("${reconday}", "'" + reconday + "'");
				}
			}
			*/
			String conditionQuery = null;
			if(reconday!=null && reconday.equalsIgnoreCase("adhoc")) {
				conditionQuery = prop.getProperty("RUNRECON_GIWIN_ADHOC");
			} else if(reconday!=null) {
				conditionQuery = prop.getProperty("RUNRECON_GIWIN_NONADHOC");
				conditionQuery = conditionQuery.replace("${reconday}", "'" + reconday + "'");
			}

	        if(conditionQuery != null && !conditionQuery.isEmpty()) {
				List<String> serversList = new ArrayList<String>();
				List<Thread> arrThreads = new ArrayList<Thread>();

				getAllServersQry = prop.getProperty("RUNRECON_GETALLSERVERS");
				getAllServersQry = getAllServersQry.replace("${condquery}",conditionQuery);
				logger.info("getAllServersQry - " + getAllServersQry);

				getAllServersStmt=con.createStatement();
				allServers=getAllServersStmt.executeQuery(getAllServersQry);

			    while(allServers.next()) {
					String epName = allServers.getString(1);

					logger.info("Endpoint Name - " + epName);
					serversList.add(epName);
				}
			    int totalTasks = Integer.parseInt(prop.getProperty("RUNRECON_TOTALTHREADS"));
			    int totalInGrps = serversList.size()/totalTasks;
			    //int mod = serversList.size()%totalTasks;

				logger.info("totalInGrps - " + totalInGrps);
				//logger.info("mod - " + mod);

				List<List<String>> parts = null;
			    if(totalInGrps>0) {
					parts = chopped(serversList, totalInGrps+1);
				} else {
					parts = chopped(serversList, 1);
				}
				for (int i = 0; i < parts.size(); i++) {
					//Thread thread = new Thread(new ReconThread((i+1), parts.get(i), servertype, prop, con));
					Thread thread = new Thread(new ReconThread((i+1), parts.get(i), prop, con));
					thread.start();
					arrThreads.add(thread);
				}
				for (int i = 0; i < arrThreads.size(); i++) {
					arrThreads.get(i).join();
				}
	        } else {
	        	logger.error("Recon day is mandatory.");
			}
		} catch(Exception e) {
			logger.error("Exception - " +  e.getMessage(), e);
		}
		finally {
			try {
				if(con!=null)		
					con.close();
				if(getAllServersStmt!=null)
					getAllServersStmt.close();
				logger.info("***** Run all GI Win recons End *****\n\n");
			} catch(Exception e) {
				logger.error("Exception - " +  e.getMessage(), e);
			}
		}
	}

	// chops a list into non-view sublists of length L
	static <T> List<List<T>> chopped(List<T> list, final int L) {
		List<List<T>> parts = new ArrayList<List<T>>();
		final int N = list.size();
		for (int i = 0; i < N; i += L) {
			parts.add(new ArrayList<T>(
					list.subList(i, Math.min(N, i + L)))
			);
		}
		return parts;
	}

	private String join(List<String> namesList) {
		return String.join(",", namesList
				.stream()
				.map(name -> ("'" + name + "'"))
				.collect(Collectors.toList()));
	}

	public boolean checkOOBNestedGroups(Connection con, Properties prop) {
		boolean status = false;

		System.out.println("Inside getOOBNestedGroups");

		try {
			String getNestedGrpStr = prop.getProperty("GIWIN_GETOOBNESTEDGROUPS");

			if(getNestedGrpStr != null && !getNestedGrpStr.isEmpty()) {
				System.out.println("getNestedGrpStr - " + getNestedGrpStr);
				PreparedStatement pstmt = con.prepareStatement(getNestedGrpStr);

				pstmt = con.prepareStatement(getNestedGrpStr);
				System.out.println("Executing statement: " + pstmt.toString());
				ResultSet rs = pstmt.executeQuery();
				System.out.println("Statement executed successfully");
				while (rs.next()) {
					Integer roleKey = rs.getInt(1);
					Integer parentEnt = rs.getInt(2);
					Integer childEnt = rs.getInt(3);
					Integer epKey = rs.getInt(4);
					Integer ssKey = rs.getInt(5);
					Integer userkey = rs.getInt(6);

					PreparedStatement arsChildStmt=con.prepareStatement(prop.getProperty("GIWIN_ARSTASK_REMOVECHILDGRP"));

					if(roleKey!=null && parentEnt!=null && childEnt!=null && epKey!=null && ssKey!=null && userkey!=null) {
						//Create the remove child ent task

						arsChildStmt.setInt(1, childEnt);
						arsChildStmt.setString(2, "Created by custom OOB job for Nested Group - Windows");
						arsChildStmt.setInt(3, epKey);
						arsChildStmt.setInt(4, ssKey);
						arsChildStmt.setInt(5, roleKey);
						arsChildStmt.setInt(6, userkey);
						logger.info("Executing statement: " + arsChildStmt.toString());

						//int rows=arsChildStmt.executeUpdate();
						//logger.info("Remove child OOB group task created - " + rows);
					}
				}
				status =true;
			}
		}
		catch (Exception e) {
			logger.error("Exception - " +  e.getMessage(), e);
		}
		finally {
			return status;
		}
	}
}
