package com.SaviyntCustom.EncryptPassword;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class AESCrypt {
    public String decryptItInput(String strToDecrypt) throws  Exception {
        byte[] key = "@ccessHUB2O21_AH".getBytes();
        if(strToDecrypt !=null ) {
            try {

                Cipher cipher  = Cipher.getInstance("AES/ECB/PKCS5PADDING");
                final SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
                cipher.init(Cipher.DECRYPT_MODE, secretKey);
                final String decryptedString = new String(cipher.doFinal(Base64.decodeBase64(strToDecrypt)));
                return decryptedString;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public String encryptItInput(String strToEncrypt) {
        byte[] key = "@ccessHUB2O21_AH".getBytes();
        try {
            // Test with CBC or CTR mode.

            Cipher cipher  = Cipher.getInstance("AES/ECB/PKCS5Padding");
            final SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            final String encryptedString = Base64.encodeBase64String(cipher.doFinal(strToEncrypt.getBytes()));
            return encryptedString;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }
    public static void main (String args[]) {
        AESCrypt aes = new AESCrypt();
        if(args.length == 0) {
            System.out.println("Please provide the password");
            System.exit(0);
        } else {
            String b = aes.encryptItInput(args[0]);
            System.out.println(b);
        }
    }
}
