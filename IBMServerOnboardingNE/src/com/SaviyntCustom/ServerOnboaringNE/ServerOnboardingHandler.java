package com.SaviyntCustom.ServerOnboaringNE;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.sql.*;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

public class ServerOnboardingHandler {
    public Connection con;
    Logger logger = null;
    static StringBuilder comments=new StringBuilder();
    public String stepData;
    public String outhToken;

    public HashMap<String, String> errInputFiles = new HashMap<>();
    public HashMap<String, String> errInputLinesServer = new HashMap<>();
    public HashMap<String, String> invalidService = new HashMap<>();

    public ServerOnboardingHandler() {
        String saviyntHome = System.getenv("SAVIYNT_HOME");
        //System.setProperty("LOG_PATH", saviyntHome);
        PropertyConfigurator.configure(saviyntHome + File.separator + "customJarsLog4j.properties");
        logger = org.apache.log4j.Logger.getLogger("boardingNELogger");
    }

    public HashSet<String> processBoardingRequests(Properties prop, HashMap<String,String> requestsList, String outhTokenIn) throws IOException, SQLException {
        // update sql
        ResultSet rs3 = null;
        String selectSQL = "";
        HashSet<String> caflagchangeList = new HashSet<String>();
        HashMap<Integer, ServerOnboardingStatusPojo> serverObkeys=new HashMap<>();
        HashSet<String> serverRequests = new HashSet<String>();
        outhToken = outhTokenIn;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            String DB_URL = prop.getProperty("db_url");
            String DB_USER = prop.getProperty("bulkboarduser");
            String DB_PASSWORD = prop.getProperty("bulkboardpass");
            //String JDBC_DRIVER = prop.getProperty("JDBC_DRIVER");
            logger.info("Selected schema: " + DB_URL);
            con = DriverManager.getConnection(DB_URL, DB_USER, (new AESCrypt()).decryptIt(DB_PASSWORD.trim()));
            String pendingRequests = requestsList.keySet().stream().map(Object::toString).collect(Collectors.joining(","));
            HashMap<String, ArsRequestAttachmentPojo> pendingGIRequestkeys = new HashMap<>();
            //HashMap<String,ServerOnboardingStatusPojo> processedRequestkeys=new HashMap<>();
            HashMap<String, String> serverNames = new HashMap<>();
            HashMap<String, String> boarded = new HashMap<>();
            ArsRequestAttachmentPojo reqOb = null;

            if (pendingRequests != null && !pendingRequests.equals("")) {
                String[] bbRequests = pendingRequests.split(",");
                for (int i = 0; i < bbRequests.length; i++) {
                    reqOb = new ArsRequestAttachmentPojo();
                    reqOb.requestkey = bbRequests[i];
                    pendingGIRequestkeys.put(bbRequests[i], reqOb);
                    if (!serverRequests.contains(bbRequests[i])) {
                        serverRequests.add(bbRequests[i]);
                    }
                }
                selectSQL = "SELECT ars_requestkey,fileattached FROM arsrequest_attachments " +
                        "WHERE ars_requestkey in (" + pendingRequests + ") order by arsrequest_attachmentskey desc";
                logger.info("Executing query - " + selectSQL);
                Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                        ResultSet.CONCUR_UPDATABLE);
                rs3 = stmt.executeQuery(selectSQL);

                logger.info("Statement executed successfully");

                while (rs3.next()) {
                    String[] blobdataList = null;
                    //String[] blobdataOwnerList = null;
                    java.sql.Blob myBlob = rs3.getBlob("fileattached");
                    String ars_requestkey = rs3.getString("ars_requestkey");

                    if (myBlob != null) {
                        InputStream input = myBlob.getBinaryStream();
                        String blobdata = new BufferedReader(new InputStreamReader(input)).lines().collect(java.util.stream.Collectors.joining("\n"));
                        String[] blobdataList1 = blobdata.split("\n");
                        if (blobdataList1.length > 0) {
                            if (blobdataList1[0].contains(prop.getProperty("SERVERFILE_KEYWORD"))) {
                                blobdataList = blobdataList1;
                            }
                        }
                    }

                    //if (blobdataList != null || blobdataOwnerList != null)
                    if (blobdataList != null) {
                        if (!pendingGIRequestkeys.containsKey(ars_requestkey)) {
                            reqOb = new ArsRequestAttachmentPojo();
                            reqOb.requestkey = ars_requestkey;
                            pendingGIRequestkeys.put(ars_requestkey, reqOb);
                        } else {
                            reqOb = pendingGIRequestkeys.get(ars_requestkey);
                        }
                        if (blobdataList != null) {
                            reqOb.blobdataList = blobdataList;
                        }
                        /*if (blobdataOwnerList != null) {
                            reqOb.blobdataOwnerList = blobdataOwnerList;
                        }*/
                    }
                }
            }

            for (Map.Entry<String, ArsRequestAttachmentPojo> entry : pendingGIRequestkeys.entrySet()) {
                String requestkey = entry.getKey();
                boolean reqProceed = false;
                ArsRequestAttachmentPojo serverObArs = entry.getValue();
                logger.info("\n***** Processing RequestID = " + requestsList.get(requestkey)
                        + ". RequestKey = " + requestkey + " *****\n");
                String[] blobdataList = serverObArs.blobdataList;
                //String[] blobdataOwnerList = serverObArs.blobdataOwnerList;
                HashMap<Integer, String> headercolumnMapping = new HashMap();

                //Clear the objects which are used for each request
                serverObkeys.clear();
                serverNames.clear();
                boarded.clear();

                if (validateInput(prop, requestkey, blobdataList)) {
                    reqProceed = true;
                }

                if (reqProceed) {
                    int index = 0;
                    int connectionnameindex = 0, squadnameindex = 0,
                            osnameindex = 0, osversionindex = 0, categoryindex = 0;

                    String[] headerInfo = blobdataList[0].split(",");
                    for (String headerStr : headerInfo) {
                        headerStr = headerStr.trim();
                        if (headerStr.equalsIgnoreCase(prop.getProperty("CONNECTIONNAMEFROMHEADER"))) {
                            connectionnameindex = index;
                            serverObArs.connectionnameindex = connectionnameindex;
                        }
                        if (headerStr.equalsIgnoreCase(prop.getProperty("SQUADNAMEFROMHEADER"))) {
                            squadnameindex = index;
                            serverObArs.squadnameindex = squadnameindex;
                        }
                        if (headerStr.equalsIgnoreCase(prop.getProperty("OSNAMEFROMHEADER"))) {
                            osnameindex = index;
                            serverObArs.osnameindex = osnameindex;
                        }
                        if (headerStr.equalsIgnoreCase(prop.getProperty("OSVERSIONFROMHEADER"))) {
                            osversionindex = index;
                            serverObArs.osversionindex = osversionindex;
                        }
                        if (headerStr.equalsIgnoreCase(prop.getProperty("CATEGORYFROMHEADER"))) {
                            categoryindex = index;
                            serverObArs.categoryindex = categoryindex;
                        }
                        headercolumnMapping.put(index, headerStr);
                        logger.info("Header : " + headerStr);
                        index++;
                    }

                    int lastMaxVersion = getMaxVersionForRequest(con, requestkey);

                    for (int i = 1; i < blobdataList.length; i++) {
                        String[] dataInfo = blobdataList[i].split(",", -1);

                        //Check if data has the same no of attrs or columns
                        if (dataInfo.length > 0 && dataInfo.length == headerInfo.length) {
                            //TRIM all input values just to remove any white spaces
                            for (int j = 0; j < dataInfo.length; j++)
                                dataInfo[j] = dataInfo[j].trim();

                            HashMap<String, String> lastStatusList = findStatusDetails(con, entry.getKey(), dataInfo[connectionnameindex]);
                            int lastsuccessfullstep = Integer.parseInt(lastStatusList.get("lastsuccessfullstep"));
                            int lastversion = Integer.parseInt(lastStatusList.get("version"));
                            String comments = lastStatusList.get("comments");

                            ServerOnboardingStatusPojo serverOb = null;
                            serverOb = new ServerOnboardingStatusPojo();
                            serverOb.lastsuccessfullstep = lastsuccessfullstep;
                            if (lastversion < lastMaxVersion) {
                                serverOb.version = lastMaxVersion;
                            } else {
                                serverOb.version = lastversion;
                            }
                            serverOb.proceed = true;
                            serverOb.servername = dataInfo[connectionnameindex];
                            //serverNames.put(serverOb.servername);
                            serverOb.blobdataList = blobdataList;
                            //serverOb.blobdataOwnerList = serverObArs.blobdataOwnerList;
                            serverOb.connectionnameindex = serverObArs.connectionnameindex;
                            serverOb.squadnameindex = serverObArs.squadnameindex;
                            serverOb.osnameindex = serverObArs.osnameindex;
                            serverOb.osversionindex = serverObArs.osversionindex;
                            serverOb.categoryindex = serverObArs.categoryindex;
                            //serverOb.blobdataOwnerList = serverObArs.blobdataOwnerList;
                            serverOb.requestkey = serverObArs.requestkey;

                            if (serverOb.lastsuccessfullstep == 0) {
                                if (alreadyBoarded(prop, con, serverOb)) {
                                    boarded.put(serverOb.servername, serverOb.requestkey);
                                    serverOb.comments.append("Step0Prevalidaion: Server is already boarded.");
                                    serverOb.lastsuccessfullstep = 10;
                                    serverOb.proceed = false;
                                }
                            } else if ((serverOb.lastsuccessfullstep == 10)
                                    || (serverOb.lastsuccessfullstep == 11)
                                    || (serverOb.lastsuccessfullstep == 12)) {
                                serverOb.comments.append(comments);
                                serverOb.proceed = false;
                            }

                            if (lastsuccessfullstep > 0 && serverOb.proceed) {
                                logger.info("\n***** Processing for " + dataInfo[connectionnameindex] + " was completed till step - " + lastsuccessfullstep + ". *****\n");
                            }


                            if (lastsuccessfullstep < 1 && serverOb.proceed) {
                                //getting hostnames based on customproperty1 here
                                HashMap<String, String> templateAttrs =
                                        getTemplateConnectionAttributes(dataInfo[connectionnameindex],
                                                dataInfo[osnameindex], dataInfo[osversionindex], con, prop);
                                String error = checkMandatory(prop, headercolumnMapping,
                                        dataInfo, headerInfo);
                                logger.info("Returned Connection Attrs Size : " + templateAttrs.size());
                                //logger.info("\nConnection Attrs are : " + templateAttrs.toString());
                                if (templateAttrs.size() == 1) {
                                    serverOb.proceed = false;
                                    serverOb.lastsuccessfullstep = 11;
                                    serverOb.comments.append("OS or OS Version is not defined propery. Please check!");                                    //serverNames.put(serverOb.servername, "<span style=\"color:red;\">Failed </span> : Server Type not defined in MAD");
                                } else if (error.length() > 0 && !error.equalsIgnoreCase("SOX_SERVER_DONOTBOARD")) {
                                    serverOb.proceed = false;
                                    serverOb.lastsuccessfullstep = 11;
                                    serverOb.comments.append("Mandatory Attributes are not set. Attrs - " + error);
                                    //serverNames.put(serverOb.servername, error);
                                }  else if(invalidService.containsKey(dataInfo[connectionnameindex])) {
                                    serverOb.proceed = false;
                                    serverOb.lastsuccessfullstep = 11;
                                    if(invalidService.containsKey(dataInfo[connectionnameindex])) {
                                        serverOb.comments.append(invalidService.get(dataInfo[connectionnameindex]) + " | ");
                                    }
                                    /*if(noSavOrOrg.containsKey(dataInfo[connectionnameindex])) {
                                        serverOb.comments.append(noSavOrOrg.get(dataInfo[connectionnameindex]) + " | ");
                                    }*/
                                } else {
                                    templateAttrs.put(prop.getProperty("CONNECTIONNAMEFROMHEADER"), dataInfo[connectionnameindex]);

                                /*
                                for (int j = 1; j < dataInfo.length; j++) {
                                    String val = dataInfo[j];
                                    String headername = headercolumnMapping.get(j);
                                    templateAttrs.put(headername, val);
                                }
                                */
                                    logger.info("\n***** After Reading all required info *****\n");
                                    if (createConnection(prop, templateAttrs, dataInfo[0],
                                            dataInfo[connectionnameindex], serverOb, dataInfo)) {
                                        if (createUpdateOrganization(con, prop,
                                                dataInfo[squadnameindex], dataInfo[connectionnameindex], serverOb)) {
                                            serverNames.put(serverOb.servername, "Connection Created Successfully. Updating Connection and Organization successful.");
                                            serverOb.lastsuccessfullstep = 1;

                                        } else {
                                            serverOb.proceed = false;
                                            serverNames.put(serverOb.servername, "Connection Created Successfully. Updating Connection/Organization failed.");
                                        }

                                    } else {
                                        serverOb.proceed = false;
                                        serverNames.put(serverOb.servername, "Connection Failed.");
                                    }

                                    logger.info("\n***** After Step1 - Create connection and update Org done for all servers *****\n");

                                }
                                //serverObkeys.put(i, serverOb);
                            }
                            serverObkeys.put(i, serverOb);

                            /*
                            String accountsReconTriggerName = null;
                            try {
                                if (serverOb.servername.matches(".*[a-zA-Z]+.*")) {
                                    accountsReconTriggerName = "ACCOUNTS_MT_" +
                                            (serverOb.servername.split("\\.")[0]).toUpperCase() +
                                            "_NE";
                                } else {
                                    accountsReconTriggerName = "ACCOUNTS_MT_" +
                                            serverOb.servername.toUpperCase() +
                                            "_NE";
                                }
                            } catch (Exception ex) {
                                accountsReconTriggerName = "ACCOUNTS_MT_" +
                                        serverOb.servername.toUpperCase() +
                                        "_NE";
                            }
                            */
                            String accountsReconTriggerName = "ACCOUNTS_MT_" +
                                    serverOb.servername.replaceAll(prop.getProperty("TRIGGERNAME_REGEX"), "").toUpperCase() +
                                    "_NE";

                            if (serverOb.lastsuccessfullstep < 2 && serverOb.proceed) {

                                HashMap<Integer, String> endpointcolumnMapping = new HashMap();
                                HashMap<String, String> endpointAttrsMapping = new HashMap<>();
                                HashMap<String, String> endpointAttrs = new HashMap<>();
                                for (String epkey : prop.stringPropertyNames()) {
                                    String epvalue = prop.getProperty(epkey);
                                    if (epkey.startsWith("ENDPOINT_")) {
                                        endpointAttrsMapping.put(epkey.replace("ENDPOINT_", ""), epvalue);
                                    }
                                }
                                int epindex = 0;

                                String[] headerInfo1 = blobdataList[0].split(",");
                                for (String headerStr : headerInfo1) {
                                    headerStr = headerStr.trim();
                                    endpointcolumnMapping.put(epindex, headerStr);
                                    logger.info("Header : " + headerStr);
                                    epindex++;
                                }

                                for (int j = 1; j < dataInfo.length; j++) {
                                    String val = dataInfo[j];
                                    String headername = endpointcolumnMapping.get(j);
                                    if (headername != null && val != null) {
                                        if (headername.equals(prop.getProperty("CONNECTIONNAMEFROMHEADER"))) {
                                            logger.info("Ignoring attribute " + prop.getProperty("CONNECTIONNAMEFROMHEADER"));
                                        }
                                        //Commenting below code since we need Squad name to be populated to cp11
                                    /*else if(headername.equals(prop.getProperty("SQUADNAMEFROMHEADER")))
                                    {
                                        logger.info("Ignoring attributes"+prop.getProperty("SQUADNAMEFROMHEADER"));
                                    }*/
                                        else {
                                            endpointAttrs.put(endpointAttrsMapping.get(headername), val);
                                        }
                                    }

                                }
                                logger.info("endpointAttrs - " + endpointAttrs);

                                if (updateEndpointStep2(prop, dataInfo[serverOb.connectionnameindex], serverOb, endpointAttrs, con))
                                    serverOb.lastsuccessfullstep = 2;
                                else
                                    serverOb.proceed = false;

                                logger.info("\n***** After Step2 - updateEndpoint (create dyn attr and update ent type) *****\n");
                            }

                            //TESTING
                            //serverOb.proceed = false;

                            String connectionname = dataInfo[serverOb.connectionnameindex];
                            com.SaviyntCustom.ServerOnboaringNE.MethodPollerTest<String> poller = new MethodPollerTest<>();
                            if (serverOb.lastsuccessfullstep < 3 && serverOb.proceed) {
                                String tempTriggerName = accountsReconTriggerName;
                                logger.info("Creating Accounts trigger. TriggerName - " + tempTriggerName);
                                if (createAccountsAccessTriggerMT(prop, connectionname,
                                        serverOb, tempTriggerName, "accounts", con, "Step3AccountsImport")) {
                                    JSONObject accimportsubjson = new JSONObject();
                                    JSONArray secSysArr = new JSONArray();
                                    secSysArr.add(0, connectionname);
                                    accimportsubjson.put("securitysystems", secSysArr);
                                    accimportsubjson.put("connectionname", connectionname);
                                    accimportsubjson.put("fullorincremental", "full");
                                    accimportsubjson.put("accountsoraccess", "accounts");

                                    com.SaviyntCustom.ServerOnboaringNE.ServerOnboardingStatusPojo serverOb1 = serverOb;
                                    if (runTrigger(prop, serverOb, tempTriggerName, "ApplicationDataImportJob", "data",
                                            accimportsubjson, "Step3AccountsImport")) {
                                        logger.info("Started " + tempTriggerName + ". Start date - " + stepData);
                                        Thread.sleep(60000);
                                        String triggerstatus = poller.poll(Duration.ofSeconds(Long.parseLong(
                                                prop.getProperty("pollDurationSec"))), Long.parseLong(prop.getProperty("pollIntervalMillis"))).method(()
                                                -> checkTriggerStatusDB(prop, con, serverOb1, dataInfo[serverOb1.connectionnameindex], tempTriggerName, "ApplicationDataImportJob",
                                                "data", stepData, "Step3AccountsImport"))
                                                .until(s -> s.startsWith("COMPLETED") || s.startsWith("FAILED"))
                                                .execute();

                                        logger.info(tempTriggerName + " job status : " + triggerstatus);
                                        if (triggerstatus.equalsIgnoreCase("COMPLETED")) {
                                            //serverOb.lastsuccessfullstep = 3;
                                            if (updateETType(prop, connectionname)) {
                                                if (tagOSRGroupsAndAccounts(prop, con, connectionname)) {
                                                    //Update cp22 of endpoint to Network Engineering Server before running NE Correlation trigger
                                                    if (updateEndpointStatusAndCp22(prop, con,
                                                            connectionname, "Network Engineering Server")) {
                                                        if (unixAccountCorrelation(prop, con, connectionname)) {
                                                            serverOb.lastsuccessfullstep = 3;
                                                        } else {
                                                            serverOb.comments.append("Step3AccountsImport: Unix account correlation failed|");
                                                            serverOb.proceed = false;
                                                        }
                                                    } else {
                                                        serverOb.comments.append("Step3AccountsImport:Update Endpoint CP22 failed|");
                                                        serverOb.proceed = false;
                                                    }
                                                } else {
                                                    serverOb.comments.append("Step3AccountsImport:Baseline and tagging failed|");
                                                    serverOb.proceed = false;
                                                }
                                            } else {
                                                serverOb.comments.append("Step3AccountsImport:Update ETType failed|");
                                                serverOb.proceed = false;
                                            }
                                        } else {
                                            serverOb.proceed = false;
                                            serverOb.comments.append("Step3AccountsImport:Account import timed-out or failed|");
                                        }
                                    } else {
                                        serverOb.proceed = false;
                                        serverOb.comments.append("Step3AccountsImport:Running Account import trigger failed|");
                                    }
                                } else {
                                    serverOb.proceed = false;
                                    serverOb.comments.append("Step3AccountsImport:Creating Account import trigger failed|");
                                }

                                logger.info("\n***** After Step3 - Accounts import *****\n");
                            }


                            if (serverOb.lastsuccessfullstep < 4 && serverOb.proceed) {
                                if (updateEndpoint(prop, dataInfo[serverOb.connectionnameindex], serverOb)
                                        && updateEndpointStatusAndCp22(prop, con, connectionname, "Network Engineering Server")) {
                                    //Set all required attributes in the sec system once all changes are done
                                    if (updateSecuritySystem(prop, con, connectionname)) {
                                        serverOb.lastsuccessfullstep = 4;
                                        serverOb.proceed = true;
                                    } else {
                                        serverOb.proceed = false;
                                        serverOb.comments.append("Step4CompleteServerConfig:Error while updating Security System config|");
                                    }
                                } else {
                                    serverOb.proceed = false;
                                    serverOb.comments.append("Step4CompleteServerConfig:Error while updating Enpoint config|");
                                }

                                logger.info("\n***** After Step4 - Updating sec system and endpoint configurations *****\n");
                            }


                            if (serverOb.lastsuccessfullstep < 5 && serverOb.proceed) {
                                if (updateUserUpdateRule(prop, con, connectionname)) {
                                    String serverrecontriggername = prop.getProperty("SERVERRECONTRIGGERNAME");
                                    //if (UpdateTrigger(prop, serverOb, serverrecontriggername, accountsReconTriggerName, con)) {
                                    if (updateReconday(prop, serverOb, con, dataInfo[serverOb.categoryindex])) {
                                        serverOb.proceed = true;
                                        serverOb.lastsuccessfullstep = 5;
                                    } else {
                                        serverOb.proceed = false;
                                        serverOb.comments.append("Step5UURRecondayUpdate:" +
                                                "Error updating Recon day for the server|");
                                    }
                                } else {
                                    serverOb.comments.append("Step5UURRecondayUpdate:" +
                                            "Error updating the User Update Rules for the server|");
                                }

                                logger.info("\n***** After Step5 - Updating User update rules and recon day *****\n");
                            }


                            if (serverOb.lastsuccessfullstep < 10) {
                                //Add group related errors also
                                StringBuilder stepsstatus = new StringBuilder();

                                if (serverOb.lastsuccessfullstep >= 1) {
                                    serverOb.lastsuccessfullstepname = "Connection and Org update";
                                    stepsstatus.append("<br>Following steps were completed:<br>");
                                    stepsstatus.append("Step1 - Connection creation<br>");
                                }
                                if (serverOb.lastsuccessfullstep >= 2) {
                                    serverOb.lastsuccessfullstepname = "Endpoint Configuration";
                                    stepsstatus.append("Step2 - Update Endpoint configuration<br>");
                                }
                                if (serverOb.lastsuccessfullstep >= 3) {
                                    serverOb.lastsuccessfullstepname = "Import";
                                    stepsstatus.append("Step3 - Accounts import<br>");
                                }
                                if (serverOb.lastsuccessfullstep >= 4) {
                                    serverOb.lastsuccessfullstepname = "Complete server configuration";
                                    stepsstatus.append("Step4 - Complete Server configuration<br>");
                                }
                                if (serverOb.lastsuccessfullstep >= 5) {
                                    serverOb.lastsuccessfullstepname = "Update recon trigger chain";
                                    stepsstatus.append("Step5 - Update Recon trigger chain<br>");
                                }

                                if (serverOb.lastsuccessfullstep < 5) {
                                    stepsstatus.append(prop.getProperty("EMAIL_BOARDINGERROR"));
                                    stepsstatus.append(serverOb.comments + "<br");
                                }
                                serverOb.emailbody = stepsstatus.toString();
                            } else {
                                if (serverOb.lastsuccessfullstep == 10) {
                                    serverOb.lastsuccessfullstepname = "Server Already Boarded";
                                    serverOb.emailbody = prop.getProperty("EMAIL_ALREADYBOARDEDMAIN");
                                } else if (serverOb.lastsuccessfullstep == 11) {
                                    serverOb.lastsuccessfullstepname = "Input file error";
                                    String status = prop.getProperty("EMAIL_INPUTERROR");
                                    status = status + serverOb.comments + "<br>";
                                    serverOb.emailbody = status;
                                } else if (serverOb.lastsuccessfullstep == 12) {
                                    serverOb.lastsuccessfullstepname = "Error while boarding";
                                    String status = prop.getProperty("EMAIL_BOARDINGSTOPPED");
                                    status = status + serverOb.comments + "<br>";
                                    serverOb.emailbody = status;
                                }
                            }
                            updateServerOnboardingStatusTable(con, serverOb);
                            serverObkeys.put(i,serverOb);
                        }
                    }
                } // end of all steps


                StringBuilder cbody = new StringBuilder();

                // Set input file error and line error
                cbody.append(prop.getProperty("EMAIL_HEADER"));
                if (!errInputFiles.containsKey(requestkey)) {
                    if (errInputLinesServer.get(requestkey) != null && !errInputLinesServer.get(requestkey).equals("")) {
                        cbody.append("<div>");
                        cbody.append(prop.getProperty("EMAIL_INVALIDSERVERENTRIES"));
                        cbody.append(errInputLinesServer.get(requestkey));
                        cbody.append("</div><br>");
                        cbody.append(prop.getProperty("EMAIL_DIVIDER"));
                    }
                    /*if(!serversNoGroupEntries.isEmpty()) {
                        cbody.append("<div>" + prop.getProperty("EMAIL_SERVERSWITHNOGROUPENTRIES"));
                        Iterator<String> it = serversNoGroupEntries.iterator();
                        while(it.hasNext()) {
                            String server = it.next();
                            cbody.append(server + "<br>");
                        }
                        cbody.append("</div><br>");
                        cbody.append(prop.getProperty("EMAIL_DIVIDER"));
                    }*/

                    if (!serverObkeys.isEmpty()) {
                        for (Map.Entry<Integer, ServerOnboardingStatusPojo> sentry : serverObkeys.entrySet()) {
                            Integer rkey = sentry.getKey();
                            ServerOnboardingStatusPojo serverObL = sentry.getValue();
                            //updateServerOnboardingStatusTable(con, serverOb);
                            cbody.append("<div>");
                            cbody.append(prop.getProperty("EMAIL_BOARDINGSTATUS") +
                                    serverObL.servername + "<br>" +
                                    serverObL.emailbody + "<br>");
                            cbody.append("</div>");
                        }
                        cbody.append(prop.getProperty("EMAIL_DIVIDER"));
                    }
                } else if (errInputFiles.containsKey(requestkey)) {
                    cbody.append("<div>");
                    cbody.append(prop.getProperty("EMAIL_ERROR"));
                    cbody.append(errInputFiles.get(requestkey));
                    cbody.append("</div><br>");
                } else {
                    cbody.append(prop.getProperty("EMAIL_ERROR"));
                }
                cbody.append(prop.getProperty("EMAIL_FOOTER"));

                try {
                    SendEmail se = new SendEmail();
                    se.sendEmail(prop, prop.getProperty("EMAILSUBJECT") + " for Request ID : "
                            + requestsList.get(requestkey), cbody.toString(), prop.getProperty("EMAILTOADDRESS"), null);
                } catch (Exception ex) {
                    logger.info("\n***** BULKBOARDING : Error in sending 'Boarding status' email. ");
                }
            }

            logger.info("End of all steps");

            if(!serverRequests.isEmpty()) {
                if (!checkAndUpdateRequest(prop, con, serverRequests)) {
                    logger.info("\n***** BULKBOARDING : Error in updating requests. ");
                }
                updateRequestInvalidData(prop,con);
            }

        } catch (Exception e) {
            logger.info(e.getMessage());
            logger.error("Exception - " +  e.getMessage(), e);;
        } finally {
            try {
                if (rs3 != null) {
                    rs3.close();
                }
            } catch (SQLException e) {
                logger.info(e.getMessage());
                logger.error("Exception - " +  e.getMessage(), e);;
            }
            return caflagchangeList;
        }
    }

    public  boolean updateUserUpdateRule(Properties prop, Connection con, String connectionname) {
        boolean res=true;
        try {
            logger.info("Inside updateUserUpdateRule");

            String uptQuery=prop.getProperty("UPDATEUUR").replace("${endpointname}","'"+connectionname+"'");
            PreparedStatement pstmt=con.prepareStatement(uptQuery);

            logger.info("Executing statement: "+pstmt.toString());
            int result=pstmt.executeUpdate();
            logger.info("Statement executed successfully");

        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
            res=false;
        }
        finally {
            return res;
        }
    }

    public boolean updateReconday(Properties prop,
                                  ServerOnboardingStatusPojo serverOb,
                                  Connection con, String category)
    {
        boolean status = true;
        try {
            ResultSet rs3=null;

            /*
            //Check if the server is SOX or not. If SOX, add it to the SOX trigger chain.
            //If non-SOX, check and add it to one of the 7 trigger chains and also update cp32 of the endpoint
            String query1 = prop.getProperty("FETCHSERVERCATEGORYQUERY");
            PreparedStatement pst = con.prepareStatement(query1);
            pst.setString(1,serverOb.servername);
            logger.info("Executing statement: "+pst.toString());
            ResultSet rs = pst.executeQuery();
            rs.next();
            String soxData = rs.getString(1);
            String soxList = prop.getProperty("SOXLISTS");
            String[] soxListA = null;
            if(soxList != null) {
                soxListA = soxList.toUpperCase().split(",");
            }

            if(soxData != null && soxListA != null && StringUtils.indexOfAny(soxData.toUpperCase(),soxListA)!=-1) {
                sox = true;
            }
            */

            String reconDay = "No";
            if(category.equalsIgnoreCase("SOX")) {
                reconDay = "Yes";
            } else {
                String selectSQL = prop.getProperty("GETALLENDPOINTRECONDAYS");
                PreparedStatement pstmt = con.prepareStatement(selectSQL);

                logger.info("Executing statement: " + pstmt.toString());
                rs3 = pstmt.executeQuery();
                logger.info("Statement executed successfully");

                int min = 0;
                int i = 0;
                reconDay = "1";

                while (rs3.next()) {
                    if (i == 0) {
                        min = rs3.getInt("COUNT");
                        i++;
                    }
                    int cnt = rs3.getInt("COUNT");

                    if (cnt < min) {
                        min = cnt;
                        reconDay = rs3.getString("RECONDAY");

                        if(cnt==0) {
                            break;
                        }
                    }
                }
            }

            //Update cp32
            logger.info("Updating CP32");
            String uptQuery = prop.getProperty("UPDATESTATUSCP32").
                    replace("${endpointname}", "'" + serverOb.servername + "'");
            uptQuery = uptQuery.replace("${value}", "'" + reconDay + "'");
            PreparedStatement pstmt1 = con.prepareStatement(uptQuery);

            logger.info("Executing statement: " + pstmt1.toString());
            int result = pstmt1.executeUpdate();
            logger.info("Query execution status - " + result);
        } catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
            serverOb.comments.append("Step5UURRecondayUpdate:"+e.getMessage()+"|");
            status=false;
        } finally {
            //return cpsEmptyList.toString();
            return status;
        }
    }

    public String checkMandatory(Properties prop,HashMap<Integer,String> headerMap,String [] data,
                                        String[] headerInfo)
    {
        logger.info("Inside Check Manadatory Attributes");
        String [] attrs = prop.getProperty("MandatoryAttributes").split(",");
        String error = "";
        String serviceOwner = null;

        try {
            if (attrs.length > 1) {
                for (int i = 0; i < attrs.length; i++) {
                    int key = getKeybyValue(headerMap, attrs[i]);
                    logger.info("Checking Key position : " + key + "value of header : " + attrs[i] + "value of data : " + data[key]);
                    if (data[key] == null) {
                        error = error + attrs[i] + ",";
                        logger.info("Failed : Mandatory Attributes Missing :" + error);
                    } else {
                        if (attrs[i].equalsIgnoreCase("Service Owner")) {
                            serviceOwner = data[key];
                        }
                    }
                }
                if (error.equals("")) {
                    String serverName = data[0];

                    if (data.length == headerInfo.length) {
                        //Check if Service owner is valid
                        if (serviceOwner != null) {
                            try {
                                String query = "select statuskey from users where username = ?";
                                PreparedStatement pst = con.prepareStatement(query);
                                pst.setString(1, serviceOwner);
                                logger.info("Executing query - " + pst.toString());
                                ResultSet rs = pst.executeQuery();
                                String serviceownerstatus = null;
                                if (rs.next()) {
                                    serviceownerstatus = rs.getString(1);
                                }

                                if ((serviceownerstatus != null && serviceownerstatus.equals("1"))) {
                                } else {
                                    invalidService.put(serverName, "Invalid/Inactive Service Owner");
                                }
                            } catch (Exception e) {
                                invalidService.put(serverName, "Error while reading Service Owner");
                            }
                        } else {
                            invalidService.put(serverName, "Service Owner not present in the input");
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);;
            error = "Error in checking Mandatory input";
        } finally {
            return error;
        }
    }


    public void updateServerOnboardingStatusTable(Connection con, ServerOnboardingStatusPojo serverOb) {
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            ResultSet rs=null;
            String selectSQL = "INSERT INTO neserveronboardingstatus" +
                    "(arsrequestkey,servername,version,lastsuccessfullstep,createdate,comments,lastsuccessfullstepname)" +
                    " values(?,?,?,?,?,?,?)";

            PreparedStatement pstmt = con.prepareStatement(selectSQL);
            pstmt=con.prepareStatement(selectSQL);
            pstmt.setString(1,serverOb.requestkey);
            pstmt.setString(2,serverOb.servername);
            pstmt.setInt(3, serverOb.version+1);
            pstmt.setInt(4, serverOb.lastsuccessfullstep);
            pstmt.setTimestamp(5, timestamp);
            pstmt.setString(6, serverOb.comments.toString());
            pstmt.setString(7, serverOb.lastsuccessfullstepname);

            logger.info("Executing statement: "+pstmt.toString());
            pstmt.execute();
            logger.info("Statement executed successfully");
        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
        }
    }


    public boolean checkUserStatus(Properties prop,String username)
    {
        boolean status = false;
        String fin = "username : " + username;
        try {
            String url = prop.getProperty("URLPATH") + "api/v5/getUser";
            com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn httpConn = new com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn();
            HttpsURLConnection con1 = null;
            JSONObject getConnectionResponse = null;
            JSONParser parser1 = new JSONParser();
            JSONObject userjson = new JSONObject();
            JSONArray arr = new JSONArray();
            arr.add(fin);
            userjson.put("filtercrteria", arr);
            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            con1 = httpConn.sendPutHttps(url, userjson.toString(), httpConnectionHeaders);
            int code = con1.getResponseCode();
            if(code == 401) {
                refreshToken(prop);
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                con1 = httpConn.sendPutHttps(url, userjson.toString(), httpConnectionHeaders);
            }
            
            getConnectionResponse = httpConn.fetchJsonObject(con1);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(getConnectionResponse.toString());

            String statuskey = (String)json.get("statuskey");
            String endpointcreatestatus = (String) json.get("msg");
            String errorCode = (String) json.get("errorCode");
            if (errorCode != null && errorCode.equals("0") && statuskey.equalsIgnoreCase("1"))
                status = true;
            else
                status = false;
        }
        catch (Exception e)
        {
            logger.error("Exception - " +  e.getMessage(), e);;

        }
        finally {
            return status;
        }

    }

    public Integer getKeybyValue (Map <Integer,String> map, String value) {
        for (Map.Entry<Integer, String> entry : map.entrySet()) {
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }


    public void deleteTrigger(Properties prop, com.SaviyntCustom.ServerOnboaringNE.ServerOnboardingStatusPojo serverOb, String triggerName)
    {
        try {
            String url=prop.getProperty("URLPATH")+"api/v5/deleteTrigger";
            com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn httpConn = new com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn();

            HttpsURLConnection con1 = null;
            JSONObject getJobMetadataResponse = null;
            JSONParser parser1 = new JSONParser();
            JSONObject endpointjson =new JSONObject();
            endpointjson.put("triggername",triggerName);
            endpointjson.put("jobname","TriggerChainJob");
            endpointjson.put("jobgroup","utility");
            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            con1 = httpConn.sendPostHttps(url, endpointjson.toString(), httpConnectionHeaders);
            int code = con1.getResponseCode();
            if(code == 401) {
                refreshToken(prop);
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                con1 = httpConn.sendPostHttps(url, endpointjson.toString(), httpConnectionHeaders);
            }
            getJobMetadataResponse = httpConn.fetchJsonObject(con1);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(getJobMetadataResponse.toString());

            Long errorCode = (Long)json.get("errorCode");

            if(errorCode!=null && errorCode==0)
            {
                logger.info("Trigger deleted : "+triggerName);
            }
            else
            {
                //comments.append(" "+jobname+":"+"Could not execute trigger chain job");
                logger.info("Could not delete trigger : "+triggerName);
            }
        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
            serverOb.comments.append(" DeleteTriggers"+":"+e.getMessage());
        }
    }


    public void updateServerOnboardingStatus(Properties prop, com.SaviyntCustom.ServerOnboaringNE.ServerOnboardingStatusPojo serverOb) {
        logger.info("Inside updateServerOnboardingStatus...");

        try {
            logger.info("Setting : " + serverOb.servername + "=" + String.valueOf(serverOb.lastsuccessfullstep));
            prop.setProperty(serverOb.servername,String.valueOf(serverOb.lastsuccessfullstep));

            String saviyntHome = System.getenv("SAVIYNT_HOME");
            File f = new File(saviyntHome + File.separator + "NEBoardingStatus.txt");
            OutputStream out = new FileOutputStream( f );
            prop.store(out, null);

        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
        }
    }

    public int getMaxVersionForRequest(Connection con, String reqkey) {

        int version=0;
        logger.info("Inside getMaxVersionForRequest...");

        try {
            ResultSet rs=null;
            String selectSQL = "SELECT max(version) as maxversion FROM neserveronboardingstatus " +
                    "WHERE arsrequestkey = ?  limit 1";


            PreparedStatement pstmt = con.prepareStatement(selectSQL);
            pstmt=con.prepareStatement(selectSQL);
            pstmt.setString(1,reqkey);

            logger.info("Executing statement: "+pstmt.toString());
            rs=pstmt.executeQuery();
            logger.info("Statement executed successfully");

            while(rs.next())
            {
                version = rs.getInt("maxversion");
            }
        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
        }
        finally {
            return version;
        }
    }

    public boolean validateInput(Properties prop,String requestKey,
                                        String [] serverData)
    {
        boolean status = false;
        String fileErr = null;
        String lineErr = null;

        logger.info("Inside validateInput...");

        if (serverData == null) {
            fileErr = "Server file is missing in the request. Please check and try again!";
            logger.info("Server file is missing in the request.");
        } else if (serverData != null && serverData.length > 0) {
            String[] serverHeaderInfo = serverData[0].split(",");

            if (serverHeaderInfo != null) {
                String[] serverHeaderNeeded = prop.getProperty("SERVERFILE_HEADERS").split(",");
                int serverHeaderNeededCnt = Integer.parseInt(prop.getProperty("SERVERFILE_HEADERS_COUNT"));
                boolean wrongHeader = false;
                int i;
                if(serverHeaderInfo.length == serverHeaderNeededCnt) {
                    for (i = 0; i < serverHeaderNeededCnt; i++) {
                        serverHeaderInfo[i] = serverHeaderInfo[i].trim();
                        if(!serverHeaderInfo[i].equalsIgnoreCase(serverHeaderNeeded[i])) {
                            wrongHeader = true;
                            break;
                        }
                    }
                    if(wrongHeader) {
                        fileErr = "Error in Server file headers. (" + serverHeaderNeeded[i]
                                + ") not found. Please check and try again!";
                    }
                } else {
                    fileErr = "Error in Server file headers. Please check and try again!";
                    logger.info("Error in Server file headers.");
                }
            } else {
                fileErr = "Error in Server file headers. Please check and try again!";
                logger.info("Error in Server file headers.");
            }

            if (fileErr == null) {
                status = true;
                int i;
                lineErr = "";
                //logger.info("Total lines - " + data.length);
                //logger.info("Total header count - " + headerInfo.length);
                for (i = 1; i < serverData.length; i++) {
                    String[] dataInfo = serverData[i].split(",", -1);
                    //logger.info("Line # - " + i + " Length - " + dataInfo.length);

                    if (dataInfo.length != serverHeaderInfo.length) {
                        lineErr = lineErr + serverData[i] + "<br>";
                    }
                }
                if (lineErr != null) {
                    errInputLinesServer.put(requestKey, lineErr);
                }
            }
        } else {
            fileErr = "Invalid Server file. Please check and try again!";
            logger.info("Invalid Server file.");
        }
        if (fileErr != null) {
            errInputFiles.put(requestKey, fileErr);
        }
        return status;
    }


    public HashMap<String,String> findStatusDetails(Connection con, String reqkey,String servername) {

        String lastsuccessfullstep="0";
        String version="0";
        String comments = "";
        HashMap<String,String> llist=new HashMap<>();
        llist.put("lastsuccessfullstep",lastsuccessfullstep);
        llist.put("version",version);
        llist.put("comments",comments);
        try {
            ResultSet rs=null;
            String selectSQL = "SELECT * FROM neserveronboardingstatus " +
                    "WHERE arsrequestkey = ? and servername=? order by version desc limit 1";

            PreparedStatement pstmt = con.prepareStatement(selectSQL);
            pstmt=con.prepareStatement(selectSQL);
            pstmt.setString(1,reqkey);
            pstmt.setString(2,servername);

            logger.info("Executing statement: "+pstmt.toString());
            rs=pstmt.executeQuery();
            logger.info("Statement executed successfully");

            while(rs.next()) {
                lastsuccessfullstep = rs.getString("lastsuccessfullstep");
                version = rs.getString("version");
                comments = rs.getString("comments");
                llist.put("lastsuccessfullstep",lastsuccessfullstep);
                llist.put("version",version);
                llist.put("comments",comments);
            }
        }
        catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);;
        }
        finally {
            return llist;
        }
    }


    public HashMap<String,String> findStatusDetails(Properties StatusProp,String servername) {
        logger.info("Inside findStatusDetails...");

        String lastsuccessfullstep = null;
        String version="0";

        lastsuccessfullstep = StatusProp.getProperty(servername);
        if(lastsuccessfullstep == null) {
            lastsuccessfullstep = "0";
        }

        HashMap<String,String> llist=new HashMap<>();
        llist.put("lastsuccessfullstep",lastsuccessfullstep);
        llist.put("version",version);

        return llist;
    }

    public String findECMImportJobId(Connection con) {
        com.SaviyntCustom.ServerOnboaringNE.ServerOnboardingStatusPojo serverobj=new com.SaviyntCustom.ServerOnboaringNE.ServerOnboardingStatusPojo();
        String jobid="";
        try {
            ResultSet rs=null;
            String selectSQL = "select jobid from ecmimportjob where jobname='TriggerChainJob' " +
                    "and jobstartdate is not null and jobenddate is null order by jobstartdate desc limit 1";

            PreparedStatement pstmt = con.prepareStatement(selectSQL);
            pstmt=con.prepareStatement(selectSQL);

            logger.info("Executing statement: "+pstmt.toString());
            rs=pstmt.executeQuery();
            logger.info("Statement executed successfully");

            while(rs.next())
            {
                jobid = rs.getString("jobid");

            }
        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
        }
        finally {
            return jobid;
        }
    }

    public String checkTriggerStatusDB(Properties prop,Connection connection, com.SaviyntCustom.ServerOnboaringNE.ServerOnboardingStatusPojo serverOb,
                                              String connectionname,String triggerName,
                                              String jobName, String jobgroup,String startDate,String stepName) {
        String status="CONTINUE";
        try {
            logger.info("Inside checkTriggerStatusDB - "+triggerName);

            if(jobName.equalsIgnoreCase("SapImportJob") || jobName.equalsIgnoreCase("ApplicationDataImportJob")) {
                ResultSet rs1=null;
                String selectSQL1 = "select job.JOBID,job.COMENTS,job.SAVRESPONSE,job.JOBSTARTDATE,job.JOBENDDATE,log.LOGDATAASXML from ecmimportjob job left join importlog log on job.jobid = log.jobid where job.jobname = ? and (job.EXTERNALCONNECTION = ? || job.SYSTEMNAME = ? ) and job.JOBSTARTDATE >= STR_TO_DATE( ? , '%Y-%m-%d %T' ) and job.UPDATEUSER = ? limit 1;";
                PreparedStatement pstmt = con.prepareStatement(selectSQL1);
                pstmt=con.prepareStatement(selectSQL1);
                pstmt.setString(1,jobName);
                pstmt.setString(2,connectionname);
                pstmt.setString(3,connectionname);
                pstmt.setString(4,startDate);
                pstmt.setString(5,prop.getProperty("SAVUSERNAME"));

                logger.info("Executing statement: "+pstmt.toString());
                rs1=pstmt.executeQuery();
                logger.info("Statement executed successfully");

                if(rs1.next() != false) {
                    String savresponse = rs1.getString("SAVRESPONSE");
                    String logdataasxml = rs1.getString("LOGDATAASXML");
                    String jobstartdate = rs1.getString("JOBSTARTDATE");
                    String jobenddate = rs1.getString("JOBENDDATE");

                    logger.info("Response - "+savresponse + ".\nLogXML - " + logdataasxml
                            + ".\nJob Start Date - " + jobstartdate+ ".Job End Date - " + jobenddate + "\n");

                    if(jobenddate!=null && !jobenddate.equals(jobstartdate) && logdataasxml!=null) {
                        if (savresponse.equalsIgnoreCase("Success") &&
                                !( (logdataasxml.contains("<Groups_Updated>0</Groups_Updated>") && logdataasxml.contains("<Groups_Created>0</Groups_Created>"))
                                        || (logdataasxml.contains("<Accounts_Updated>0</Accounts_Updated>") && logdataasxml.contains("<Accounts_Created>0</Accounts_Created>")) ) ) {
                            status = "COMPLETED";
                        } else if (savresponse.equalsIgnoreCase("Success") &&
                                (logdataasxml.contains("<Groups_Updated>0</Groups_Updated>") || logdataasxml.contains("<Accounts_Updated>0</Accounts_Updated>"))) {
                            status = "FAILED";
                            serverOb.comments.append(stepName+":"+savresponse+". Groups and Accounts are not updated. Please check permission for service account|");
                        } else if (!savresponse.equalsIgnoreCase("Success")){
                            serverOb.comments.append(stepName+":"+savresponse+"|");
                            status = "FAILED";
                        }
                    }
                } else {
                    logger.info("Job has not started yet");
                }
            } else if(jobName.equalsIgnoreCase("CustomQueryJob")) {
                ResultSet rs2=null;
                String selectSQL2 = "select JOBID,COMENTS,SAVRESPONSE,JOBSTARTDATE,JOBENDDATE from ecmimportjob where COMENTS like '%jobtriggername:${triggerName}%' and jobname = ? and JOBSTARTDATE >= STR_TO_DATE( ? , '%Y-%m-%d %T' ) and UPDATEUSER = ? limit 1;";
                selectSQL2=selectSQL2.replace("${triggerName}",triggerName);

                PreparedStatement pstmt2 = con.prepareStatement(selectSQL2);
                pstmt2=con.prepareStatement(selectSQL2);
                pstmt2.setString(1,jobName);
                pstmt2.setString(2,startDate);
                pstmt2.setString(3,prop.getProperty("SAVUSERNAME"));

                logger.info("Executing statement: "+pstmt2.toString());
                rs2=pstmt2.executeQuery();
                logger.info("Statement executed successfully");

                if(rs2.next() != false) {
                    String savresponse = rs2.getString("SAVRESPONSE");
                    String jobstartdate = rs2.getString("JOBSTARTDATE");
                    String jobenddate = rs2.getString("JOBENDDATE");

                    logger.info("Response - "+savresponse +  ".\nJob Start Date - " + jobstartdate+ ".\nJob End Date - " + jobenddate);

                    if (savresponse.equalsIgnoreCase("Success")) {
                        status = "COMPLETED";
                    } else if (!savresponse.equalsIgnoreCase("Success")){
                        serverOb.comments.append(stepName+":"+savresponse+"|");
                        status = "FAILED";
                    }
                } else {
                    logger.info("Job has not started yet");
                }
            } else if(jobName.equalsIgnoreCase("TriggerChainJob")) {
                ResultSet rs3=null;
                String selectSQL3 = "select JOBID,COMENTS,SAVRESPONSE,JOBSTARTDATE,JOBENDDATE from ecmimportjob where jobname = ? and JOBSTARTDATE >= STR_TO_DATE( ? , '%Y-%m-%d %T' ) and UPDATEUSER = ? limit 1;";

                PreparedStatement pstmt3 = con.prepareStatement(selectSQL3);
                pstmt3=con.prepareStatement(selectSQL3);
                pstmt3.setString(1,jobName);
                pstmt3.setString(2,startDate);
                pstmt3.setString(3,prop.getProperty("SAVUSERNAME"));

                logger.info("Executing statement: "+pstmt3.toString());
                rs3=pstmt3.executeQuery();
                logger.info("Statement executed successfully");

                if(rs3.next() != false) {
                    String savresponse = rs3.getString("SAVRESPONSE");
                    String jobstartdate = rs3.getString("JOBSTARTDATE");
                    String jobenddate = rs3.getString("JOBENDDATE");

                    logger.info("Response - "+savresponse +  ".\nJob Start Date - " + jobstartdate+ ".\nJob End Date - " + jobenddate);

                    if ((jobenddate!=null && !jobenddate.equals(jobstartdate))
                            && savresponse.equalsIgnoreCase("Success")) {
                        status = "COMPLETED";
                    } else if (!savresponse.equalsIgnoreCase("Success")){
                        serverOb.comments.append(stepName+":"+savresponse+"|");
                        status = "FAILED";
                    }
                } else {
                    logger.info("Job has not started yet");
                }
            } else {
            }
        }
        catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);;
            serverOb.comments.append(stepName+":"+e.getMessage()+"|");
            status="FAILED";
        }
        finally {
            return status;
        }
    }

    public String checkTriggerStatus(Properties prop,
                                            com.SaviyntCustom.ServerOnboaringNE.ServerOnboardingStatusPojo serverOb, String triggerName, String jobName) {
        String triggerrunstatus = null;
        try {
            logger.info("Running trigger : " + triggerName + " of type "+jobName);

            String url=prop.getProperty("URLPATH")+"api/v5/fetchJobMetadata";
            com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn httpConn = new com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn();

            HttpsURLConnection con1 = null;
            JSONObject getJobMetadataResponse = null;
            JSONParser parser1 = new JSONParser();
            JSONObject endpointjson =new JSONObject();
            endpointjson.put("jobname",jobName);
            endpointjson.put("triggername",triggerName);

            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            con1 = httpConn.sendPostHttps(url, endpointjson.toString(), httpConnectionHeaders);
            int code = con1.getResponseCode();
            if(code == 401) {
                refreshToken(prop);
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                con1 = httpConn.sendPostHttps(url, endpointjson.toString(), httpConnectionHeaders);
            }
            
            getJobMetadataResponse = httpConn.fetchJsonObject(con1);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(getJobMetadataResponse.toString());

            Long errorCode = (Long)json.get("errorCode");
            logger.info("Trigger status : "+json);

            if(errorCode!=null && errorCode==0)
            {
                triggerrunstatus = "COMPLETED";
            }
            else
            {
                //comments.append(" "+jobname+":"+"Could not execute trigger chain job");
                triggerrunstatus="FAILED";
            }
        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
            serverOb.comments.append(" "+triggerName+":"+e.getMessage());
            triggerrunstatus="FAILED";
        }
        finally {
            return triggerrunstatus;
        }
    }

    public String checkTriggerChainStatus(Properties prop, com.SaviyntCustom.ServerOnboaringNE.ServerOnboardingStatusPojo serverOb,
                                                 String jobname, String lasttrigger, String runningjobname) {
        String triggerrunstatus ="false";
        try {
            String url=prop.getProperty("URLPATH")+"api/v5/fetchJobMetadata";
            com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn httpConn = new com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn();

            HttpsURLConnection con1 = null;
            JSONObject getJobMetadataResponse = null;
            JSONParser parser1 = new JSONParser();
            JSONObject endpointjson =new JSONObject();
            endpointjson.put("jobname",runningjobname);
            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            con1 = httpConn.sendPostHttps(url, endpointjson.toString(), httpConnectionHeaders);
            int code = con1.getResponseCode();
            if(code == 401) {
                refreshToken(prop);
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                con1 = httpConn.sendPostHttps(url, endpointjson.toString(), httpConnectionHeaders);
            }
            
            getJobMetadataResponse = httpConn.fetchJsonObject(con1);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(getJobMetadataResponse.toString());

            Long errorCode = (Long)json.get("errorCode");

            if(errorCode!=null && errorCode==0)
            {

                if(jobname.equalsIgnoreCase("Step5accountgroupcustomquery"))
                {
                    JSONObject resultjson = (JSONObject)json.get("result");
                    JSONObject CustomQueryJobjson = (JSONObject)resultjson.get(runningjobname);
                    String lastruntrigger = (String)CustomQueryJobjson.get("jobtriggername");
                    if(lastruntrigger.equalsIgnoreCase(lasttrigger))
                    {
                        triggerrunstatus = "true";
                    }
                    else
                    {
                        triggerrunstatus = "false";
                        logger.info("Executing query : "+lastruntrigger);
                    }
                }
                else
                {
                    triggerrunstatus = "true";
                }

            }
            else
            {
                //comments.append(" "+jobname+":"+"Could not execute trigger chain job");
                triggerrunstatus="false";
            }
        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
            serverOb.comments.append(" "+jobname+":"+e.getMessage());
        }
        finally {
            return triggerrunstatus;
        }
    }


    public boolean runTrigger(Properties prop, com.SaviyntCustom.ServerOnboaringNE.ServerOnboardingStatusPojo serverOb,
                                      String triggername, String jobName,
                                      String jobgroup, JSONObject valueMap,String stepName)
    {
        boolean status=false;
        try {
            String url=prop.getProperty("URLPATH")+"api/v5/runJobTrigger";
            com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn httpConn = new com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn();

            //String[] triggerSequenceList=triggerSequence.split(",");
            HttpsURLConnection con1 = null;
            JSONObject getConnectionResponse = null;
            JSONObject triggerJson =new JSONObject();
            triggerJson.put("triggername",triggername);
            triggerJson.put("jobname",jobName);
            triggerJson.put("jobgroup",jobgroup);
            triggerJson.put("createJobIfDoesNotExist","false");

            if(valueMap!=null) {
                triggerJson.put("valueMap",valueMap);
            }

            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            con1 = httpConn.sendPostHttps(url, triggerJson.toString(), httpConnectionHeaders);
            int code = con1.getResponseCode();
            if(code == 401) {
                refreshToken(prop);
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                con1 = httpConn.sendPostHttps(url, triggerJson.toString(), httpConnectionHeaders);
            }
            
            getConnectionResponse = httpConn.fetchJsonObject(con1);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(getConnectionResponse.toString());

            String triggerchainstatus = (String)json.get("errorCode");
            String msg = (String)json.get("msg");
            logger.info("Trigger chain status : "+triggerchainstatus);

            if(triggerchainstatus.equals("1")){
                stepData = null;
                serverOb.comments.append(stepName+":"+msg+"|");
            }
            else {
                stepData = (String)json.get("timestamp");
                status = true;
            }

        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
            serverOb.comments.append(stepName+":"+e.getMessage()+"|");
            status=false;
        }
        finally {
            return status;
        }
    }

    public boolean  createTriggerChain(Properties prop,String servername, com.SaviyntCustom.ServerOnboaringNE.ServerOnboardingStatusPojo serverOb,
                                               String triggername, String triggerSequence,Connection con,String stepName)
    {
        boolean status=false;
        try {
            ResultSet rs1=null;
            String selectSQL = "select job_data from qrtz_triggers where trigger_name=? and job_name=?";

            PreparedStatement pstmt = con.prepareStatement(selectSQL);
            pstmt=con.prepareStatement(selectSQL);
            pstmt.setString(1,triggername);
            pstmt.setString(2,"TriggerChainJob");

            logger.info("Checking it trigger exists: "+pstmt.toString());
            rs1=pstmt.executeQuery();
            logger.info("Statement executed successfully");
            if(!rs1.next()) {
                String url = prop.getProperty("URLPATH") + "api/v5/createUpdateTrigger";
                com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn httpConn = new com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn();

                //String[] triggerSequenceList=triggerSequence.split(",");
                HttpsURLConnection con1 = null;
                JSONObject getConnectionResponse = null;
                JSONObject triggerchainjson =new JSONObject();
                triggerchainjson.put("triggername",triggername);
                triggerchainjson.put("jobname","TriggerChainJob");
                triggerchainjson.put("jobgroup","utility");
                JSONObject triggerchainsubjson =new JSONObject();
                triggerchainsubjson.put("savtriggerorderform",triggerSequence);
                triggerchainsubjson.put("onFailureForm","Continue");

                triggerchainjson.put("valueMap",triggerchainsubjson);
                Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                logger.info("triggerchainjson : "+triggerchainjson);

                con1 = httpConn.sendPostHttps(url, triggerchainjson.toString(), httpConnectionHeaders);
                int code = con1.getResponseCode();
                if(code == 401) {
                    refreshToken(prop);
                    httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                    con1 = httpConn.sendPostHttps(url, triggerchainjson.toString(), httpConnectionHeaders);
                }
                getConnectionResponse = httpConn.fetchJsonObject(con1);
                String triggerchainstatus = null;
                String msg = "Trigger sequence creation failed";
                if(getConnectionResponse!=null) {
                    JSONParser parser = new JSONParser();
                    JSONObject json = (JSONObject) parser.parse(getConnectionResponse.toString());

                    triggerchainstatus = (String) json.get("errorCode");
                    msg = (String) json.get("msg");
                }
                logger.info("Trigger chain status : "+triggerchainstatus);

                if((triggerchainstatus==null) || (triggerchainstatus!=null && triggerchainstatus.equals("1"))) {
                    serverOb.comments.append("Step5accountgroupcustomquery:"+msg+"|");
                } else if(triggerchainstatus!=null && triggerchainstatus.equals("0")) {
                    status=true;
                }
            } else {
                logger.info("Trigger "+triggername+" exists.");
                status = true;
            }
        }
        catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);;
            serverOb.comments.append(stepName+":"+e.getMessage()+"|");
            status=false;
        }
        finally {
            //return cpsEmptyList.toString();
            return status;
        }
    }

    public boolean  createRunTriggerChain(Properties prop, String triggername,
                                                  String triggerSequence, com.SaviyntCustom.ServerOnboaringNE.ServerOnboardingStatusPojo serverOb, String stepName)
    {
        boolean status=false;
        try {
            logger.info("Inside triggerChainAccountsGroups : "+triggerSequence);

            String url=prop.getProperty("URLPATH")+"api/v5/runJobTrigger";
            com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn httpConn = new com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn();

            //String[] triggerSequenceList=triggerSequence.split(",");
            HttpsURLConnection con1 = null;
            JSONObject getConnectionResponse = null;
            JSONObject triggerchainjson =new JSONObject();
            triggerchainjson.put("triggername",triggername);
            triggerchainjson.put("jobname","TriggerChainJob");
            triggerchainjson.put("jobgroup","utility");
            triggerchainjson.put("createJobIfDoesNotExist","true");
            JSONObject triggerchainsubjson =new JSONObject();
            triggerchainsubjson.put("savtriggerorderform",triggerSequence);
            triggerchainsubjson.put("onFailureForm","Continue");

            triggerchainjson.put("valueMap",triggerchainsubjson);
            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            logger.info("triggerchainjson : "+triggerchainjson);

            con1 = httpConn.sendPostHttps(url, triggerchainjson.toString(), httpConnectionHeaders);
            int code = con1.getResponseCode();
            if(code == 401) {
                refreshToken(prop);
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                con1 = httpConn.sendPostHttps(url, triggerchainjson.toString(), httpConnectionHeaders);
            }
            getConnectionResponse = httpConn.fetchJsonObject(con1);
            String triggerchainstatus = null;
            String msg = "Creating/Running trigger sequence failed";
            if(getConnectionResponse!=null) {
                JSONParser parser = new JSONParser();
                JSONObject json = (JSONObject) parser.parse(getConnectionResponse.toString());

                triggerchainstatus = (String) json.get("errorCode");
                msg = (String) json.get("msg");
            }
            logger.info("Trigger chain status : "+triggerchainstatus);

            if((triggerchainstatus==null) || (triggerchainstatus!=null && triggerchainstatus.equals("1")))
            {
                serverOb.comments.append(stepName+":"+msg+"|");
            } else if(triggerchainstatus!=null && triggerchainstatus.equals("0"))
            {
                status=true;
            }

        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
            serverOb.comments.append(stepName+":"+e.getMessage()+"|");
            status=false;
        }
        finally {
            //return cpsEmptyList.toString();
            return status;
        }
    }

    public boolean  triggerChainAccountsGroups(Properties prop, String triggername,
                                                       String triggerSequence, com.SaviyntCustom.ServerOnboaringNE.ServerOnboardingStatusPojo serverOb)
    {
        boolean status=false;
        try {
            logger.info("Inside triggerChainAccountsGroups : "+triggerSequence);

            String url=prop.getProperty("URLPATH")+"api/v5/runJobTrigger";
            com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn httpConn = new com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn();

            //String[] triggerSequenceList=triggerSequence.split(",");
            HttpsURLConnection con1 = null;
            JSONObject getConnectionResponse = null;
            JSONObject triggerchainjson =new JSONObject();
            triggerchainjson.put("triggername",triggername);
            triggerchainjson.put("jobname","TriggerChainJob");
            triggerchainjson.put("jobgroup","utility");
            triggerchainjson.put("createJobIfDoesNotExist","true");
            JSONObject triggerchainsubjson =new JSONObject();
            triggerchainsubjson.put("savtriggerorderform",triggerSequence);
            triggerchainsubjson.put("onFailureForm","Continue");

            triggerchainjson.put("valueMap",triggerchainsubjson);
            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            logger.info("triggerchainjson : "+triggerchainjson);

            con1 = httpConn.sendPostHttps(url, triggerchainjson.toString(), httpConnectionHeaders);
            int code = con1.getResponseCode();
            if(code == 401) {
                refreshToken(prop);
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                con1 = httpConn.sendPostHttps(url, triggerchainjson.toString(), httpConnectionHeaders);
            }

            getConnectionResponse = httpConn.fetchJsonObject(con1);
            String triggerchainstatus = null;
            String msg = "Running trigger sequence failed";
            if(getConnectionResponse!=null) {
                JSONParser parser = new JSONParser();
                JSONObject json = (JSONObject) parser.parse(getConnectionResponse.toString());

                triggerchainstatus = (String) json.get("errorCode");
                msg = (String) json.get("msg");
            }
            logger.info("Trigger chain status : "+triggerchainstatus);

            if((triggerchainstatus==null) || (triggerchainstatus!=null && triggerchainstatus.equals("1")))
            {
                serverOb.comments.append("Step5accountgroupcustomquery:"+msg+"|");
            } else if(triggerchainstatus!=null && triggerchainstatus.equals("0"))
            {
                status=true;
            }

        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
            serverOb.comments.append("Step5accountgroupcustomquery:"+e.getMessage()+"|");
            status=false;
        }
        finally {
            //return cpsEmptyList.toString();
            return status;
        }
    }

    public boolean  createAccountsAccessTriggerMT(Properties prop, String servername,
                                                  ServerOnboardingStatusPojo serverOb, String triggername, String accOrAccess,
                                                  Connection con, String stepName)
    {
        boolean status=false;
        try {
            ResultSet rs1=null;
            String selectSQL = "select job_data from qrtz_triggers where trigger_name=? and job_name=?";

            PreparedStatement pstmt = con.prepareStatement(selectSQL);
            pstmt=con.prepareStatement(selectSQL);
            pstmt.setString(1,triggername);
            pstmt.setString(2,"ApplicationDataImportJob");

            logger.info("Checking it trigger exists: "+pstmt.toString());
            rs1=pstmt.executeQuery();
            logger.info("Statement executed successfully");
            if(!rs1.next()) {

                String url = prop.getProperty("URLPATH") + "api/v5/createUpdateTrigger";
                HttpUrlConn httpConn = new HttpUrlConn();

                //String[] triggerSequenceList=triggerSequence.split(",");
                HttpsURLConnection con1 = null;
                JSONObject getConnectionResponse = null;
                JSONObject accsImportjson = new JSONObject();
                JSONObject accsImportJsonMain = new JSONObject();
                JSONArray accsImportArr = new JSONArray();
                accsImportjson.put("triggername", triggername);
                accsImportjson.put("jobname", "ApplicationDataImportJob");
                accsImportjson.put("jobgroup", "data");
                accsImportjson.put("cronexpression", prop.getProperty("ACCOUNTSTRIGGERCRONEXP"));
                JSONObject accimportsubjson = new JSONObject();
                JSONArray secSysArr = new JSONArray();
                secSysArr.add(0, servername);
                accimportsubjson.put("securitysystems", secSysArr);
                accimportsubjson.put("connectionname", servername);
                accimportsubjson.put("fullorincremental", "full");
                accimportsubjson.put("accountsoraccess", accOrAccess);
                //accimportsubjson.put("triggergroup", "GRAILS_JOBS");

                accsImportjson.put("valueMap", accimportsubjson);

                accsImportArr.add(0, accsImportjson);

                accsImportJsonMain.put("triggers", accsImportArr);

                logger.info("JSON data : " + accsImportJsonMain);

                Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                con1 = httpConn.sendPostHttps(url, accsImportJsonMain.toString(), httpConnectionHeaders);
                int code = con1.getResponseCode();
                if(code == 401) {
                    refreshToken(prop);
                    httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                    con1 = httpConn.sendPostHttps(url, accsImportJsonMain.toString(), httpConnectionHeaders);
                }

                getConnectionResponse = httpConn.fetchJsonObject(con1);
                JSONParser parser = new JSONParser();
                JSONObject json = (JSONObject) parser.parse(getConnectionResponse.toString());

                String triggerchainstatus = (String) json.get("errorCode");
                String msg = (String) json.get("msg");
                logger.info("Create/Update trigger status : " + triggerchainstatus);

                if (triggerchainstatus.equals("1")) {
                    serverOb.comments.append(stepName + ":" + msg + "|");
                } else
                    status = true;
            } else {
                logger.info("Trigger "+triggername+" exists.");
                status = true;
            }
        }
        catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
            serverOb.comments.append(stepName+":"+e.getMessage()+"|");
            status=false;
        }
        finally {
            //return cpsEmptyList.toString();
            return status;
        }
    }

    public boolean  createAccountsAccessTrigger(Properties prop, String servername,
                                                        com.SaviyntCustom.ServerOnboaringNE.ServerOnboardingStatusPojo serverOb, String triggername, String accOrAccess,
                                                        Connection con, String stepName)
    {
        boolean status=false;
        try {
            ResultSet rs1=null;
            String selectSQL = "select job_data from qrtz_triggers where trigger_name=? and job_name=?";

            PreparedStatement pstmt = con.prepareStatement(selectSQL);
            pstmt=con.prepareStatement(selectSQL);
            pstmt.setString(1,triggername);
            pstmt.setString(2,"SapImportJob");

            logger.info("Checking it trigger exists: "+pstmt.toString());
            rs1=pstmt.executeQuery();
            logger.info("Statement executed successfully");
            if(!rs1.next()) {

                String url = prop.getProperty("URLPATH") + "api/v5/createUpdateTrigger";
                com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn httpConn = new com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn();

                //String[] triggerSequenceList=triggerSequence.split(",");
                HttpsURLConnection con1 = null;
                JSONObject getConnectionResponse = null;
                JSONObject accsImportjson = new JSONObject();
                JSONObject accsImportJsonMain = new JSONObject();
                JSONArray accsImportArr = new JSONArray();
                accsImportjson.put("triggername", triggername);
                accsImportjson.put("jobname", "SapImportJob");
                accsImportjson.put("jobgroup", "data");
                accsImportjson.put("cronexpression", prop.getProperty("ACCOUNTSTRIGGERCRONEXP"));
                JSONObject accimportsubjson = new JSONObject();
                JSONArray secSysArr = new JSONArray();
                secSysArr.add(0, servername);
                accimportsubjson.put("securitysystems", secSysArr);
                accimportsubjson.put("connectionname", servername);
                accimportsubjson.put("fullorincremental", "full");
                accimportsubjson.put("accountsoraccess", accOrAccess);

                accsImportjson.put("valueMap", accimportsubjson);

                accsImportArr.add(0, accsImportjson);

                accsImportJsonMain.put("triggers", accsImportArr);

                logger.info("JSON data : " + accsImportJsonMain);

                Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                con1 = httpConn.sendPostHttps(url, accsImportJsonMain.toString(), httpConnectionHeaders);
                int code = con1.getResponseCode();
                if(code == 401) {
                    refreshToken(prop);
                    httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                    con1 = httpConn.sendPostHttps(url, accsImportJsonMain.toString(), httpConnectionHeaders);
                }

                getConnectionResponse = httpConn.fetchJsonObject(con1);
                JSONParser parser = new JSONParser();
                JSONObject json = (JSONObject) parser.parse(getConnectionResponse.toString());

                String triggerchainstatus = (String) json.get("errorCode");
                String msg = (String) json.get("msg");
                logger.info("Create/Update trigger status : " + triggerchainstatus);

                if (triggerchainstatus.equals("1")) {
                    serverOb.comments.append(stepName + ":" + msg + "|");
                } else
                    status = true;
            } else {
                logger.info("Trigger "+triggername+" exists.");
                status = true;
            }
        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
            serverOb.comments.append(stepName+":"+e.getMessage()+"|");
            status=false;
        }
        finally {
            //return cpsEmptyList.toString();
            return status;
        }
    }

    public boolean  UpdateTriggerOld(Properties prop, com.SaviyntCustom.ServerOnboaringNE.ServerOnboardingStatusPojo serverOb,
                                          String triggername,String accountsTriggerName, Connection con)
    {
        boolean status=false;
        try {
            ResultSet rs3=null;
            String selectSQL = "select job_data from qrtz_triggers where trigger_name=? and job_name=?";

            PreparedStatement pstmt = con.prepareStatement(selectSQL);
            pstmt=con.prepareStatement(selectSQL);
            pstmt.setString(1,triggername);
            pstmt.setString(2,"TriggerChainJob");

            logger.info("Executing statement: "+pstmt.toString());
            rs3=pstmt.executeQuery();
            logger.info("Statement executed successfully");
            String triggernames="";
            String finaltriggersequence=null;
            while(rs3.next())
            {
                java.sql.Blob myBlob = rs3.getBlob("job_data");
                InputStream input = myBlob.getBinaryStream();

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buf = new byte[1024];

                InputStream in = myBlob.getBinaryStream();

                int n = 0;
                while ((n=in.read(buf))>=0)
                {
                    baos.write(buf, 0, n);
                }

                in.close();
                byte[] bytes = baos.toByteArray();
                String strImport = new String(bytes, "UTF-8");
                strImport = strImport.replaceAll("\uFFFD", "\"").replaceAll("t\u25AF", "\"");
                if(strImport.contains("jobnamelabelt")) {
                    strImport = strImport.substring(strImport.lastIndexOf("savtriggerorderformt"), strImport.indexOf("jobnamelabelt")).trim();
                }
                if(strImport.contains("cronexpressiont")) {
                    strImport = strImport.substring(strImport.lastIndexOf("savtriggerorderformt"), strImport.indexOf("cronexpressiont")).trim();
                }
                if(strImport.contains("ACCOUNTS_")) {
                    strImport=strImport.substring(strImport.indexOf("ACCOUNTS_"),strImport.length());
                } else if(strImport.contains("UNIX_ACCOUNT_CORRELATION")) {
                    strImport=strImport.substring(strImport.indexOf("UNIX_ACCOUNT_CORRELATION"),strImport.length());
                }
                strImport=strImport.replace("savtriggerorderformt","");
                strImport=strImport.substring(0,strImport.length()-1).trim();
                triggernames=strImport;
            }

            logger.info("Present sequence : "+triggernames);

            if(triggernames!=null && !triggernames.equals(""))
            {
                triggernames = triggernames.replace("\"", "");
                if(!triggernames.contains(accountsTriggerName)) {
                    finaltriggersequence = accountsTriggerName + "," + triggernames;
                }
                else {
                    finaltriggersequence=triggernames;
                }
            }
            else {
                finaltriggersequence=accountsTriggerName+",UNIX_ACCOUNT_CORRELATION";
            }

            logger.info("Updating trigger chain : "+triggername+".\nFinal Sequence : "+finaltriggersequence);

            String url=prop.getProperty("URLPATH")+"api/v5/createUpdateTrigger";
            com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn httpConn = new com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn();

            //String[] triggerSequenceList=triggerSequence.split(",");
            HttpsURLConnection con1 = null;
            JSONObject getConnectionResponse = null;
            JSONObject triggerchainjson =new JSONObject();
            JSONObject triggerchainjsonmain =new JSONObject();
            JSONArray triggerChainArr=new JSONArray();
            triggerchainjson.put("triggername",triggername);
            triggerchainjson.put("jobname","TriggerChainJob");
            triggerchainjson.put("jobgroup","utility");
            triggerchainjson.put("cronexpression",prop.getProperty("SERVERRECONCRONEXP"));
            JSONObject triggerchainsubjson =new JSONObject();
            triggerchainsubjson.put("savtriggerorderform",finaltriggersequence);
            triggerchainsubjson.put("onFailureForm","Continue");

            triggerchainjson.put("valueMap",triggerchainsubjson);

            triggerChainArr.add(0,triggerchainjson);

            triggerchainjsonmain.put("triggers",triggerChainArr);

            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            con1 = httpConn.sendPostHttps(url, triggerchainjsonmain.toString(), httpConnectionHeaders);
            int code = con1.getResponseCode();
            if(code == 401) {
                refreshToken(prop);
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                con1 = httpConn.sendPostHttps(url, triggerchainjsonmain.toString(), httpConnectionHeaders);
            }

            getConnectionResponse = httpConn.fetchJsonObject(con1);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(getConnectionResponse.toString());

            String triggerchainstatus = (String)json.get("errorCode");
            String msg = (String)json.get("msg");
            logger.info("Trigger chain status : "+triggerchainstatus);

            if(triggerchainstatus.equals("1"))
            {
                serverOb.comments.append("Step5UpdateServerReconTrigger:"+msg+"|");
            }
            else {
                status = true;
            }
        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
            serverOb.comments.append("Step5UpdateServerReconTrigger:"+e.getMessage()+"|");
            status=false;
        }
        finally {
            //return cpsEmptyList.toString();
            return status;
        }
    }

    public  boolean  UpdateTrigger(Properties prop, ServerOnboardingStatusPojo serverOb,
                                   String triggername,String accountsTriggerName, Connection con)
    {
        boolean status=false;
        boolean err = true;
        boolean serverPresent = false;

        try {
            ResultSet rs3=null;
            String selectSQL = "select job_data from qrtz_triggers where trigger_name=? and job_name=?";

            PreparedStatement pstmt = con.prepareStatement(selectSQL);
            pstmt=con.prepareStatement(selectSQL);
            pstmt.setString(1,triggername);
            pstmt.setString(2,"TriggerChainJob");

            logger.info("Executing statement: "+pstmt.toString());
            rs3=pstmt.executeQuery();
            logger.info("Statement executed successfully");
            String triggernames="";
            String finaltriggersequence="";
            while(rs3.next())
            {
                java.sql.Blob myBlob = rs3.getBlob("job_data");
                InputStream input = myBlob.getBinaryStream();

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buf = new byte[1024];

                InputStream in = myBlob.getBinaryStream();

                int n = 0;
                while ((n=in.read(buf))>=0)
                {
                    baos.write(buf, 0, n);
                }

                in.close();
                byte[] bytes = baos.toByteArray();
                String strImport = new String(bytes, "UTF-8");
                strImport = strImport.replaceAll("\uFFFD", "\"").replaceAll("t\u25AF", "\"");
                if(strImport.contains("jobnamelabelt")) {
                    strImport = strImport.substring(strImport.lastIndexOf("savtriggerorderformt"), strImport.indexOf("jobnamelabelt")).trim();
                }
                if(strImport.contains("cronexpressiont")) {
                    strImport = strImport.substring(strImport.lastIndexOf("savtriggerorderformt"), strImport.indexOf("cronexpressiont")).trim();
                }
                if(strImport.contains(prop.getProperty("FIRSTTRIGGERINCHAIN"))) {
                    strImport=strImport.substring(strImport.indexOf(prop.getProperty("FIRSTTRIGGERINCHAIN")),
                            strImport.length());
                }
                strImport=strImport.replace("savtriggerorderformt","");
                strImport=strImport.substring(0,strImport.length()-1).trim();
                triggernames=strImport;
            }

            logger.info("Trigger Name : " + triggername + ".\nSequence : " + triggernames);

            if (triggername != null && triggernames != null && !triggernames.equals("")) {
                triggernames = triggernames.replace("\"", "");
                if (!triggernames.contains(accountsTriggerName + ",")) {
                    if (triggernames.contains(prop.getProperty("FIRSTTRIGGERINCHAIN"))) {
                        int noOfTriggersBeforeRecon = Integer.parseInt(prop.getProperty("NOOFTRIGGERSBEFORERECON"));
                        String triggerChain[] = triggernames.split(",",
                                noOfTriggersBeforeRecon + 1);

                        if (triggerChain != null && triggerChain.length == (noOfTriggersBeforeRecon + 1)) {
                            for (int i = 0; i < (triggerChain.length - 1); i++) {
                                finaltriggersequence = finaltriggersequence + triggerChain[i] + ",";
                            }
                            finaltriggersequence = finaltriggersequence
                                    + accountsTriggerName + "," + triggerChain[noOfTriggersBeforeRecon];
                            err = false;
                        } else {
                            serverOb.comments.append("Step5UpdateServerReconTrigger:" +
                                    "Recon trigger chain is not proper(count). Please check|");
                            serverOb.lastsuccessfullstep = 12;
                        }
                    } else {
                        serverOb.comments.append("Step5UpdateServerReconTrigger:" +
                                "Recon trigger chain is not proper(first). Please check|");
                        serverOb.lastsuccessfullstep = 12;
                    }
                } else {
                    err = false;
                    serverPresent = true;
                    status = true;
                    serverOb.comments.append("Step5UpdateServerReconTrigger:Recon trigger " +
                            "Trigger chain not updated since server is already present|");
                }
            } else {
                serverOb.comments.append("Step5UpdateServerReconTrigger:Recon trigger chain is not present or empty|");
                serverOb.lastsuccessfullstep = 12;
            }

            if(!err && !serverPresent) {

                logger.info("Updating trigger chain : " + triggername + ".\nFinal Sequence : " + finaltriggersequence);

                String url = prop.getProperty("URLPATH") + "api/v5/createUpdateTrigger";
                HttpUrlConn httpConn = new HttpUrlConn();

                //String[] triggerSequenceList=triggerSequence.split(",");
                HttpsURLConnection con1 = null;
                JSONObject getConnectionResponse = null;
                JSONObject triggerchainjson = new JSONObject();
                JSONObject triggerchainjsonmain = new JSONObject();
                JSONArray triggerChainArr = new JSONArray();
                triggerchainjson.put("triggername", triggername);
                triggerchainjson.put("jobname", "TriggerChainJob");
                triggerchainjson.put("jobgroup", "utility");
                triggerchainjson.put("cronexpression", prop.getProperty("SERVERRECONCRONEXP"));
                JSONObject triggerchainsubjson = new JSONObject();
                triggerchainsubjson.put("savtriggerorderform", finaltriggersequence);
                triggerchainsubjson.put("onFailureForm", "Continue");

                triggerchainjson.put("valueMap", triggerchainsubjson);

                triggerChainArr.add(0, triggerchainjson);

                triggerchainjsonmain.put("triggers", triggerChainArr);

                Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                con1 = httpConn.sendPostHttps(url, triggerchainjsonmain.toString(), httpConnectionHeaders);
                int code = con1.getResponseCode();
                if (code == 401) {
                    refreshToken(prop);
                    httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                    con1 = httpConn.sendPostHttps(url, triggerchainjsonmain.toString(), httpConnectionHeaders);
                }

                getConnectionResponse = httpConn.fetchJsonObject(con1);
                JSONParser parser = new JSONParser();
                JSONObject json = (JSONObject) parser.parse(getConnectionResponse.toString());

                String triggerchainstatus = (String) json.get("errorCode");
                String msg = (String) json.get("msg");
                logger.info("Trigger chain status : " + triggerchainstatus);

                if (triggerchainstatus.equals("1")) {
                    serverOb.comments.append("Step5UpdateServerReconTrigger:" + msg + "|");
                } else {
                    status = true;
                }
            }
        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
            serverOb.comments.append("Step5UpdateServerReconTrigger:"+e.getMessage()+"|");
            status=false;
        }
        finally {
            //return cpsEmptyList.toString();
            return status;
        }
    }


/*
Rest api impl for validateAccounts
 */
    /*
    public boolean  validateAccounts(Properties prop,String connectionname)
    {
        boolean status=false;
        HashSet<String> cpsEmptyList=new HashSet<>();
        HashSet<String> inactiveAccountList=new HashSet<>();
        try {
            String url=prop.getProperty("URLPATH")+"api/v5/getAccounts";
            HttpUrlConn httpConn = new HttpUrlConn();
            String accountCPsToBeValidated=prop.getProperty("ACCOUNTCPSTOBEVALIDATED");
            String[] accountCPsToBeValidatedList=accountCPsToBeValidated.split(",");
            HttpsURLConnection con1 = null;
            JSONObject getConnectionResponse = null;
            JSONParser parser1 = new JSONParser();
            JSONObject endpointjson =new JSONObject();
            endpointjson.put("endpoint",connectionname);
            endpointjson.put("max",prop.getProperty("MAXREQUESTS"));

            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            con1 = httpConn.sendPostHttps(url, endpointjson.toString(), httpConnectionHeaders);
            getConnectionResponse = httpConn.fetchJsonObject(con1);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(getConnectionResponse.toString());

            String connctioncreatestatus = (String)json.get("msg");
            logger.info("Endpoint create status : "+connctioncreatestatus);

            JSONArray accresults = (JSONArray)json.get("Accountdetails");
            for (int i = 0; i < accresults.size(); i++) {
                JSONObject userDetails=(JSONObject)accresults.get(i);
                String accname = (String)userDetails.get("name");
                String accstatus = (String)userDetails.get("status");

                if(accstatus!=null && accname!=null && accstatus.equals("2"))
                {
                    inactiveAccountList.add(accname);
                }

                for(String cp:accountCPsToBeValidatedList)
                {
                    String cpval = (String)userDetails.get(cp);
                    if(cpval==null)
                    {
                        cpsEmptyList.add(cp);
                    }
                }
            }
            logger.info("Empty cps founds for - "+cpsEmptyList.toString());

            logger.info("Updating following inactive accounts with cp2 as current date - " + inactiveAccountList.toString());
            java.sql.Timestamp startdate = new java.sql.Timestamp(new java.util.Date().getTime());
            String datetime=String.valueOf(startdate);
            for(String accname:inactiveAccountList)
            {
                JSONObject accObj=new JSONObject();
                accObj.put("securitysystem",connectionname);
                accObj.put("endpoint",connectionname);
                accObj.put("customproperty2",datetime);
                accObj.put("name",accname);
                updateAccount(prop,outhToken,accObj);
            }

            if(cpsEmptyList.size()>0)
            {
                comments.append("Step7validateaccounts:Following cps are null - "+cpsEmptyList.toString());
            }
            status=true;

        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
            comments.append("Step7validateaccounts:"+e.getMessage());
            status=false;
        }
        finally {
            return status;
        }
    }
*/


    public boolean updateAndValidateAccounts(Properties prop, com.SaviyntCustom.ServerOnboaringNE.ServerOnboardingStatusPojo serverOb, String connectionname, Connection con)
    {
        boolean status=false;
        boolean proceed=false;
        HashSet<String> cpsEmptyList=new HashSet<>();
        HashSet<String> inactiveAccountList=new HashSet<>();
        HashSet<String> inactiveAccountKey=new HashSet<>();
        try {
            //Run account correlation for inactive accounts
            if (correlateInctiveAccounts(connectionname, con, prop)) {
                proceed = true;
            } else {
                serverOb.comments.append("Step7ValidateAccounts:Correlating inactive accounts failed|");
            }
            if(proceed) {
                proceed = false;
                //Update cp22 of endpoint to "giserver before running GI Correlation trigger
                if (updateEndpointStatusAndCp22(prop, con, connectionname, "giserver")) {
                    if (unixAccountCorrelation(prop, con, connectionname)) {
                        proceed = true;
                    } else {
                        serverOb.comments.append("Step7ValidateAccounts: Unix account correlation failed|");
                    }
                } else {
                    serverOb.comments.append("Step7ValidateAccounts:Updating cp22 to giserver failed|");
                }
            }

            if(proceed) {
                String accountCPsToBeValidated = prop.getProperty("ACCOUNTCPSTOBEVALIDATED");
                String[] accountCPsToBeValidatedList = accountCPsToBeValidated.split(",");
                String selectSQL = "select " + accountCPsToBeValidated + " from accounts,endpoints where accounts.endpointkey=endpoints.endpointkey and endpoints.endpointname=?";
                PreparedStatement pstmt = con.prepareStatement(selectSQL);
                pstmt = con.prepareStatement(selectSQL);
                pstmt.setString(1, connectionname);

                logger.info("Executing statement: " + pstmt.toString());
                ResultSet rs3 = pstmt.executeQuery();
                logger.info("Statement executed successfully");

                while (rs3.next()) {
                    String accname = (String) rs3.getString("name");
                    String accstatus = (String) rs3.getString("status");
                    String acckey = (String) rs3.getString("accountkey");
                    if (accstatus != null && accname != null && accstatus.equals("2")) {
                        inactiveAccountList.add(accname);
                        inactiveAccountKey.add(acckey);
                    }


                    for (String cp : accountCPsToBeValidatedList) {
                        String cpval = (String) rs3.getString(cp);
                        if (cpval == null) {
                            cpsEmptyList.add(cp);
                        }
                    }

                }

                logger.info("Empty cps founds for - " + cpsEmptyList.toString());

                if(!inactiveAccountList.isEmpty()) {

                    logger.info("Updating following inactive accounts with cp2 as current date - " + inactiveAccountList.toString());
                    java.sql.Timestamp startdate = new java.sql.Timestamp(new java.util.Date().getTime());
                    String datetime = String.valueOf(startdate);
                    String accList = "(";
                    for (String accKey : inactiveAccountKey) {
                        accList = accList + accKey + ",";
                    }
                    if ((accList != null) && (accList.length() > 0)) {
                        accList = accList.substring(0, accList.length() - 1);
                    }
                    accList = accList + ")";
                    if(!updateInactiveAccounts(prop,con,connectionname,datetime,accList)) {
                        serverOb.comments.append("Step7validateaccounts:Error in updating cp2 of inactive accounts");
                    }

                }

                if (cpsEmptyList.size() > 0) {
                    serverOb.comments.append("Step7validateaccounts:Following cps are null - " + cpsEmptyList.toString());
                }
                status = true;
            }
        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
            serverOb.comments.append("Step7validateaccounts:"+e.getMessage());
            status=false;
        }
        finally {
            return status;
        }
    }


    public void  updateAccount(Properties prop,JSONObject accObj)
    {
        try {
            String url=prop.getProperty("URLPATH")+"api/v5/updateAccount";
            com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn httpConn = new com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn();

            HttpsURLConnection con1 = null;
            //HttpURLConnection con2 = null;
            JSONObject getAccountResponse = null;
            JSONParser parser1 = new JSONParser();
            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            con1 = httpConn.sendPostHttps(url, accObj.toString(), httpConnectionHeaders);
            int code = con1.getResponseCode();
            if(code == 401) {
                refreshToken(prop);
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                con1 = httpConn.sendPostHttps(url, accObj.toString(), httpConnectionHeaders);
            }

            getAccountResponse = httpConn.fetchJsonObject(con1);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(getAccountResponse.toString());

            String connctioncreatestatus = (String)json.get("message");
            logger.info("Update account status : "+connctioncreatestatus);
        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
        }
    }

    public boolean updateEndpointStatusAndCp22(Properties prop, Connection con, String connectionname, String value) {
        boolean res=true;
        try {
            logger.info("Inside updateEndpointCp22");

            String uptQuery=prop.getProperty("UPDATESTATUSCP22").replace("${endpointname}","'"+connectionname+"'");
            uptQuery=uptQuery.replace("${value}","'"+value+"'");
            PreparedStatement pstmt=con.prepareStatement(uptQuery);

            logger.info("Executing statement: "+pstmt.toString());
            int result=pstmt.executeUpdate();
            logger.info("Statement executed successfully");

        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
            res=false;
        }
        finally {
            return res;
        }
    }

    public boolean updateOtherEndpointAttrs(Properties prop,Connection con,String connectionname) {
        boolean res=true;
        try {
            logger.info("Inside updateOtherEndpointAttrs");

            String query1 ="select customproperty1 from endpoints where endpointname = ?";
            PreparedStatement pst = con.prepareStatement(query1);
            pst.setString(1,connectionname);
            ResultSet rs = pst.executeQuery();
            rs.next();
            String osType = rs.getString(1);
            String defaultpg = null;
            if(osType!=null && osType.toUpperCase().contains("AIX")) {
                defaultpg = "staff";
            } else {
                defaultpg = "users";
            }

            String uptQuery=prop.getProperty("UPDATEENPOINTATRRS");
            uptQuery=uptQuery.replace("${endpointname}","'"+connectionname+"'");
            uptQuery=uptQuery.replace("${defaultpg}","'"+defaultpg+"'");
            PreparedStatement pstmt=con.prepareStatement(uptQuery);

            logger.info("Executing statement: "+pstmt.toString());
            int result=pstmt.executeUpdate();
            logger.info("Statement executed successfully");

        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
            res=false;
        }
        finally {
            return res;
        }
    }

    public boolean checkKeyRotationStatus(Properties prop,Connection con,String connectionname) {
        boolean res=false;

        try {
            logger.info("Inside checkKeyRotationStatus");

            String query1 ="select lastsuccessfullattempt,comments from keyrotationdetails where Endpointname = ?";
            PreparedStatement pst = con.prepareStatement(query1);
            pst.setString(1,connectionname);
            logger.info("Executing statement: " + pst.toString());
            ResultSet rs = pst.executeQuery();
            logger.info("Statement executed successfully");
            if(rs.next()) {
                String lastsuccessfullattempt = rs.getString(1);
                String comments = rs.getString(2);

                if(lastsuccessfullattempt!=null && comments.equalsIgnoreCase("Successfully rotated key")) {
                    res = true;
                }
            }
        }
        catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);;
        }
        finally {
            return res;
        }
    }

    public boolean tagOSRGroupsAndAccounts(Properties prop,Connection con,String connectionname) {
        boolean res=true;
        logger.info("\nInside tagOSRGroupsAndAccounts");

        try {

            String taggroupquery=prop.getProperty("BASELINEACCGROUPQUERY").replace("${endpointname}","'"+connectionname+"'");
            PreparedStatement pstmt=con.prepareStatement(taggroupquery);

            logger.info("Executing statement: "+pstmt.toString());
            int result=pstmt.executeUpdate();
            logger.info("Query execution status - " + result);

            String uptQuery=prop.getProperty("NE_TAG_OSR_ACCOUNTS_GROUPS").replace("${endpointname}","'"+connectionname+"'");
            PreparedStatement pstmt1=con.prepareStatement(uptQuery);

            logger.info("Executing statement: "+pstmt1.toString());
            int result1=pstmt1.executeUpdate();
            logger.info("Query execution status - " + result1);
        }
        catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);;
            res=false;
        }
        finally {
            return res;
        }
    }

    public boolean unixAccountCorrelation(Properties prop,Connection con,String connectionname) {
        boolean res=true;
        logger.info("\nInside unixAccountCorrelation");

        try {
            String uptQuery=prop.getProperty("NE_UNIX_ACCOUNT_CORRELATION").replace("${endpointname}","'"+connectionname+"'");
            PreparedStatement pstmt=con.prepareStatement(uptQuery);

            logger.info("Executing statement: "+pstmt.toString());
            int result=pstmt.executeUpdate();
            logger.info("Statement executed successfully");
        }
        catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);;
            res=false;
        }
        finally {
            return res;
        }
    }

    public boolean orphanGrpOwnershipAssignment(Properties prop,Connection con,String connectionname) {
        boolean res=true;
        logger.info("\nInside orphanGrpOwnershipAssignment");

        try {
            String uptQuery=prop.getProperty("GI_ORPHAN_GROUP_OWNERSHIP_ASSIGNMENT").replace("${endpointname}","'"+connectionname+"'");
            PreparedStatement pstmt=con.prepareStatement(uptQuery);

            logger.info("Executing statement: "+pstmt.toString());
            int result=pstmt.executeUpdate();
            logger.info("Statement executed successfully");
        }
        catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);;
            res=false;
        }
        finally {
            return res;
        }
    }

    public boolean executeAdditionalQueries(Properties prop,Connection con,String connectionname) {
        boolean res=true;
        logger.info("\nInside executeAdditionalQueries");

        try {
            String uptQuery=prop.getProperty("ADDITIONAL_QUERIES").replace("${replacename}","'"+connectionname+"'");
            PreparedStatement pstmt=con.prepareStatement(uptQuery);

            logger.info("Executing statement: "+pstmt.toString());
            int result=pstmt.executeUpdate();
            logger.info("Statement executed successfully");

        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
            res=false;
        }
        finally {
            return res;
        }
    }

    public boolean updateInactiveAccounts(Properties prop,Connection con,String connectionname, String dateTime, String accList) {
        boolean res=true;
        logger.info("\nInside updateInactiveAccounts");

        try {
            String uptQuery=prop.getProperty("UPDATEINACTIVEACCS");
            uptQuery = uptQuery.replace("${endpointname}","'"+connectionname+"'");
            uptQuery = uptQuery.replace("${date}","'"+dateTime+"'");
            uptQuery = uptQuery.replace("${accountlist}",accList);
            PreparedStatement pstmt=con.prepareStatement(uptQuery);

            logger.info("Executing statement: "+pstmt.toString());
            int result=pstmt.executeUpdate();
            logger.info("Statement executed successfully");

        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
            res=false;
        }
        finally {
            return res;
        }
    }

    public boolean updateRequestInvalidData(Properties prop,Connection con) {
        boolean res=true;
        logger.info("\nInside updateRequestInvalidData");
        logger.info("inputErr - "+ errInputFiles.toString());

        try {
            if(errInputFiles !=null && !errInputFiles.isEmpty()) {
                for (Map.Entry<String, String> err : errInputFiles.entrySet()) {
                    String uptQuery = prop.getProperty("UPDATEREQUEST");
                    uptQuery = uptQuery.replace("${requestkey}", err.getKey());
                    uptQuery = uptQuery.replace("${arstatus}", "6");
                    uptQuery = uptQuery.replace("${comments}", err.getValue());

                    PreparedStatement pstmt1 = con.prepareStatement(uptQuery);

                    logger.info("Executing statement: " + pstmt1.toString());
                    int result = pstmt1.executeUpdate();
                    logger.info("Statement executed successfully");
                }
            }
        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
            res=false;
        }
        finally {
            return res;
        }
    }


    public boolean checkAndUpdateRequest(Properties prop,Connection con, HashSet<String> requestKeys) {
        boolean res=true;
        logger.info("\nInside checkAndUpdateRequest");
        logger.info("serverRequests - "+ requestKeys);

        try {
            if(requestKeys!=null && !requestKeys.isEmpty()) {
                Iterator<String> it = requestKeys.iterator();
                while(it.hasNext()){
                    String reqKey = it.next();
                    boolean allSuccessful = false;
                    logger.info("RequestKey - "+reqKey);

                    String selectSQL = prop.getProperty("CHECKBOARDINGSTATUS");
                    selectSQL = selectSQL.replace("${requestkey}",reqKey);

                    PreparedStatement pstmt = con.prepareStatement(selectSQL);
                    pstmt = con.prepareStatement(selectSQL);
                    logger.info("Executing statement: " + pstmt.toString());
                    ResultSet rs3 = pstmt.executeQuery();
                    logger.info("Statement executed successfully");

                    while (rs3.next()) {
                        allSuccessful = true;
                        String serverName = (String) rs3.getString("serverName");
                        Integer lastSuccessfulStep = (Integer) rs3.getInt("lastsuccessfullstep");
                        if (serverName != null && lastSuccessfulStep != null ) {
                            if(lastSuccessfulStep < 5) {
                                allSuccessful=false;
                                break;
                            }
                        }
                    }
                    if(allSuccessful) {
                        //logger.info("ALL SUCCESSFUL");

                        String uptQuery = prop.getProperty("UPDATEREQUEST");
                        uptQuery = uptQuery.replace("${requestkey}",reqKey);
                        uptQuery = uptQuery.replace("${arstatus}", "3");
                        uptQuery = uptQuery.replace("${comments}", "Completed by BULKBOARDING");
                        PreparedStatement pstmt1 = con.prepareStatement(uptQuery);

                        logger.info("Executing statement: " + pstmt1.toString());
                        int result = pstmt1.executeUpdate();
                        logger.info("Statement executed successfully");

                    }
                }
            }

        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
            res=false;
        }
        finally {
            return res;
        }
    }


    public boolean updateSecuritySystem(Properties prop,Connection con,String connectionname) {
        boolean res=true;
        logger.info("\nInside updateSecuritySystem");

        try {
            String uptQuery=prop.getProperty("UPDATESECSYSTEM").replace("${endpointname}","'"+connectionname+"'");
            PreparedStatement pstmt=con.prepareStatement(uptQuery);

            logger.info("Executing statement: "+pstmt.toString());
            int result=pstmt.executeUpdate();
            logger.info("Statement executed successfully");

        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
            res=false;
        }
        finally {
            return res;
        }
    }


    public boolean tagGroupsAndAccounts(Properties prop,Connection con,String connectionname) {
        boolean res=true;
        try {
            String taggroupquery=prop.getProperty("TAGGROUPQUERY").replace("${endpointname}","'"+connectionname+"'");
            PreparedStatement pstmt=con.prepareStatement(taggroupquery);

            logger.info("Executing statement: "+pstmt.toString());
            int result=pstmt.executeUpdate();
            logger.info("Statement executed successfully");

            String tagaccquery=prop.getProperty("TAGACCOUNTQUERY").replace("${endpointname}","'"+connectionname+"'");

            logger.info("Executing query : "+tagaccquery);
            PreparedStatement pstmt1=con.prepareStatement(tagaccquery);

            logger.info("Executing statement: "+pstmt1.toString());
            int result1=pstmt1.executeUpdate();
            logger.info("Statement executed successfully");

        }
        catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);;
            res=false;
        }
        finally {
            return res;
        }
    }


    public String  checkImportStatus(Properties prop, com.SaviyntCustom.ServerOnboaringNE.ServerOnboardingStatusPojo serverOb,
                                             String outhToken,String connectionname,String jobname)
    {
        String importstatus =null;
        try {
            String url=prop.getProperty("URLPATH")+"api/v5/checkImportStatus";
            com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn httpConn = new com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn();

            HttpsURLConnection con1 = null;
            //HttpURLConnection con2 = null;
            JSONObject getConnectionResponse = null;
            JSONParser parser1 = new JSONParser();
            //JSONObject connectionjson = (JSONObject) parser1.parse(prop.getProperty("CONNECTIONJSON"));
            JSONObject endpointjson =new JSONObject();
            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            con1 = httpConn.sendPostHttps(url, endpointjson.toString(), httpConnectionHeaders);
            int code = con1.getResponseCode();
            if(code == 401) {
                refreshToken(prop);
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                con1 = httpConn.sendPostHttps(url, endpointjson.toString(), httpConnectionHeaders);
            }

            getConnectionResponse = httpConn.fetchJsonObject(con1);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(getConnectionResponse.toString());

            importstatus = (String)json.get("importStatus");
            String errorCode = (String)json.get("errorCode");
            if(!errorCode.equals("0"))
                serverOb.comments.append(" "+jobname+":"+importstatus);
            logger.info("Job status : "+importstatus);
        }
        catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);;
            serverOb.comments.append("Step3AccountsImport:"+e.getMessage());
        }
        finally {
            return importstatus;
        }
    }



    public boolean importEntitlements(Properties prop, String connectionname, com.SaviyntCustom.ServerOnboaringNE.ServerOnboardingStatusPojo serverOb)
    {
        boolean status=false;
        try {
            String url=prop.getProperty("URLPATH")+"api/v5/importData";
            com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn httpConn = new com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn();
            HttpsURLConnection con1 = null;
            //HttpURLConnection con2 = null;
            JSONObject getConnectionResponse = null;
            JSONParser parser1 = new JSONParser();
            JSONObject endpointjson =new JSONObject();
            endpointjson.put("systemname",connectionname);
            endpointjson.put("connectionName",connectionname);
            endpointjson.put("connectiontype","Unix");
            endpointjson.put("fullorincremental","full");
            endpointjson.put("accountsoraccess","access");
            endpointjson.put("CREATEUSERS","No");
            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            con1 = httpConn.sendPostHttps(url, endpointjson.toString(), httpConnectionHeaders);
            int code = con1.getResponseCode();
            if(code == 401) {
                refreshToken(prop);
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                con1 = httpConn.sendPostHttps(url, endpointjson.toString(), httpConnectionHeaders);
            }

            getConnectionResponse = httpConn.fetchJsonObject(con1);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(getConnectionResponse.toString());

            String importstatus = (String)json.get("msg");
            String errorCode = (String)json.get("errorCode");
            logger.info("Import account status : "+importstatus);

            if(!errorCode.equals("0"))
                serverOb.comments.append("step4accountsimport:"+importstatus+"|");
            else
                status=true;
        }
        catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);;
            serverOb.comments.append("step4accountsimport:"+e.getMessage()+"|");
        }
        finally {
            return status;
        }
    }


    public boolean  importAccounts(Properties prop, String connectionname, com.SaviyntCustom.ServerOnboaringNE.ServerOnboardingStatusPojo serverOb)
    {
        boolean status=false;
        try {
            String url=prop.getProperty("URLPATH")+"api/v5/importData";
            com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn httpConn = new com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn();

            HttpsURLConnection con1 = null;
            //HttpURLConnection con2 = null;
            JSONObject getConnectionResponse = null;
            JSONParser parser1 = new JSONParser();
            //JSONObject connectionjson = (JSONObject) parser1.parse(prop.getProperty("CONNECTIONJSON"));
            JSONObject endpointjson =new JSONObject();
            endpointjson.put("systemname",connectionname);
            endpointjson.put("connectionName",connectionname);
            endpointjson.put("connectiontype","Unix");
            endpointjson.put("fullorincremental","full");
            endpointjson.put("accountsoraccess","accounts");
            endpointjson.put("CREATEUSERS","No");
            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            con1 = httpConn.sendPostHttps(url, endpointjson.toString(), httpConnectionHeaders);
            int code = con1.getResponseCode();
            if(code == 401) {
                refreshToken(prop);
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                con1 = httpConn.sendPostHttps(url, endpointjson.toString(), httpConnectionHeaders);
            }

            getConnectionResponse = httpConn.fetchJsonObject(con1);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(getConnectionResponse.toString());

            String importstatus = (String)json.get("msg");
            String errorCode = (String)json.get("errorCode");
            logger.info("Import account status : "+importstatus);

            if(!errorCode.equals("0"))
                serverOb.comments.append("Step3AccountsImport:"+importstatus+"|");
            else
                status=true;
        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
            serverOb.comments.append("Step3AccountsImport:"+e.getMessage()+"|");
        }
        finally {
            return status;
        }
    }



    public boolean  updateEndpointStep2(Properties prop, String connectionname, com.SaviyntCustom.ServerOnboaringNE.ServerOnboardingStatusPojo serverOb, HashMap<String,String> endpointAttrs, Connection con)
    {
        boolean status=false;
        boolean proceed=false;
        String serviceowner = null;

        try {
            if(endpointAttrs!=null && endpointAttrs.get("serviceowner")!=null && endpointAttrs.get("customproperty11")!=null) {
                serviceowner = endpointAttrs.get("serviceowner");
                endpointAttrs.remove("serviceowner");

                String query = "select statuskey from users where username = ?";
                PreparedStatement pst = con.prepareStatement(query);
                pst.setString(1, serviceowner);
                logger.info("Executing query - "+pst.toString());
                ResultSet rs = pst.executeQuery();
                String serviceownerstatus = null;

                if (rs != null) {
                    rs.next();
                    serviceownerstatus = rs.getString(1);
                }
                if (serviceownerstatus != null && serviceownerstatus.equals("1")) {
                    proceed = true;
                } else {
                    serverOb.comments.append("Step2UpdateEndpoint:Inactive or invalid service owner|");
                }
            } else {
                serverOb.comments.append("Step2UpdateEndpoint:Service owner or default BP not present|");
            }
            //Endpoint status will be disabled in preboarding.
            //Enable and set other attributes before setting other values which come from boarding file
            if (proceed) {
                proceed = false;
                if (updateEndpointStatusAndCp22(prop, con, connectionname, "Network Engineering Server – Pending")
                        && updateOtherEndpointAttrs(prop, con, connectionname)) {
                    if (addDynamicAttributes(connectionname, con, prop)) {
                        proceed = true;
                    }
                } else {
                    serverOb.comments.append("Step2UpdateEndpoint:Updating cp22 and other attrs failed|");
                }
            }

            if (proceed) {
                String url = prop.getProperty("URLPATH") + "api/v5/updateEndpoint";
                com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn httpConn = new com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn();
                String useracccorrrule = prop.getProperty("USERACCOUNTCORRELATIONRULE");
                HttpsURLConnection con1 = null;
                //HttpURLConnection con2 = null;
                JSONObject getConnectionResponse = null;
                JSONParser parser1 = new JSONParser();
                JSONObject endpointjson =new JSONObject(endpointAttrs);
                endpointjson.put("endpointname", connectionname);
                endpointjson.put("userAccountCorrelationRule", useracccorrrule);
                endpointjson.put("displayName", "Server: " + connectionname);
                endpointjson.put("resourceOwnerType", "User");
                endpointjson.put("resourceOwner", serviceowner);
                endpointjson.put("description", connectionname + "  - Network Engineering - " + endpointAttrs.get("customproperty11"));
                endpointjson.put("disableRemoveAccount", "true");

                Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                logger.info("endpointjson : " + endpointjson);

                con1 = httpConn.sendPutHttps(url, endpointjson.toString(), httpConnectionHeaders);
                int code = con1.getResponseCode();
                if(code == 401) {
                    refreshToken(prop);
                    httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                    con1 = httpConn.sendPutHttps(url, endpointjson.toString(), httpConnectionHeaders);
                }

                getConnectionResponse = httpConn.fetchJsonObject(con1);

                String endpointcreatestatus = null;
                String errorCode =  null;

                if(getConnectionResponse != null) {
                    JSONParser parser = new JSONParser();
                    JSONObject json = (JSONObject) parser.parse(getConnectionResponse.toString());

                    endpointcreatestatus = (String) json.get("msg");
                    errorCode = (String) json.get("errorCode");
                }
                logger.info("Endpoint create status : " + endpointcreatestatus);
                // && createDynamic(outhToken,prop,connectionname) && updateETType(outhToken,prop,connectionname))
                if (errorCode != null && errorCode.equals("0")) {
                    status = true;
                } else {
                    serverOb.comments.append("Step2UpdateEndpoint:" + endpointcreatestatus + "|");
                }
            }
        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
            serverOb.comments.append("Step2UpdateEndpoint:"+e.getMessage()+"|");
        }
        finally {
            return status;
        }
    }

    public boolean updateConnection(String connectionname,Connection con,Properties prop)
    {
        boolean status = false;
        int counter = 0;

        logger.info("Inside updateConnection");

        try {
            String updateConnectionStr = prop.getProperty("UPDATE_CONNECTION");
            updateConnectionStr = updateConnectionStr.replace("${connname}","'"+connectionname+"'");

            if(updateConnectionStr != null && !updateConnectionStr.isEmpty())
            {
                logger.info("updateConnectionStr - " + updateConnectionStr);
                PreparedStatement pstmt = con.prepareStatement(updateConnectionStr);

                pstmt = con.prepareStatement(updateConnectionStr);
                logger.info("Executing statement: " + pstmt.toString());
                //ResultSet rs1 = pstmt.executeQuery();
                pstmt.execute();
                logger.info("Statement executed successfully");
                status =true;
            }
        }
        catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);;
        }
        finally {
            return status;
        }

    }


    public boolean correlateInctiveAccounts(String connectionname,Connection con,Properties prop)
    {
        boolean status = false;
        int counter = 0;

        logger.info("Inside correlateInctiveAccounts");

        try
        {
            String correlateQuery = prop.getProperty("CORRELATEINACTIVEACCOUNTSQRY");
            if(correlateQuery != null && !correlateQuery.isEmpty())
            {
                logger.info("correlateQuery - " + correlateQuery);
                PreparedStatement pstmt = con.prepareStatement(correlateQuery);
                pstmt = con.prepareStatement(correlateQuery);
                pstmt.setString(1, connectionname);
                logger.info("Executing statement: " + pstmt.toString());
                //ResultSet rs1 = pstmt.executeQuery();
                pstmt.execute();
                logger.info("Statement executed successfully");
                status =true;
            }
        }
        catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);;
        }
        finally {
            return status;
        }

    }

    public boolean alreadyBoarded(Properties prop, Connection con, com.SaviyntCustom.ServerOnboaringNE.ServerOnboardingStatusPojo serverObj)
    {
        boolean status = false;
        int counter = 0;

        logger.info("\nInside alreadyBoarded...");

        try
        {
            String query1=prop.getProperty("ALREADYBOARDED").replace("${sysname}","'"+serverObj.servername+"'");
            PreparedStatement pst = con.prepareStatement(query1);
            logger.info("Executing statement: " + pst.toString());
            ResultSet rs = pst.executeQuery();
            logger.info("Statement executed successfully");

            if(rs.next()) {
                status = true;
            }
        }
        catch (Exception e)
        {
            logger.error("Exception - " +  e.getMessage(), e);;

        }
        finally {
            return status;
        }

    }

    public boolean addDynamicAttributes(String connectionname, Connection con, Properties prop)
    {
        boolean status = false;
        int counter = 0;

        logger.info("\nInside addDynamicAttributes...");

        try {
            String query1 ="select endpointkey,customproperty1 from endpoints where endpointname = ?";
            PreparedStatement pst = con.prepareStatement(query1);
            pst.setString(1,connectionname);
            ResultSet rs = pst.executeQuery();
            rs.next();
            String endpointkey = rs.getString(1);
            String osType = rs.getString(2);

            String defaultpg = null;
            if(osType!=null && osType.toUpperCase().contains("AIX")) {
                defaultpg = "staff";
            } else {
                defaultpg = "users";
            }
            String dynAttrs = prop.getProperty("DYNAMIC_ATTRIBUTES");

            if(endpointkey!=null && osType!=null && dynAttrs != null && !dynAttrs.isEmpty()) {
                dynAttrs = dynAttrs.replace("${endpointname}","'"+connectionname+"'");
                dynAttrs = dynAttrs.replace("${endpointkey}",endpointkey);
                dynAttrs = dynAttrs.replace("${defaultpg}","'"+defaultpg+"'");
                logger.info("DYNAMIC_ATTRIBUTES - " + dynAttrs);
                PreparedStatement pstmt = con.prepareStatement(dynAttrs);
                pstmt = con.prepareStatement(dynAttrs);
                logger.info("Executing statement: " + pstmt.toString());
                //ResultSet rs1 = pstmt.executeQuery();
                pstmt.execute();
                logger.info("Statement executed successfully");
                status =true;
            }
        }
        catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);;
        }
        finally {
            return status;
        }

    }


    public boolean updateETType(Properties prop,String connectionname)
    {
        boolean status = false;
        logger.info("\nInside update Entitlementtype");
        try {
            String url = prop.getProperty("URLPATH") + "api/v5/updateEntitlementType";
            com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn httpConn = new com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn();
            HttpsURLConnection con1 = null;
            JSONObject getConnectionResponse = null;
            JSONParser parser1 = new JSONParser();
            JSONObject etjson = new JSONObject();
            etjson.put("entitlementname", "Groups");
            etjson.put("endpointname", connectionname);
            etjson.put("workflow", prop.getProperty("GroupCreationApproval"));
            etjson.put("requestoption", "TABLE");
            etjson.put("arsRequestableEntitlementQuery", prop.getProperty("arsRequestableEntitlementQuery"));
            etjson.put("arsSelectedEntitlementQuery", prop.getProperty("arsSelectedEntitlementQuery"));
            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            con1 = httpConn.sendPutHttps(url, etjson.toString(), httpConnectionHeaders);
            int code = con1.getResponseCode();
            if(code == 401) {
                refreshToken(prop);
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                con1 = httpConn.sendPutHttps(url, etjson.toString(), httpConnectionHeaders);
            }
            
            getConnectionResponse = httpConn.fetchJsonObject(con1);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(getConnectionResponse.toString());

            String endpointcreatestatus = (String) json.get("msg");
            String errorCode = (String) json.get("errorCode");
            if (errorCode != null && errorCode.equals("0"))
                status = true;
            else
                status = false;
        }
        catch (Exception e)
        {
            logger.error("Exception - " +  e.getMessage(), e);;

        }
        finally {
            logger.info("Exiting Entitlementtype");
            return status;
        }

    }


    public boolean  updateEndpoint(Properties prop, String connectionname, com.SaviyntCustom.ServerOnboaringNE.ServerOnboardingStatusPojo serverOb)
    {
        boolean status=false;
        try {
            logger.info("Inside updateEndpoint : USERACCOUNTCORRELATIONRULE2");

            String url=prop.getProperty("URLPATH")+"api/v5/updateEndpoint";
            com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn httpConn = new com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn();
            String useracccorrrule=prop.getProperty("USERACCOUNTCORRELATIONRULE2");
            HttpsURLConnection con1 = null;
            //HttpURLConnection con2 = null;
            JSONObject getConnectionResponse = null;
            JSONParser parser1 = new JSONParser();
            JSONObject endpointjson =new JSONObject();
            endpointjson.put("endpointname",connectionname);
            endpointjson.put("userAccountCorrelationRule",useracccorrrule);
            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            con1 = httpConn.sendPutHttps(url, endpointjson.toString(), httpConnectionHeaders);
            int code = con1.getResponseCode();
            if(code == 401) {
                refreshToken(prop);
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                con1 = httpConn.sendPutHttps(url, endpointjson.toString(), httpConnectionHeaders);
            }
            getConnectionResponse = httpConn.fetchJsonObject(con1);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(getConnectionResponse.toString());

            String endpointcreatestatus = (String)json.get("msg");
            String errorCode = (String)json.get("errorCode");
            logger.info("Endpoint create status : "+errorCode);
            if(errorCode!=null && errorCode.equals("0"))
                status=true;
            else
                serverOb.comments.append("Step4CompleteServerConfig:"+endpointcreatestatus+"|");
        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
            serverOb.comments.append("Step4CompleteServerConfig:"+e.toString()+"|");
        }
        finally {
            return status;
        }
    }



    public HashMap<String,String> getTemplateConnectionAttributes(String hostname, String os, String osver,
                                                                  Connection con,Properties prop) {
        String servertype = "Bulkboard_NE";
        boolean proceed = false;
        HashMap<String,String> templateAttrs=new HashMap<>();
        try {
            if (os.contains("Linux")) {
                String osverList = prop.getProperty("OSVERSIONLISTS");
                String[] osverListA = null;
                if(osverList != null) {
                    osverListA = osverList.toUpperCase().split(",");
                }
                if(osver != null && osverListA != null && StringUtils.indexOfAny(osver.toUpperCase(),osverListA)!=-1) {
                    servertype = servertype + "_" + osver.toUpperCase();
                }
                logger.info("Server Type is " + servertype);
                proceed = true;
            } else {
                servertype = "Operating System is not defined or not supported";
                logger.info("Operating System is not defined or not supported");
            }

            //if there is customproperty found then only return the values for connections
            if (proceed) {
                String selectSQL = "select ect.attributekey,ect.attributevalue from externalconnattvalue ect,externalconnection ec where ect.connectiontype=ec.externalconnectionkey and ec.CONNECTIONNAME = ?";
                PreparedStatement pstmt1 = con.prepareStatement(selectSQL);
                pstmt1 = con.prepareStatement(selectSQL);
                pstmt1.setString(1, servertype);
                logger.info("Executing statement: " + pstmt1.toString());
                ResultSet rs3 = pstmt1.executeQuery();
                logger.info("Statement executed successfully");

                while (rs3.next()) {

                    //if (prop.getProperty("ENCRYPTED_ATTRIBUTES").contains(rs3.getString(1))) {
                    //    templateAttrs.put(rs3.getString(1), decryptIt(rs3.getString(2)));
                    if ("PASSPHRASE".equals(rs3.getString(1))) {
                        //logger.info("Attribute Name:" + rs3.getString(1) + "    " + "Attribute Value: *****\n");
                        templateAttrs.put(rs3.getString(1),decryptIt(prop.getProperty("BOARDINGPASSPHRASE").trim()));
                        //} else if ("PASSWORD".equals(rs3.getString(1))) {
                        //    //logger.info("Attribute Name:" + rs3.getString(1) + "    " + "Attribute Value: *****\n");
                        //    templateAttrs.put(rs3.getString(1),decryptIt(prop.getProperty("BOARDINGPASSWORD").trim()));
                    } else {
                        //logger.info("Attribute Name:" + rs3.getString(1) + "    " + "Attribute Value:" + rs3.getString(2));
                        templateAttrs.put(rs3.getString(1), rs3.getString(2));
                    }
                }
            } else {
                templateAttrs.put(hostname, servertype);
            }
        }
        catch(Exception e)  {
            logger.error("Exception - " +  e.getMessage(), e);;
        }
        return templateAttrs;
    }


    public String decryptIt(String strToDecrypt) {
        byte[] key = "S@v!ynt_s@V!YNt_".getBytes();
        //byte[] key = "S@v!ynt_s@V!YNt_".getBytes();
        if(strToDecrypt !=null ) {
            try {

                Cipher cipher  = Cipher.getInstance("AES/ECB/PKCS5PADDING");
                final SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
                cipher.init(Cipher.DECRYPT_MODE, secretKey);
                final String decryptedString = new String(cipher.doFinal(Base64.decodeBase64(strToDecrypt)));
                return decryptedString;
            } catch (Exception e) {
                logger.error("Exception - " +  e.getMessage(), e);;
            }
        }
        return null;
    }


    public boolean createUpdateOrganization(Connection con, Properties prop, String squadname, String connectionname, com.SaviyntCustom.ServerOnboaringNE.ServerOnboardingStatusPojo serverOb)
    {
        boolean status=false;
        boolean orgPresent=false;
        boolean orgUpdated=false;

        try {
            com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn httpConn = new com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn();
            String url1=prop.getProperty("URLPATH")+"api/v5/updateOrganization";

            logger.info("Updating organization: "+prop.getProperty("ORG_NE_ADMINNAME"));
            HttpsURLConnection con2 = null;
            JSONObject getUpdateOrgResponse2 = null;
            JSONObject updateorgjson2 =new JSONObject();
            JSONArray endpointarr2 = new JSONArray();
            JSONObject endpointjson2 =new JSONObject();
            endpointjson2.put("endpointname",connectionname);
            endpointjson2.put("updatetype","ADD");
            endpointarr2.add(0,endpointjson2);
            updateorgjson2.put("organizationname",prop.getProperty("ORG_NE_ADMINNAME"));
            updateorgjson2.put("endpoints",endpointarr2);
            updateorgjson2.put("username",prop.getProperty("SAVUSERNAME"));
            //"endpoints"
            Map<String, String> httpConnectionHeaders2 = new HashMap<String, String>();
            httpConnectionHeaders2.put("Authorization", "Bearer " + outhToken);
            con2 = httpConn.sendPutHttps(url1, updateorgjson2.toString(), httpConnectionHeaders2);
            int code = con2.getResponseCode();
            if(code == 401) {
                refreshToken(prop);
                httpConnectionHeaders2.put("Authorization", "Bearer " + outhToken);
                con2 = httpConn.sendPutHttps(url1, updateorgjson2.toString(), httpConnectionHeaders2);
            }
            getUpdateOrgResponse2 = httpConn.fetchJsonObject(con2);
            String orgUpdateStatus2 = null;
            String orgUpdateMsg2 = null;
            if (getUpdateOrgResponse2!=null) {
                JSONParser parser = new JSONParser();
                JSONObject json = (JSONObject) parser.parse(getUpdateOrgResponse2.toString());
                orgUpdateStatus2 = (String) json.get("errorCode");
                orgUpdateMsg2 = (String) json.get("msg");
            }
            logger.info("Organization Update status : " + orgUpdateStatus2);

            if(orgUpdateStatus2!=null && orgUpdateStatus2.equals("0")) {
                orgUpdated=true;
            }
            else {
                serverOb.comments.append("Step1CreateConnection:Could not update organization "+prop.getProperty("ORG_GI_AdminNAME")+"|");
            }

            if(orgUpdated) {
                logger.info("\n***** Getting list of available savroles ... *****\n");
                String url2 = prop.getProperty("URLPATH") + "api/v5/getSavRoles?max=1000";

                HttpsURLConnection con3 = null;
                JSONObject getSavRoleResponse = null;
                Map<String, String> httpConnectionHeaders3 = new HashMap<String, String>();
                httpConnectionHeaders3.put("Authorization", "Bearer " + outhToken);
                con3 = httpConn.sendGetHttps(url2, httpConnectionHeaders3);
                code = con3.getResponseCode();
                if(code == 401) {
                    refreshToken(prop);
                    httpConnectionHeaders3.put("Authorization", "Bearer " + outhToken);
                    con3 = httpConn.sendGetHttps(url2, httpConnectionHeaders3);
                }
                getSavRoleResponse = httpConn.fetchJsonObject(con3);
                JSONParser parser2 = new JSONParser();
                JSONObject json2 = (JSONObject) parser2.parse(getSavRoleResponse.toString());

                String savrolestatus = (String) json2.get("errorCode");
                logger.info("Get SAV ROLES status : " + savrolestatus);
                if (savrolestatus != null && savrolestatus.equals("0")) {
                    JSONArray availablesavroles = (JSONArray) json2.get("savRoles");
                    logger.info(availablesavroles.toJSONString());

                    logger.info("\n***** Adding SAVROLES to the connection created - " + connectionname + " *****\n");
                    String savrolesfinal = prop.getProperty("ORGANIZATION_SAVROLES");
                    logger.info("SAV Roles to be added - " + savrolesfinal + "\n");

                    if (savrolesfinal != null) {
                        String[] savroleList = savrolesfinal.split(",");
                        if (savroleList.length > 0) {
                            for (String savrole : savroleList) {
                                if (availablesavroles.contains(savrole)) {
                                    logger.info(savrole + " found!");
                                    String insertSQL = "insert ignore into SAVROLE_PERMISSION(ACCESSKEY,SAVROLEKEY,ACCESSTYPE,UPDATEDATE,UPDATEUSER) values((select externalconnectionkey from externalconnection where connectionname='" + connectionname + "'),(select rolekey from savroles where rolename='" + savrole + "'),'ExternalConnection',now(),'" + prop.getProperty("SAVUSERNAME") + "')";
                                    PreparedStatement pstmt = con.prepareStatement(insertSQL);
                                    pstmt = con.prepareStatement(insertSQL);
                                    logger.info("Executing query : " + insertSQL);

                                    logger.info("Executing statement: " + pstmt.toString());
                                    boolean rs3 = pstmt.execute();
                                    logger.info("Statement executed successfully");
                                }
                            }
                            status = true;
                        }
                    } else {
                        logger.info("SAV ROLES not defined");
                        serverOb.comments.append("Step1CreateConnection:SAV ROLES not defined" + "|");
                    }
                } else {
                    logger.info("No SAVROLES found");
                    serverOb.comments.append("Step1CreateConnection:No SAVROLES found in AH" + "|");
                }
            }
        }
        catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);;
            serverOb.comments.append("Step1CreateConnection:"+e.getMessage()+"|");
        }
        finally {
            return status;
        }
    }


    public boolean createConnection(Properties prop, HashMap<String,String> templateAttrs,
                                    String servertype, String connectionname,
                                    ServerOnboardingStatusPojo serverOb, String[] dataInfo) {
        boolean status=false;

        logger.info("\n***** Creating connection - "+connectionname+" *****\n");

        templateAttrs.put("connectiontype","Unix");
        templateAttrs.put("saveconnection","Y");
        templateAttrs.put("systemname",connectionname); // should be hostname
        //Random random = new Random();
        //int num = random.nextInt(900) + 100;
        templateAttrs.put("connectionName",connectionname);
        try {
            String url=prop.getProperty("URLPATH")+"api/v5/testConnection";
            com.SaviyntCustom.ServerOnboaringNE.HttpUrlConn httpConn = new HttpUrlConn();

            HttpsURLConnection con1 = null;
            //HttpURLConnection con2 = null;
            JSONObject getConnectionResponse = null;
            JSONParser parser1 = new JSONParser();
            //JSONObject connectionjson = (JSONObject) parser1.parse(prop.getProperty("CONNECTIONJSON"));
            JSONObject connectionjson =new JSONObject(templateAttrs);
            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            con1 = httpConn.sendPostHttps(url, connectionjson.toString(), httpConnectionHeaders);
            int code = con1.getResponseCode();
            if(code == 401) {
                refreshToken(prop);
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                con1 = httpConn.sendPostHttps(url, connectionjson.toString(), httpConnectionHeaders);
            }
            
            getConnectionResponse = httpConn.fetchJsonObject(con1);
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(getConnectionResponse.toString());

            String connctioncreatestatus = (String)json.get("msg");
            logger.info("Connection create status : "+connctioncreatestatus);

            if(connctioncreatestatus!=null &&
                    connctioncreatestatus.equalsIgnoreCase("Connection Successful")) {
                if(createEndpointFromInput(prop, con,
                        connectionname,
                        dataInfo[serverOb.osnameindex],
                        dataInfo[serverOb.osversionindex],
                        dataInfo[serverOb.categoryindex], serverOb)) {
                    if (updateConnection(connectionname, con, prop)) {
                        status = true;
                    }  else {
                        serverOb.comments.append("Step1CreateConnection:failed while setting additional attrs|");
                    }
                }  else {
                    serverOb.comments.append("Step1CreateConnection:failed while creating/updating endpoint|");
                }
            } else {
                //status=false;
                serverOb.comments.append("Step1CreateConnection:"+connctioncreatestatus+"|");
            }

        } catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);;
            serverOb.comments.append("Step1CreateConnection:"+e.getMessage()+"|");
        }
        finally {

            return status;
        }
    }

    public void refreshToken(Properties prop) {
        try {
            TokenPojo token = new TokenPojo();
            outhToken = token.callBearerToken(prop);
        } catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);;
        }
    }
    public String generateRandomString()
    {
        String str=UUID.randomUUID().toString();
        logger.info("Generated str : "+str);
        return str;
    }

    public boolean createEndpointFromInput(Properties prop, Connection con,
                                           String connectionname,
                                           String os, String osver, String category,
                                           ServerOnboardingStatusPojo serverOb) {
        boolean status = false;
        try {
            String url = prop.getProperty("URLPATH") + "api/v5/createEndpoint";
            HttpUrlConn httpConn = new HttpUrlConn();

            //String[] triggerSequenceList=triggerSequence.split(",");
            HttpsURLConnection con1 = null;
            JSONObject getConnectionResponse = null;
            JSONObject inputJson = new JSONObject();
            inputJson.put("endpointname", connectionname);
            inputJson.put("displayName", connectionname);
            inputJson.put("securitysystem", connectionname);
            inputJson.put("customproperty1", os);
            inputJson.put("customproperty6", osver);
            inputJson.put("customproperty2", category);
            if(category.equalsIgnoreCase("SOX")) {
                inputJson.put("customproperty3", "Yes");
            } else {
                inputJson.put("customproperty3", "No");
            }

            Map<String, String> httpConnectionHeaders = new HashMap<String, String>();
            httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
            logger.info("inputJson : " + inputJson);

            con1 = httpConn.sendPostHttps(url, inputJson.toString(), httpConnectionHeaders);
            int code = con1.getResponseCode();
            if (code == 401) {
                refreshToken(prop);
                httpConnectionHeaders.put("Authorization", "Bearer " + outhToken);
                con1 = httpConn.sendPostHttps(url, inputJson.toString(), httpConnectionHeaders);
            }
            getConnectionResponse = httpConn.fetchJsonObject(con1);
            String apiStatus = null;
            String msg = "Endpoint creation failed";
            if (getConnectionResponse != null) {
                JSONParser parser = new JSONParser();
                JSONObject json = (JSONObject) parser.parse(getConnectionResponse.toString());

                apiStatus = (String) json.get("errorCode");
                msg = (String) json.get("msg");
            }
            logger.info("Create Endpoint API status : " + apiStatus);

            if ((apiStatus == null) || (apiStatus != null && apiStatus.equals("1"))) {
                serverOb.comments.append("Step1CreateConnection:" + msg + "|");
            } else if (apiStatus != null && apiStatus.equals("0")) {
                status = true;
            }
        } catch(Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);
            status=false;
        }
        finally {
            //return cpsEmptyList.toString();
            return status;
        }
    }
    public boolean updateEndpointFromInput(Properties prop, Connection con,
                                           String connectionname,
                                           String os, String osver, String category) {
        boolean res=true;
        try {
            logger.info("Inside updateEndpointFromInput");

            String uptQuery=prop.getProperty("UPDATEENDPOINTINPUT").replace("${endpointname}","'"+connectionname+"'");
            uptQuery=uptQuery.replace("${os}","'"+os+"'");
            uptQuery=uptQuery.replace("${osver}","'"+osver+"'");
            uptQuery=uptQuery.replace("${category}","'"+category+"'");
            if(category.equalsIgnoreCase("SOX")) {
                uptQuery=uptQuery.replace("${sox}","'Yes'");
            } else {
                uptQuery=uptQuery.replace("${sox}","'No'");
            }
            PreparedStatement pstmt=con.prepareStatement(uptQuery);

            logger.info("Executing statement: "+pstmt.toString());
            int result=pstmt.executeUpdate();
            logger.info("Query execution status - " + result);
        } catch(SQLException e) {
            logger.error("Exception - " +  e.getMessage(), e);
            res=false;
        } finally {
            return res;
        }
    }


    public void main(String[] args)
    {
        try {

            /*MethodPollerTest<String> poller = new MethodPollerTest<>();
            String uuidThatStartsWithOneTwoThree = poller.poll(Duration.ofSeconds(60),10000).method(() -> generateRandomString())
                    .until(s -> s.startsWith("123676767888"))
                    .execute();
            logger.info(uuidThatStartsWithOneTwoThree);
*/
        } catch (Exception e) {
            logger.error("Exception - " +  e.getMessage(), e);;
            logger.info(e);

        }
    }
}


