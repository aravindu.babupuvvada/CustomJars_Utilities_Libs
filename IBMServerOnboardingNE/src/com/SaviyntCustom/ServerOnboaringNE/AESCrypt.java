package com.SaviyntCustom.ServerOnboaringNE;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Saviynt on 3/6/2017.
 */
public class AESCrypt {


    public static String decryptIt(String strToDecrypt) {
        byte[] key = "S@v!ynt_s@V!YNt_".getBytes();
        //byte[] key = "thrinathtransfer".getBytes();
        if(strToDecrypt !=null ) {
            try {

                Cipher cipher  = Cipher.getInstance("AES/ECB/PKCS5PADDING");
                final SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
                cipher.init(Cipher.DECRYPT_MODE, secretKey);
                final String decryptedString = new String(cipher.doFinal(Base64.decodeBase64(strToDecrypt)));
                return decryptedString;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    public static String encryptIt(String strToEncrypt) {
        byte[] key = "S@v!ynt_s@V!YNt_".getBytes();
        //byte[] key = "thrinathtransfer".getBytes();
        try {
            // Test with CBC or CTR mode.

            Cipher cipher  = Cipher.getInstance("AES/ECB/PKCS5Padding");
            final SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            final String encryptedString = Base64.encodeBase64String(cipher.doFinal(strToEncrypt.getBytes()));
            return encryptedString;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }


  public static void main (String args[]) {
   // public static void getEncryptedValue(String a) {

        //String a = encryptIt("$aviynt$AW$1");
        //String a1 = encryptIt("BHXU3Rm8LXC5henD");
        //String a2 = encryptIt("6a8bTAeBRLHKSQf6");
        //String a = encryptIt("$aviyntclouddb1");
        //String a1 = encryptIt("tD4-S2PW-~Q*Hwbf");
        //String a2 = encryptIt("vmvD8httcBWZHbSY");

        //Accesshub@123456789
        //M7d5Nxj7n!YW

        //System.out.println(a);
        //System.out.println(a1);
        //System.out.println(a2);

        /*
      String  accountsReconTriggerName = null;
      String data = "bldbz172095.cloud.dst.ibm.com";
      if (data.matches(".*[a-zA-Z]+.*")) {
          accountsReconTriggerName = "ACCOUNTS_"+
                  ((data.split("\\."))[0]).toUpperCase();
      } else {
          accountsReconTriggerName = "ACCOUNTS_"+
                  data.toUpperCase();
      }
      System.out.println("accountsReconTriggerName = "+accountsReconTriggerName);
        */
      /*String a = "GI_CLEAR_ACCOUNTID,ACCOUNTS_MOPBZ5187,ACCOUNTS_B20AEIIAMEP01T,ACCOUNTS_B20AEIIAMEP02T,ACCOUNTS_B19LEIIAMEP01T,ACCOUNTS_B20ZEIIAMEP01T,ACCOUNTS_MOPBZP174227,ACCOUNTS_CNWBZP1191_SERVER,Accounts_b01aeibirt002_server,ACCOUNTS_B03AVI17003880_SERVER,ACCOUNTS_B03AVI17003881_SERVER,ACCOUNTS_B03AVI18318930_SERVER,ACCOUNTS_TSTXGSAND1_SERVER, ACCOUNTS_TSTXGSAND2_SERVER,ACCOUNTS_B01LVI19510640_SERVER,ACCOUNTS_B01LVI21562660_SERVER,ACCOUNTS_TSTLGSADM1_SERVER,ACCOUNTS_TSTLGSADR1_SERVER,GI_UNIX_ACCOUNT_CORRELATION,GI_ACCOUNTS_CP30_MAXAGE";
      String b[] = a.split(",", 2);

      System.out.println("Value1 = "+b[0]);
      System.out.println("Value2 = "+b[1]);*/

      String a = decryptIt("uSSIUZIlKHc6OUPvlzdAKLPyTDSeC1javBA5IYhg9sA=");
      System.out.println("Val - " + a);
    }
}
