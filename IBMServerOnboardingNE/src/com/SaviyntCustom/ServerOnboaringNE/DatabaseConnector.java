package com.SaviyntCustom.ServerOnboaringNE;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Saviynt on 4/5/2017.
 */
public class DatabaseConnector {
    final static Logger logger = Logger.getLogger(ServerOnboardingNEMain.class.getName());
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";

    public static Connection getDatabaseConnection(String db_url, String user, String pass/*ConfigParamsPojo configParams*/) {
        Connection conn = null;
        try{
            Class.forName(JDBC_DRIVER);
            //logger.info("Connecting to database...");
            ////logger.info("Connecting to database...");

            /*String db_url = configParams.getDatabaseUrl();
            String user = configParams.getDatabaseUsername();
            String pass = configParams.getDatabasePassword();*/
            //String db_url = "jdbc:mysql://mminternaldevrds.ctj2y9unoin4.us-east-1.rds.amazonaws.com:3306/saviyntdev";


            if(db_url!=null && user!=null && pass!=null && db_url.trim().length()>0
                    && user.trim().length()>0 && pass.trim().length()>0) {
                conn = DriverManager.getConnection(db_url, user, pass);
            } else {
                logger.info("Database server path or username or password is null");
            }
            return conn;

        }catch(SQLException se){
            logger.log(Level.SEVERE, se.getMessage(), se);
            logger.log(Level.SEVERE,se.getMessage(),se);
            return null;
        }catch(Exception e){
            logger.log(Level.SEVERE, e.getMessage(), e);
            logger.log(Level.SEVERE,e.getMessage(),e);
            return null;
        }
    }

    public Statement getDbCreateStatement(Connection conn) {
        Statement stmt = null;

        try {
            if (conn != null) {
                stmt = conn.createStatement();
            } else {
                logger.info("Connection is null");
            }
            return stmt;
        } catch (SQLException se) {
            logger.log(Level.SEVERE, se.getMessage(), se);
            logger.log(Level.SEVERE,se.getMessage(),se);
            return null;
        } catch(Exception e){
            logger.log(Level.SEVERE, e.getMessage(), e);
            logger.log(Level.SEVERE,e.getMessage(),e);
            return null;
        }
    }

    public ResultSet getResultSet(Statement stmt, String query) {
        ResultSet rs = null;
        try {
            if (stmt != null && query != null) {
                rs = stmt.executeQuery(query);
                return rs;
            } else {
                logger.info("Connection or query is null");
            }
            return null;
        } catch (SQLException se) {
            logger.log(Level.SEVERE, se.getMessage(), se);
            logger.log(Level.SEVERE, se.getMessage(), se);
            return null;
        } catch(Exception e){
            logger.log(Level.SEVERE, e.getMessage(), e);
            logger.log(Level.SEVERE,e.getMessage(),e);
            return null;
        }
    }

    public void updateTable(Statement stmt, String query) {
        try {
            if (stmt != null && query != null) {
                stmt.executeUpdate(query);
            } else {
                logger.info("Connection or query is null");
            }
        } catch (SQLException se) {
            logger.log(Level.SEVERE, se.getMessage(), se);
            logger.log(Level.SEVERE, se.getMessage(), se);
        } catch(Exception e){
            logger.log(Level.SEVERE, e.getMessage(), e);
            logger.log(Level.SEVERE,e.getMessage(),e);
        }
    }
}
