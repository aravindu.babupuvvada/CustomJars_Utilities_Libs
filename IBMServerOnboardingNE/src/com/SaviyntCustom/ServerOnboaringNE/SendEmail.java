package com.SaviyntCustom.ServerOnboaringNE;


import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;

public class SendEmail {

    private static Logger logger = null;

    public SendEmail() {
        String saviyntHome = System.getenv("SAVIYNT_HOME");
        //System.setProperty("LOG_PATH", saviyntHome);
        PropertyConfigurator.configure(saviyntHome + File.separator + "customJarsLog4j.properties");
        logger = org.apache.log4j.Logger.getLogger("boardingNELogger");
    }

    public static void main(String[] args) {
        try
        {
            String saviyntHome = System.getenv("SAVIYNT_HOME");
            InputStream inputStream = null;
            String configFileName = saviyntHome + File.separator + "configurationsServerOnboarding.properties";
            //System.out.println("savmonitoring.properties found at :" + configFileName);
            Properties prop = new Properties();
            inputStream = new FileInputStream(configFileName);
            prop.load(inputStream);
            //sendEmail(prop,"test");
            sendEmail(prop,"Requst number","testing...",prop.getProperty("EMAILTOADDRESS"),null);
        }
        catch(Exception e) {
            e.printStackTrace();
        }


    }

    public static void sendConnectionFailureEmail(Properties prop,String subject,String text) {
        // Recipient's email ID needs to be mentioned.

        String to = prop.getProperty("EMAILFAILURETOADDRESS");

        // Sender's email ID needs to be mentioned
        String from = prop.getProperty("EMAILFROMADDRESS");

        final String username = prop.getProperty("SMTPUSERNAME");//change accordingly
        final String password = prop.getProperty("SMTPPASSWORD");//change accordingly


        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", prop.getProperty("SMTPHOST"));
        props.put("mail.smtp.port", "587");

        // Get the Session object.
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {
            // Create a default MimeMessage object.
            Message message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));

            // Set Subject: header field
            message.setSubject(subject);

            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Now set the actual message
            messageBodyPart.setText(text);

            // Create a multipar message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            // Send the complete message parts
            message.setContent(multipart);

            // Send message
            Transport.send(message);

            //System.out.println("Sent message successfully....");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    //public static void sendEmail(Properties prop,String subject,String requestid,String requesttype,String requestor,String endpoints,String assignee,String email,String manager,String scopeFinalValues,String bactivity,String managername,String approvers,String ccEmailRecipients) {
    public static void sendEmail(Properties prop,String subject,String body,String toFields,String ccEmails) {
            if(ccEmails!=null)
            {
                ccEmails = ccEmails.replaceAll(",$", "");
                ccEmails = ccEmails.replaceFirst("^,", "");
            }
            String to = prop.getProperty("EMAILTOADDRESS");
            logger.info("Inside Send Email :: Final ccEmail List:: " + ccEmails);

        // Sender's email ID needs to be mentioned
        String from = prop.getProperty("EMAILFROMADDRESS");

        final String username = prop.getProperty("SMTPUSERNAME");//change accordingly
        final String password = prop.getProperty("SMTPPASSWORD");//change accordingly


        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", prop.getProperty("SMTPHOST"));
        props.put("mail.smtp.port", "587");

        // Get the Session object.
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {
            // Create a default MimeMessage object.
            Message message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            if(toFields.equals("")) {
                message.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(to));
            }
            else {
                System.out.println("Message will be sent to :"+toFields);
                message.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(toFields));
            }

            /*

            String[] cc=ccEmailRecipients.split(",");
            InternetAddress[] ccAddress = new InternetAddress[cc.length];
            // To get the array of ccaddresses
            for( int i = 0; i < cc.length; i++ ) {
                ccAddress[i] = new InternetAddress(cc[i]);
                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
            }


*/
            if(ccEmails!=null && !ccEmails.equals(""))
            {
                String[] cc=ccEmails.split(",");
                InternetAddress[] ccAddress = new InternetAddress[cc.length];
                // To get the array of ccaddresses
                for( int i = 0; i < cc.length; i++ ) {
                    ccAddress[i] = new InternetAddress(cc[i]);
                    message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
                }
            }

            // Set Subject: header field
            message.setSubject(subject);

            // Create the message part
            //BodyPart messageBodyPart = new MimeBodyPart();

            // Now set the actual message
            //messageBodyPart.setText("Attached customer stats");

            Multipart multipart = new MimeMultipart();
            BodyPart messageBodyPart = new MimeBodyPart();
            //String body="<img width=\"726\" alt=\"LE\" src=\"https://dev.accessnow.laureate.net/ECM/images/AccessNow.png\" height=\"85\" title=\"Image: https://dev.accessnow.laureate.net/ECM/images/AccessNow.png\"><br><br><center><b><u>Manager Approval Notification</center></b></u><br>Hello,<br> <br>An AccessNow request has been assigned to you /your team for access fulfillment for the following user.<br><br><b>Request ID :</b> "+requestid+" <br><br><b>Request Type : </b>"+requesttype+"<br> <br><b>Employee Name :</b> "+requestor+"<br> <br><b>Employee Email :</b> "+email+"<br> <br><b>Employee Manager : </b>"+managername+"<br><br><b>Application  Requested : </b> "+endpoints+"<br><br><b>Approved By : </b>"+approvers+"<br><br>"+bactivity+"<br><br><b>Scope Requested : </b> "+scopeFinalValues+"<br><br><br>Please use <a href=\"https://dev.accessnow.laureate.net/ECM/jbpmworkflowmanagement/viewopenrequests\" target=\"\" rel=\"\">this link</a> to access  your pending approval requests. Search by <b>Request ID:</b> 1300072. For help on the navigation approval workflow, please refer to this AccessNow Job Aid \"\" <br><br>Thank you,<br>AccessNow Team<br><br></pre><small><i>This notification originated from a no reply mailbox and is not actively monitored.  Please reach out to <a href=\"mailto:Frontline.Team@laureate.net\">Frontline Team</a> with any inquiries. </i></small></body></html>";
            messageBodyPart.setContent(body, "text/html; charset=utf-8");
            multipart.addBodyPart(messageBodyPart);
            //multipart.addBodyPart(attachment);
            message.setContent(multipart);
            // Send message
            Transport.send(message);
            System.out.println("Sent message successfully....");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}